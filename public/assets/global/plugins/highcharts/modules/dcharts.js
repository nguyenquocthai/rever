var scripts = document.getElementsByTagName("script"),
    url_src = scripts[scripts.length-1].src,
    url_src = url_src.substring(0, url_src.lastIndexOf("/"));

$.fn.dCharts = function(data) {

  data = data || {};

  //---------------------------------------------------------------------------------
  var eid = $(this).attr('id');

  var id_config_popup = '#' + eid + ' #configPopup';
  var class_highcharts_button = '#' + eid + ' .highcharts-button';
  var class_options_chart = '#' + eid + ' .options-chart';

  var id_type_chart = '#' + eid + ' #typeChart';
  var id_type_stacking = '#' + eid + ' #typeStacking';
  var id_show_marker = '#' + eid + ' #showMarker';

  $.ajax({
    type: "GET",
    cache: true,
    url: url_src + '/html/config-popup.html',
    success: function(e){
      $(id_config_popup).html(e);

      $(id_type_chart).val(options.chart.type);
      $(id_type_stacking).val(options.plotOptions.series.stacking);
      $(id_show_marker).val(options.plotOptions.series.marker.enabled.toString());
    }
  });

  var gradient_color = {
    stops: [
      [0, '#ffffff'],
      [0.5, '#eee'],
      [1, '#bbb']
    ]
  };

  var options = {
    credits: { enabled: false },
    chart: {
      renderTo: $(this).attr('container-id'),
      type: data.chart_type || 'line',
      height: data.chart_height || null,
      zoomType: 'x'
    },

    title: { text: data.title || 'Chart Title' },
    subtitle: { text: data.subtitle || 'Chart Subtitle' },
    colors: data.color || ['#009688', '#FF7043', '#5C6BC0', '#d4282d', '#03a9f4', '#ec407a'],

    xAxis: {
      tickInterval: 7, // one week
      tickWidth: 0,
      gridLineWidth: 1,
      labels: (data.xAxis ? data.xAxis.labels : {}) || {},
      min: (data.xAxis ? data.xAxis.min : undefined) || undefined,
      max: (data.xAxis ? data.xAxis.max: undefined) || undefined,
      categories: data.xData || null
    },

    yAxis: {
      title: { text: null},
      labels: {
        formatter: function() {
          return this.value + (data.yLabel || '');
        }
      },
      showFirstLabel: false
    },

    legend: {
      layout: 'horizontal',
      align: 'center',
      verticalAlign: 'bottom',
      borderWidth: 0
    },

    tooltip: {
      formatter: function() {
        var series = this.series.chart.series,
            each = Highcharts.each,
            x = this.point.x,
            txt = '<span style="font-size: 10px">' + this.key + '</span><br/>';

        each(series, function(serie, i) {
          each(serie.data, function(data, j){
            if (data.x === x) {
              var suffix = '';

              if (serie.userOptions.tooltip && serie.userOptions.tooltip.valueSuffix) {
                suffix = serie.userOptions.tooltip.valueSuffix;

                if (serie.userOptions.params)
                  for (var index = 0; index < serie.userOptions.params.length; index++)
                    suffix = suffix.replace("#param" + index, serie.userOptions.params[index][j]);
              }

              txt += '<span style="color:' + data.color + '">\u25CF</span> ' + serie.name + ': <b>' + suffix + '</b><br/>';

              return false;
            }
          });
        });

        return txt;
      }
    },

    plotOptions: {
      series: {
        marker: {
          fillColor: '#FFFFFF',
          lineColor: null,
          lineWidth: 1.5,
          enabled: data.showMarker || false,
        },
        stacking: data.stacking || null,
      }
    },

    series: data.yData || null,

    noData: {
      style: {
        fontWeight: 'bold',
        fontSize: '15px',
        color: '#303030'
      }
    },

    exporting: {
      buttons: {
        contextButton: {
          symbol: 'url(/shop/img/download.png)',
          width: 31,
          height: 30,
          symbolX: 16,
          symbolY: 15,
          x: -45,
          symbolSize: 20,
          theme: {
            'stroke-width': 1,
            stroke: '#aaa',
            width: 30,
            height: 30,
            r: 2,
            fill: gradient_color
          }
        },
        configButton: {
          symbol: 'url(/shop/img/setting.png)',
          width: 31,
          height: 30,
          symbolX: 16,
          symbolY: 15.5,
          menuClassName: 'aaa',
          x: -5,
          symbolSize: 20,
          theme: {
            'stroke-width': 1,
            stroke: '#aaa',
            width: 30,
            height: 30,
            r: 2,
            fill: gradient_color
          },
          onclick: function(e) {
            $(id_config_popup).show();

            var max = 0;
            $(".highcharts-menu-item").each(function(index) {
              if ($(this).outerWidth() > max)
                max = $(this).outerWidth();
            });

            $(".highcharts-menu-item").each(function(index) {
              $(this).outerWidth(max);
            });
          }
        }
      }
    }
  };

  //---------------------------------------------------------------------------------
  c = new Highcharts.Chart(options, function(chart) {

    $(document).on('change', class_options_chart, function() {
      options.chart.type = $(id_type_chart).val();
      options.plotOptions.series.stacking = $(id_type_stacking).val();
      options.plotOptions.series.marker.enabled = ($(id_show_marker).val() == 'true');
      chart = new Highcharts.Chart(options);
      chart.reflow();
    });

    var hideTimerConfig;

    $(document).on('click', id_config_popup, function(e) {
      e.stopPropagation();
    });

    $(document).on('mouseleave', id_config_popup, function() {
      hideTimerConfig = setTimeout(function() {
        $(id_config_popup).hide();
      }, 500);
    });

    $(document).on('mouseleave', class_highcharts_button, function() {
      hideTimerConfig = setTimeout(function() {
        $(id_config_popup).hide();
      }, 500);
    });

    $(document).on('mouseenter', id_config_popup, function() {
      clearTimeout(hideTimerConfig);
    });

    $(document).on('mouseenter', class_highcharts_button, function() {
      clearTimeout(hideTimerConfig);
    });

    $(document).on('click', function(e) {
      $(id_config_popup).hide();
    });

    setTimeout(function() {
      $(window).trigger('resize');
    }, 200);
  });

  //---------------------------------------------------------------------------------
  var timerResize;

  $(window).on('resize', function() {
    c.reflow();
  });
};