WebWorldApp.controller('authorities.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Quản trị';

        // Title block
        $scope.detail_block_title = 'Tạo tài khoản quản trị';
        $scope.seo_block_title = 'Tối ưu SEO';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // INIT DATA
            $scope.authority = {};
            $scope.authority.del_flg = '0';
            $scope.authority.status = '0';

            // Init select2 for categories
            select2_function(null, {
                commonService: commonService,
                multiple: true,
            });

        };

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };


        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            fileUpload.uploadFileToUrl({}, $scope.authority, 'create_authority', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/authorities');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $('.select2-function').on('change', function (e) {
            $scope.$apply(function() {
                if($('.select2-function').val() == null) {
                    $scope.authority.list_rule = '';
                } else {
                    $scope.authority.list_rule = $('.select2-function').val().map(function(e) {
                        return { id: e };
                    });
                }
            });
        });

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
