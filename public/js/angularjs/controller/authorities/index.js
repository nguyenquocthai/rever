WebWorldApp.controller('authorities.index', ['$scope','$rootScope', 'commonService',
    function ($scope, $rootScope, commonService) {

        $rootScope.app.title = 'Tài khoản quản trị';

        $scope.title = 'Tài khoản quản trị';

        // DATATABLE
        $('.status').val(null);

        commonService.requestFunction('datatable_authority', {}, function(e) {
            $scope.datatable = $('#tbl-data').DataTable({
                order: [[ 2, 'desc' ]],
                columnDefs: [
                    { sortable: false, searchable: false, targets: [ 4 ] },
                    { class: 'text-center actions', targets: [ 3 ] },
                    { class: 'text-center', targets: [ 2, 3, 4 ] },
                ],
                displayStart: 0,
                displayLength: 20,
                data: e.data
            });

            $scope.datatable.columns(0).search('').draw();
        });


        //-------------------------------------------------------------------------------
        $scope.search_datatable = function() {
            var name = $('.name').val() || '';

            $scope.datatable.columns(0).search(name)
                            .draw();
        };

        //-------------------------------------------------------------------------------
        $scope.clear = function() {
            $('.name').val(null);

            $scope.search_datatable();
        };

        //-------------------------------------------------------------------------------
        $scope.delete = function(id) {
            confirmPopup('Xóa tài khoản quản trị', 'Bạn muốn xóa tài khoản quản trị này không ?', function() {
                commonService.requestFunction('delete_authority/' + id + $rootScope.api_token, {}, function(e) {
                    var tr = $('#delete'+id).parents('tr')[0];
                    $scope.datatable.row(tr).remove().draw( false );
                });
            });
        };

    }
]);
