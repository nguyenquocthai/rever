WebWorldApp.controller('authorities.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Tạo tài khoản quản trị';

        // Title block
        $scope.detail_block_title = 'Chỉnh sửa tài khoản quản trị';
        $scope.seo_block_title = 'Tối ưu SEO';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // LOAD DATA
            commonService.requestFunction('show_authority/' + $routeParams.id + $rootScope.api_token, {}, function(e) {

                if (!e.data) {
                    $location.path('/admin/authorities');
                    return false;
                }

                $scope.authority = e.data;
                $scope.authority.list_rule = !!$scope.authority.list_rule ? JSON.parse(e.data.list_rule) : [];
                $scope.authority.status = String(e.data.status);

                select2_function(null, {
                    commonService: commonService,
                    multiple: true,
                    callback: function(e) {
                        setTimeout(function() {
                            if ($scope.authority.list_rule) {
                                e.val($scope.authority.list_rule.map(function(e) { return e.id; })).trigger('change');
                            }
                        });
                    }
                });

            }); // END LOAD DATA

        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            $scope.authority._method = 'PUT';

            fileUpload.uploadFileToUrl({}, $scope.authority, 'update_authority/' + $routeParams.id + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/authorities');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $('.select2-function').on('change', function (e) {
            $scope.$apply(function() {
                if($('.select2-function').val() == null) {
                    $scope.authority.list_rule = '';
                } else {
                    $scope.authority.list_rule = $('.select2-function').val().map(function(e) {
                        return { id: e };
                    });
                }
            });
        });

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

    }
]);
