WebWorldApp.controller('DashboardCtrl', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Quản lý chung';
        $scope.base = {};
        // Title block
        $scope.detail_block_title = 'Quản lý chung';
        $scope.revenue_block_title = 'Doanh thu';

        //-------------------------------------------------------------------------------
		$scope.initApp = function () {
			commonService.requestFunction('index_setting', {status_code:'count'}, function(e) {
			$('#tinthue').html(e.data.thue);
			$('#tinban').html(e.data.ban);
			$('#tintuc').html(e.data.tintuc);
			$('#lienhe').html(e.data.lienhe);
			});
		}

		//-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
    
]);
