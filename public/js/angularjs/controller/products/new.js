WebWorldApp.controller('products.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới: tin thuê';

        // Title block
        $scope.detail_block_title = 'Chi tiết tin thuê';
        $scope.image_block_title = 'Hình ảnh';
        $scope.category_block_title = 'Phân loại';
        $scope.seo_block_title = 'Tối ưu SEO';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // Catlist
            commonService.requestFunction('index_productcat', {status_code:'getcat',id:0}, function(e) {

                $('.load_cat').html(e.nestable);

            });
            
            tinymce.remove();
            load_tinymce('#content', null);
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.product.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];

            var cat_id = [];
            $('.dd-handle.active').each(function(index, el) {
                cat_id.push($(this).attr('data-id'));
            });

            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['content'] = tinymce.get('content').getContent();

            data['id_productcat'] = JSON.stringify(cat_id);

            request['value'] = data;
            request['status_code'] = 'edit';

            fileUpload.uploadFileToUrl(files, request, 'create_product', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/products');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

        //-------------------------------------------------------------------------------
        $('.load_cat').on('click', '.dd-handle', function(event) {
            $(this).toggleClass('active');
        });
    }
]);
