WebWorldApp.controller('products.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Quản trị: tin thuê';

        // Title block
        $scope.detail_block_title = 'Chi tiết tin thuê';
        $scope.image_block_title = 'Hình ảnh';
        $scope.category_block_title = 'Phân loại';
        $scope.seo_block_title = 'Tối ưu SEO';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            // LOAD DATA
            commonService.requestFunction('show_product/' + $routeParams.id + $rootScope.api_token, {}, function(e) {
                if (!e.data) {
                    $location.path('/admin/products');
                    return false;
                }

                $scope.product = e.data;

                // load image avatar
                $('.image_review').attr('src', e.data.avatar+'?'+e.data.updated_at);

                $scope.product.status = String(e.data.status);

                $('.load_position').html(e.list_position);

                if ($scope.product.endtime != null) {
                    $scope.product.endtime = datevn($scope.product.endtime);
                }
                if ($scope.product.starttime != null) {
                    $scope.product.starttime = datevn($scope.product.starttime);
                }

                if ($scope.product.end != null) {
                    $scope.product.end = dayvn($scope.product.end);
                }
                if ($scope.product.start != null) {
                    $scope.product.start = dayvn($scope.product.start);
                }
                
                tinymce.get('content').setContent(html_entity_decode($scope.product.content) || '');

            }); // END LOAD DATA

            tinymce.remove();
            load_tinymce('#content', null);

            // Catlist
            commonService.requestFunction('index_productcat', {status_code:'getcat',id:$routeParams.id}, function(e) {

                $('.load_cat').html(e.nestable);

            });
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.product.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var cat_id = [];
            $('.dd-handle.active').each(function(index, el) {
                cat_id.push($(this).attr('data-id'));
            });

            var request = {};
            var data = {};
            var files = [];

            var id_edit = $routeParams.id;

            var curenpage_string = $('#form_box').attr('data-curenpage');
            var curenpage = parseInt(curenpage_string, 10);

            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['content'] = tinymce.get('content').getContent();
            
            data['id_productcat'] = JSON.stringify(cat_id);

            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";
            request['vitri'] = parseInt($('.item-position.active').html(), 10);

            fileUpload.uploadFileToUrl(files, request, 'update_product/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/products');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $('.select2-tags').on('change', function (e) {
           $('#tags').val($('.select2-tags').val().join(','));
        });

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

        $('.load_position').on('click', '.item-position', function(event) {

            var here = $(this);
            var data = {};
            data['id'] = $scope.product.id;
            data['id_swap'] = $(this).attr('data-id');
            data['position_old'] = $scope.product.position;
            data['position_new'] = $(this).attr('data-value');
            data['status_code'] = "change";

            if(data['id_swap'] == data['id']){
                return false;
            }

            commonService.requestFunction('pos_product', data, function(e) {
                switch (e.code) {
                    case 200:
                        $('.item-position.active').attr('data-id',data['id_swap']);
                        here.attr('data-id',data['id']);
                        $('.item-position').removeClass('active');
                        here.addClass('active');
                        $scope.product.position = data['position_new'];
                        break;
                    default:
                        break;
                }
            });
        });

        $('.load_cat').on('click', '.dd-handle', function(event) {
            $(this).toggleClass('active');
        });
    }
]);
