WebWorldApp.directive('datepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                var text = 'dpa' + Math.floor((Math.random() * 1000000000) + 1);
                $(element).attr('id', text);
                $(element).attr('readonly', true);
                $('#' + text).datepicker({
                    dateFormat: 'dd/mm/yy',
                    showButtonPanel: true,
                    beforeShow: function (input) {
                        // load button clear
                        setTimeout(function () {
                            $(input).datepicker('widget').find('.ui-datepicker-current').hide();
                            $(input).datepicker('widget').find('.ui-datepicker-close').hide();
                            var buttonPane = $(input).datepicker('widget').find('.ui-datepicker-buttonpane');
                            $('<button>', {
                                text: 'Clear',
                                click: function () {
                                    $.datepicker._clearDate(input);
                                    ngModelCtrl.$setViewValue(null);
                                }
                            }).appendTo(buttonPane).addClass('ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all');
                        }, 100);
                    },
                    onSelect: function (date, input) {
                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(date);
                        });
                        // reload button clear
                        if ($('.ui-datepicker-clear').length <= 0) {
                            setTimeout(function () {
                                $(input).datepicker('widget').find('.ui-datepicker-current').hide();
                                $(input).datepicker('widget').find('.ui-datepicker-close').hide();
                                var buttonPane = $(input).datepicker('widget').find('.ui-datepicker-buttonpane');
                                $('<button>', {
                                    text: 'クリア',
                                    click: function () {
                                        $.datepicker._clearDate(input);
                                        ngModelCtrl.$setViewValue(null);
                                    }
                                }).appendTo(buttonPane).addClass('ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all');
                            }, 100);
                        }
                    }
                });
            });
        }
    };
});

WebWorldApp.directive('monthpicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        replace: true,
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                var text = 'dpa' + Math.floor((Math.random() * 1000000000) + 1);
                $(element).attr('id', text);
                $('#' + text).datetimepicker({
                    format: 'YYYY/MM',
                    viewMode: 'months',
                    locale: 'ja'
                }).on('dp.change', function (ev) {
                    if (ev.currentTarget.value) {
                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(ev.currentTarget.value);
                        });
                    }
                });
            });
        }
    };
});