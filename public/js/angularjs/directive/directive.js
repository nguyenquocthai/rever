//-------------------------------------------------------------------------------
WebWorldApp.directive('numbersSearchOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            ngModelCtrl.$parsers.push(function (text) {

                var transformedInput = text.replace(/[^0-9\*\=\.\.\.]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});

//-------------------------------------------------------------------------------
WebWorldApp.directive('compareTo', function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function(modelValue) {
                if (modelValue == scope.otherModelValue )
                    return true;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
});

//-------------------------------------------------------------------------------
WebWorldApp.directive('numbersOnly', ['$filter', function ($filter) {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) return;
            ngModelCtrl.$formatters.push(function (value) {
                return $filter(attr.numbersOnly)(value);
            });

            ngModelCtrl.$parsers.push(function (text) {
                var transformedInput = text.replace(/[^\d|\|\,]/g, '');
                transformedInput = transformedInput.replace(/\,/g, '.');

                if ( !transformedInput || transformedInput <= 0 )
                    transformedInput = 1;

                if ( transformedInput >= 9999999999)
                    transformedInput = 9999999999;

                ngModelCtrl.$setViewValue($filter(attr.numbersOnly)(transformedInput));
                ngModelCtrl.$render();

                return transformedInput;

            });
        }
    };
}]);

//-------------------------------------------------------------------------------
WebWorldApp.directive('priceFilter', ['$filter', function ($filter) {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) return;
            ngModelCtrl.$formatters.push(function (value) {
                return $filter(attr.priceFilter)(value);
            });

            ngModelCtrl.$parsers.push(function (text) {
                var transformedInput = text.replace(/[^\d|\|\,]/g, '');
                transformedInput = transformedInput.replace(/\,/g, '.');

                if ( transformedInput >= 9999999999)
                    transformedInput = 9999999999;

                if (transformedInput) {
                    ngModelCtrl.$setViewValue($filter(attr.priceFilter)(transformedInput));
                } else {
                    ngModelCtrl.$setViewValue(transformedInput);
                }

                ngModelCtrl.$render();

                return transformedInput;

            });
        }
    };
}]);

//-------------------------------------------------------------------------------
WebWorldApp.directive('floatFilter', ['$filter', function ($filter) {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) return;

            ngModelCtrl.$formatters.push(function (value) {
                return $filter(attr.floatFilter)(value);
            });

            element.bind('blur', function() {
                var value = element.val();

                if (value) {
                    value = value.replace(/\./g, '');
                    value = value.replace(/\,/g, '.');
                }

                element.val($filter(attr.floatFilter)(value) || 0);
            });

            ngModelCtrl.$parsers.push(function (text) {
                var transformedInput = text.replace(/[^\d|\|\,]/g, '');
                transformedInput = transformedInput.replace(/\,/g, '.');

                if ( transformedInput >= 9999999999)
                    transformedInput = 9999999999;

                return transformedInput;
            });
        }
    };
}]);

//-------------------------------------------------------------------------------
WebWorldApp.directive('format', ['$filter', function ($filter) {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;

            ctrl.$formatters.push(function (value) {
                return $filter(attrs.format)(value);
            });

            ctrl.$parsers.push(function (value) {

                var plainNumber = value.replace(/[^\d|\-+|\.+]/g, '');
                ctrl.$setViewValue($filter(attrs.format)(plainNumber));
                ctrl.$render();

                return plainNumber;
            });
        }
    };
}]);
/*
WebWorldApp.directive('numbersFaxOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            ngModelCtrl.$parsers.push(function (text) {
                var transformedInput = text.replace(/[^0-9\-]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

WebWorldApp.directive('fileModel', ['$parse',
    function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var _function = attrs.fileModelOnchange;
                element.bind('change', function () {
                    if (typeof _function !== 'undefined') {
                        scope[_function](element);
                    }
                });
            }
        };
    }
]);

WebWorldApp.directive('bnList', function () {
    function link(scope, element, attributes) {
        scope.$watch(
                attributes.bnList,
                function handlePersonBindingChangeEvent(newValue) {
                    scope.bnList = newValue;
                }
        );
    }

    // Return the isolate-scope directive configuration.
    return({
        link: link,
        templateUrl: 'list.html'
    });
});

WebWorldApp.directive('telnoOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            element.bind('input', function (e) {
                var obj = e.target;
                var value = obj.value;
                var transformedInput = converToUnicode(value);
                transformedInput = transformedInput ? transformedInput.replace(/[^\d.]/g, '') : null;
                ngModelCtrl.$setViewValue(transformedInput);
                ngModelCtrl.$render();
            });

        }
    };
});

WebWorldApp.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            ngModelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g, '') : null;
                if (transformedInput !== inputValue) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

WebWorldApp.directive('trustedHtml', ['$sce',
    function ($sce) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$formatters.push(function (value) {
                    function htmlDecode(input) {
                        var elem = document.createElement('div');
                        elem.innerHTML = input;
                        return elem.childNodes.length === 0 ? '' : elem.childNodes[0].nodeValue;
                    }
                    return htmlDecode(value);
                });
            }
        };
    }
]);

WebWorldApp.directive('repeatDone', function () {
    return function (scope, element, attrs) {
        if (scope.$last) { // all are rendered
            scope.$eval(attrs.repeatDone);
        }
    };
});


WebWorldApp.directive('select2', function ($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, model) {
            var e = $(element);
            $timeout(function () {
                e.select2();
            });
            scope.$watch(attrs.ngModel, function (val) {
                $timeout(function () {
                    e.select2();
                });
            });
        }
    };
});

WebWorldApp.directive('viewDate', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            ngModelCtrl.$parsers.push(function (text) {
                var transformedInput = text.replace(/[^0-9\-]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});*/

//-------------------------------------------------------------------------------
// validate phone
WebWorldApp.directive('valiPhone', function () {
    return {
        scope:{
        },
        replace: true,
        template: '<input class="form-control" ng-pattern="/^\\+?\\d{1,3}?[- .]?\\(?(?:\\d{2,3})\\)?[- .]?\\d\\d\\d[- .]?\\d\\d\\d\\d$/" type="text">',
    };
});

//-------------------------------------------------------------------------------
WebWorldApp .directive('messagePhone', function() {
    var html = 'Số điện thoại không đúng';
    return {
        template: html
    };
});

//-------------------------------------------------------------------------------
// validate email
WebWorldApp.directive('valiEmail', function () {
    return {
        scope:{
        },
        replace: true,
        template: '<input class="form-control" type="email">',
    };
});

//-------------------------------------------------------------------------------
WebWorldApp .directive('messageEmail', function() {
    var html = 'Email chưa đúng định dạng';
    return {
        template: html
    };
});

//-------------------------------------------------------------------------------
// validate image
WebWorldApp.directive('valiImage', function () {
    return {
        require: 'ngModel',
        link: function(scope, elem, attr, ngModel) {
            elem.bind('change',function(e) {
                scope.$apply(function(){
                    if (e.target.files.length == 0) {
                        ngModel.$setValidity('image_error', true);
                        scope.check_image = function() {
                            return false;
                        };
                        return false;
                    }

                    fileType = e.target.files[0].type;

                    ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];

                    if (ValidImageTypes.indexOf(fileType) < 0) {
                        ngModel.$setValidity('image_error', false);
                        scope.check_image = function() {
                            return true;
                        };
                    } else {
                        ngModel.$setValidity('image_error', true);
                        scope.check_image = function() {
                            return false;
                        };
                    }
                });
            });
        }
   };
});

//-------------------------------------------------------------------------------
// validate image
WebWorldApp.directive('isImage', function () {
    return {
        require: 'ngModel',
        link: function(scope, elem, attr, ngModel) {
            // check type image
            elem.bind('change', function(e) {
                scope.$apply(function(){
                    fileType = e.target.files[0].type;

                    ValidImageTypes = ['image/gif', 'image/jpeg', 'image/png'];

                    error_element = $(e.target).parent().parent().parent().find('.error');

                    if (ValidImageTypes.indexOf(fileType) == -1) {
                        error_element.html('Hình không đúng định dạng');
                    } else {
                        error_element.html('');
                    }
                });
            });

            // load image to view
            elem.change(readURL);
        }
   };
});

//-------------------------------------------------------------------------------
WebWorldApp .directive('messageImage', function() {
    var html = 'Hình không đúng định dạng';
    return {
        template: html
    };
});

//-------------------------------------------------------------------------------
WebWorldApp .directive('formAvatar', function() {
    var html = '<div>';
    html += '<div class="form-group hirehide is-empty is-fileinput width100">';
    html += '<div class="socialmediaside2">';
    html += '<input id="upload_input" vali-image ng-model="avatar_main" class="fileUpload" accept="image/jpeg, image/jpg, image/png" name="data[image_avatar]" type="file" value="Choose a file">';
    html += '</div>';
    html += '</div>';
    html += '<div class="upload-demo">';
    html += '<div class="upload-demo-wrap"><img alt="your image" class="portimg" src="/public/img/no-image.png"></div>';
    html += '</div>';
    html += '</div>';

    return {
        template: html
    };
});

//-------------------------------------------------------------------------------
function converToUnicode(param) {
    var chars = param;
    var ascii = '';
    for (var i = 0, l = chars.length; i < l; i++) {
        var c = chars[i].charCodeAt(0);

        // make sure we only convert half-full width char
        if (c >= 0xFF00 && c <= 0xFFEF) {
            c = 0xFF & (c + 0x20);
        }
        ascii += String.fromCharCode(c);
    }
    return ascii;
}

//-------------------------------------------------------------------------------
WebWorldApp.directive('fileUpload', function() {



    $(document).on('click', '.upload_button', function(e) {
        $(this).parent().find('.file-upload').click();
        e.preventDefault();
    });

    return {
        template: function(element, attrs) {
            var html = '<div>';
                html += '<div class="form-group hirehide is-empty is-fileinput width100">';
                html += '<div class="socialmediaside2">';
                html += '<input class="file-upload"';
                html += '       is-image ';
                html += '       accept="image/jpeg, image/jpg, image/png" ';
                html += '       name="' + attrs.fileUpload + '" ';
                html += '       type="file" ';
                html += '       ng-model="image">';
                html += '</div>';
                html += '</div>';
                html += '<div class="upload-demo">';
                html += '<div class="upload-demo-wrap"><img alt="your image" class="portimg" src="#"></div>';
                html += '</div>';
                html += '<span class="error"></span>';
                html += '<button type="button" class="upload_button">Chọn hình</button>';
                html += '</div>';

            return html;
        }
    };

    // return {
    //     template: html
    // };
});