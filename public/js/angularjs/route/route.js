WebWorldApp.config(['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        var _prefix = '/admin';

        var baseURL = $('base').attr('href');

        $routeProvider.when('/auth/logout', {
            controller: 'LogoutCtrl',
            url: baseURL + '/auth/logout'
        }).when(_prefix, {
            templateUrl: '/public/html/dashboard/index.html',
            controller: 'DashboardCtrl',
        })

        for (var i = name_apps.length - 1; i >= 0; i--) {
            $routeProvider.when(_prefix + '/'+name_apps[i]+'s', {
                templateUrl: '/resources/views/Backend/'+name_apps[i]+'/index.html',
                controller: name_apps[i]+'s.index',
            }).when(_prefix + '/'+name_apps[i]+'/new', {
                templateUrl: '/resources/views/Backend/'+name_apps[i]+'/new.html',
                controller: name_apps[i]+'s.new',
            }).when(_prefix + '/'+name_apps[i]+'/edit/:id', {
                templateUrl: '/resources/views/Backend/'+name_apps[i]+'/edit.html',
                controller: name_apps[i]+'s.edit',
            })
        };

        $routeProvider.when(_prefix + '/tin-thue', {
            templateUrl: '/resources/views/Backend/item_project/index_thue.html',
            controller: 'item_projects.index_thue',

        }).when(_prefix + '/tin-ban', {
            templateUrl: '/resources/views/Backend/item_project/index_ban.html',
            controller: 'item_projects.index_ban',
        })

        .otherwise({
            redirectTo: _prefix
        });

        $locationProvider.html5Mode(true);
    }
]).run(function ($rootScope) {
    window.rootScope = $rootScope;

    $rootScope.ip_server = ip_server;

    $rootScope.number_format = number_format;
    $rootScope.price_format = price_format;
    $rootScope.vat_format = vat_format;
    $rootScope.get_discount = get_discount;
    $rootScope.product_price_vat = product_price_vat;
    $rootScope.product_price = product_price;
    $rootScope.weight_format = weight_format;
    $rootScope.shipping_price_format = shipping_price_format;
});