var WebWorldApp = angular.module('WebWorldApp', [
    'ngRoute',
    // 'xeditable',
    // 'ngSanitize',
    'angularUtils.directives.dirPagination',
    // 'checklist-model'
]);


//-------------------------------------------------------------------------------
// CHECK AJAX RESPONSE HAVE SESSION
$( document ).ajaxSuccess(function( event, request, settings ) {
  var e = request.responseJSON;



if (e && e.code == '401') {
    showPageLoading();

    location.href = 'admin/authenticate/login';
  }
});

var data = {
  url: window.location.pathname
};
$.ajax({
    type: "POST",
    url: '/admin/session',
    data: data,
    dataType: 'json',
    success: function(result) {
        if(result.code == 300) {
          location.href = '/admin';
        }
    }
});