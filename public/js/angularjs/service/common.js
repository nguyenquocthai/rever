WebWorldApp.service('commonService', function ($http, $compile, $location) {
    var self = this;
    this.baseURL = $('base').attr('href');
    this.apiURL = self.baseURL.replace('admin', '');
    this.commonPopup = $('#listMainSubOfSub');
    this.api_token = '?api_token=PBsQwZyv';

    var listjs = {

        // session user
        load_session_user: {url: 'api/adminsession/get_session', method: 'GET'},

        show_info_introduce: {url: 'api/adminsettings/show_info_introduce', method: 'GET'},
        update_info_introduce: {url: 'api/adminsettings/update_info_introduce', method: 'PUT'},

        show_info_checkout: {url: 'api/adminsettings/show_info_checkout', method: 'GET'},
        update_info_checkout: {url: 'api/adminsettings/update_info_checkout', method: 'PUT'},

        // change password
        change_password: {url: 'api/adminsettings/change_password', method: 'PUT'},

        // theme categoires
        index_theme_categories: {url: 'api/adminthemecategories', method: 'GET'},
        
        // select2
        select2: {url: 'api/adminselect2s', method: 'GET'},

        // site_theme
        index_site_theme: {url: 'api/adminsitetheme', method: 'GET'},
        update_site_theme: {url: 'api/adminsitetheme', method: 'PUT'},

        update_promotion_products: {url: 'api/adminproducts/update_promotion_products', method: 'POST'},
        update_lasthours_products: {url: 'api/adminproducts/update_lasthours_products', method: 'POST'},

        // stores
        datatable_store: {url: 'api/adminstores/search_datatable', method: 'GET'},
        datatable_store2: {url: 'api/adminstores/search_datatable2', method: 'GET'},
        show_tq: {url: 'api/adminstores/show_tq', method: 'GET'},
        get_stores: {url: 'api/adminstores/get_stores', method: 'GET'},
        show_store: {url: 'api/adminstores', method: 'GET'},
        create_store: {url: 'api/adminstores', method: 'POST'},
        update_store: {url: 'api/adminstores', method: 'PUT'},
        delete_store: {url: 'api/adminstores', method: 'DELETE'},

        // img_mb
        datatable_img_mb: {url: 'api/adminimg_mbs/search_datatable', method: 'GET'},
        show_img_mb: {url: 'api/adminimg_mbs', method: 'GET'},
        create_img_mb: {url: 'api/adminimg_mbs', method: 'POST'},
        update_img_mb: {url: 'api/adminimg_mbs', method: 'PUT'},
        delete_img_mb: {url: 'api/adminimg_mbs', method: 'DELETE'},
        // img_phong
        datatable_img_phong: {url: 'api/adminimg_phongs/search_datatable', method: 'GET'},
        show_img_phong: {url: 'api/adminimg_phongs', method: 'GET'},
        create_img_phong: {url: 'api/adminimg_phongs', method: 'POST'},
        update_img_phong: {url: 'api/adminimg_phongs', method: 'PUT'},
        delete_img_phong: {url: 'api/adminimg_phongs', method: 'DELETE'},

        // tienich
        datatable_tienich: {url: 'api/admintienichs/search_datatable', method: 'GET'},
        show_tienich: {url: 'api/admintienichs', method: 'GET'},
        create_tienich: {url: 'api/admintienichs', method: 'POST'},
        update_tienich: {url: 'api/admintienichs', method: 'PUT'},
        delete_tienich: {url: 'api/admintienichs', method: 'DELETE'},
        // tongquan
        datatable_tongquan: {url: 'api/admintongquans/search_datatable', method: 'GET'},
        show_tongquan: {url: 'api/admintongquans', method: 'GET'},
        create_tongquan: {url: 'api/admintongquans', method: 'POST'},
        update_tongquan: {url: 'api/admintongquans', method: 'PUT'},
        delete_tongquan: {url: 'api/admintongquans', method: 'DELETE'},

        // districts
        gettable: {url: 'api/admintables/gettable', method: 'POST'},

    }

    for (var i = name_apps.length - 1; i >= 0; i--) {
        listjs['index_'+name_apps[i]] = {url: 'api/admin'+name_apps[i]+'s', method: 'GET'};
        listjs['show_'+name_apps[i]] = {url: 'api/admin'+name_apps[i]+'s', method: 'GET'};
        listjs['create_'+name_apps[i]] = {url: 'api/admin'+name_apps[i]+'s' + self.api_token, method: 'POST'};
        listjs['update_'+name_apps[i]] = {url: 'api/admin'+name_apps[i]+'s', method: 'PUT'};
        listjs['delete_'+name_apps[i]] = {url: 'api/admin'+name_apps[i]+'s', method: 'DELETE'};
    }

    this.LIST_API = listjs;

    this.requestFunction = function (api, params, callback) {
        if (typeof api !== 'undefined') {
            if (api.indexOf('/') != -1) {
                var rParams = api.substr(api.indexOf('/'));
                api = api.replace(rParams, '');
            }

            var oAPI = self.LIST_API[api];
            var url = self.apiURL + oAPI.url + (rParams || '');

            if (oAPI.method === 'POST' || oAPI.method === 'PUT' || oAPI.method === 'DELETE') {
                var httpConfig = {
                    method: oAPI.method,
                    data: $.param(params),
                    url: url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                };
            } else if (oAPI.method === 'GET') {
                if ($.isEmptyObject(params) === false) {
                    url += '?' + $.param(params);
                }
                var httpConfig = {
                    method: oAPI.method,
                    url: url
                };
            }
            //multipart/form-data
            if (!params.disable_loading)
                showPageLoading();

            $http(httpConfig).then(function successCallback(response) {
                if (!params.disable_loading)
                    hidePageLoading();

                switch (response.data.code) {
                    case 200:
                        if (response.data.message)
                            self.showSuccess(response.data.message);

                        callback(response.data);
                        break;
                    case 5002:
                        if (response.data.message)
                            self.showAlert(response.data.message);

                        callback(response.data);
                        break;
                    case 3000:
                        if (response.data.error)
                            self.showError(response.data.message);
                        else if (response.data.message)
                            self.showError(response.data.message);

                        callback(response.data);
                        break;

                    case 300:
                        if (response.data.error)
                            self.showError(response.data.error);
                        else if (response.data.error)
                            self.showError(response.data.error);

                        callback(response.data);
                        break;

                    case 5000:
                        $location.path('/logout');
                        break;
                    default:
                        if (response.data.message)
                            self.showAlert(response.data.message);

                        callback(response.data);
                        break;
                }
            }, function errorCallback(response) {
                // console.log(response);
                if (!params.disable_loading)
                    hidePageLoading();
            });
        }
    };

    this.requestajaxform = function (api, params, callback) {
        if (typeof api !== 'undefined') {
            if (api.indexOf('/') != -1) {
                var rParams = api.substr(api.indexOf('/'));
                api = api.replace(rParams, '');
            }

            var oAPI = self.LIST_API[api];
            var url = self.apiURL + oAPI.url + (rParams || '');

            showPageLoading();

            $.ajax({
                url: url,
                type: 'POST',
                data: params,
                contentType: false,
                processData: false,
                success: function (returndata) {
                    rootScope.$apply(function() {
                        hidePageLoading();
                        var response =  returndata;
                        switch (response.code) {
                        case 200:
                            if (response.message)
                                self.showSuccess(response.message);

                            callback(response);
                            break;
                        case 404:
                            if (response.message)
                                self.showError(response.message);

                            callback(response);
                            break;
                        case 5002:
                            if (response.message)
                                self.showAlert(response.message);

                            callback(response);
                            break;
                        case 3000:
                            if (response.error)
                                self.showError(response.error);

                            callback(response);
                            break;
                        case 5000:
                            $location.path('/logout');
                            break;
                        default:
                            if (response.message)
                                self.showAlert(response.message);

                            callback(response);
                            break;
                        }
                    });

                },
                error: function(returndata) {
                    self.showError('Có lỗi xảy ra , vui lòng thử lại');
                    hidePageLoading();
                }
            });
        }
    };

    this.showAlert = function (mes) {
        toastr.warning(mes)
    };

    this.showSuccess = function (mes) {
        toastr.success(mes)
    };

    this.commonPopupClose = function () {
        self.commonPopup.modal('hide');
    };
    this.commonPopupOpen = function ($scope, $config) {
        var btnTemplate = '<button type="button" class="hvr-rectangle-in" ng-click="{function}">{title}</button>';
        var obj = self.commonPopup;
        obj.find('.modal-title').html($config.title);
        obj.find('.content-popup').html($config.content);
        var btn = '';
        angular.forEach($config.button, function (v, k) {
            var temp = btnTemplate.replace('{function}', v._function).replace('{title}', v.title);
            btn += temp;
        });
        obj.find('.modal-button').html(btn);
        $compile(obj)($scope);
        obj.modal({show: true, backdrop: true});
    };

    this.showError = function ($error) {
        $return = '';

        switch (typeof $error) {
            case 'string':
                $return = $error;
                break;
            case 'object':
                for (var prop in $error) {
                    if ($error.hasOwnProperty(prop)) {
                        $return += $error[prop] + '</br>';
                    }
                }
                break;
            case 'array':
                $.each($error, function ($i, $v) {
                    $return += $v + '</br>';
                });
                break;
            default:
                break;
        }

        toastr.error($return);
    };

    this.serializeObject = function (form)
    {
        var o = {};
        var a = form.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    this.download = function (url, filename)
    {
        var sFilename = '';
        if (typeof filename !== 'undefined') {
            sFilename = filename;
        } else {
            sFilename = url.substring(url.lastIndexOf("/") + 1).split("?")[0];
        }
        var xhr = new XMLHttpRequest();
        xhr.responseType = 'blob';
        xhr.onload = function () {
            var a = document.createElement('a');
            a.href = window.URL.createObjectURL(xhr.response); // xhr.response is a blob
            a.download = sFilename;
            a.style.display = 'none';
            document.body.appendChild(a);
            a.click();
            delete a;
        };
        xhr.open('GET', url);
        xhr.send();
    };
});