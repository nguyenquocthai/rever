WebWorldApp.service('fileUpload', function ($http, $location, commonService) {
    this.uploadFileToUrl = function (files, params, urlUpload, callback) {
        var fd = new FormData();

        if ($.isEmptyObject(params) === false) {
            fd = objectToFormData(params, fd);
        }

        for (var i = 0; i < files.length; i++) {
            if (files[i].constructor.name == 'File')
                fd.append('files[' + i + ']', files[i]);
            else if (files[i].files && files[i].files[0] && files[i].files[0].constructor.name == 'File')
                fd.append('files[' + i + ']', files[i].files[0]);
            else
                fd.append('files[' + i + ']', 'uploaded');
        }

        // mix url
        if (urlUpload.indexOf('/') != -1) {
            var rParams = urlUpload.substr(urlUpload.indexOf('/'));
            urlUpload = urlUpload.replace(rParams, '');
        }

        var oAPI = commonService.LIST_API[urlUpload];
        var url = commonService.apiURL + oAPI.url + (rParams || '');

        showPageLoading();

        $http.post(url, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
            var response = response.data;

            switch (response.code) {
                case 200:
                    commonService.showSuccess(response.message);
                    callback(response);
                    break;
                default:
                    if (!$.isEmptyObject(response.error))
                        commonService.showError(response.error);
                    else
                        commonService.showError(response.message);

                    callback(response);
                    break;
            }
            hidePageLoading();
        }).catch(function () {
            hidePageLoading();
        });
    };
});
