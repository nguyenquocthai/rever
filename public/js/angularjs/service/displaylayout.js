WebWorldApp.service('displaylayoutservice', function ($http, $q, commonService) {
    var self = this;
    this.getListDisplayLayout = function ($scope) {
        return commonService.requestFunction('displayLayoutList', [], function (res) {
            if (res.code === 200) {
                $scope.displayLayout.list = res.data;
            }
        });
    };

    this.getListConfig = function ($scope) {
        if (typeof $scope.displayLayout.config === 'undefined' || $scope.displayLayout.config.length <= 0) {
            commonService.requestFunction('getConfig', {
                filename: 'call_list',
                key: 't_display_layout_detail.disp_column_id'
            }, function (res) {
                if (res.code === 200) {
                    $scope.displayLayout.config = res.data;
                    $scope.replace.listColumnSource = [];
                    $scope.replace.listColumnDest = [];
                    var skipSourceColumn = [
                        't_checking_list.id', 't_checking_list.provided_list_id', 't_checking_list.apokin_result',
                        't_checking_list.apokin_reg_pic', 't_checking_list.apokin_reg_dt',
                        't_checking_list.o_tokyo_list_user_id', 't_checking_list.o_yokohama_list_user_id',
                        't_checking_list.o_osaka_list_user_id', 't_checking_list.o_fukuoka_list_user_id',
                        't_checking_list.okinawa_list_user_id', 't_checking_list.m_tokyo_list_user_id',
                        't_checking_list.m_yokohama_list_user_id', 't_checking_list.m_nagoiya_list_user_id',
                        't_checking_list.m_osaka_list_user_id', 't_checking_list.m_fukuoka_list_user_id',
                        't_checking_list.im_list_user_id', 't_checking_list.oss_list_user_id',
                        't_checking_list.bns_list_user_id', 't_checking_list.tns_list_user_id',
                        't_checking_list.contact_result_cd', 't_checking_list.re_contact_dt',
                        't_checking_list.list_dist_dt', 't_checking_list.t_list_nm', 't_checking_list.list_m_type_cd',
                        't_checking_list.list_use_dept_cd', 't_checking_list.list_id',
                        't_checking_list.ecc_judge_emp_id', 't_checking_list.ecc_judged_emp_id',
                        't_checking_list.dup_combined_id', 't_checking_list.tel_no_num_only',
                        't_list_checkup.check_result_cd', 't_list_dest.release_dest_dept_cd',
                        't_list_master.list_nm', 't_list_master.list_memo'
                    ];
                    var i = 1;
                    angular.forEach($scope.displayLayout.config, function ($v) {
                        if ($.inArray($v[0], skipSourceColumn) < 0) {
                            $scope.replace.listColumnSource.push($v);
                        }

                        var text = $v[0];
                        var title = $v[1];
                        var n = text.indexOf('.');
                        $scope.displayLayout.defaultCols.push({
                            table: text.substring(0, n),
                            field: text.substring(n + 1),
                            title: title,
                            show: true,
                            order: i++
                        });
                        $scope.labelSearch[text] = title;
                    });
                }
            });
        }
    };

    this.detail = function ($scope, $id) {
        commonService.requestFunction('displayLayoutDetail', {layout_id: $id}, function (res) {
            if (res.code === 200) {
                $('#listMainSub4010').modal('show');
                $scope.displayLayout.current = res.data;
            }
        });
    };

    this.delete = function ($scope, $id) {
        commonService.requestFunction('displayLayoutDelete', {disp_layout_id: $id}, function (res) {
            if (res.code === 200) {
                $scope.displayLayout.current = {};
                $scope.displayLayout.list = res.data;
                commonService.showAlert(res.message);
            }
        });
    };

    this.new = function ($scope) {
        $('#listMainSub4010').modal('show');
        $scope.displayLayout.current = {};
    };

    this.save = function ($scope, $params) {
        $params.disp_layout_id = $scope.displayLayout.current.id;
        commonService.requestFunction('displayLayoutUpsert', $params, function (res) {
            if (res.code === 200) {
                self.getListDisplayLayout($scope);
                $scope.loadDisplaylayout();
                $('#listMainSub4010').modal('hide');
                commonService.showAlert(res.message);
            }
        });
    };
});
