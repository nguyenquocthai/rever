// active parent menu
$('.active-menu').parents('li').each(function(i, e) {
    $(e).children().addClass('active-menu');
});

//-------------------------------------------------------------------------------
// CONFIG TOASTR
if (typeof toastr != 'undefined') {
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
}

//-------------------------------------------------------------------------------
function popupText(data) {
  try {
    data = JSON.parse(data);
  } catch(e) { }

  switch(jQuery.type(data)) {
    case 'object':
      var t = [];

      $.each(data, function(key,val) {
        t = t.concat(val);
      });

      data = t;
      break;
    case 'array':
      break;
    default:
      data = [data];
      break;
  }

  var result = '<ul class="error-popup">';

  for (var i = 0; i < data.length; i++) {
    result += '<li>' + data[i] + '</li>';
  }

  result += '</ul>';

  return result
}

//-------------------------------------------------------------------------------
function showErrors(errors) {
  $text = popupText(errors);
  if ($text != '') toastr.error($text);
}

//-------------------------------------------------------------------------------
function showSuccesses(successes) {
  $text = popupText(successes);
  if ($text != '') toastr.success($text);
}

//-------------------------------------------------------------------------------
window.countBlockUI = 0;

function showPageLoading(options) {
  options = options || {};

  if (!options.message)
    options.message = '<div class="box-loading"><div class="cssload"><span></span></div></div>';

  if (countBlockUI == 0)
    $.blockUI({
      message: options.message,
      overlayCSS: {
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        opacity: 0.8,
        zIndex: 1200,
        cursor: null
      },
      css: {
        border: 0,
        color: '#fff',
        padding: 0,
        zIndex: 1201,
        width: 46,
        height: 46,
        top: '50%',
        left: '50%',
        marginTop: '-23px',
        marginLeft: '-23px',
        backgroundColor: 'transparent'
      }
    });

  countBlockUI++;
}

//-------------------------------------------------------------------------------
function hidePageLoading() {
  if (countBlockUI > 0) {
    countBlockUI--;

    if (countBlockUI == 0)
      $.unblockUI();
  }
}

//-------------------------------------------------------------------------------
function slugify(str) {
    // Chuyển hết sang chữ thường
    str = typeof str != 'undefined' ? str.toLowerCase() : '';

    // xóa dấu
    str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
    str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
    str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
    str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
    str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
    str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
    str = str.replace(/(đ)/g, 'd');

    // Xóa ký tự đặc biệt
    str = str.replace(/([^0-9a-z-\s])/g, '');

    // Xóa khoảng trắng thay bằng ký tự -
    str = str.replace(/(\s+)/g, '-');

    // xóa phần dự - ở đầu
    str = str.replace(/^-+/g, '');

    // xóa phần dư - ở cuối
    str = str.replace(/-+$/g, '');

    // return
    return str;
}

//-------------------------------------------------------------------------------------
function execFunc(execFunction, delay) {
  var temp = setInterval(function() {
    try {
      execFunction();
      clearInterval(temp);
    } catch(e) { }
  }, delay);
}

//-------------------------------------------------------------------------------
function confirmPopup(title, message, success_callback) {
  bootbox.dialog({
    title: title || null,
    message: message,
    onEscape: function() {},
    show: true,
    backdrop: true,
    closeButton: true,
    animate: true,
    buttons: {
      cancel: {
        label: 'Hủy bỏ',
        className: 'btn-close width120',
        icon: '<i class="fa fa-close"></i>',
        callback: function() {}
      },
      success: {
        label: 'Đồng ý',
        className: 'btn-style width120',
        icon: '<i class="fa fa-check"></i>',
        callback: success_callback
      }
    }
  });
}

//-------------------------------------------------------------------------------------
function html_entity_decode(str) {
  return $("<textarea/>").html(str).text();
}

//-------------------------------------------------------------------------------
function isImage(e) {
  var pattern = /^.*\.{1}(jpg|jpeg|png|gif)$/i;
  return e.length > 0 && pattern.test(e);
}

//-------------------------------------------------------------------------------
function isPhone(e) {
  var pattern = /^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/;
  return e.length > 0 && pattern.test(e);
}

//-------------------------------------------------------------------------------
function isEmail(e) {
  var pattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
  return e.length > 0 && pattern.test(e);
}

//-------------------------------------------------------------------------------
function object_to_array(obj) {
    return Object.keys(obj).map(function (key) { return obj[key]; });
}

//-------------------------------------------------------------------------------
function in_array(array, value, options) {
  options = options || {};

  return array.some(function(item) {
    if (options.key)
      return item[options.key] === value;
    else
      return item === value;
  });
}

//-------------------------------------------------------------------------------
function rtrim(string, s) {
    return string.replace(new RegExp(s + "*$"),'');
};

//-------------------------------------------------------------------------------
function number_format( number, decimals, dec_point, thousands_sep ) {
  var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 0 : decimals;
  var d = dec_point == undefined ? "," : dec_point;
  var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
  var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;

  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

//-------------------------------------------------------------------------------
function price_format(price, options) {
  if (typeof unit_price === 'undefined' || !unit_price) unit_price = '₫';

  options = options || {};

  if (price && price > 0) {
    return number_format(price, 0, ',', '.') + unit_price;
  }

  if (options.default_value)
    return options.default_value + unit_price;
  else
    return 'Liên hệ';
}

//-------------------------------------------------------------------------------
function number_animation() {
  $('.count').each(function () {
    var _this = $(this);

    var interval = setInterval(function() {
      if (_this.attr('count-to') > 0) {
        _this.prop('Counter', 0).animate({
          Counter: _this.attr('count-to')
        }, {
          duration: 500,
          easing: 'swing',
          step: function (now) {
            _this.text(Math.ceil(now));
          },
          complete: function() {
            var text_format = number_format(_this.text());
            _this.text(text_format);
          }
        });

        clearInterval(interval);
      };
    }, 200);

  });
}

//-------------------------------------------------------------------------------
function vat_format(vat) {
  if (vat || vat > 0) {
    return number_format(vat, 0, ',', '.') + '%';
  }

  return '0%';
}

//-------------------------------------------------------------------------------
function get_discount(product) {
  if (typeof unit_price === 'undefined' || !unit_price) unit_price = '₫';

  if (product.discount_type) {
    switch (product.discount_type) {
      case '0':
        return '-' + product.discount + unit_price;

      case '1':
        return '-' + product.discount + '%';
    }
  }

  return '-0%';
}

//-------------------------------------------------------------------------------
function product_price_vat(product) {
    return product.price * (1 + product.vat / 100);
}

//-------------------------------------------------------------------------------
function product_price(product) {
    if (product.discount_type != '')
        return product.newprice;
    else
        return product.price * (1 + $product.vat / 100);
}

//-------------------------------------------------------------------------------
function weight_format(weight) {
  if (typeof unit_weight === 'undefined' || !unit_weight) unit_weight = 'kg';

  if (weight && weight != '' && weight != 0) {
    return rtrim(rtrim(number_format(weight, 4, ',', '.'), '0'), ',') + ' ' + unit_weight;
  }

  return '0 ' + unit_weight;
}

//-------------------------------------------------------------------------------
function shipping_price_format(price) {
  if (typeof unit_price === 'undefined' || !unit_price) unit_price = '₫';

  if (price && price > 0) {
    return number_format(price, 0, '.', ',') + unit_price;
  }

  return '0' + unit_price;
}

//-------------------------------------------------------------------------------
function objectToFormData(obj, form, namespace) {

  var fd = form || new FormData();
  var formKey;

  for(var property in obj) {
    if(obj.hasOwnProperty(property)) {

      if(namespace) {
        formKey = namespace + '[' + property + ']';
      } else {
        formKey = property;
      }

      // if the property is an object, but not a File,
      // use recursivity.
      if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {

        objectToFormData(obj[property], fd, formKey);

      } else {

        // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }

    }
  }

  return fd;
};

// login facebook
function statusChangeCallback(response) {
    if (accessToken) {
        var accessToken = response.authResponse.accessToken;
    }
    if (response.status === 'connected') {
        graph_api(accessToken);
    } else if (response.status === 'not_authorized') {
        Alert.show('Có lỗi xảy ra , Vui lòng thử lại', 'error');
    } else {
        Alert.show('Có lỗi xảy ra , Vui lòng thử lại', 'error');
    }
}

window.fbAsyncInit = function () {
    FB.init({
      appId      : '1341219729364685',
      xfbml      : true,
      version    : 'v3.2'
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_EN/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// end facebook
var Request = {
    objectConfig: {},
    config: function (object) {
        if (typeof object == 'undefined' || typeof object != 'object') {
            return;
        }
        // dataType
        this.objectConfig.dataType = object.dataType? object.dataType: 'json';
        // url
        if (typeof object.url == 'undefined' || object.url == '') {
            alert("URL is not null.");
            return;
        } else {
            this.objectConfig.url = object.url;
        }
        // data
        //this.objectConfig.data = object.data? $.param(object.data): $.param({});
        this.objectConfig.data = object.data;
        // contentType
        if (typeof object.contentType != 'undefined' || object.contentType != '') {
            this.objectConfig.contentType = object.contentType;
        }
        // processData
        if (typeof object.processData != 'undefined' || object.processData != '') {
            this.objectConfig.processData = object.processData;
        }
        // type
        this.objectConfig.type = object.type? object.type: 'GET';

        // function
        // beforeSend
        if (typeof object.beforeSend == 'function') {
            this.objectConfig.beforeSend = object.beforeSend;
        }
        // error
        if (typeof object.error == 'function') {
            this.objectConfig.error = object.error;
        }
        showPageLoading();
    },
    // function apply, excute configuration in config function
    apply: function(callback) {
        hidePageLoading();
        this.objectConfig.success = function (res) {
            callback(res);
        }
        if (typeof this.objectConfig.error != 'function') {
            this.objectConfig.error = function() {
                toastr.error('Http request error!', null, {timeOut: 4000});
            }
        }
        $.ajax(this.objectConfig);
    },

    send: function(url, method, params, callback) {
        var method = method || "GET";
        if (typeof url == 'undefined' || url == '') {
            alert("URL is not null.");
            return;
        }

        if (typeof params == 'undefined' || typeof params != 'object') {
            var params = {};
        }
        showPageLoading();
        $.ajax({
            type: method,
            url: url,
            data: $.param(params),
            dataType: 'json',
            success: function(result) {
                hidePageLoading();
                callback(result)
            },
            error: function() {
                hidePageLoading();
                toastr.error('Http request error!', null, {timeOut: 4000});
            }
        });
    }
};

var Alert = {
    /*
     * type: success || error
     * msg: message
    */
    show: function(msg, type) {
        var type = type || 'success';
        var msg = msg || '';
        if (type == "success") {
            toastr.success(msg, null, {timeOut: 4000});
        } else {
            toastr.error(msg, null, {timeOut: 4000});
        }
    },
    success: function(msg) {
        toastr.success(msg, null, {timeOut: 4000});
    },
    error: function(msg) {
        toastr.error(msg, null, {timeOut: 4000});
    }
};