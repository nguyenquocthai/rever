// load_tongquan
var data_tq = [["<label class=\"mt-checkbox mt-checkbox-outline\"><input class=\"check_player\" value=\"fas fa-chart-pie\" type=\"checkbox\" data-id=\"1\" data-title=\"Đầu tư\" data-title_en=\"Đầu tư en\" data-value=\"260 nhà đầu tư\" data-value_en=\"260 nhà đầu tư en\"><span></span></label>",1,"<i class=\"fas fa-chart-pie\"</i>","Đầu tư","260 nhà đầu tư","Đầu tư en","260 nhà đầu tư en"],["<label class=\"mt-checkbox mt-checkbox-outline\"><input class=\"check_player\" value=\"fab fa-accusoft\" type=\"checkbox\" data-id=\"2\" data-title=\"Bin mặt trời\" data-title_en=\"Bin mặt trời en\" data-value=\"1300 tấm bin\" data-value_en=\"1300 tấm bin en\"><span></span></label>",2,"<i class=\"fab fa-accusoft\"</i>","Bin mặt trời","1300 tấm bin","Bin mặt trời en","1300 tấm bin en"],["<label class=\"mt-checkbox mt-checkbox-outline\"><input class=\"check_player\" value=\"fas fa-box\" type=\"checkbox\" data-id=\"4\" data-title=\"Kho hàng\" data-title_en=\"Kho hàng en\" data-value=\"rộng 500m\" data-value_en=\"rộng 500m en\"><span></span></label>",4,"<i class=\"fas fa-box\"</i>","Kho hàng","rộng 500m","Kho hàng en","rộng 500m en"],["<label class=\"mt-checkbox mt-checkbox-outline\"><input class=\"check_player\" value=\"fab fa-android\" type=\"checkbox\" data-id=\"10\" data-title=\"Điện thoại\" data-title_en=\"phone\" data-value=\"rất nhiều điện thoại\" data-value_en=\"many phone\"><span></span></label>",10,"<i class=\"fab fa-android\"</i>","Điện thoại","rất nhiều điện thoại","phone","many phone"],["<label class=\"mt-checkbox mt-checkbox-outline\"><input class=\"check_player\" value=\"fas fa-book\" type=\"checkbox\" data-id=\"16\" data-title=\"Thư viện\" data-title_en=\"\" data-value=\"1 triệu quyển sách\" data-value_en=\"\"><span></span></label>",16,"<i class=\"fas fa-book\"</i>","Thư viện","1 triệu quyển sách",null,null]];
var datatable = $('#tbl-dataz').DataTable({
    order: [[ 1, 'desc' ]],
    columnDefs: [
        { sortable: false, searchable: false, targets: [ 0 ] },
        { class: 'hidden', targets: [ 1 ] },
    ],
    displayStart: 0,
    displayLength: 10,
    data: data_tq,
    "autoWidth": false
});
datatable.search('').draw();
// End load_tongquan

// load_tienich
var data_ti = [["<label class=\"mt-checkbox mt-checkbox-outline\"><input class=\"check_player2\" value=\"fas fa-book\" type=\"checkbox\" data-id=\"11\" data-title=\"Thư viện\" data-title_en=\"Thư viện en\"><span></span></label>",11,"<i class=\"fas fa-book\"</i>","Thư viện","Thư viện en"],["<label class=\"mt-checkbox mt-checkbox-outline\"><input class=\"check_player2\" value=\"fas fa-ambulance\" type=\"checkbox\" data-id=\"12\" data-title=\"Bệnh viện\" data-title_en=\"Bệnh viện en\"><span></span></label>",12,"<i class=\"fas fa-ambulance\"</i>","Bệnh viện","Bệnh viện en"],["<label class=\"mt-checkbox mt-checkbox-outline\"><input class=\"check_player2\" value=\"fas fa-basketball-ball\" type=\"checkbox\" data-id=\"13\" data-title=\"Sân bóng rổ\" data-title_en=\"Sân bóng rổ en\"><span></span></label>",13,"<i class=\"fas fa-basketball-ball\"</i>","Sân bóng rổ","Sân bóng rổ en"],["<label class=\"mt-checkbox mt-checkbox-outline\"><input class=\"check_player2\" value=\"fab fa-docker\" type=\"checkbox\" data-id=\"14\" data-title=\"Hồ bơi ngoài trời\" data-title_en=\"Hồ bơi en\"><span></span></label>",14,"<i class=\"fab fa-docker\"</i>","Hồ bơi ngoài trời","Hồ bơi en"],["<label class=\"mt-checkbox mt-checkbox-outline\"><input class=\"check_player2\" value=\"fas fa-bath\" type=\"checkbox\" data-id=\"15\" data-title=\"Nhà tắm\" data-title_en=\"Nhà tắm en\"><span></span></label>",15,"<i class=\"fas fa-bath\"</i>","Nhà tắm","Nhà tắm en"],["<label class=\"mt-checkbox mt-checkbox-outline\"><input class=\"check_player2\" value=\"fas fa-beer\" type=\"checkbox\" data-id=\"17\" data-title=\"Nhà hàng lớn\" data-title_en=\"\"><span></span></label>",17,"<i class=\"fas fa-beer\"</i>","Nhà hàng lớn",null]];
var datatable2 = $('#tbl-dataz2').DataTable({
    order: [[ 1, 'desc' ]],
    columnDefs: [
        { sortable: false, searchable: false, targets: [ 0 ] },
        { class: 'hidden', targets: [ 1 ] },
    ],
    displayStart: 0,
    displayLength: 10,
    data: data_ti,
    "autoWidth": false
});
datatable2.search('').draw();
// End load_tienich

//-------------------------------------------------------------------------------
$('#add_tienich').click(function(event) {
    var tienich = [];

    var id = 'key'+$.now();
    var data = $('#data_icon').val();
    var title = $('#title_tienich').val();
    var title_en = $('#title_tienich_en').val();

    if (data == '') {
        alert('Chưa chọn icon');
        return false;
    }

    if (title == '') {
        alert('Chưa nhập tên tiện ích');
        return false;
    }

    if (title_en == '') {
        alert('Chưa nhập tên tiện ích (English)');
        return false;
    }

    tienich['id'] = id;
    tienich['data'] = data;
    tienich['title'] = title;
    tienich['title_en'] = title_en;

    $scope.product1.tienich.push(tienich);
    $('.load-add-tienich').prepend('<li data-id="'+id+'" class="col-sm-3 item-tienich"><div class="item-mod"><span class="icon-mod"><i class="'+data+'"></i></span><span class="title-mod">'+title+'</span><span class="title-mod _en">'+title_en+'</span><i onclick="delete_tienich(&#39;'+id+'&#39;)" class="fas fa-times delete-mod"></i></div></li>');

    $('#data_icon').val('');
    $('#title_tienich').val('');
    $('#title_tienich_en').val('');
});

//-------------------------------------------------------------------------------
$('#add_tq').click(function(event) {
    var tongquan = [];

    var id = 'key'+$.now();
    var data = $('#data_tq').val();
    var title = $('#title_tq').val();
    var value = $('#value_tq').val();

    var title_en = $('#title_tq_en').val();
    var value_en = $('#value_tq_en').val();

    if (data == '') {
        alert('Chưa chọn icon');
        return false;
    }

    if (title == '') {
        alert('Chưa nhập tên tổng quan');
        return false;
    }

    if (value == '') {
        alert('Chưa nhập giá trị tổng quan');
        return false;
    }

    if (title_en == '') {
        alert('Chưa nhập tên tổng quan (English)');
        return false;
    }

    if (value_en == '') {
        alert('Chưa nhập giá trị tổng quan (English)');
        return false;
    }

    tongquan['id'] = id;
    tongquan['data'] = data;
    tongquan['title'] = title;
    tongquan['value'] = value;

    tongquan['title_en'] = title_en;
    tongquan['value_en'] = value_en;

    $scope.product1.tongquan.push(tongquan);
    $('.load-add-tq').prepend('<li data-id="'+id+'" class="col-sm-3 item-tq"><div class="item-mod"><span class="icon-mod"><i class="'+data+'"></i></span><span class="title-mod">'+title+'</span><span class="title-mod _en">'+title_en+'</span><span class="title-mod">'+value+'</span><span class="title-mod _en">'+value_en+'</span><i onclick="delete_tq(&#39;'+id+'&#39;)" class="fas fa-times delete-mod"></i></div></li>');

    $('#data_tq').val('');
    $('#title_tq').val('');
    $('#value_tq').val('');

    $('#title_tq_en').val('');
    $('#value_tq_en').val('');
});

delete_tienich = function(id) {
    $('.item-tienich[data-id="'+id+'"]').remove();
    $scope.product1.tienich = $scope.product1.tienich.filter(function( obj ) {
        return obj.id !== id;
    });
};

delete_tq = function(id) {
    $('.item-tq[data-id="'+id+'"]').remove();
    $scope.product1.tongquan = $scope.product1.tongquan.filter(function( obj ) {
        return obj.id !== id;
    });
};


//-------------------------------------------------------------------------------
$('.themtq').click(function(event) {
    $('#modal_tongquan').modal('show');
});

$('#all_id_player').change(function(){
    var check = this.checked ? '1' : '0';
    if (check == 1) {
        $('.check_player').prop('checked', true);
    } else {
        $('.check_player').prop('checked', false);
    }
});

//-------------------------------------------------------------------------------
var addstore = function() {
    var list_ids = [];
    $('.check_player').each(function(i, e) {
        var value = {};
        var check_id = this.checked ? '1' : '0';
        if (check_id == '1') {
            value['data'] = $(this).val();
            value['id'] = $(this).attr('data-id');
            value['title'] = $(this).attr('data-title');
            value['title_en'] = $(this).attr('data-title_en');
            value['value'] = $(this).attr('data-value');
            value['value_en'] = $(this).attr('data-value_en');
            list_ids.push(value);
        }
    });

    if (list_ids == '') {
        alert('Chưa chọn bất kỳ tổng quan...')
        return false;
    }

    list_tongquan(list_ids);

    $('#modal_tongquan').modal('hide');

    $('#all_id_player').prop('checked', false);

    $('.check_player').prop('checked', false);
};

//
// Initialize stuff
//

var grid = null;
var docElem = document.documentElement;
var demo = document.querySelector('.grid-demo');
var gridElement = demo.querySelector('.grid');
var filterField = demo.querySelector('.filter-field');
var searchField = demo.querySelector('.search-field');
var sortField = demo.querySelector('.sort-field');
var layoutField = demo.querySelector('.layout-field');
var addItemsElement = demo.querySelector('.add-more-items');
var characters = 'abcdefghijklmnopqrstuvwxyz';
var filterOptions = ['red', 'blue', 'green'];
var dragOrder = [];
var uuid = 0;
var limit = 12;
var filterFieldValue;
var sortFieldValue;
var layoutFieldValue;
var searchFieldValue;

//
// Grid helper functions
//

function list_tongquan(arr) {
    // Generate new elements.
    var newElems = generateElements(arr);

    // Set the display of the new elements to "none" so it will be hidden by
    // default.
    newElems.forEach(function (item) {
        item.style.display = 'none';
    });

    // Add the elements to the grid.
    var newItems = grid.add(newElems);

    // Update UI indices.
    updateIndices();

    // Sort the items only if the drag sorting is not active.
    if (sortFieldValue !== 'order') {
        grid.sort(sortFieldValue === 'title' ? compareItemTitle : compareItemColor);
        dragOrder = dragOrder.concat(newItems);
    }

    // Finally filter the items.
    filter();
}

function initDemo() {

    initGrid();

    // Reset field values.
    searchField.value = '';
    [sortField, filterField, layoutField].forEach(function (field) {
        field.value = field.querySelectorAll('option')[0].value;
    });

    // Set inital search query, active filter, active sort value and active layout.
    searchFieldValue = searchField.value.toLowerCase();
    filterFieldValue = filterField.value;
    sortFieldValue = sortField.value;
    layoutFieldValue = layoutField.value;

    // Search field binding.
    searchField.addEventListener('keyup', function () {
        var newSearch = searchField.value.toLowerCase();
        if (searchFieldValue !== newSearch) {
            searchFieldValue = newSearch;
            filter();
        }
    });

    // Filter, sort and layout bindings.
    filterField.addEventListener('change', filter);
    sortField.addEventListener('change', sort);
    layoutField.addEventListener('change', changeLayout);

    // Add/remove items bindings.
    addItemsElement.addEventListener('click', addItems);

    gridElement.addEventListener('click', function (e) {
        if (elementMatches(e.target, '.card-remove, .card-remove i')) {
            removeItem(e);
        }
    });

}

function initGrid() {

    var dragCounter = 0;
    var arr = [];

    grid = new Muuri(gridElement, {
            items: generateElements(arr),
            layoutDuration: 400,
            layoutEasing: 'ease',
            dragEnabled: true,
            dragSortInterval: 50,
            dragContainer: document.body,
            dragStartPredicate: function (item, event) {
                var isDraggable = sortFieldValue === 'order';
                var isRemoveAction = elementMatches(event.target, '.card-remove, .card-remove i');
                return isDraggable && !isRemoveAction ? Muuri.ItemDrag.defaultStartPredicate(item, event) : false;
            },
            dragReleaseDuration: 400,
            dragReleseEasing: 'ease'
        })
        .on('dragStart', function () {
            ++dragCounter;
            docElem.classList.add('dragging');
        })
        .on('dragEnd', function () {
            if (--dragCounter < 1) {
                docElem.classList.remove('dragging');
            }
        })
        .on('move', updateIndices)
        .on('sort', updateIndices);

}

function filter() {

    filterFieldValue = filterField.value;
    grid.filter(function (item) {
        var element = item.getElement();
        var isSearchMatch = !searchFieldValue ? true : (element.getAttribute('data-title') || '').toLowerCase().indexOf(searchFieldValue) > -1;
        var isFilterMatch = !filterFieldValue ? true : (element.getAttribute('data-color') || '') === filterFieldValue;
        return isSearchMatch && isFilterMatch;
    });

}

function sort() {

    // Do nothing if sort value did not change.
    var currentSort = sortField.value;
    if (sortFieldValue === currentSort) {
        return;
    }

    // If we are changing from "order" sorting to something else
    // let's store the drag order.
    if (sortFieldValue === 'order') {
        dragOrder = grid.getItems();
    }

    // Sort the items.
    grid.sort(
        currentSort === 'title' ? compareItemTitle :
        currentSort === 'color' ? compareItemColor :
        dragOrder
    );

    // Update indices and active sort value.
    updateIndices();
    sortFieldValue = currentSort;

}

function addItems() {

    var arr = [];

    // Generate new elements.
    var newElems = generateElements(arr);

    // Set the display of the new elements to "none" so it will be hidden by
    // default.
    newElems.forEach(function (item) {
        item.style.display = 'none';
    });

    // Add the elements to the grid.
    var newItems = grid.add(newElems);

    // Update UI indices.
    updateIndices();

    // Sort the items only if the drag sorting is not active.
    if (sortFieldValue !== 'order') {
        grid.sort(sortFieldValue === 'title' ? compareItemTitle : compareItemColor);
        dragOrder = dragOrder.concat(newItems);
    }

    // Finally filter the items.
    filter();

}

function removeItem(e) {

    var elem = elementClosest(e.target, '.item_code');
    grid.hide(elem, {
        onFinish: function (items) {
            var item = items[0];
            grid.remove(item, {
                removeElements: true
            });
            if (sortFieldValue !== 'order') {
                var itemIndex = dragOrder.indexOf(item);
                if (itemIndex > -1) {
                    dragOrder.splice(itemIndex, 1);
                }
            }
        }
    });
    updateIndices();

}

function changeLayout() {

    layoutFieldValue = layoutField.value;
    grid._settings.layout = {
        horizontal: false,
        alignRight: layoutFieldValue.indexOf('right') > -1,
        alignBottom: layoutFieldValue.indexOf('bottom') > -1,
        fillGaps: layoutFieldValue.indexOf('fillgaps') > -1
    };
    grid.layout();

}

//
// Generic helper functions
//

function generateElements(arr) {
    var ret = [];
    var last = $('.load_tongquan .item_code').length;
    var last2 = parseInt(last, 10);
    var slot = limit - last2;
    if (last2 + arr.length > limit) {
        alert('Bạn còn ' + slot + ' chổ trống.');
        return ret;
    }
    for (var i = 0; i < arr.length; i++) {
        ret.push(generateElement(
            ++uuid,
            arr[i].id,
            arr[i].data,
            arr[i].title,
            arr[i].title_en,
            arr[i].value,
            arr[i].value_en,
        ));
    }
    return ret;
}

function generateElement(id, id_tq, data, title, title_en, value, value_en) {
    var itemElem = document.createElement('div');
    var classNames = 'item_code h1 w2';
    var itemTemplate = '' +
        '<div class="' + classNames + '" data-id="' + id + '" data-color="green" data-title="' + title + '">' +
        '<div class="item-content">' +
        '<div class="card">' +
        '<div class="card-id">' + id + '</div>' +
        '<div class="card_child">' +
        '<div class="card-id_tq">' + id_tq + '</div>' +
        '<div class="card-id_data"><i class="' + data + '"></i></div>' +
        '<div class="card-title">' + title + '</div>' +
        '<div class="card-title_en">' + title_en + '</div>' +
        '<div class="card-value">' + value + '</div>' +
        '<div class="card-value_en">' + value_en + '</div>' +
        '</div>' +
        '<div class="card-remove"><i class="fas fa-trash-alt"></i></div>' +
        '</div>' +
        '</div>' +
        '</div>';

    itemElem.innerHTML = itemTemplate;
    return itemElem.firstChild;
}

function getRandomItem(collection) {

    return collection[Math.floor(Math.random() * collection.length)];

}

// https://stackoverflow.com/a/7228322
function getRandomInt(min, max) {

    return Math.floor(Math.random() * (max - min + 1) + min);

}

function generateRandomWord(length) {

    var ret = '';
    for (var i = 0; i < length; i++) {
        ret += getRandomItem(characters);
    }
    return ret;

}

function compareItemTitle(a, b) {

    var aVal = a.getElement().getAttribute('data-title') || '';
    var bVal = b.getElement().getAttribute('data-title') || '';
    return aVal < bVal ? -1 : aVal > bVal ? 1 : 0;

}

function compareItemColor(a, b) {

    var aVal = a.getElement().getAttribute('data-color') || '';
    var bVal = b.getElement().getAttribute('data-color') || '';
    return aVal < bVal ? -1 : aVal > bVal ? 1 : compareItemTitle(a, b);

}

function updateIndices() {

    grid.getItems().forEach(function (item, i) {
        item.getElement().setAttribute('data-id', i + 1);
        item.getElement().querySelector('.card-id').innerHTML = i + 1;
    });

}

function elementMatches(element, selector) {

    var p = Element.prototype;
    return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector).call(element, selector);

}

function elementClosest(element, selector) {

    if (window.Element && !Element.prototype.closest) {
        var isMatch = elementMatches(element, selector);
        while (!isMatch && element && element !== document) {
            element = element.parentNode;
            isMatch = element && element !== document && elementMatches(element, selector);
        }
        return element && element !== document ? element : null;
    } else {
        return element.closest(selector);
    }

}

//
// Fire it up!
//

initDemo();
var arr = [];
list_tongquan(arr);


// Tiện ích-------------------------------------------------------------------------
//-------------------------------------------------------------------------------
$('.themtienich').click(function(event) {
    $('#modal_tienich').modal('show');
});

$('#all_id_player2').change(function(){
    var check = this.checked ? '1' : '0';
    if (check == 1) {
        $('.check_player2').prop('checked', true);
    } else {
        $('.check_player2').prop('checked', false);
    }
});

//-------------------------------------------------------------------------------
var addstore2 = function() {
    var list_ids2 = [];
    $('.check_player2').each(function(i, e) {
        var value = {};
        var check_id = this.checked ? '1' : '0';
        if (check_id == '1') {
            value['data'] = $(this).val();
            value['id'] = $(this).attr('data-id');
            value['title'] = $(this).attr('data-title');
            value['title_en'] = $(this).attr('data-title_en');
            list_ids2.push(value);
        }
    });

    if (list_ids2 == '') {
        alert('Chưa chọn bất kỳ tiện ích...')
        return false;
    }

    list_tienich(list_ids2);

    $('#modal_tienich').modal('hide');

    $('#all_id_player2').prop('checked', false);

    $('.check_player2').prop('checked', false);
};

var grid2 = null;
var docElem2 = document.documentElement;
var demo2 = document.querySelector('.grid-demo2');
var gridElement2 = demo2.querySelector('.grid2');
var filter2Field2 = demo2.querySelector('.filter2-field2');
var searchField2 = demo2.querySelector('.search-field2');
var sortField2 = demo2.querySelector('.sort-field2');
var layoutField2 = demo2.querySelector('.layout-field2');
var addItemsElement2 = demo2.querySelector('.add-more-items2');
var characters2 = 'abcdefghijklmnopqrstuvwxyz';
var filter2Options2 = ['red', 'blue', 'green'];
var dragOrder2 = [];
var uuid2 = 0;
var limit2 = 12;
var filter2FieldValue2;
var sortFieldValue2;
var layoutFieldValue2;
var searchFieldValue2;

function list_tienich(arr2) {
    // Generate new elements.
    var newElems2 = generateElements2(arr2);

    // Set the display of the new elements to "none" so it will be hidden by
    // default.
    newElems2.forEach(function (item) {
        item.style.display = 'none';
    });

    // Add the elements to the grid.
    var newItems = grid2.add(newElems2);

    // Update UI indices.
    updateIndices2();

    // Sort the items only if the drag sorting is not active.
    if (sortFieldValue2 !== 'order') {
        grid2.sort2(sortFieldValue2 === 'title' ? compareItemTitle2 : compareItemColor2);
        dragOrder2 = dragOrder2.concat(newItems);
    }

    // Finally filter2 the items.
    filter2();
}

function initDemo2() {

    initGrid2();

    // Reset field values.
    searchField2.value = '';
    [sortField2, filter2Field2, layoutField2].forEach(function (field) {
        field.value = field.querySelectorAll('option')[0].value;
    });

    // Set inital search query, active filter2, active sort value and active layout.
    searchFieldValue2 = searchField2.value.toLowerCase();
    filter2FieldValue2 = filter2Field2.value;
    sortFieldValue2 = sortField2.value;
    layoutFieldValue2 = layoutField2.value;

    // Search field binding.
    searchField2.addEventListener('keyup', function () {
        var newSearch = searchField2.value.toLowerCase();
        if (searchFieldValue2 !== newSearch) {
            searchFieldValue2 = newSearch;
            filter2();
        }
    });

    // Filter, sort and layout bindings.
    filter2Field2.addEventListener('change', filter2);
    sortField2.addEventListener('change', sort2);
    layoutField2.addEventListener('change', changeLayout2);

    // Add/remove items bindings.
    addItemsElement2.addEventListener('click', addItems2);

    gridElement2.addEventListener('click', function (e) {
        if (elementMatches2(e.target, '.card-remove, .card-remove i')) {
            removeItem2(e);
        }
    });

}

function initGrid2() {

    var dragCounter = 0;
    var arr2 = [];

    grid2 = new Muuri(gridElement2, {
            items: generateElements2(arr2),
            layoutDuration: 400,
            layoutEasing: 'ease',
            dragEnabled: true,
            dragSortInterval: 50,
            dragContainer: document.body,
            dragStartPredicate: function (item, event) {
                var isDraggable = sortFieldValue2 === 'order';
                var isRemoveAction = elementMatches2(event.target, '.card-remove, .card-remove i');
                return isDraggable && !isRemoveAction ? Muuri.ItemDrag.defaultStartPredicate(item, event) : false;
            },
            dragReleaseDuration: 400,
            dragReleseEasing: 'ease'
        })
        .on('dragStart', function () {
            ++dragCounter;
            docElem2.classList.add('dragging');
        })
        .on('dragEnd', function () {
            if (--dragCounter < 1) {
                docElem2.classList.remove('dragging');
            }
        })
        .on('move', updateIndices2)
        .on('sort2', updateIndices2);

}

function filter2() {

    filter2FieldValue2 = filter2Field2.value;
    grid2.filter(function (item) {
        var element = item.getElement();
        var isSearchMatch = !searchFieldValue2 ? true : (element.getAttribute('data-title') || '').toLowerCase().indexOf(searchFieldValue2) > -1;
        var isFilterMatch = !filter2FieldValue2 ? true : (element.getAttribute('data-color') || '') === filter2FieldValue2;
        return isSearchMatch && isFilterMatch;
    });

}

function sort2() {

    // Do nothing if sort value did not change.
    var currentSort = sortField2.value;
    if (sortFieldValue2 === currentSort) {
        return;
    }

    // If we are changing from "order" sorting to something else
    // let's store the drag order.
    if (sortFieldValue2 === 'order') {
        dragOrder2 = grid2.getItems();
    }

    // Sort the items.
    grid2.sort2(
        currentSort === 'title' ? compareItemTitle2 :
        currentSort === 'color' ? compareItemColor2 :
        dragOrder2
    );

    // Update indices and active sort value.
    updateIndices2();
    sortFieldValue2 = currentSort;

}

function addItems2() {

    var arr2 = [];

    // Generate new elements.
    var newElems2 = generateElements2(arr2);

    // Set the display of the new elements to "none" so it will be hidden by
    // default.
    newElems2.forEach(function (item) {
        item.style.display = 'none';
    });

    // Add the elements to the grid.
    var newItems = grid2.add(newElems2);

    // Update UI indices.
    updateIndices2();

    // Sort the items only if the drag sorting is not active.
    if (sortFieldValue2 !== 'order') {
        grid2.sort2(sortFieldValue2 === 'title' ? compareItemTitle2 : compareItemColor2);
        dragOrder2 = dragOrder2.concat(newItems);
    }

    // Finally filter2 the items.
    filter2();

}

function removeItem2(e) {

    var elem = elementClosest2(e.target, '.item_code');
    grid2.hide(elem, {
        onFinish: function (items) {
            var item = items[0];
            grid2.remove(item, {
                removeElements: true
            });
            if (sortFieldValue2 !== 'order') {
                var itemIndex = dragOrder2.indexOf(item);
                if (itemIndex > -1) {
                    dragOrder2.splice(itemIndex, 1);
                }
            }
        }
    });
    updateIndices2();

}

function changeLayout2() {

    layoutFieldValue2 = layoutField2.value;
    grid2._settings.layout = {
        horizontal: false,
        alignRight: layoutFieldValue2.indexOf('right') > -1,
        alignBottom: layoutFieldValue2.indexOf('bottom') > -1,
        fillGaps: layoutFieldValue2.indexOf('fillgaps') > -1
    };
    grid2.layout();

}

//
// Generic helper functions
//

function generateElements2(arr2) {
    var ret = [];
    var last = $('.load_tienich .item_code').length;
    var last2 = parseInt(last, 10);
    var slot = limit2 - last2;
    if (last2 + arr2.length > limit2) {
        alert('Bạn còn ' + slot + ' chổ trống.');
        return ret;
    }
    for (var i = 0; i < arr2.length; i++) {
        ret.push(generateElement2(
            ++uuid2,
            arr2[i].id,
            arr2[i].data,
            arr2[i].title,
            arr2[i].title_en,
        ));
    }
    return ret;
}

function generateElement2(id, id_tq, data, title, title_en) {

    var itemElem = document.createElement('div');
    var classNames = 'item_code h1 w2';
    var itemTemplate = '' +
        '<div class="' + classNames + '" data-id="' + id + '" data-color="green" data-title="' + title + '">' +
        '<div class="item-content">' +
        '<div class="card">' +
        '<div class="card-id">' + id + '</div>' +
        '<div class="card_child">' +
        '<div class="card-id_tq">' + id_tq + '</div>' +
        '<div class="card-id_data"><i class="' + data + '"></i></div>' +
        '<div class="card-title">' + title + '</div>' +
        '<div class="card-title_en">' + title_en + '</div>' +
        '</div>' +
        '<div class="card-remove"><i class="fas fa-trash-alt"></i></div>' +
        '</div>' +
        '</div>' +
        '</div>';

    itemElem.innerHTML = itemTemplate;
    return itemElem.firstChild;
}

function getRandomItem2(collection) {

    return collection[Math.floor(Math.random() * collection.length)];

}

// https://stackoverflow.com/a/7228322
function getRandomInt2(min, max) {

    return Math.floor(Math.random() * (max - min + 1) + min);

}

function generateRandomWord2(length) {

    var ret = '';
    for (var i = 0; i < length; i++) {
        ret += getRandomItem2(characters2);
    }
    return ret;

}

function compareItemTitle2(a, b) {

    var aVal = a.getElement().getAttribute('data-title') || '';
    var bVal = b.getElement().getAttribute('data-title') || '';
    return aVal < bVal ? -1 : aVal > bVal ? 1 : 0;

}

function compareItemColor2(a, b) {

    var aVal = a.getElement().getAttribute('data-color') || '';
    var bVal = b.getElement().getAttribute('data-color') || '';
    return aVal < bVal ? -1 : aVal > bVal ? 1 : compareItemTitle2(a, b);

}

function updateIndices2() {

    grid2.getItems().forEach(function (item, i) {
        item.getElement().setAttribute('data-id', i + 1);
        item.getElement().querySelector('.card-id').innerHTML = i + 1;
    });

}

function elementMatches2(element, selector) {

    var p = Element.prototype;
    return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector).call(element, selector);

}

function elementClosest2(element, selector) {

    if (window.Element && !Element.prototype.closest) {
        var isMatch = elementMatches2(element, selector);
        while (!isMatch && element && element !== document) {
            element = element.parentNode;
            isMatch = element && element !== document && elementMatches2(element, selector);
        }
        return element && element !== document ? element : null;
    } else {
        return element.closest(selector);
    }

}

//
// Fire it up!
//

initDemo2();
var arr2 = [];
list_tienich(arr2);