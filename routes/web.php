<?php
//-< FRONTEND >--------------------------------------------------------------------------

// Home
Route::get('/', 'Frontend\HomeController@index');


// cho thue
Route::get('/thue/{slug}', 'Frontend\ProductController@detail_rent');
Route::get('/nha-dat-cho-thue', 'Frontend\ProductController@index_rent');
Route::get('/rent-category', 'Frontend\ChothueController@catall');
Route::get('/rent-category-grafting', 'Frontend\ChothueController@catallghep');

// ban
Route::get('/ban/{slug}', 'Frontend\ProductController@detail_sale');
Route::get('/nha-dat-ban', 'Frontend\ProductController@index_sale');
Route::post('/send_contact', 'Frontend\HomeController@send_contact');
Route::get('/lien-he', 'Frontend\HomeController@contact');
Route::get('/tim-nha-dat-ban', 'Frontend\ProductController@timnhadatban');
Route::get('/tim-nha-dat-thue', 'Frontend\ProductController@timnhadatthue');
Route::get('/tin-tuc', 'Frontend\NewController@index');
Route::get('/loai-tin-tuc/{slug}', 'Frontend\NewController@cat');
Route::get('/tin/{slug}', 'Frontend\NewController@detail');
//serch thue ban
Route::get('/search-thue', 'Frontend\HomeController@searchthue');
Route::get('/search-ban', 'Frontend\HomeController@searchban');

Route::get('/gioi-thieu', 'Frontend\GioithieuController@index');
Route::get('/gioi-thieu/{slug}', 'Frontend\GioithieuController@detail');
// can thue
Route::get('/list-your-property', 'Frontend\CanthueController@index');

//contact
Route::get('/contact', 'Frontend\ContactController@index');
Route::post('/add_contact', 'Frontend\ContactController@add_contact');
// add comment
Route::post('/add_comment', 'Frontend\CommentController@add_comment');


// nguoidungs
Route::get('/info-account', 'Frontend\NguoidungController@thongtintaikhoan');
Route::get('/create-account', 'Frontend\NguoidungController@taotaikhoan');
Route::get('/register-success', 'Frontend\NguoidungController@dangkythanhcong');
Route::get('/account_active/{active_token}', 'Frontend\NguoidungController@account_active');
Route::get('/dang-xuat', 'Frontend\NguoidungController@dangxuat');
Route::get('/change-pass', 'Frontend\NguoidungController@doimatkhau');
Route::get('/logout', 'Frontend\NguoidungController@logout');
Route::get('/forgot-password', 'Frontend\NguoidungController@forgotpassword');
Route::get('/forget/{token_forget}', 'Frontend\NguoidungController@changepass');

Route::post('/add_nguoidung', 'Frontend\NguoidungController@add_nguoidung');
Route::post('/dangnhap', 'Frontend\NguoidungController@dangnhap');
Route::post('/suataikhoan', 'Frontend\NguoidungController@suataikhoan');
Route::post('/suataikhoan', 'Frontend\NguoidungController@suataikhoan');
Route::post('/mail_forgot_pass', 'Frontend\NguoidungController@mail_forgot_pass');
Route::post('/set_pass', 'Frontend\NguoidungController@set_pass');
Route::post('/login_social', 'Frontend\NguoidungController@login_social');


// Demogid
Route::get('/store', 'Frontend\StoreController@index');
Route::get('/datatable_store', 'Frontend\StoreController@datatable_store');
Route::get('/datatable_store2', 'Frontend\StoreController@datatable_store2');

// Seach
Route::get('/search', 'Frontend\SearchController@index');

//-< FRONTEND >--------------------------------------------------------------------------


//-< BACKEND >--------------------------------------------------------------------------

Route::get('login', 'Backend\AdminController@login');
Route::group(['prefix' => 'admin', 'middleware' => 'AdminLogin'], function() {

    Route::post('/session', 'Backend\SessionController@index');

    Route::get('/logout', 'Backend\AdminController@logout');

    Route::get('/', 'Backend\AdminController@index');
    Route::get('{multi}', 'Backend\AdminController@index');

    $name_apps = config('general.name_apps');
    foreach ($name_apps as $key => $name) {
	    Route::get( $name.'/new', 'Backend\AdminController@index' );
    	Route::get( $name.'/edit/{multi}', 'Backend\AdminController@index' );
	}
});

// API -------------------------------------------------------------
Route::post('/api/adminusers/change_pass', 'Backend\api\AdminusersController@change_pass');
Route::group(['prefix' => 'api', 'middleware' => ['api']], function () {

    // Login
    Route::get('login', 'Backend\api\AuthController@login');
    Route::get('adminselect2s', 'Backend\api\Adminselect2sController@index');

    Route::post('adminusers', 'Backend\api\AdminusersController@change_pass');

    Route::get('save_session', 'Backend\api\AuthController@save_session');

    // stores
    Route::get('api/adminstores/search_datatable', 'AdminStoresController@search_datatable');
    Route::get('api/adminstores/search_datatable2', 'AdminStoresController@search_datatable2');
    Route::get('api/adminstores/show_tq', 'AdminStoresController@show_tq');
    Route::get('api/adminstores/get_stores', 'AdminStoresController@get_stores');
    Route::resource('api/adminstores', 'AdminStoresController');

    Route::post('admintables/gettable', 'Backend\api\AdmintablesController@gettable');

    $name_apps = config('general.name_apps');
    foreach ($name_apps as $key => $name) {
	    Route::resource('admin'.$name.'s', 'Backend\api\Admin'.$name.'sController');
	}
});

//-< BACKEND >--------------------------------------------------------------------------

// clear cache
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "view is cleared";
});


// DangtinController
Route::get('/dang-tin', 'DangtinController@index');
Route::get('/dang-tin-thue', 'DangtinController@dangtinthue');
Route::get('/sua-dang-tin/{slug}', 'DangtinController@edit');
Route::get('/lien-he-bds', 'DangtinController@lienhebds');

Route::get('/tao-dang-tin', 'DangtinController@create_project');
Route::get('/tao-dang-tin-b1/{id}', 'DangtinController@create_project_b1');
Route::get('/tao-dang-tin-b2/{id}', 'DangtinController@create_project_b2');
Route::get('/tao-dang-tin-b3/{id}', 'DangtinController@create_project_b3');

Route::get('/tao-tin-thue', 'DangtinController@create_item');
Route::get('/tao-tin-thue-b1/{id}', 'DangtinController@create_item_b1');
Route::get('/tao-tin-thue-b2/{id}', 'DangtinController@create_item_b2');
Route::get('/tao-tin-thue-b3/{id}', 'DangtinController@create_item_b3');

Route::get('/dang-tin-ban', 'DangtinController@dangtinban');
Route::get('/tao-tin-ban', 'DangtinController@create_item_ban');
Route::get('/tao-tin-ban-b1/{id}', 'DangtinController@create_item_ban_b1');
Route::get('/tao-tin-ban-b2/{id}', 'DangtinController@create_item_ban_b2');
Route::get('/tao-tin-ban-b3/{id}', 'DangtinController@create_item_ban_b3');


Route::get('/sua-tin-thue/{id}', 'DangtinController@edit_item');
Route::get('/thong-tin-nguoi-dung', 'DangtinController@profile');
Route::get('/get_tq_ti/{id}', 'DangtinController@get_tq_ti');
Route::get('/get_tq_ti_item/{id}', 'DangtinController@get_tq_ti_item');
Route::get('/du-an-yeu-thich', 'DangtinController@yeuthich');
Route::get('/bat-dong-san-yeu-thich', 'DangtinController@yeuthich2');
Route::get('/doi-mat-khau', 'DangtinController@changepass');

Route::post('/edit_profile', 'DangtinController@edit_profile');

Route::post('/api_search', 'DangtinController@api_search');
Route::post('/api_dangtin', 'DangtinController@api_dangtin');
Route::post('/api_crate_item', 'DangtinController@api_crate_item');
Route::post('/api_lienhebds', 'DangtinController@api_lienhebds');