<?php

namespace App\Exceptions;

class ShopCommon
{
    //-------------------------------------------------------------------------------
    public static function get_filename_from_url($sUrl, $delimiter = '/')
    {
        if (empty($sUrl)) {
            return false;
        }

        $arrTemp = explode($delimiter, $sUrl);
        return end($arrTemp);
    }

    //-------------------------------------------------------------------------------
    public static function getExtension($s) {
        $pos = strrpos($s, '.');

        $extension = null;

        if ($pos)
            $extension = substr($s, $pos);

        return $extension;
    }

    //-------------------------------------------------------------------------------
    public static function get_tree(&$tree, $list, $root) {
        foreach ($list as $node) {
            if ($node['parent_id'] == $root) {
                $node['childs'] = [];
                array_push($tree, $node);
                $i = count($tree) - 1;
                self::get_tree($tree[$i]['childs'], $list, $node['id']);
            }
        }
    }

    //-------------------------------------------------------------------------------
    public static function data_select2_tree(&$data, $tree, $level) {
        foreach ($tree as $node) {
            $node['level'] = $level;
            array_push($data, $node);

            if (!empty($node['childs']))
                self::data_select2_tree($data, $node['childs'], $level + 1);
        }
    }

    //-------------------------------------------------------------------------------
    public static function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //-------------------------------------------------------------------------------
    public static function generateOrderNO() {
        $str = '';
        $str = strtoupper(uniqid());
        return $str;
    }
}