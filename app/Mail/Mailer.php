<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Mailer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The forgetpass object instance.
     *
     * @var forgetpass
     */
    public $forgetpass;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($forgetpass)
    {
        $this->forgetpass = $forgetpass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('sender@example.com')
                    ->view('frontend.mails.forgetpass')
                    ->with(
                      [
                            'testVarOne' => '1',
                            'testVarTwo' => '2',
                      ]);
    }
}