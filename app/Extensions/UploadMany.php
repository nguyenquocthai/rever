<?php

namespace App\Extensions;

use Intervention\Image\ImageManagerStatic as Image;
use App\Extensions\ShopCommon;
use Illuminate\Http\Request;

class UploadMany
{
    //-------------------------------------------------------------------------------
    public static function upload($file, $link, $options = []) {

        if ( $file->extension('files') != 'jpeg' && $file->extension('files') != 'png' && $file->extension('files') != 'gif') {
            return [
                'code' => 300,
                'file_name' => $file->getClientOriginalName('files'),
                'size' => $file->getClientSize(),
                'error' => 'Incorrect image format.<b> jpeg - png - gif</b>'
            ];
        }

        $extension = ShopCommon::getExtension($file->getClientOriginalName('files'));

        $name_file = md5(gmdate('Y-m-d H:i:s') . $file->getClientOriginalName('files')) . $extension;

        if (!empty($options['file_name'])){
            $image_path = $link['path'] . $options['file_name'];
            if(file_exists($image_path)) {
                unlink($image_path);
            }
        }

        $image_resize = Image::make($file->getRealPath());
        if ($file->getClientSize() >= 2000000) {
            return [
                'code' => 300,
                'file_name' => $file->getClientOriginalName('files'),
                'size' => $file->getClientSize(),
                'error' => 'Image size is too large. <b>Up to 2MB</b>'
            ];
        }
        if ($file->getClientSize() >= 1500000) {
           $image_resize->resize($image_resize->width(), $image_resize->height());
        }

        $result = $image_resize->save($link['path'] . $name_file);

        if ($result) {
            return [
                'code' => 200,
                'file_name' => $name_file,
                'size' => $file->getClientSize(),
            ];

        } else {
            return false;
            /*return [
                'result' => 1,
                'string' => $option,
            ];*/
        }
    }
}