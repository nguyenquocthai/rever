<?php

namespace App\Extensions;

use Intervention\Image\ImageManagerStatic as Image;
use App\Extensions\ShopCommon;
use Illuminate\Http\Request;

class ShopUpload
{
    //-------------------------------------------------------------------------------
    public static function upload_image($option = null, $file_image)
    {
        $_this = new self;
        // upload avatar
        /*if($request->hasFile('files') != 1) {
            $data['code'] = 300;
            $data['msg'] = 'Hình bị lỗi';
            return response()->json($data, 200);
        }*/

        $results = [];
        $condition = null;

        $file_link['path'] = $option['path'];
        $file_link['url'] = $option['url'];

        if (!empty($option['file_name']))
            $condition['file_name'] = $option['file_name'];

        $upload = $_this->upload($file_image, $file_link, $condition);

        $image_name = $upload ? $upload['file_name'] : '';
        $image_size = $upload ? $upload['size'] : 0;

        $results['type'] = 1;
        $results['name'] = $image_name;
        $results['size'] = $image_size;

        return $results;
    }
    
    //-------------------------------------------------------------------------------
    public static function upload($file, $link, $options = []) {

        // $file->isValid('files') != 1
        if ( $file->extension('files') != 'jpeg' && $file->extension('files') != 'png' && $file->extension('files') != 'gif') {
            return false;
            /*return [
                'result' => 1,
                'string' => $option,
            ];*/
        }

        $extension = ShopCommon::getExtension($file->getClientOriginalName('files'));

        $name_file = md5(gmdate('Y-m-d H:i:s') . $file->getClientOriginalName('files')) . $extension;

        if (!empty($options['file_name'])){
            $image_path = $link['path'] . $options['file_name'];
            if(file_exists($image_path)) {
                unlink($image_path);
            }
        }

        // if (empty($options['file_name']))
                
        // else
        //     $name_file = $options['file_name'];

        $image_resize = Image::make($file->getRealPath());
        if ($file->getClientSize() >= 10000000) {
           return 'error';
        }
        if ($file->getClientSize() >= 8000000) {
           $image_resize->resize($image_resize->width(), $image_resize->height());
        }

        //$result = $file->move($link['path'], $name_file);
        $result = $image_resize->save($link['path'] . $name_file);

        if ($result) {
            return [
                'result' => 0,
                'file_name' => $name_file,
                'size' => $file->getClientSize(),
                //'file_name' => $link['url'] . $name_file,
            ];

        } else {
            return false;
            /*return [
                'result' => 1,
                'string' => $option,
            ];*/
        }
    }

    //-------------------------------------------------------------------------------
    public static function upload_file($file, $link, $options = []) {

        $extension = ShopCommon::getExtension($file->getClientOriginalName('files'));

        $name_file = md5(gmdate('Y-m-d H:i:s') . $file->getClientOriginalName('files')) . $extension;

        if (!empty($options['file_name'])){
            $image_path = $link['path'] . $options['file_name'];
            if(file_exists($image_path)) {
                unlink($image_path);
            }
        }

        // if (empty($options['file_name']))
                
        // else
        //     $name_file = $options['file_name'];


        $result = $file->move($link['path'], $name_file);

        if ($result) {
            return [
                'result' => 0,
                'file_name' => $name_file,
                // 'size' => $file->getClientSize(),
                //'file_name' => $link['url'] . $name_file,
            ];

        } else {
            return false;
            /*return [
                'result' => 1,
                'string' => $option,
            ];*/
        }
    }

    //-------------------------------------------------------------------------------
    public static function upload_avatar($file, $link, $options = []) {

        // $file->isValid('files') != 1
        if ( $file->extension('files') != 'jpeg' && $file->extension('files') != 'png' && $file->extension('files') != 'gif') {
            return false;
            /*return [
                'result' => 1,
                'string' => $option,
            ];*/
        }

        $extension = ShopCommon::getExtension($file->getClientOriginalName('files'));

        if (!empty($options['file_name'])){
            $image_path = $link['path'] . $options['file_name'];
            if(file_exists($image_path)) {
                unlink($image_path);
            }
        }
        // if (empty($options['file_name']))
        //         $name_file = md5(gmdate('Y-m-d H:i:s') . $file->getClientOriginalName('files')) . $extension;
        // else
        //     $name_file = $options['file_name'];

        $image_resize = Image::make($file->getRealPath());
        if ($file->getClientSize() >= 10000000) {
           return 'error';
        }
        if ($file->getClientSize() >= 8000000) {
           $image_resize->resize($image_resize->width(), $image_resize->height());
        }

        //$result = $file->move($link['path'], $name_file);
        $result = $image_resize->save($link['path'] . $name_file);

        if ($result) {
            return [
                'result' => 0,
                'file_name' => $name_file,
                'size' => $file->getClientSize(),
                //'file_name' => $link['url'] . $name_file,
            ];

        } else {
            return false;
            /*return [
                'result' => 1,
                'string' => $option,
            ];*/
        }
    }
}