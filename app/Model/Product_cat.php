<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product_cat extends Model
{
    protected $table = 'product_cats';
    public $timestamps = false;

    public static function validate() {
        return [
            'pattern' => [
                'title' =>'required',
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
            ],

            'customName' => [
                'title'=>'Tiêu đề',
            ]
        ];
    }
}
