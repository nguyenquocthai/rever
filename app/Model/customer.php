<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    protected $table = 'customers';
    //public $timestamps = false;

    //-------------------------------------------------------------------------------
    public static function validate($id) {
        return [
            'pattern' => [
                
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute không được trùng'
            ],

            'customName' => [
                
            ]
        ];
    }
}
