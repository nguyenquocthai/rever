<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class slider extends Model
{
    protected $table = 'sliders';
    //public $timestamps = false;

    //-------------------------------------------------------------------------------
    public static function validate($id=0) {
        return [
            'pattern' => [
                'title' =>'required|unique:sliders,title,' . $id . ',id,del_flg,0',
                
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute không được trùng'
            ],

            'customName' => [
                'title'=>'Tiêu đề',
                'slug'=>'Đường dẫn',
            ]
        ];
    }
}
