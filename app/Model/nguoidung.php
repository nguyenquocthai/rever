<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nguoidung extends Model
{
    protected $table = 'nguoidungs';
    //public $timestamps = false;

    public static function validate($id) {
        return [
            'pattern' => [
                'email' => 'required|unique:nguoidungs,email,' . $id . ',id,del_flg,0',
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique'=>':attribute không được trùng',
                'numeric'=>':attribute phải là số',
            ],

            'customName' => [
                'email'=>'Email',
            ]
        ];
    }
}
