<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemProject extends Model
{
    protected $table = 'item_projects';
    //public $timestamps = false;

    public function item_album() {
        return $this->hasMany('App\Model\ItemAlbum', 'item_id', 'id');
    }


    //-------------------------------------------------------------------------------
    public static function validate($id) {
        return [
            'pattern' => [
                'title' =>'required',
                'slug' => 'unique:item_projects,slug,' . $id . ',id,del_flg,0',
                'map' =>'required',
                'category_id' =>'required',
                'short_content' =>'required'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute không được trùng'
            ],

            'customName' => [
                'title'=>'Tiêu đề',
                'slug'=> 'Tiêu đề',
                'map' =>'Bản đồ',
                'category_id' =>'Danh mục dự án',
                'short_content' =>'Nội dung tin đăng'
            ]
        ];
    }
}
