<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MNew extends Model
{
    protected $table = 'news';
    public $timestamps = false;

    public static function validate($id) {
        return [
            'pattern' => [
                'title' =>'required',
                 'slug' => 'required|unique:news,slug,' . $id . ',id,del_flg,0',
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique'=>':attribute không được trùng',
            ],

            'customName' => [
                'title'=>'Tiêu đề',
                'slug'=>'Tiêu đề',
            ]
        ];
    }
}
