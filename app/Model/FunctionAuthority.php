<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FunctionAuthority extends Model
{
    protected $table = 'function_authorities';
    //public $timestamps = true;

    //-------------------------------------------------------------------------------
    public static function validate() {
        return [
            'pattern' => [
                'name' =>'required',
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
            ],

            'customName' => [
                'name'=>'Tên',
            ]
        ];
    }
}
