<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model;
use App\Model\FunctionAuthority;
use Illuminate\Support\Facades\DB;

class AdminController extends BaseAdminController
{
    public function index () {
        if (Auth::user()->list_rule != null)
            $list_rules = json_decode(Auth::user()->list_rule, true);
        else
            $list_rules = [];

        $list_url = [];
        foreach($list_rules as $list_rule) {
            $function = FunctionAuthority::where([
                ['id', '=', $list_rule['id']],
            ])->first();

            if($function) {
                $list_url[] = $function->role_json;
            }
        }
        $role = Auth::user()->role;

        $count_customer = DB::table('customers')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['trang_thai', '=', 0]
            ])
            ->count();
        
        return view('Backend/Admin/index')->with(compact('list_url', 'role', 'count_customer'));
    }

    public function login (Request $request) {

        if (Auth::check()) {
            $nguoidung = DB::table('nguoidungs')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id_admin', '=', Auth::user()->id]
                ])
                ->first();

            session(['userSession'=>$nguoidung]);

            return redirect('admin');
        }
        return view('Backend/Admin/login');
    }

    public function logout (Request $request) {
        Auth::logout();
        session()->forget('admin');
        session()->forget('admin_style');
        session()->forget('userSession');
        return redirect('/admin');
    }

}
