<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\User;
use Hash;

class AuthController extends BaseAdminController
{    
    public function login(Request $request) {
        
        $username = $request->username;
        $password = $request->password;

        $user = User::where([
            ['username', '=', $request->username],
            //['domain_id', '=', Auth::guard('api')->id()],
            ['del_flg', '=', 0]
        ])->first();


        if (!$user) {
            $data['code'] = 300;
            $data['error'] = 'Sai tên đăng nhập hoặc mật khẩu';
            return response()->json($data, 200);
        }

        if (password_verify($request->password, $user->password)) {
            $data['code'] = 200;
            $data['data'] = $user;
            $data['msg'] = 'Đăng nhập thành công';
            return response()->json($data, 200);
        } else {
            $data['code'] = 300;
            $data['error'] = 'Sai tên đăng nhập hoặc mật khẩux';
            return response()->json($data, 200);
        }
    }

    public function save_session(Request $request) {
        $username = $request->username;
        $password = $request->password;
        Auth::attempt(['username' => $username, 'password' => $password]);
        $data['code'] = 200;
        if ($username == 'admin' || $username == 'quantri') {
            session()->put('admin', 'ready');
        }
        if ($username == 'admin_style') {
            session()->put('admin_style', 'ready');
        }
        return response()->json($data, 200);
    }
}
