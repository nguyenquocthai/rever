<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\slider;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminslidersController extends BaseAdminController
{

    public function index (Request $request) {

        $sliders = DB::table('sliders')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($sliders as $key => $slider) {
            $vitri = $key+1;
            $row = $this->GetRow($slider, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = slider::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'slider');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = slider::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'slider', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $slider = DB::table('sliders')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$slider) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            slider::where([
                ['id', $slider->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.slider_path').$slider->avatar;
            //rename($image_path, config('general.slider_path')."del_".$slider->avatar);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($slider, $vitri)
    {
        $row = [];
        $row[] = $slider->id;
        $row[] = $slider->position;
        $row[] = $vitri;
        $row[] = $slider->title;
        $row[] = $this->GetImg([
            'avatar'=> $slider->avatar,
            'data'=> 'slider',
            'time'=> $slider->updated_at
        ]);
        $row[] = '<span class="hidden">'.$slider->updated_at.'</span>'.date('d/m/Y', strtotime($slider->updated_at));
        $view = View::make('Backend/slider/_status', ['status' => $slider->status]);
        $row[] = $view->render();
        $view = View::make('Backend/slider/_actions', ['id' => $slider->id,'page' => 'slider']);
        $row[] = $view->render();

        return $row;
    }
}
