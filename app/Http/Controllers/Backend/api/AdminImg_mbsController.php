<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use View;
use App\Model\Img_mb;
use App\Exceptions\ErrorCodes;
use App\Extensions\ShopCommon;
use App\Extensions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminImg_mbsController extends BaseAdminController
{

    public function search_datatable () {

        $img_mbs = Img_mb::where('del_flg', 0)->get()->toJson();
        $img_mbs = json_decode($img_mbs, true);

        $output = [];
        foreach ($img_mbs as $img_mb) {
            $row = [];
            $row[] = $img_mb['id'];
            
            $avatar = config('general.shop_img_mb_url') . $img_mb['avatar'];
            $view = View::make('/backend/adminimg_mb/_image', ['avatar' => $avatar]);
            $row[] = $view->render();

            $row[] = $img_mb['name'];
            $row[] = date('d/m/Y', strtotime($img_mb['created_at']));

            $view = View::make('/backend/adminimg_mb/_actions', ['data' => $img_mb]);
            $row[] = $view->render();

            $output[] = $row;
        }

        $this->resp(ErrorCodes::E_OK, null, $output);
        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {
            // Create folder
            if ( !file_exists(config('general.img_mb_path')) )
                mkdir(config('general.img_mb_path'), config('permission_folder'), true);

            $files = $request->file('files');
            $avatar = $files[0];
            $avatar_link['path'] = config('general.img_mb_path');
            $avatar_link['url'] = config('general.img_mb_url');
            $upload = ShopUpload::upload($avatar, $avatar_link);
            $file_name = $upload ? $upload['file_name'] : '';

            // save
            $db_img_mbs = new Img_mb;
            $db_img_mbs->id_product = $request->id_product;
            $db_img_mbs->image = $file_name;
            $db_img_mbs->save();

            $view = View::make('/backend/adminimg_mb/_addimage', ['img_mb' => $db_img_mbs]);
            $img_mb = $view->render();

        } catch (Exception $e) {
            $this->exception($e);
            $this->response();
        }
        $this->resp(ErrorCodes::E_OK, 'Lưu thành công', $img_mb);
        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {
            // check $id isset in database
            $img_mb = Img_mb::where('id', '=', $id)->first();

            if (!$img_mb) {
                $this->resp(ErrorCodes::E_VALIDATION_ERROR_FIELD, 'Lưu thất bại');
               $this->response();
            }

            $file_name = $img_mb['avatar'];

            // Create folder
            if ( !file_exists(config('general.shop_img_mb_path')) )
                mkdir(config('general.shop_img_mb_path'), config('permission_folder'), true);

            // upload avatar
            if($request->hasFile('files') == 1) {

                $files = $request->file('files');
                $avatar = $files[0];
                $avatar_link['path'] = config('general.shop_img_mb_path');
                $avatar_link['url'] = config('general.shop_img_mb_url');
                $options['file_name'] = $file_name;

                $upload = ShopUpload::upload($avatar, $avatar_link, $options);

                $file_name = $upload ? $upload['file_name'] : $img_mb['avatar'];

            }

            // save
            $shop_img_mbs = Img_mb::find($id);

            $shop_img_mbs->name = $request->name;
            $shop_img_mbs->avatar = $file_name;
            $shop_img_mbs->link = $request->link;
            $shop_img_mbs->content = $request->content;
            $shop_img_mbs->del_flg = 0;

            $shop_img_mbs->save();

        } catch (Exception $e) {
            $this->exception($e);
            $this->response();
        }

        $this->resp(ErrorCodes::E_OK, 'Cập nhật thành công', $shop_img_mbs);
        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {
            $img_mbs = DB::table('img_mbs')
                ->select('*')
                ->where('id_product', '=', $id)
                ->orderBy('id', 'desc')
                ->get();

            $view = View::make('/backend/adminimg_mb/_image', ['img_mbs' => $img_mbs]);
            $img_mb = $view->render();

            $this->resp(ErrorCodes::E_OK, null, $img_mb);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {
            $img_mb_find = Img_mb::where([
                ['id', $id]
            ])->first();

            $image_path = config('general.img_mb_path').$img_mb_find->image;
            $img_mb = Img_mb::find($id);
            $img_mb->delete();

            if(file_exists($image_path)) {
                unlink($image_path);
            }

        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }

}
