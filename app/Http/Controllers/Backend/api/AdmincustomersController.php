<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\customer;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdmincustomersController extends BaseAdminController
{

    public function index (Request $request) {

        $where = [
            ['customers.del_flg', '=', 0]
        ];

        if ($request->id_group) {
            $where[] = ['customers.id_group', '=', $request->id_group];
        }

        $customers = DB::table('customers')
            ->select('*')
            ->where($where)
            ->orderBy('id', 'desc')
            ->get();

        $output = [];
        foreach ($customers as $key => $customer) {
            $vitri = $key+1;
            $row = $this->GetRow($customer, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = customer::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'customer');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            $tables = 'customers';
            $db_table = DB::table($tables)
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', $id]
            ])
            ->first();

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = customer::validate($id);
                $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }else if ($request->status_code == "edit_b1") {

                $update_values = [];
                

                $update_values['name_contact'] = $request->name_contact;
                $update_values['phone_contact'] = $request->phone_contact;
                $update_values['email_contact'] = $request->email_contact;
                $update_values['trang_thai'] = $request->trang_thai;
                $update_values['comment'] = $request->comment;
                
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'customers', $id);
                //$edit_db = $this->EditDB($request->all(),'customer', $id);

                
                
                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            if ($request['status_code'] == "change_status") {

                if ($db_table->trang_thai == 0) {
                    $trang_thai = 1;
                } else {
                    $trang_thai = 0;
                }

                DB::table($tables)
                    ->where('id', $id)
                    ->update([
                        'trang_thai' => $trang_thai,
                        'updated_at' => date("Y-m-d H:i:s")
                    ]);

                $view = View::make('Backend/customer/_status', ['trang_thai' => $trang_thai]);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                $data['trang_thai'] = $view->render();
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'customer', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }
            

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
        
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $customer = DB::table('customers')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$customer) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            customer::where([
                ['id', $customer->id]
            ])->update(['del_flg' => 1]);

            if (isset($customer->avatar)) {
                $image_path = config('general.customer_path').$customer->avatar;
                rename($image_path, config('general.customer_path')."del_".$customer->avatar);
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($customer, $vitri)
    {
        $row = [];
        $row[] = $customer->id;
        $row[] = $customer->name_contact;
        $row[] = $customer->email_contact;
        $row[] = $customer->phone_contact;
        $row[] = $customer->created_at;
        $view = View::make('Backend/customer/_status', ['trang_thai' => $customer->trang_thai]);
        $row[] = $view->render();
        $view = View::make('Backend/customer/_actions', ['id' => $customer->id,'page' => 'customer']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $customer = customer::findOrFail($id);
            
            $item =  DB::table('item_projects')
                    ->where('id', $customer->id_itemproject)
                    ->first();

            

            $customer->slugi = @$item->slug;
            $customer->titlei = @$item->title;

            $this->resp(ErrorCodes::E_OK, null, $customer);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
