<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\select2;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminselect2sController extends BaseAdminController
{
    public function index (Request $request) {

        $table = $request->name;
        $limit = 50;
        $where = [
            ['del_flg', '=', 0],
            ['status', '=', 0]
        ];
        
        if ($request->where) {
            foreach ($request->where as $key => $value) {
                $where[] = explode(',', $value);
            }
        }

        if ($request->limit) {
            $limit = $request->limit;
        }

        $output = DB::table($table)
            ->select('*')
            ->where($where)
            ->offset(0)
            ->limit($limit)
            ->orderBy('position', 'desc')
            ->get();

        if ($request->title) {
            foreach ($output as $key => $value) {
                $output[$key]->title = $value->{$request->title};
            }
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }
}
