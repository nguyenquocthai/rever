<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\ShopUser;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminusersController extends BaseAdminController
{

    public function search_datatable (Request $request) {

        $users = ShopUser::where([
            ['del_flg', 0],
            //['domain_id', Auth::guard('api')->id()]
        ])->get();
        $users = json_decode($users, true);

        $output = [];
        foreach ($users as $user) {
            $row = [];

            $row[] = $user['fullname'];
            $row[] = $user['email'];

            $view = View::make('Backend/Adminuser/_status', ['data' => $user]);
            $row[] = $view->render();

            $row[] = $user['count_log'];

            $row[] = '<span class="hidden">'.$user['updated_at'].'</span>'.date("d/m/Y", strtotime($user['updated_at']));

            $view = View::make('Backend/Adminuser/_actions', ['data' => $user]);
            $row[] = $view->render();

            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // save
            $shop_user = new ShopUser;
            $shop_user->status = $request->status;
            $shop_user->password_main = $request->password_main;
            $shop_user->confirmPassword = $request->confirmPassword;
            $shop_user->province_id = $request->province_id;
            $shop_user->district_id = $request->district_id;
            $shop_user->fullname = $request->fullname;
            $shop_user->phone = $request->phone;
            $shop_user->gender = $request->gender;
            $shop_user->address = $request->address;
            $shop_user->email = $request->email;
            $shop_user->del_flg = 0;
            $shop_user->save();

            $data['code'] = 200;
            $data['message'] = 'Lưu thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'show') {
                $shop_user = DB::table('shop_users')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['status', '=', 0],
                        ['id', '=', $id]
                    ])
                    ->first();

                if ($shop_user) {
                    if ($shop_user->product_view == null) {
                        $shop_user->product_view = [];
                    } else {
                        $shop_user->product_view = json_decode($shop_user->product_view);
                    }
                    $shop_products = DB::table('shop_products')
                        ->select('*')
                        ->where([
                            ['del_flg', '=', 0],
                            ['status', '=', 0],
                        ])
                        ->whereIn('id', $shop_user->product_view)
                        ->get();

                    $view = View::make('Backend/Adminuser/_show', ['shop_user' => $shop_user, 'shop_products' => $shop_products]);
                    $output = $view->render();

                    $data['code'] = 200;
                    $data['data'] = $output;
                    return response()->json($data, 200);

                } else {
                    $data['code'] = 300;
                    $data['error'] = 'Lỗi kết nối';
                    return response()->json($data, 200);
                }
            }

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function change_pass(Request $request)
    {
        try {

            $find_user = DB::table('users')
                ->select('id','password')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', @Auth::user()->id],
                    ['remember_token', '=', @Auth::user()->remember_token],
                ])
                ->first();
            $error = [];

            if ($request->password_old == "") {
                $error[] = 'Chưa nhập mật khẩu củ.';
            } elseif (!Hash::check($request->password_old, $find_user->password)) {
                $error[] = 'Sai mật khẩu củ.';
            }

            if ($request->password_new == "") {
                $error[] = 'Chưa nhập mật khẩu mới.';
            } elseif (strlen($request->password_new) < 8) {
                $error[] = 'Mật khẩu mới phải nhiều hơn 8 ký tự.';
            }

            if (count($error) != 0) {
                $data_error = "";
                foreach ($error as $key => $value) {
                    $data_error .= "<div>".$value."</div>";
                }
                $data['code'] = 300;
                $data['error'] = $data_error;
                return response()->json($data, 200);
            }

            if ($find_user) {
                DB::table('users')
                    ->where('id', $find_user->id)
                    ->update(['password' => bcrypt($request->password_new)]);

                $data['code'] = 200;
                $data['message'] = 'Đổi mật khẩu thành công.';
                return response()->json($data, 200);
            } else {
                $data['code'] = 300;
                $data['error'] = 'Sai thông tin.';
                return response()->json($data, 200);
            }

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {

    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {
            $user = ShopUser::where('id', $id)->update(['del_flg' => 1]);
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

}