<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\location;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminlocationsController extends BaseAdminController
{

    public function index (Request $request) {

        $locations = DB::table('locations')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($locations as $key => $location) {
            $vitri = $key+1;
            $row = $this->GetRow($location, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = location::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'location');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = location::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'location', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $location = DB::table('locations')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$location) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            location::where([
                ['id', $location->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.location_path').$location->avatar;
            rename($image_path, config('general.location_path')."del_".$location->avatar);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($location, $vitri)
    {
        $row = [];
        $row[] = $location->id;
        $row[] = $location->position;
        $row[] = $vitri;
        $row[] = $location->title;
        $row[] = $this->GetImg([
            'avatar'=> $location->avatar,
            'data'=> 'location',
            'time'=> $location->updated_at
        ]);
        $row[] = '<span class="hidden">'.$location->updated_at.'</span>'.date('d/m/Y', strtotime($location->updated_at));
        $view = View::make('Backend/location/_status', ['status' => $location->status]);
        $row[] = $view->render();
        $view = View::make('Backend/location/_actions', ['id' => $location->id,'page' => 'location']);
        $row[] = $view->render();

        return $row;
    }
}
