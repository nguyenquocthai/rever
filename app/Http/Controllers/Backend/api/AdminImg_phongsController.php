<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\Img_phong;
use App\Exceptions\ErrorCodes;
use App\Extensions\ShopCommon;
use App\Extensions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminImg_phongsController extends BaseAdminController
{

    public function search_datatable () {

        $img_phongs = Img_phong::where('del_flg', 0)->get()->toJson();
        $img_phongs = json_decode($img_phongs, true);

        $output = [];
        foreach ($img_phongs as $img_phong) {
            $row = [];
            $row[] = $img_phong['id'];
            
            $avatar = config('general.shop_img_phong_url') . $img_phong['avatar'];
            $view = View::make('/backend/adminimg_phong/_image', ['avatar' => $avatar]);
            $row[] = $view->render();

            $row[] = $img_phong['name'];
            $row[] = date('d/m/Y', strtotime($img_phong['created_at']));

            $view = View::make('/backend/adminimg_phong/_actions', ['data' => $img_phong]);
            $row[] = $view->render();

            $output[] = $row;
        }

        $this->resp(ErrorCodes::E_OK, null, $output);
        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {
            // Create folder
            if ( !file_exists(config('general.img_phong_path')) )
                mkdir(config('general.img_phong_path'), config('permission_folder'), true);

            $files = $request->file('files');
            $avatar = $files[0];
            $avatar_link['path'] = config('general.img_phong_path');
            $avatar_link['url'] = config('general.img_phong_url');
            $upload = ShopUpload::upload($avatar, $avatar_link);
            $file_name = $upload ? $upload['file_name'] : '';

            // save
            $db_img_phongs = new Img_phong;
            $db_img_phongs->id_product1 = $request->id_product1;
            $db_img_phongs->image = $file_name;
            $db_img_phongs->save();

            $view = View::make('/backend/adminimg_phong/_addimage', ['img_phong' => $db_img_phongs]);
            $img_phong = $view->render();

        } catch (Exception $e) {
            $this->exception($e);
            $this->response();
        }
        $this->resp(ErrorCodes::E_OK, 'Lưu thành công', $img_phong);
        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {
            // check $id isset in database
            $img_phong = Img_phong::where('id', '=', $id)->first();

            if (!$img_phong) {
                $this->resp(ErrorCodes::E_VALIDATION_ERROR_FIELD, 'Lưu thất bại');
               $this->response();
            }

            $file_name = $img_phong['avatar'];

            // Create folder
            if ( !file_exists(config('general.shop_img_phong_path')) )
                mkdir(config('general.shop_img_phong_path'), config('permission_folder'), true);

            // upload avatar
            if($request->hasFile('files') == 1) {

                $files = $request->file('files');
                $avatar = $files[0];
                $avatar_link['path'] = config('general.shop_img_phong_path');
                $avatar_link['url'] = config('general.shop_img_phong_url');
                $options['file_name'] = $file_name;

                $upload = ShopUpload::upload($avatar, $avatar_link, $options);

                $file_name = $upload ? $upload['file_name'] : $img_phong['avatar'];

            }

            // save
            $shop_img_phongs = Img_phong::find($id);

            $shop_img_phongs->name = $request->name;
            $shop_img_phongs->avatar = $file_name;
            $shop_img_phongs->link = $request->link;
            $shop_img_phongs->content = $request->content;
            $shop_img_phongs->del_flg = 0;

            $shop_img_phongs->save();

        } catch (Exception $e) {
            $this->exception($e);
            $this->response();
        }

        $this->resp(ErrorCodes::E_OK, 'Cập nhật thành công', $shop_img_phongs);
        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {
            $img_phongs = DB::table('img_phongs')
                ->select('*')
                ->where('id_product1', '=', $id)
                ->orderBy('id', 'desc')
                ->get();

            $view = View::make('/backend/adminimg_phong/_image', ['img_phongs' => $img_phongs]);
            $img_phong = $view->render();

            $this->resp(ErrorCodes::E_OK, null, $img_phong);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {
            $img_phong_find = Img_phong::where([
                ['id', $id]
            ])->first();

            $image_path = config('general.img_phong_path').$img_phong_find->image;
            $img_phong = Img_phong::find($id);
            $img_phong->delete();

            if(file_exists($image_path)) {
                unlink($image_path);
            }

        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }

}
