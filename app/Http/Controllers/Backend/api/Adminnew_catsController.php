<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\New_cat;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminnew_catsController extends BaseAdminController
{

    public function index (Request $request) {

        $new_cats = DB::table('new_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($new_cats as $key => $new_cat) {
            $vitri = $key+1;
            $row = $this->GetRow($new_cat, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = New_cat::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'new_cat');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = New_cat::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                

                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['short_content'] = $request->short_content;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $this->DB_update($update_values, 'new_cats', $id);
                $edit_db = $this->EditDB($request->all(),'new_cat', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'new_cat', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $new_cat = DB::table('new_cats')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$new_cat) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            New_cat::where([
                ['id', $new_cat->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.new_cat_path').$new_cat->avatar;
            
            if(file_exists($image_path)) {
                unlink($image_path);
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($new_cat, $vitri)
    {
        $row = [];
        $row[] = $new_cat->id;
        $row[] = $new_cat->position;
        $row[] = $vitri;
        $row[] = $new_cat->title;
        $row[] = $this->GetImg([
            'avatar'=> $new_cat->avatar,
            'data'=> 'new_cat',
            'time'=> $new_cat->updated_at
        ]);
        $row[] = '<span class="hidden">'.$new_cat->updated_at.'</span>'.date('d/m/Y', strtotime($new_cat->updated_at));
        $view = View::make('Backend/new_cat/_status', ['status' => $new_cat->status]);
        $row[] = $view->render();
        $view = View::make('Backend/new_cat/_actions', ['id' => $new_cat->id,'page' => 'new_cat']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $new_cat = New_cat::findOrFail($id);
            $new_cat['avatar'] = config('general.new_cat_url') . $new_cat['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $new_cat);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
