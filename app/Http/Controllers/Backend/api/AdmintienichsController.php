<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\store;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdmintienichsController extends BaseAdminController
{

    public function index (Request $request) {

        $stores = DB::table('stores')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id_code', '=', 1]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($stores as $key => $store) {
            $vitri = $key+1;
            $row = $this->GetRow($store, $vitri);
            $output[] = $row;
        }

        $view = View::make('Backend/store/_icons', []);

        $data['code'] = 200;
        $data['data'] = $output;
        $data['icons'] = $view->render();
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = store::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'store');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = store::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $where = [
                ['id_code', '=', 1]
            ];

            $edit_db = $this->EditDB($request->all(),'store', $id, $where);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $store = DB::table('stores')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$store) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            store::where([
                ['id', $store->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.store_path').$store->avatar;
            rename($image_path, config('general.store_path')."del_".$store->avatar);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($store, $vitri)
    {
        $row = [];
        $row[] = $store->id;
        $row[] = $store->position;
        $row[] = $vitri;
        $row[] = $store->title;
        $row[] = $store->value;
        $row[] = '<i class="'.$store->data.'"></i>';
        $row[] = '<span class="hidden">'.$store->updated_at.'</span>'.date('d/m/Y', strtotime($store->updated_at));
        $view = View::make('Backend/tienich/_status', ['status' => $store->status]);
        $row[] = $view->render();
        $view = View::make('Backend/tienich/_actions', ['id' => $store->id,'page' => 'store']);
        $row[] = $view->render();

        return $row;
    }
}
