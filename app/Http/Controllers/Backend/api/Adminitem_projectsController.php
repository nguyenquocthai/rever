<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\item_project;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminitem_projectsController extends BaseAdminController
{

    public function index (Request $request) {

        $where = [
            ['del_flg', '=', 0]
        ];

        if ($request->type != '') {
            $where[] = ['type', '=', $request->type];
        }

        $item_projects = DB::table('item_projects')
            ->select('*')
            ->where($where)
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($item_projects as $key => $item_project) {
            $vitri = $key+1;
            $row = $this->GetRow($item_project, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = item_project::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'item_project');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = item_project::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'item_project', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $item_project = DB::table('item_projects')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$item_project) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            item_project::where([
                ['id', $item_project->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.item_project_path').$item_project->avatar;
            
            if(file_exists($image_path)) {
                unlink($image_path);
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($item_project, $vitri)
    {
        $row = [];
        $row[] = $item_project->id;
        $row[] = '<span class="hidden">'.$item_project->updated_at.'</span>'.date('d/m/Y H:i:s', strtotime($item_project->updated_at));
        $row[] = $item_project->title;
        $row[] = $this->GetImg([
            'avatar'=> $item_project->avatar,
            'data'=> 'item_project',
            'time'=> $item_project->updated_at
        ]);
        $view = View::make('Backend/item_project/_status', ['status' => $item_project->status]);
        $row[] = $view->render();
        $view = View::make('Backend/item_project/_actions', ['id' => $item_project->id,'page' => 'item_project']);
        $row[] = $view->render();

        return $row;
    }

    public function show($id)
    {
        try {
            $item_project = item_project::findOrFail($id);
            $item_project['avatar'] = config('general.item_project_url') . $item_project['avatar'];

            $item_project->thongtin = json_decode($item_project->thongtin);

            $medias = DB::table('item_albums')
                ->select('*')
                ->where([
                    ['item_id', '=', $id],
                ])
                ->orderBy('id', 'desc')
                ->get();

            $view_medias = View::make('/Backend/item_project/_media', ['medias' => $medias]);

            $tongquans = DB::table('tongquans')
                ->select('*')
                ->where([
                    ['id_product', '=', $id],
                    ['id_group', '=', 4]
                ])
                ->orderBy('vitri', 'asc')
                ->get();

            $tienichs = DB::table('tienichs')
                ->select('*')
                ->where([
                    ['id_product', '=', $id],
                    ['id_group', '=', 4]
                ])
                ->orderBy('vitri', 'asc')
                ->get();
            
            $item_project['tongquans'] = $tongquans;
            $item_project['tienichs'] = $tienichs;
            $item_project['medias'] = $view_medias->render();

            $this->resp(ErrorCodes::E_OK, null, $item_project);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
