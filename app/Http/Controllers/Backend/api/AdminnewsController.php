<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\MNew;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminnewsController extends BaseAdminController
{

    public function index (Request $request) {

        $news = DB::table('news')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($news as $key => $new) {
            $vitri = $key+1;
            $row = $this->GetRow($new, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = MNew::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'new');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = MNew::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                

                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['short_content'] = $request->short_content;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $this->DB_update($update_values, 'news', $id);
                $edit_db = $this->EditDB($request->all(),'new', $id);

                
        
                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'new', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $new = DB::table('news')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$new) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            MNew::where([
                ['id', $new->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.new_path').$new->avatar;
            
            if(file_exists($image_path)) {
                unlink($image_path);
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($new, $vitri)
    {
        $row = [];
        $row[] = $new->id;
        $row[] = $new->position;
        $row[] = $vitri;
        $row[] = $new->title;
        $row[] = $this->GetImg([
            'avatar'=> $new->avatar,
            'data'=> 'new',
            'time'=> $new->updated_at
        ]);
        $row[] = '<span class="hidden">'.$new->updated_at.'</span>'.date('d/m/Y', strtotime($new->updated_at));
        $view = View::make('Backend/new/_status', ['status' => $new->status]);
        $row[] = $view->render();
        $view = View::make('Backend/new/_actions', ['id' => $new->id,'page' => 'new']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $new = MNew::findOrFail($id);
            $new['avatar'] = config('general.new_url') . $new['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $new);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
