<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Http\Request;
use View;
use App\Model\setting;
use App\Exceptions\ErrorCodes;
use App\Extensions\ShopCommon;
use App\Extensions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminsettingsController extends BaseAdminController
{

    public function index(Request $request)
    {
        if ($request->status_code == "count") {
            
                $thue = DB::table('item_projects')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['type', '=', 0],
                    ])
                    ->count();

                $ban = DB::table('item_projects')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['type', '=', 1],
                    ])
                    ->count();
                $tintuc = DB::table('news')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                    ])
                    ->count();

                $lienhe = DB::table('customers')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                    ])
                    ->count();

                $data['tintuc'] = $tintuc;
                $data['thue'] = $thue;
                $data['ban'] = $ban;
                $data['lienhe'] = $lienhe;

                $this->resp(ErrorCodes::E_OK, null, $data);
                $this->response();
        }
        try {
           // $setting = setting::all();
            $setting = setting::where('key', '=', 'info_website')->first();

            //$setting['avatar'] = config('general.setting_url') . $setting['avatar'];
            if (empty($setting)) {
                $setting = [
                    'id' => '0',
                    'key' => 'info_website',
                    'site_id' => 0,
                    'info_website' => [],
                ];
            } else {
                $convert_json = json_decode($setting, true);
                $convert_value_json = json_decode($convert_json['value'], true);

                $result['setting']['info_website'] = $convert_value_json;
                $result['setting']['id'] = $convert_json['id'];
                $setting = $result['setting'];

            }

            $this->resp(ErrorCodes::E_OK, null, $setting);
            
        } catch (Exception $e) {
            $this->exception($e);
        }
        $this->response();
    }

    public function update(Request $request, $id)
    {
        try {
            
            // check $id isset in database
            $setting = setting::where('id', '=', $id)->first();

            if ($setting) {
                $setting = json_decode($setting, true);
                $setting_value = json_decode($setting['value'], true);
            }

            $file_name_logo = @$setting_value['logo'];
            $file_name_logo_white = @$setting_value['logo_white'];
            $file_name_favicon = @$setting_value['favicon'];

            // Create folder
            if ( !file_exists(config('general.setting_path')) )
                mkdir(config('general.setting_path'), config('permission_folder'), true);

            if (isset($request->logo)) {
                $avatar = $request->logo;
                $avatar_link['path'] = config('general.setting_path');
                $avatar_link['url'] = config('general.setting_url');
                $options['file_name'] = $file_name_logo;

                $upload = ShopUpload::upload_file($avatar, $avatar_link, $options);
                $file_name_logo = $upload ? $upload['file_name'] : @$setting_value['logo'];
            }

            if (isset($request->logo_white)) {
                $avatar = $request->logo_white;
                $avatar_link['path'] = config('general.setting_path');
                $avatar_link['url'] = config('general.setting_url');
                $options['file_name'] = $file_name_logo_white;

                $upload = ShopUpload::upload_file($avatar, $avatar_link, $options);
                $file_name_logo_white = $upload ? $upload['file_name'] : @$setting_value['logo_white'];
            }

            if (isset($request->favicon)) {
                $avatar = $request->favicon;
                $avatar_link['path'] = config('general.setting_path');
                $avatar_link['url'] = config('general.setting_url');
                $options['file_name'] = $file_name_favicon;

                $upload = ShopUpload::upload_file($avatar, $avatar_link, $options);
                $file_name_favicon = $upload ? $upload['file_name'] : @$setting_value['favicon'];
            }

            // get all value
            $alldata = $request->all();

            // remove data
            unset($alldata['logo']);
            unset($alldata['logo_white']);
            unset($alldata['favicon']);

            // add new name avatar logo
            $alldata['logo'] = $file_name_logo;

            // add new name avatar logo_white
            $alldata['logo_white'] = $file_name_logo_white;

            // add new name avatar logo_white
            $alldata['favicon'] = $file_name_favicon;

            // Converting $alldata to $jsondata
            $jsondata = json_encode($alldata, true);

            if($id != 0){
                // save
                $settings = setting::find($id);
                $settings->key = 'info_website';
                $settings->value = $jsondata;
                $settings->google_analytics = $request->google_analytics;
                $settings->facebook_code = $request->facebook_code;
                $settings->save();
            }else{
                // save
                $settings = new setting;

                $settings->key = 'info_website';
                $settings->value = $jsondata;
                $settings->google_analytics = $request->google_analytics;
                $settings->facebook_code = $request->facebook_code;

                $settings->save();
            }

            $result['id'] = $settings->id;
            $result['logo'] = $file_name_logo;
            $result['favicon'] = $file_name_favicon;

            $data['code'] = 200;
            $data['message'] = 'Cập nhật thành công';
            $data['data'] = $result;
            return response()->json($data, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

}
