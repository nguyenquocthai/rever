<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\User;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;

class AdminauthoritiesController extends BaseAdminController
{

    public function search_datatable (Request $request) {

        $data = $request->all();
        $users = User::where([
            ['del_flg', 0],
            ['role', 0],
        ])->get();
        $users = json_decode($users, true);

        $output = [];
        foreach ($users as $user) {
            $row = [];

            $row[] = $user['fullname'];
            $row[] = $user['office'];
            $row[] = '<span class="hidden">'.$user['updated_at'].'</span>'.date('d/m/Y', strtotime($user['updated_at']));

            $view = View::make('Backend/Adminauthorities/_status', ['status' => $user['status']]);
            $row[] = $view->render();

            $view = View::make('Backend/Adminauthorities/_actions', ['id' => $user['id']]);
            $row[] = $view->render();

            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {
            // validate
            $error_validate = User::validate();
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            // save
            $user = new User;

            //$user->domain_id = Auth::guard('api')->id();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->fullname = $request->fullname;
            $user->birthday = $request->birthday;
            $user->office = $request->office;
            $user->workday = $request->workday;
            $user->status = $request->status;
            $user->password = bcrypt($request->password);
            $user->list_rule = !empty($request->list_rule) ? json_encode($request->list_rule) : null;
            $user->del_flg = 0;

            $user->save();

            $data['code'] = 200;
            $data['message'] = 'Lưu thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {
            $error_validate = User::validate();
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            // save
            $user = new User();
            $user->exists = true;

            $user->id = $id;

            $user->name = $request->name;
            $user->email = $request->email;
            $user->fullname = $request->fullname;
            $user->birthday = $request->birthday;
            $user->office = $request->office;
            $user->workday = $request->workday;
            $user->status = $request->status;
            if(isset($request->password)) $user->password = bcrypt($request->password);
            $user->list_rule = !empty($request->list_rule) ? json_encode($request->list_rule) : null;
            $user->del_flg = 0;

            $user->save();

            $data['code'] = 200;
            $data['message'] = 'Cập nhật thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {
            $user = User::where([
                ['id', $id],
                ['del_flg', 0],
                ['role', 0],
                //['domain_id', Auth::guard('api')->id()]
            ])->first();

            $data['code'] = 200;
            $data['data'] = $user;
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {
            $user = User::where('id', $id)->update(['del_flg' => 1]);
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

}
