<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\FunctionAuthority;
use App\Model\ShopGroupUrl;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;

class AdminfunctionauthoritiesController extends BaseAdminController
{

    public function search_select2_function (Request $request) {
        $function = FunctionAuthority::where([
            ['del_flg', 0],
        ])->get();
        //$sliders = json_decode($sliders, true);

        $data['code'] = 200;
        $data['data'] = $function;
        return response()->json($data, 200);
    }

    public function search_select2_group_url (Request $request) {
        $urls = ShopGroupUrl::get();
        //$sliders = json_decode($sliders, true);

        $data['code'] = 200;
        $data['data'] = $urls;
        return response()->json($data, 200);
    }

    public function search_datatable(Request $request) {
        $data = $request->all();
        $functions = FunctionAuthority::where([
            ['del_flg', 0],
        ])->get();
        $functions = json_decode($functions, true);

        $output = [];
        foreach ($functions as $function) {
            $row = [];

            $row[] = $function['name'];
            $row[] = date("d/m/Y", strtotime($function['created_at']));

            $view = View::make('Backend/Adminfunctionauthorities/_actions', ['id' => $function['id']]);
            $row[] = $view->render();

            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {
            // validate
            $error_validate = FunctionAuthority::validate();
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            // save
            $function = new FunctionAuthority;

            $function->name = $request->name;
            $function->role_json = !empty($request->role_json) ? json_encode($request->role_json) : null;
            $function->del_flg = 0;
            $function->save();

            $data['code'] = 200;
            $data['message'] = 'Lưu thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {
            // check $id isset in database
            $check_function = FunctionAuthority::where([
                ['id', '=', $id],
            ])->first();

            if (!$check_function) {
                $data['code'] = 300;
                $data['error'] = 'Lưu thất bại';
                return response()->json($data, 200);
            }

            $error_validate = FunctionAuthority::validate();
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            // save
            $function = new FunctionAuthority();
            $function->exists = true;

            $function->id = $id;

            $function->name = $request->name;
            $function->role_json = !empty($request->role_json) ? json_encode($request->role_json) : null;
            $function->del_flg = 0;

            $function->save();

            $data['code'] = 200;
            $data['message'] = 'Cập nhật thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {
            $function = FunctionAuthority::where([
                ['id', $id],
                ['del_flg', 0],
            ])->first();


            $data['code'] = 200;
            $data['data'] = $function;
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {
            $function = FunctionAuthority::where([
                ['id', $id],
            ])->update(['del_flg' => 1]);

            if ($function) {
                $data['code'] = 200;
                $data['error'] = 'Xóa thành công';
                return response()->json($data, 200);
            } else {
                $data['code'] = 300;
                $data['error'] = 'Xóa thất bại';
                return response()->json($data, 200);
            }
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

}
