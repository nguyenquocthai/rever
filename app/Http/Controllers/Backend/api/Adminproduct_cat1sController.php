<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\product_cat1;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminproduct_cat1sController extends BaseAdminController
{

    public function index (Request $request) {

        $product_cat1s = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['type', '=', 0],
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($product_cat1s as $key => $product_cat1) {
            $vitri = $key+1;
            $row = $this->GetRow($product_cat1, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = product_cat1::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'product_cat1');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = product_cat1::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'product_cat1', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $product_cat1 = DB::table('product_cat1s')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$product_cat1) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            product_cat1::where([
                ['id', $product_cat1->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.product_cat1_path').$product_cat1->avatar;
            rename($image_path, config('general.product_cat1_path')."del_".$product_cat1->avatar);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($product_cat1, $vitri)
    {
        $row = [];
        $row[] = $product_cat1->id;
        $row[] = $product_cat1->position;
        $row[] = $vitri;
        $row[] = $product_cat1->title;
        $row[] = $this->GetImg([
            'avatar'=> $product_cat1->avatar,
            'data'=> 'product_cat1',
            'time'=> $product_cat1->updated_at
        ]);
        $row[] = '<span class="hidden">'.$product_cat1->updated_at.'</span>'.date('d/m/Y', strtotime($product_cat1->updated_at));
        $view = View::make('Backend/product_cat1/_status', ['status' => $product_cat1->status]);
        $row[] = $view->render();
        $view = View::make('Backend/product_cat1/_actions', ['id' => $product_cat1->id,'page' => 'product_cat1']);
        $row[] = $view->render();

        return $row;
    }
}
