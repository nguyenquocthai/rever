<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\bannerfb;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminbannerfbsController extends BaseAdminController
{

    public function index (Request $request) {

        $bannerfbs = DB::table('bannerfbs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($bannerfbs as $key => $bannerfb) {
            $vitri = $key+1;
            $row = $this->GetRow($bannerfb, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    
    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = bannerfb::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'bannerfb');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = bannerfb::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'bannerfb', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $bannerfb = DB::table('bannerfbs')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$bannerfb) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            bannerfb::where([
                ['id', $bannerfb->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.bannerfb_path').$bannerfb->avatar;
            //rename($image_path, config('general.bannerfb_path')."del_".@$bannerfb->avatar);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($bannerfb, $vitri)
    {
        $row = [];
        $row[] = $bannerfb->id;
        $row[] = $bannerfb->position;
        $row[] = $vitri;
        $row[] = $this->GetImg([
            'avatar'=> $bannerfb->avatar,
            'data'=> 'bannerfb',
            'time'=> $bannerfb->updated_at
        ]);
        $row[] = '<span class="hidden">'.$bannerfb->updated_at.'</span>'.date('d/m/Y', strtotime($bannerfb->updated_at));
        $view = View::make('Backend/bannerfb/_status', ['status' => $bannerfb->status]);
        $row[] = $view->render();
        $view = View::make('Backend/bannerfb/_actions', ['id' => $bannerfb->id,'page' => 'bannerfb']);
        $row[] = $view->render();

        return $row;
    }
}
