<?php

namespace App\Http\Controllers\Backend\api;
use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;
use App\Model\Store;
use App\Exceptions\ErrorCodes;
use App\Extensions\ShopCommon;
use App\Extensions\ShopUpload;


class AdminStoresController extends BaseAdminController
{

    public function search_datatable() {

        $stores = DB::table('stores')
            ->select('*')
            ->where('id_code', '=', 0)
            ->get();

        $output = [];
        foreach ($stores as $store) {
            $row = [];

            $row[] = '<label class="mt-checkbox mt-checkbox-outline"><input class="check_player" value="'.$store->data.'" type="checkbox" data-id="'.$store->id.'" data-title="'.$store->title.'" data-title_en="'.$store->title_en.'" data-value="'.$store->value.'" data-value_en="'.$store->value_en.'"><span></span></label>';

            $row[] = $store->id;

            $row[] = '<i class="' . $store->data . '"</i>';

            $row[] = $store->title;

            $row[] = $store->title_en;

            $row[] = $store->value;

            $row[] = $store->value_en;

            $output[] = $row;
        }

        $this->resp(ErrorCodes::E_OK, null, $output);
        $this->response();
    }

    public function search_datatable2() {

        $stores = DB::table('stores')
            ->select('*')
            ->where('id_code', '=', 1)
            ->get();

        $output = [];
        foreach ($stores as $store) {
            $row = [];

            $row[] = '<label class="mt-checkbox mt-checkbox-outline"><input class="check_player2" value="'.$store->data.'" type="checkbox" data-id="'.$store->id.'" data-title="'.$store->title.'" data-title_en="'.$store->title_en.'"><span></span></label>';

            $row[] = $store->id;

            $row[] = '<i class="' . $store->data . '"</i>';

            $row[] = $store->title;

            $row[] = $store->title_en;

            $output[] = $row;
        }

        $this->resp(ErrorCodes::E_OK, null, $output);
        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function show_tq() {
        $stores = DB::table('stores')
            ->select('*')
            ->where([
                ['id_code', '=', 0],
            ])
            ->orderBy('id', 'desc')
            ->get();

        $this->resp(ErrorCodes::E_OK, null, $stores);
        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = Store::validate();
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
               $this->resp(ErrorCodes::E_VALIDATION_ERROR_FIELD, 'Lưu thất bại', $validator->errors());
               $this->response();
            }

            // save
            $db_stores = new Store;
            $db_stores->value_min = $request->value_min;
            $db_stores->value_max = $request->value_max;

            $db_stores->value_min_en = $request->value_min_en;
            $db_stores->value_max_en = $request->value_max_en;
            $db_stores->id_group = $request->id_group;
            $db_stores->save();

            $val_stores = Store::where('id_group', $request->id_group)->get()->toJson();
            $val_stores = json_decode($val_stores, true);
            $output_store = [];
            foreach ($val_stores as $val_store) {
                $row = [];

                $row[] = $val_store['id'];

                $row[] = $this->bd_nice_number($val_store['value_min']) . ' vnđ';

                $row[] = $this->bd_nice_number($val_store['value_max']) . ' vnđ';

                $row[] = $this->bd_nice_number($val_store['value_min_en']) . ' $';

                $row[] = $this->bd_nice_number($val_store['value_max_en']) . ' $';

                $view = View::make('/backend/adminproduct1/_store_actions', ['data' => $val_store]);
                $row[] = $view->render();

                $output_store[] = $row;
            }

        } catch (Exception $e) {
            $this->exception($e);
            $this->response();
        }
        $this->resp(ErrorCodes::E_OK, 'Lưu thành công', $output_store);
        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {
            // check $id isset in database
            $store = Store::where('id', '=', $id)->first();

            if (!$store) {
                $this->resp(ErrorCodes::E_VALIDATION_ERROR_FIELD, 'Lưu thất bại');
               $this->response();
            }
            $file_name = $store['avatar'];

            // validate
            $error_validate = Store::validate();
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
               $this->resp(ErrorCodes::E_VALIDATION_ERROR_FIELD, 'Lưu thất bại', $validator->errors());
               $this->response();
            }

            // Create folder
            if ( !file_exists(config('general.store_path')) )
                mkdir(config('general.store_path'), config('permission_folder'), true);

            // upload avatar
            if($request->hasFile('files') == 1) {

                $files = $request->file('files');
                $avatar = $files[0];
                $avatar_link['path'] = config('general.store_path');
                $avatar_link['url'] = config('general.store_url');
                $options['file_name'] = $file_name;

                $upload = ShopUpload::upload($avatar, $avatar_link, $options);

                $file_name = $upload ? $upload['file_name'] : $store['avatar'];

            }

            $date = str_replace('/', '-', $request->start_date);
            $start_date = date('Y-m-d', strtotime($date));

            // save
            $db_stores = Store::find($id);

            $db_stores->title = $request->title;
            $db_stores->slug = $request->slug;
            
            $db_stores->id_store_cat = $request->id_store_cat;
            $db_stores->avatar = $file_name;
            $db_stores->summary = $request->summary;
            $db_stores->content = $request->content;

            $db_stores->start_date = $start_date;

            $db_stores->time = $request->time;
            $db_stores->cost = $request->cost;
            $db_stores->number = $request->number;
            $db_stores->address = $request->address;

            $db_stores->created_at = date("Y-m-d H:i:s");
            $db_stores->status = $request->status;
            $db_stores->high_flg = $request->high_flg;
            $db_stores->promotion_flg = $request->promotion_flg;

            $db_stores->save();

            // upload medias
            $files = $request->album;
            if ($files != '') {
                foreach ($files as $file) {
                $avatar = $file;
                $option['file_name'] = '';
                $option['path'] = config('general.product_media_path');
                $option['url'] = config('general.product_media_path_url');

                $callback = ShopUpload::upload_image($option, $avatar);

                $id = DB::table('medias')->insert(
                    [
                        'image' => $callback['name'],
                        'id_group' => 2,
                        'id_product' => $db_stores->id
                    ]
                );
                }
            }

            // upload tienich
            $tienichs = $request->tienich;
            if ($tienichs != '') {
               foreach ($tienichs as $tienich) {

                    DB::table('tienichs')->insert(
                        [
                            'data' => $tienich['data'],
                            'title' => $tienich['title'],
                            'id_group' => 2,
                            'id_product' => $db_stores->id
                        ]
                    );
                }
            }  

        } catch (Exception $e) {
            $this->exception($e);
            $this->response();
        }

        $this->resp(ErrorCodes::E_OK, 'Cập nhật thành công');
        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {
            
            $store = DB::table('stores')
                ->select('*')
                ->where('id', '=', $id)
                ->first();

            $this->resp(ErrorCodes::E_OK, null, $store);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {
            $data = Store::find($id);
            $data->delete();

            $this->resp(ErrorCodes::E_OK, 'Xóa thành công');
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function get_stores()
    {
        try {
            $stores = Store::where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])->get();

            $this->resp(ErrorCodes::E_OK, null, $stores);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function add_courese_sub(Request $request)
    {
        try {
            $view = View::make('/backend/adminstore/_sub', ['request' => $request]);
            $data = $view->render();

            $this->resp(ErrorCodes::E_OK, null, $data);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}