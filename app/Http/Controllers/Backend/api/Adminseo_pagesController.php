<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\seo_page;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminseo_pagesController extends BaseAdminController
{

    public function index (Request $request) {

        $seo_pages = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($seo_pages as $key => $seo_page) {
            $vitri = $key+1;
            $row = $this->GetRow($seo_page, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = seo_page::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'seo_page');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = seo_page::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'seo_page', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $seo_page = DB::table('seo_pages')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$seo_page) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            seo_page::where([
                ['id', $seo_page->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.seo_page_path').$seo_page->avatar;
            rename($image_path, config('general.seo_page_path')."del_".$seo_page->avatar);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($seo_page, $vitri)
    {
        $row = [];
        $row[] = $seo_page->id;
        $row[] = $seo_page->position;
        $row[] = $vitri;
        $row[] = $seo_page->title;
        $row[] = $this->GetImg([
            'avatar'=> $seo_page->avatar,
            'data'=> 'seo_page',
            'time'=> $seo_page->updated_at
        ]);
        $row[] = '<span class="hidden">'.$seo_page->updated_at.'</span>'.date('d/m/Y', strtotime($seo_page->updated_at));
        $view = View::make('Backend/seo_page/_status', ['status' => $seo_page->status]);
        $row[] = $view->render();
        $view = View::make('Backend/seo_page/_actions', ['id' => $seo_page->id,'page' => 'seo_page']);
        $row[] = $view->render();

        return $row;
    }
}
