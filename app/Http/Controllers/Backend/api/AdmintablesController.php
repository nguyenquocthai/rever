<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;
use App\Exceptions\ErrorCodes;
use App\Extensions\ShopCommon;
use App\Extensions\ShopUpload;


class AdmintablesController extends BaseAdminController
{
    public function gettable(Request $request)
    {
        try {

            if ($request->name == 'm_provinces') {

                $m_provinces = DB::table('m_provinces')
                    ->select('*')
                    ->get();

                $data['code'] = 200;
                $data['data'] = $m_provinces;
                return response()->json($data, 200);

            } else if($request->name == 'm_districts') {

                $m_districts = DB::table('m_districts')
                    ->select('*')
                    ->where([
                        ['m_province_id', '=', $request->tinhthanh]
                    ])
                    ->get();

                $data['code'] = 200;
                $data['data'] = $m_districts;
                return response()->json($data, 200);

            } else if($request->name == 'projects') {

                $projects = DB::table('projects')
                    ->select('id', 'title')
                    ->where([
                        ['del_flg', '=', 0]
                    ])
                    ->orderBy('id', 'desc')
                    ->get();

                $data['code'] = 200;
                $data['data'] = $projects;
                return response()->json($data, 200);

            } else if($request->name == 'tags') {

                $tags = DB::table('tags')
                    ->select('id', 'title')
                    ->where([
                        ['del_flg', '=', 0]
                    ])
                    ->orderBy('id', 'desc')
                    ->get();
                    
                $data['code'] = 200;
                $data['data'] = $tags;
                return response()->json($data, 200);

            } else {
                $data['code'] = 300;
                $data['error'] = 'Lỗi kết nối';
                return response()->json($data, 200);
            }
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}