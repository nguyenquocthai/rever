<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Model\MDistrict;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;

class MDistrictsController extends BaseRestController
{
    public function search_select2 () {
        $districts = MDistrict::get();
        $total_count = count($districts);
        $output = [
            'incomplete_results' => false,
            'data' => $districts,
            'total_count' => $total_count,
        ];
        return response()->json($output, 200);
    }

}
