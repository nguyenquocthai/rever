<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mail\ActiveEmail;
use Illuminate\Support\Facades\Mail;
use App\Model\nguoidung;
use App\Exceptions\SessionUser;
use App\Extensions\ShopUpload;
use App\Extensions\UploadMany;
use App\Mail\DemoEmail;

/* map function
    - dangnhap
    - thongtintaikhoan
    - taotaikhoan
    - add_nguoidung
    - dangkythanhcong
    - doimatkhau
    - set_pass
    - account_active
    - suataikhoan
    - logout
*/
class NguoidungController extends BaseFrontendController
{
    private $sessionUser;

    public function __construct(SessionUser $sessionUser)
    {

        $this->boot();
        $this->sessionUser = $sessionUser;
        View::share('active_nguoidung', 'active');
    }

    //-------------------------------------------------------------------------------
    public function dangnhap(Request $request)
    {

        $email = $request->email;
        $password = $request->password;

        $user = nguoidung::where([
            ['email', '=', @$request->email],
            ['del_flg', '=', 0],
        ])->first();

        if (!$user) {
            $data['code'] = 300;
            $data['error'] = 'Wrong email or password';
            return response()->json($data, 200);
        }

        if (password_verify(@$request->password, $user->password)) {

            if($user->del_flg == 1){
                $data['code'] = 300;
                $data['error'] = 'Your account has been deleted.';
                return response()->json($data, 200);
            }

            if($user->status == 1){
                $data['code'] = 300;
                $data['error'] = 'Your account has been locked.';
                return response()->json($data, 200);
            }

            session()->forget('email_kichhoat');
            if ($user->active == 0) {
                $table = nguoidung::find($user->id);
                $table->active_token = rand();
                $table->save();
                session()->put('email_kichhoat', $user->email);
                
                $data['code'] = 201;
                return response()->json($data, 200);
            }

            $data['code'] = 200;
            // set data for session
            $this->sessionUser->set($user);
            $data['msg'] = 'Logged in successfully';
            $data['linkback'] = "";
            if ($request->linkback != "") {
                $linkback = str_replace("_","/",$request->linkback);
                $data['linkback'] = $linkback;
            }
            return response()->json($data, 200);
        } else {
            $data['code'] = 300;
            $data['error'] = 'Wrong email or password';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function dangxuat()
    {
        session()->forget('userSession');
        return redirect('/');
    }

    public function doimatkhau()
    {
        if(session('userSession') == null) {
            return redirect('/');
        }

        $nguoidung = DB::table('nguoidungs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', session('userSession')->id]
            ])
            ->first();

        return view('frontend.nguoidung.doimatkhau')->with(compact('nguoidung'));
    }

    //-------------------------------------------------------------------------------
    public function thongtintaikhoan()
    {
        if(session('userSession') == null) {
            return redirect('/create-account');
        }

        $nguoidung = DB::table('nguoidungs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', session('userSession')->id]
            ])
            ->first();

        if (!$nguoidung) {
            session()->forget('userSession');
            return redirect('/404');
        }

        $active_thongtintaikhoan = "active";

        return view('frontend.nguoidung.thongtintaikhoan')->with(compact('nguoidung', 'active_thongtintaikhoan'));
    }

    //-------------------------------------------------------------------------------
    public function taotaikhoan()
    {
        if (session('userSession') !== null) {
            return redirect('/info-account');
        }

        return view('frontend.nguoidung.taotaikhoan');
    }

    //-------------------------------------------------------------------------------
    public function add_nguoidung(Request $request)
    {
        try {

            if($request->status_code == 'reactive'){

                if(session('email_kichhoat') == null){
                    $data['code'] = 300;
                    $data['error'] = 'There are no new subscriptions.';
                    return response()->json($data, 200);
                }

                $nguoidung = DB::table('nguoidungs')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['email', '=', session('email_kichhoat')]
                    ])
                    ->first();

                if (!$nguoidung) {
                    $data['code'] = 300;
                    $data['error'] = 'This email is not registered.';
                    return response()->json($data, 200);
                }

                if ($nguoidung->active == 1) {
                    $data['code'] = 300;
                    $data['error'] = 'This account is activated.';
                    return response()->json($data, 200);
                }

                if ($nguoidung->active_token == null) {
                    $table = nguoidung::find($nguoidung->id);
                    $table->active_token = rand();
                    $table->save();

                    $nguoidung->active_token = $table->active_token;
                }

                $obj = new \stdClass();
                $obj->sender = 'sosanhnhadat.com';
                $obj->receiver = $nguoidung->name;
                $obj->token = $nguoidung->active_token;
                Mail::to($nguoidung->email)->send(new ActiveEmail($obj));

                $data['message'] = 'Resend the activation email. <b>'.$nguoidung->email.'</b>';
                $data['code'] = 200;
                return response()->json($data, 200);
            }

            $email = DB::table('nguoidungs')
                ->select('*')
                ->where([
                    ['email', '=', $request->email],
                    ['del_flg', '=', 0],
                    
                ])
                ->first();

            if($email){
                $data['code'] = 300;
                $data['error'] = 'Email already exists';
                return response()->json($data, 200);
            }

            $active_token = rand();
            $insert_value = $request->all();

            $insert_value['active_token'] = $active_token;
            $insert_value['password'] = bcrypt($request->password);

            DB::table('nguoidungs')->insert($insert_value);

            $obj = new \stdClass();
            $obj->sender = 'vivaland.net';
            $obj->receiver = $request->name;
            $obj->token = $active_token;
     
            Mail::to($request->email)->send(new ActiveEmail($obj));

            session()->put('email_kichhoat', $request->email);
            $data['message'] = 'Account successfully created.';
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Connection errors';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function dangkythanhcong()
    {
        if(session('email_kichhoat') == null){
            return redirect('/create-account');
        } 

        $nguoidung = DB::table('nguoidungs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['email', '=', session('email_kichhoat')]
            ])
            ->first();

        if (!$nguoidung) {
            return redirect('/404');
        }

        return view('frontend.nguoidung.thanhcong')->with(compact('nguoidung'));
    }

    //-------------------------------------------------------------------------------
    public function forgotpassword()
    {
        if (session('userSession') !== null) {
            return redirect('/info-account');
        }
        return view('frontend.nguoidung.forgotpassword');
    }

    //-------------------------------------------------------------------------------
    public function mail_forgot_pass(Request $request)
    {
        try {
            $email = $request->email;

            $user = Nguoidung::where([
                ['email', '=', @$request->email],
                ['del_flg', '=', 0],
            ])->first();
            if ($user) {
                $user->token_forget = rand();
                $user->save();

                $objDemo = new \stdClass();
                $objDemo->demo_one = 'Demo One Value';
                $objDemo->demo_two = 'Demo Two Value';
                $objDemo->sender = 'vivaland.net';
                $objDemo->receiver = $user->name;
                $objDemo->token = $user->token_forget;
         
                Mail::to($request->email)->send(new DemoEmail($objDemo));

                $data['code'] = 200;
                $data['msg'] = 'Success';
                return response()->json($data, 200);
            }
            else{
                 $data['code'] = 300;
                $data['error'] = 'Error.';
                return response()->json($data, 200);
            }
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Error.';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function changepass($token_forget)
    {
        $nguoidung = DB::table('nguoidungs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['token_forget', '=', $token_forget]
            ])
            ->first();
        if(!$nguoidung){
            return view('error.404');
        }

        return view('frontend.nguoidung.changepass')->with(compact('nguoidung','token_forget'));
    }

    //-------------------------------------------------------------------------------
    public function set_pass(Request $request)
    {
        $data = [];

        $token = DB::table('nguoidungs')
            ->select('id','token_forget')
            ->where([
                ['token_forget', '=', $request->token_forget],
                
            ])
            ->first();

        DB::table('nguoidungs')->where('token_forget','=', $token->token_forget)->update([
            'password' => bcrypt($request->password),
            'token_forget' => rand(),
        ]);

        $data['code'] = 200;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function account_active($active_token)
    {

        $nguoidung = DB::table('nguoidungs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['active_token', '=', $active_token]
            ])
            ->first();   
        if(!$nguoidung){
            return view('error.404');
        }
        session()->forget('email_kichhoat');
        DB::table('nguoidungs')->where('active_token','=', $active_token)->update([
            'active' => 1,
        ]);

        return view('frontend.nguoidung.account_active')->with(compact('nguoidung'));
    }

    //-------------------------------------------------------------------------------
    public function suataikhoan(Request $request)
    {
        try {
            $nguoidung = DB::table('nguoidungs')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', session('userSession')->id]
                ])
                ->first();

            if (!$nguoidung) {
                $data['code'] = 300;
                $data['error'] = 'Connection errors';
                return response()->json($data, 200);
            }

            if ($request->status_code == "change_avatar") {

                if ($nguoidung) {
                    $avatar_name = $nguoidung->avatar;
                }

                $avatar = $request->avatar;
                $avatar_link['path'] = config('general.nguoidung_path');
                $avatar_link['url'] = config('general.nguoidung_url');
                $options['file_name'] = $avatar_name;

                $upload = ShopUpload::upload($avatar, $avatar_link, $options);
                if ($upload == 'error') {
                    $data['code'] = 300;
                    $data['error'] = 'Picture is over ' . config('general.notification_image');
                    return response()->json($data, 200);
                }
                $file_name = $upload ? $upload['file_name'] : $avatar_name;

                // save
                $nguoidung = new Nguoidung();
                $nguoidung->exists = true;
                $nguoidung->id = session('userSession')->id;
                $nguoidung->avatar = $file_name;
                $nguoidung->updated_at = date("Y-m-d H:i:s");
                $nguoidung->save();

                $data['code'] = 200;
                $data['message'] = 'Update successful';
                return response()->json($data, 200);
            }

            if ($request->status_code == "changpass") {
                $get_password = DB::table('nguoidungs')
                    ->select('password')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $nguoidung->id],
                    ])
                    ->first();

                if (password_verify($request->pass_old, $get_password->password)) {

                    $new_password = bcrypt($request->new_password);

                } else {
                    $data['code'] = 300;
                    $data['error'] = 'The old password is incorrect.';
                    return response()->json($data, 200);
                }

                if ($request->pass_new == '') {
                    $data['code'] = 300;
                    $data['error'] = 'No new password entered.';
                    return response()->json($data, 200);
                }

                if ($request->pass_new != $request->pass_new_re) {
                    $data['code'] = 300;
                    $data['error'] = 'Repeat the password incorrectly.';
                    return response()->json($data, 200);
                }

                // save
                $tb_nguoidung = Nguoidung::find($nguoidung->id);
                $tb_nguoidung->password = bcrypt($request->pass_new);
                $tb_nguoidung->save();

                $data['code'] = 200;
                $data['message'] = 'New password has been updated.';
                return response()->json($data, 200);
            } 

            $email_check = DB::table('nguoidungs')
                ->select('email')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '!=', session('userSession')->id],
                    ['email', '=', $request->email]
                ])
                ->first();

            if ($email_check) {
                $data['code'] = 300;
                $data['error'] = 'This email is already in use';
                return response()->json($data, 200);
            }

            $gioithieu = [];
            $gioithieu['occupation'] =  @$request->occupation;
            $gioithieu['workplace'] =  @$request->workplace;
            $gioithieu['hometown'] =  @$request->hometown;
            $gioithieu['first_language'] =  @$request->first_language;
            $gioithieu['other_languages'] =  @$request->other_languages;
            $gioithieu['relationship_status'] =  @$request->relationship_status;
            $gioithieu['facebook'] =  @$request->facebook;
            
            // save
            $tb_nguoidung = Nguoidung::find($nguoidung->id);
            $tb_nguoidung->name = $request->name;
            $tb_nguoidung->callname = $request->callname;
            $tb_nguoidung->ngaysinh = $request->ngaysinh;
            $tb_nguoidung->gioitinh = $request->gioitinh;
            $tb_nguoidung->intro = $request->intro;
            $tb_nguoidung->phone = $request->phone;
            $tb_nguoidung->phone2 = $request->phone2;
            $tb_nguoidung->email = $request->email;
            $tb_nguoidung->address = $request->address;
            $tb_nguoidung->gioithieu = json_encode($gioithieu);
            $tb_nguoidung->save();
            
            session()->put('userSession', $tb_nguoidung);

            $data['code'] = 200;
            $data['message'] = 'Update successful';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Connection errors';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function logout(){
        session()->forget('userSession');
        return redirect('/');
    }

    public function login_social(Request $request)
    {
        try {
            if ($request->email == "") {
                $data['code'] = 300;
                $data['error'] = 'No email found. Please select another account';
                return response()->json($data, 200);
            }

            $nguoidung = DB::table('nguoidungs')
                ->select('*')
                ->where([
                    ['email', '=', @$request->email],
                    ['del_flg', '=', 0],
                ])
                ->first();

            if (!$nguoidung) {
                $nguoidung = new nguoidung;
                $nguoidung->name = $request->name;
                $nguoidung->email = $request->email;
                $nguoidung->password = bcrypt($request->id);
                $nguoidung->del_flg = 0;
                $nguoidung->active = 1;
                $nguoidung->save();
            }

            $data['code'] = 200;
            // set data for session
            $this->sessionUser->set($nguoidung);
            $data['msg'] = 'Logged in successfully';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Error !';
            return response()->json($data, 200);
        }
    }
}