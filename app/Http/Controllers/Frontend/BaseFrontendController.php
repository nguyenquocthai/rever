<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\ErrorCodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Model\setting;
use Illuminate\Support\Facades\DB;
use App\Model\activitie;
use App\Model\plan_child;
use App\Model\plan;
use App\Model\Nguoidung;

class BaseFrontendController extends Controller
{

    public function __construct()
    {
        $this->boot();
    }

    //-------------------------------------------------------------------------------
    public function boot()
    {
        date_default_timezone_set("Asia/Bangkok");

        // Get info web
        $ShopSetting_data = setting::where('key','=','info_website')->first();
        $ShopSetting = json_decode($ShopSetting_data, true);
        $info_web = json_decode($ShopSetting['value'], true);
        View::share('info_web', $info_web);

        $sliders = DB::table('sliders')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        View::share('sliders', $sliders);

        $chinhsach = DB::table('intros')
            ->select('*')
            ->where([
                ['id', '=', 6],

            ])
            ->first();
        View::share('chinhsach', $chinhsach);
        $dieukhoan = DB::table('intros')
            ->select('*')
            ->where([
                ['id', '=', 7],

            ])
            ->first();
        View::share('dieukhoan', $dieukhoan);
        $phanhoi = DB::table('intros')
            ->select('*')
            ->where([
                ['id', '=', 8],

            ])
            ->first();
        View::share('phanhoi', $phanhoi);

        $product_cat1s = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->offset(0)
            ->limit(12)
            ->orderBy('position', 'desc')
            ->get();

        View::share('product_cat1s', $product_cat1s);

        $new_cats = DB::table('new_cats')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->offset(0)
            ->limit(5)
            ->orderBy('id', 'desc')
            ->get();
        View::share('new_cats', $new_cats);

        $sliders = DB::table('sliders')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],

            ])
            ->orderBy('position', 'desc')
            ->get();
        View::share('sliders', $sliders);

        $sliderlast = DB::table('sliders')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],

            ])
            ->orderBy('position', 'desc')
            ->first();
        View::share('sliderlast', $sliderlast);

        $nguoidung = $this->get_domain($get=1);
        View::share('nguoidung', $nguoidung);

        $gioithieu = [];
        if (@$nguoidung->gioithieu != null) {
            $gioithieu = json_decode($nguoidung->gioithieu);
        }
        View::share('gioithieu', $gioithieu);

        $setting = DB::table('settings')
            ->select('*')
            ->where('key', '=', 'info_website')
            ->first();
        $info_web = json_decode($setting->value, true);
        
        View::share('info_web', $info_web);

        $banner_subs = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                
            ])
            ->orderBy('id', 'asc')
            ->get();

        View::share('banner_subs', $banner_subs);

    }
    //-------------------------------------------------------------------------------
    public static function get_domain($get=0) {
        $check_domain = env("HTTP_HOST");
        $arr_domain = explode('.',$check_domain);
        if(count($arr_domain) >= 3) {
            $domainData = Nguoidung::where([
                ['del_flg', '=', 0],
                ['active', '=', 1],
                //['sub', '=', $check_domain]
            ])->first();
        } else {
            $domainData = Nguoidung::where([
                ['del_flg', '=', 0],
                ['active', '=', 1],
                //['domain', '=', $check_domain]
            ])->first();
        }
        if ($get == 1) {
            return $domainData;
        }
        return $domainData->id;
    }

    //-------------------------------------------------------------------------------
    public static function price_format($price, $options = []) {
        $options['default'] = isset($options['default']) ? $options['default'] : '0';

        if (isset($price) && $price != '' && $price != 0) {
            return number_format($price, 0, ',', '.') . ' đ';
        }
        return $options['default'];
    }

    //-------------------------------------------------------------------------------
    public static function RandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    // ----------------------------------------------------------------------------
    // Function _app_product
    // ----------------------------------------------------------------------------

    //-------------------------------------------------------------------------------
    public function GetImg($option = "") {

        $image_path = config('general.'.@$option['data'].'_path').@$option['avatar'];

        if (file_exists($image_path)) {
            $src = "http://" . $_SERVER["SERVER_NAME"] . config('general.'. $option['data'] .'_url') . $option['avatar'] . '?'.@$option['time'];
        } else{
            $src = "http://" . $_SERVER["SERVER_NAME"] . '/public/img/no-image.png';
        }

        if (isset($option['type'])) {
            return $src;
        } else {
            return $src;
        }
    }
    //-------------------------------------------------------------------------------
    public function afterday()
    {
        $day = date('Y-m-d H:i:s', strtotime('-7 day', strtotime(date("Y-m-d H:i:s"))));
        return $day;
    }
}
