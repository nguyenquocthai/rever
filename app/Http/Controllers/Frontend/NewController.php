<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;

class NewController extends BaseFrontendController
{

    public function __construct()
    {
        $this->boot();
        View::share('tintuc_active', 'active');
    }

    public function index()
    {
        $user_id = $this->get_domain();
        $news = DB::table('news')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
            ])
            ->orderBy('id', 'desc')->paginate(9);

        $new_cats = DB::table('new_cats')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->offset(0)
            ->limit(7)
            ->orderBy('id', 'desc')
            ->get();

        return view('frontend.new.index')->with(compact('news', 'new_cats'));
    }

    public function cat($slug)
    {
        $user_id = $this->get_domain();

        $find_cat = DB::table('new_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug]
            ])
            ->first();

        if (!$find_cat) {
           return redirect('/404');
        }

        $news = DB::table('news')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
                ['id_new_cat', '=', $find_cat->id]
            ])
            ->orderBy('id', 'desc')->paginate(9);

        $new_cats = DB::table('new_cats')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->offset(0)
            ->limit(7)
            ->orderBy('id', 'desc')
            ->get();

        return view('frontend.new.cat')->with(compact('news', 'new_cats', 'find_cat'));
    }

    public function detail($slug)
    {
        $user_id = $this->get_domain();
        $new = DB::table('news')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
                ['slug', '=', $slug]
            ])
            ->first();

        if(!$new) {
            return redirect('/');
        }

        $relate_news = DB::table('news')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
                ['id_new_cat', '=', $new->id_new_cat],
                ['id', '!=', $new->id]
            ])
            ->limit(4)->orderBy('id', 'desc')->get();

        return view('frontend.new.detail')->with(compact('new', 'relate_news'));
    }


}