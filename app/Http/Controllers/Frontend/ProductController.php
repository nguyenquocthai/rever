<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;

class ProductController extends BaseFrontendController
{

    public function __construct()
    {
        $this->boot();

        $phuonghuongs = ['Đông','Tây','Nam','Bắc','Đông Đông Bắc','Đông Đông Nam','Tây Tây Bắc','Tây Tây Nam','Bắc Đông Bắc','Bắc Tây Bắc','Nam Đông Nam','Nam Tây Nam'];
        View::share('phuonghuongs', $phuonghuongs);
    }

    public function index_sale()
    {
        $user_id = $this->get_domain();
        $where = [
            // ['user_id', '=', $user_id],
            ['del_flg', '=', 0],
            ['type', '=', 1]
        ];

        $sale_products = DB::table('item_projects')
            ->select('*')
            ->where($where)->orderBy('updated_at', 'desc')->paginate(9);

        $count_products = DB::table('item_projects')
            ->select('*')
            ->where($where)->count();

        $type = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1]
            ])->get();

        $db_searchs = DB::table('db_searchs')
            ->select('*')
            ->where([
                ['id_group', '=', 3],
            ])
            ->orderBy('value_min', 'asc')
            ->get();

        $nhadatban_active = 'active';

        return view('frontend.product.index_sale')->with(compact('sale_products', 'type', 'count_products', 'db_searchs', 'nhadatban_active'));
    }

    public function tagban($slug)
    {
        $user_id = $this->get_domain();

        $find_tag = DB::table('tags')
            ->select('*')
            ->where([
                ['slug', '=', $slug],
            ])
            ->first();

        if (!$find_tag) {
            return redirect('/404');
        }

        $where = [
            // ['user_id', '=', $user_id],
            ['tags', 'like', '%"' . $find_tag->id . '"%'],
            ['del_flg', '=', 0],
            ['type', '=', 1]
        ];

        $sale_products = DB::table('item_projects')
            ->select('*')
            ->where($where)->orderBy('updated_at', 'desc')->paginate(9);

        $count_products = DB::table('item_projects')
            ->select('*')
            ->where($where)->count();

        $type = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1]
            ])->get();

        $db_searchs = DB::table('db_searchs')
            ->select('*')
            ->where([
                ['id_group', '=', 2],
            ])
            ->orderBy('value_min', 'asc')
            ->get();

        $nhadatban_active = 'active';

        return view('frontend.product.tagban')->with(compact('sale_products', 'type', 'count_products', 'db_searchs', 'find_tag', 'nhadatban_active'));
    }

    public function timnhadatban(Request $request)
    {

        $user_id = $this->get_domain();

        $data_request = $request->all();

        $where = [
            // ['user_id', '=', $user_id],
            ['del_flg', '=', 0],
            ['type', '=', 1]
        ];

        if ($request->orderBy != '') {
            $orderby = explode(',', $request->orderBy); 
        } else {
            $orderby = [];
            $orderby[0] = 'id';
            $orderby[1] = 'desc';
        }
        $orderby_mark = $orderby[0].','.$orderby[1];

        if ($request->allprice != '') {
            $allprice = explode(',', $request->allprice);
            if($allprice[0] == ">"){
                $where[] = ['allprice', '>', $allprice[1]];
            } else {
                $where[] = ['allprice', '>=', $allprice[0]];
                $where[] = ['allprice', '<=', $allprice[1]];
            }
        }

        if ($request->category_id != '') {
            $idcats = json_decode($request->category_id, true); 
        } else {
            $idcats = [];
        }

        if ($request->number_bedroom != '') {
            $number_bedrooms = json_decode($request->number_bedroom, true);
        } else {
            $number_bedrooms = [];
        }

        if ($request->huongnha != '') {
            $huongnhas = json_decode($request->huongnha, true); 
        } else {
            $huongnhas = [];
        }

        if ($request->number_bathroom != '') {
            $where[] = ['thongtin', 'like', '%"phongtam":"'.$request->number_bathroom.'"%']; 
        }

        if ($request->tags != '') {
            $where[] = ['tags', 'like', '%"' . $request->tags . '"%']; 
        }

        $sale_products = DB::table('item_projects')
            ->select('*')
            ->where($where)
            ->Where(function ($query) use($idcats) {
                foreach ($idcats as $idcat) {
                    $query->orwhere('category_id', '=', $idcat);
                }       
            })
            ->Where(function ($query) use($number_bedrooms) {
                if (in_array(6, $number_bedrooms)) {
                    for ($i=0; $i < 5; $i++) { 
                        if (!in_array($i, $number_bedrooms)) {
                            $query->where('thongtin', 'not like', '%"phongngu":"'.$i.'"%');
                        }   
                    }
                } else {
                    foreach ($number_bedrooms as $number_bedroom) {
                        $query->orwhere('thongtin', 'like', '%"phongngu":"'.$number_bedroom.'"%');
                    }      
                } 
            })
            ->Where(function ($query) use($huongnhas) {
                foreach ($huongnhas as $huongnha) {
                    $query->orwhere('thongtin', 'like', '%"huongnha":"'.$huongnha.'"%');
                }       
            })
            ->orderBy($orderby[0], $orderby[1])
            ->paginate(9);

        $count_products = DB::table('item_projects')
            ->select('*')
            ->where($where)
            ->Where(function ($query) use($idcats) {
                foreach ($idcats as $idcat) {
                    $query->orwhere('category_id', '=', $idcat);
                }       
            })
            ->Where(function ($query) use($number_bedrooms) {
                if (in_array(6, $number_bedrooms)) {
                    for ($i=0; $i < 5; $i++) { 
                        if (!in_array($i, $number_bedrooms)) {
                            $query->where('thongtin', 'not like', '%"phongngu":"'.$i.'"%');
                        }   
                    }
                } else {
                    foreach ($number_bedrooms as $number_bedroom) {
                        $query->orwhere('thongtin', 'like', '%"phongngu":"'.$number_bedroom.'"%');
                    }      
                } 
            })
            ->Where(function ($query) use($huongnhas) {
                foreach ($huongnhas as $huongnha) {
                    $query->orwhere('thongtin', 'like', '%"huongnha":"'.$huongnha.'"%');
                }       
            })
            ->orderBy('id', 'desc')->count();

        $type = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1]
            ])->get();

        $db_searchs = DB::table('db_searchs')
            ->select('*')
            ->where([
                ['id_group', '=', 2],
            ])
            ->orderBy('value_min', 'asc')
            ->get();

        $nhadatban_active = 'active';

        return view('frontend.product.timnhadatban')->with(compact('sale_products', 'type', 'count_products', 'db_searchs', 'data_request', 'idcats', 'number_bedrooms', 'huongnhas', 'orderby_mark', 'nhadatban_active'));
    }

    public function detail_sale($slug)
    {
        $user_id = $this->get_domain();
        $product = DB::table('item_projects')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
                ['slug', '=', $slug]
            ])->first();


        if (!$product) {
            return redirect('/404');
        }

        $thongtin = json_decode($product->thongtin);

        if($product->type == 1) {
            $type = DB::table('product_cat1s')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['type', '=', 1]
                ])->get();
        } else {
            $type = DB::table('product_cat1s')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['type', '=', 0]
                ])->get();
        }

        $medias = DB::table('item_albums')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['item_id', '=', $product->id],
            ])->orderBy('id', 'desc')->get();

        $tienichs = DB::table('tienichs')
            ->select('*')
            ->where([
                ['id_product', '=', $product->id],
                ['id_group', '=', 4]
            ])
            ->orderBy('vitri', 'asc')
            ->get();

        $tags_id = [];
        if ($product->tags != 'null' && $product->tags != null) {
            $tags_id = json_decode($product->tags);
        } 

        $tags = DB::table('tags')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->whereIn('id',$tags_id)
            ->orderBy('id', 'desc')
            ->get();

        $cungduans = DB::table('item_projects')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
                ['id', '!=', $product->id],
                ['id_project', '=', $product->id_project],
                ['type', '=', 1]
            ])
            ->orderBy('updated_at', 'desc')
            ->get();

        $cungloais = DB::table('item_projects')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
                ['id', '!=', $product->id],
                ['category_id', '=', $product->category_id],
                ['type', '=', 1]
            ])
            ->orderBy('updated_at', 'desc')
            ->get();

        $nhadatban_active = 'active';

        return view('frontend.product.detail_sale')->with(compact('product', 'medias', 'tienichs', 'type', 'tags', 'cungduans', 'cungloais', 'thongtin', 'nhadatban_active'));
    }

    public function index_rent()
    {
        $user_id = $this->get_domain();
        $where = [
            // ['user_id', '=', $user_id],
            ['del_flg', '=', 0],
            ['type', '=', 0]
        ];

        $sale_products = DB::table('item_projects')
            ->select('*')
            ->where($where)->orderBy('updated_at', 'desc')->paginate(9);

        $count_products = DB::table('item_projects')
            ->select('*')
            ->where($where)->count();

        $type = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 0]
            ])->get();

        $db_searchs = DB::table('db_searchs')
            ->select('*')
            ->where([
                ['id_group', '=', 2],
            ])
            ->orderBy('value_min', 'asc')
            ->get();

        $nhadatthue_active = 'active';

        return view('frontend.product.index_rent')->with(compact('sale_products', 'type', 'count_products', 'db_searchs', 'nhadatthue_active'));
    }

    public function tagthue($slug)
    {
        $user_id = $this->get_domain();

        $find_tag = DB::table('tags')
            ->select('*')
            ->where([
                ['slug', '=', $slug],
            ])
            ->first();

        if (!$find_tag) {
            return redirect('/404');
        }

        $where = [
            // ['user_id', '=', $user_id],
            ['tags', 'like', '%"' . $find_tag->id . '"%'],
            ['del_flg', '=', 0],
            ['type', '=', 0]
        ];

        $sale_products = DB::table('item_projects')
            ->select('*')
            ->where($where)->orderBy('updated_at', 'desc')->paginate(9);

        $count_products = DB::table('item_projects')
            ->select('*')
            ->where($where)->count();

        $type = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 0]
            ])->get();

        $db_searchs = DB::table('db_searchs')
            ->select('*')
            ->where([
                ['id_group', '=', 1],
            ])
            ->orderBy('value_min', 'asc')
            ->get();

        $nhadatthue_active = 'active';

        return view('frontend.product.tagthue')->with(compact('sale_products', 'type', 'count_products', 'db_searchs', 'find_tag', 'nhadatthue_active'));
    }

    public function timnhadatthue(Request $request)
    {

        $user_id = $this->get_domain();

        $data_request = $request->all();

        $where = [
            // ['user_id', '=', $user_id],
            ['del_flg', '=', 0],
            ['type', '=', 0]
        ];

        if ($request->orderBy != '') {
            $orderby = explode(',', $request->orderBy); 
        } else {
            $orderby = [];
            $orderby[0] = 'id';
            $orderby[1] = 'desc';
        }
        $orderby_mark = $orderby[0].','.$orderby[1];

        if ($request->allprice != '') {
            $allprice = explode(',', $request->allprice);
            if($allprice[0] == ">"){
                $where[] = ['allprice', '>', $allprice[1]];
            } else {
                $where[] = ['allprice', '>=', $allprice[0]];
                $where[] = ['allprice', '<=', $allprice[1]];
            }
        }

        if ($request->category_id != '') {
            $idcats = json_decode($request->category_id, true); 
        } else {
            $idcats = [];
        }

        if ($request->number_bedroom != '') {
            $number_bedrooms = json_decode($request->number_bedroom, true);
        } else {
            $number_bedrooms = [];
        }

        if ($request->huongnha != '') {
            $huongnhas = json_decode($request->huongnha, true); 
        } else {
            $huongnhas = [];
        }

        if ($request->number_bathroom != '') {
            $where[] = ['thongtin', 'like', '%"phongtam":"'.$request->number_bathroom.'"%']; 
        }

        if ($request->tags != '') {
            $where[] = ['tags', 'like', '%"' . $request->tags . '"%']; 
        }

        $sale_products = DB::table('item_projects')
            ->select('*')
            ->where($where)
            ->Where(function ($query) use($idcats) {
                foreach ($idcats as $idcat) {
                    $query->orwhere('category_id', '=', $idcat);
                }       
            })
            ->Where(function ($query) use($number_bedrooms) {
                if (in_array(6, $number_bedrooms)) {
                    for ($i=0; $i < 5; $i++) { 
                        if (!in_array($i, $number_bedrooms)) {
                            $query->where('thongtin', 'not like', '%"phongngu":"'.$i.'"%');
                        }   
                    }
                } else {
                    foreach ($number_bedrooms as $number_bedroom) {
                        $query->orwhere('thongtin', 'like', '%"phongngu":"'.$number_bedroom.'"%');
                    }      
                } 
            })
            ->Where(function ($query) use($huongnhas) {
                foreach ($huongnhas as $huongnha) {
                    $query->orwhere('thongtin', 'like', '%"huongnha":"'.$huongnha.'"%');
                }       
            })
            ->orderBy($orderby[0], $orderby[1])
            ->paginate(9);

        $count_products = DB::table('item_projects')
            ->select('*')
            ->where($where)
            ->Where(function ($query) use($idcats) {
                foreach ($idcats as $idcat) {
                    $query->orwhere('category_id', '=', $idcat);
                }       
            })
            ->Where(function ($query) use($number_bedrooms) {
                if (in_array(6, $number_bedrooms)) {
                    for ($i=0; $i < 5; $i++) { 
                        if (!in_array($i, $number_bedrooms)) {
                            $query->where('thongtin', 'not like', '%"phongngu":"'.$i.'"%');
                        }   
                    }
                } else {
                    foreach ($number_bedrooms as $number_bedroom) {
                        $query->orwhere('thongtin', 'like', '%"phongngu":"'.$number_bedroom.'"%');
                    }      
                } 
            })
            ->Where(function ($query) use($huongnhas) {
                foreach ($huongnhas as $huongnha) {
                    $query->orwhere('thongtin', 'like', '%"huongnha":"'.$huongnha.'"%');
                }       
            })
            ->orderBy('id', 'desc')->count();

        $type = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 0]
            ])->get();

        $db_searchs = DB::table('db_searchs')
            ->select('*')
            ->where([
                ['id_group', '=', 1],
            ])
            ->orderBy('value_min', 'asc')
            ->get();

        $nhadatthue_active = 'active';

        return view('frontend.product.timnhadatthue')->with(compact('sale_products', 'type', 'count_products', 'db_searchs', 'data_request', 'idcats', 'number_bedrooms', 'huongnhas', 'orderby_mark', 'nhadatthue_active'));
    }

    public function detail_rent($slug)
    {
        $user_id = $this->get_domain();
        $product = DB::table('item_projects')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
                ['slug', '=', $slug]
            ])->first();


        if (!$product) {
            return redirect('/404');
        }

        $thongtin = json_decode($product->thongtin);

        if($product->type == 1) {
            $type = DB::table('product_cat1s')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['type', '=', 1]
                ])->get();
        } else {
            $type = DB::table('product_cat1s')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['type', '=', 0]
                ])->get();
        }

        $medias = DB::table('item_albums')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['item_id', '=', $product->id],
            ])->orderBy('id', 'desc')->get();

        $tienichs = DB::table('tienichs')
            ->select('*')
            ->where([
                ['id_product', '=', $product->id],
                ['id_group', '=', 4]
            ])
            ->orderBy('vitri', 'asc')
            ->get();

        $tags_id = [];
        if ($product->tags != 'null' && $product->tags != null) {
            $tags_id = json_decode($product->tags);
        } 

        $tags = DB::table('tags')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->whereIn('id',$tags_id)
            ->orderBy('id', 'desc')
            ->get();

        $cungduans = DB::table('item_projects')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
                ['id', '!=', $product->id],
                ['id_project', '=', $product->id_project],
                ['type', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();

        $cungloais = DB::table('item_projects')
            ->select('*')
            ->where([
                // ['user_id', '=', $user_id],
                ['del_flg', '=', 0],
                ['id', '!=', $product->id],
                ['category_id', '=', $product->category_id],
                ['type', '=', 0]
            ])
            ->orderBy('updated_at', 'desc')
            ->get();

        $nhadatthue_active = 'active';

        return view('frontend.product.detail_rent')->with(compact('product', 'medias', 'tienichs', 'type', 'tags', 'cungduans', 'cungloais', 'thongtin', 'nhadatthue_active'));
    }
}