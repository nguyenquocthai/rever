<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\comment;
use App\Exceptions\ErrorCodes;
use App\Extensions\ShopCommon;
use App\Extensions\ShopUpload;
use App\Extensions\UploadMany;
use App\Model\ItemAlbum;
class CommentController extends BaseFrontendController
{
   
    public function add_comment(Request $request)
    {
        try {
            $data = [];
            if($request->comment == null)
            {
                $data['code'] = 300;
                $data['error'] = 'Đánh giá không được để trống';
                return response()->json($data, 200);
            }
            if($request->id_khach == null)
            {
                $data['code'] = 300;
                $data['error'] = 'Họ tên không được để trống';
                return response()->json($data, 200);
            }
            

            // $image_errors = [];
            // $images = [];
            // foreach (@$request->images as $key => $value) {
            //     $avatar = $value;
            //     $avatar_link['path'] = config('general.customer_path');
            //     $avatar_link['url'] = config('general.customer_url');
            //     $upload = UploadMany::upload($avatar, $avatar_link);
            //     if ($upload['code'] == 300) {
            //         $image_errors[] = $upload;
            //     } else {
            //         $file_name = $upload['file_name'];
            //         $images[] = $file_name;
            //     }
            // }

            $avatar_errors = [];
            $avatars = [];
            if($request->avatar == null){
                $data['code'] = 300;
                $data['error'] = 'Avatar không được để trống';
                return response()->json($data, 200);
            }
            foreach (@$request->avatar as $key => $value) {
                $avatar1 = $value;
                $avatar_link['path'] = config('general.customer_path');
                $avatar_link['url'] = config('general.customer_url');
                $upload = UploadMany::upload($avatar1, $avatar_link);
                if ($upload['code'] == 300) {
                    $avatar_errors[] = $upload;
                } else {
                    $file_name = $upload['file_name'];
                    $avatars[] = $file_name;
                }
            }
            

            DB::table('customers')->insert([
                'name_contact' => $request->name_contact,
                'phone_contact' => $request->phone_contact,
                'email_contact' => $request->email_contact,
                'comment' => $request->comment,
                'id_chu' => $request->id_chu,
                'id_khach' => $request->id_khach,
                'id_itemproject' => $request->id_itemproject,
                'trang_thai' => 0,
                'status' => 0,
                'del_flg' => 0,
                'created_at' => date("Y-m-d H:i:s"),
                // 'images'=>  implode("|",$images),
                'avatar'=>  implode("",$avatars)
            ]);
            
            

            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Connection errors';
            return response()->json($data, 200);
        }
        
    }

    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Connection errors';
            return response()->json($data, 200);
        }
    }
}