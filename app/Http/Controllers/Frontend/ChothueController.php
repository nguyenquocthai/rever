<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class ChothueController extends BaseFrontendController
{
    public function index($slug)
    {
        $item_project = DB::table('item_projects')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['slug', '=', $slug],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();

        DB::table('item_projects')
            ->where('id', $item_project->id)
            ->update(['luotxem' => $item_project->luotxem+1]);

        if (!$item_project) {
            return redirect('/404');
        }

        $product_cat1 = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', $item_project->category_id]
            ])
            ->first();

        $tinhthanh = DB::table('za_provinces')
            ->select('*')
            ->where([
                ['id', '=', $item_project->tinhthanh]
            ])
            ->first();
        $quanhuyen = DB::table('zb_districts')
            ->select('*')
            ->where([
                ['id', '=', $item_project->quanhuyen]
            ])
            ->first();
        $phuongxa = DB::table('zc_wards')
            ->select('*')
            ->where([
                ['id', '=', $item_project->phuongxa]
            ])
            ->first();
        $duong = DB::table('zd_streets')
            ->select('*')
            ->where([
                ['id', '=', $item_project->duong]
            ])
            ->first();
        $tienichs = DB::table('tienichs')
            ->select('*')
            ->where([
                ['id_product', '=', $item_project->id]
            ])
            ->orderBy('vitri', 'asc')
            ->get();
        $nguoidung = DB::table('nguoidungs')
            ->select('*')
            ->where([
                ['id', '=', $item_project->user_id]
            ])
            ->first();

        $albums = DB::table('item_albums')
            ->select('*')
            ->where([
                ['item_id', '=', $item_project->id],
                ['del_flg', '=', 0],
            ])
            ->orderBy('id', 'asc')
            ->get();

        // SEO
        $seopage = new \stdClass();
        $seopage->meta_title = @$item_project->title;
        $seopage->meta_description = @$item_project->address;
        $seopage->linkpage = $this->fullBaseUrl().'/rent/'.$item_project->slug;
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$item_project->avatar,
            'data'=>'item_project',
            'time'=>$item_project->updated_at
        ]);

        return view('frontend.chothue.index')->with(compact('seopage','item_project','duong','phuongxa','quanhuyen','tinhthanh','tienichs','nguoidung','albums', 'product_cat1'));
    }

    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function cat($slug)
    {
        $product_cat1 = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['slug', '=', $slug],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();

        $item_projects = DB::table('item_projects')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['category_id', '=', $product_cat1->id],
                ['active', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        foreach ($item_projects as $key => $item_project) {

            $item_album = DB::table('item_albums')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['item_id', '=', $item_project->id]
                ])
                ->orderBy('id', 'asc')
                ->first();
            if($item_album){
                $item_projects[$key]->album = $item_album;
            }

        }
        
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.chothue.cat')->with(compact('seopage','item_projects','product_cat1'));
    }
    //-------------------------------------------------------------------------------
    public function catall()
    {

        $item_projects = DB::table('item_projects')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['active', '=', 0],
                ['type', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        foreach ($item_projects as $key => $item_project) {

            $item_album = DB::table('item_albums')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['item_id', '=', $item_project->id]
                ])
                ->orderBy('id', 'asc')
                ->first();
            if($item_album){
                $item_projects[$key]->album = $item_album;
            }

        }
        
        
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.chothue.catall')->with(compact('seopage','item_projects'));
    }
    //-------------------------------------------------------------------------------
    public function catallghep()
   
    {

        $item_projects = DB::table('item_projects')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['active', '=', 0],
                ['type', '=', 1],
            ])
            ->orderBy('position', 'desc')
            ->get();
    
        foreach ($item_projects as $key => $value) {
            $item_albums = DB::table('item_albums')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['item_id', '=', $value->id]
                ])
                ->orderBy('position', 'asc')
                ->get();

            $item_projects[$key]->albums = $item_albums;
        }
        
        
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.chothue.catallghep')->with(compact('seopage','item_projects'));
    }
}