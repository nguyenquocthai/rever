<?php

namespace App\Http\Controllers\Frontend\api;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\SessionUser;
use App\Exceptions\NganLuongPayment;
use App\Exceptions\OnePayPayment;
use App\Model\athlete;
use App\Model\activitie;
use App\Model\plan_child;
use App\Model\MDistrict;
use App\Model\Order;
use App\Model\race_detail;
use Session;
use DB;
use Illuminate\Http\Request;
use Mail;
use App\Mail\LostpassCreatedMailer;
use App\Mail\OrderCreatedMailer;

class OrderController extends BaseFrontendController
{

    private $sessionUser;

    public function __construct(OnePayPayment $onePayPayment, NganLuongPayment $nganluongPayment, SessionUser $sessionUser)
    {
        $this->nganluongPayment = $nganluongPayment;
        $this->onePayPayment = $onePayPayment;
        $this->sessionUser = $sessionUser;
        $this->boot();
    }

    // ------------------------------------------------------------------
    public function sign_order(Request $request)
    {
        try {

            // get user
            $session_user = $this->sessionUser->get();
            if (!$session_user && !isset($session_user->id)) {
                $data['code'] = 300;
                $data['error'] = 'Yêu cầu đăng nhập lại.';
                return response()->json($data, 200);
            }

            $athlete = DB::table('athletes')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $session_user->id]
                ])
                ->first();

            $race = DB::table('races')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $request->id_race]
                ])
                ->first();

            if(!$race || !$athlete){
                $data['code'] = 300;
                $data['error'] = 'không tìm thấy.';
                return response()->json($data, 200);
            }

            $race_detail = DB::table('race_details')
                ->select('*')
                ->where([
                    ['race_id', '=', $race->id],
                    ['athlete_id', '=', $athlete->id]
                ])
                ->first();

            if($race_detail){
                $data['code'] = 300;
                $data['error'] = 'Bạn đã tham gia cuộc đua này.';
                return response()->json($data, 200);
            }

            $thongke = DB::table('activities')
                ->select(
                    'id_athlete',
                    DB::raw('SUM(distance) as culy'),
                    DB::raw('SUM(moving_time) as thoigian'),
                    DB::raw('COUNT(id) as solanchay')
                )
                ->where([
                    ['del_flg', '=', 0],
                    ['id_athlete', '=', $session_user->id]
                ])
                ->groupBy('id_athlete')
                ->first();

            if (!$thongke) {
                $thongke = new \stdClass();
                $thongke->culy = 0;
                $thongke->thoigian = 0;
                $thongke->solanchay = 0;
            }

            $thongke->thoigian = $this->getpace($thongke->culy,$thongke->thoigian);
            $thongke->culy = $this->changekm($thongke->culy);
            
            $order = new Order;
            $order->athlete_id = $session_user->id;
            $order->race_id = $race->id;
            $order->name = $request->name;
            $order->email = $request->email;
            $order->birthday = $request->birthday;
            $order->address = $request->address;
            $order->phone = $request->phone;
            $order->gender = $request->gender;
            $order->city_id = $request->city_id;
            $order->district_id = $request->district_id;
            $order->phukien_size = $request->phukien_size;
            $order->phukien_price = $request->phukien_price;
            $order->fund_id = $request->fund_id;
            $order->fund_price = $request->fund_price;
            $order->race_price = $request->race_price;
            $order->race_total = $request->race_total;
            $order->payment_method = $request->payment_method;
            $order->race_end = $race->end;
            $order->phukien_name = $request->phukien_name;
            $save_order = $order->save();

            $order_no = $this->RandID($order->id, 4);
            if ($save_order == 1) {
                $table = Order::find($order->id);
                $table->order_no = $order_no;
                $table->save();

                DB::table('alerts')->insert([
                [
                    'id_athlete' => $athlete->id,
                    'id_order' => $order->id,
                    'title' => 'Đơn hàng <b>'.$order_no.'</b> đã thanh toán.',
                    'start' => $race->starttime,
                    'link' => '/chi-tiet-don-hang/'.$order_no,
                    'status' => 1,
                    'step' => 1,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ],

                [
                    'id_athlete' => $athlete->id,
                    'id_order' => $order->id,
                    'title' => 'Đơn hàng <b>'.$order_no.'</b> chưa thanh toán.',
                    'start' => $race->starttime,
                    'link' => '/chi-tiet-don-hang/'.$order_no,
                    'status' => 1,
                    'step' => 2,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ],

                [
                    'id_athlete' => $athlete->id,
                    'id_order' => $order->id,
                    'title' => 'Đơn hàng <b>'.$order_no.'</b> sắp hết hạn thanh toán.',
                    'start' => $this->changetime('-24 hour', $race->end),
                    'link' => '/chi-tiet-don-hang/'.$order_no,
                    'status' => 0,
                    'step' => 3,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ],

                [
                    'id_athlete' => $athlete->id,
                    'id_order' => $order->id,
                    'title' => 'Đơn hàng <b>'.$order_no.'</b> đã hết hạn thanh toán.',
                    'start' => $race->end,
                    'link' => '/chi-tiet-don-hang/'.$order_no,
                    'status' => 0,
                    'step' => 4,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]
            ]);
            }
            

            $race_detail = new race_detail;
            $race_detail->race_id = $race->id;
            $race_detail->athlete_id = $athlete->id;
            $race_detail->club_id = $request->club_id;
            $race_detail->athlete_icode = $athlete->icode;
            $race_detail->athlete_thongke = json_encode($thongke);
            $race_detail->athlete_name = $request->name;
            $race_detail->distance = $request->distance;
            $race_detail->gender = $request->gender;
            $race_detail->race_start = $race->start;
            $race_detail->race_end = $race->end;
            $race_detail->type = $race->type;
            $race_detail->nhan_hc = $request->nhan_hc;
            $race_detail->save();

            $type_mod = "Cuộc đua";
            if ($race->type == 1) {
                $type_mod = "Thử thách";
            }

            DB::table('alerts')->insert([
                [
                    'id_athlete' => $athlete->id,
                    'id_race_detail' => $race_detail->id,
                    'title' => 'Bạn vừa đăng ký '.$type_mod.' <b>'.$race->title.'</b>',
                    'start' => $race->start,
                    'link' => '/chi-tiet-cuoc-dua/'.$race->slug,
                    'status' => 0,
                    'step' => 1,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ],

                [
                    'id_athlete' => $athlete->id,
                    'id_race_detail' => $race_detail->id,
                    'title' => $type_mod.' <b>'.$race->title.'</b> đang diễn ra.',
                    'start' => $race->start,
                    'link' => '/chi-tiet-cuoc-dua/'.$race->slug,
                    'status' => 0,
                    'step' => 2,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ],

                [
                    'id_athlete' => $athlete->id,
                    'id_race_detail' => $race_detail->id,
                    'title' => $type_mod.' <b>'.$race->title.'</b> sắp kết thúc.',
                    'start' => $this->changetime('-24 hour', $race->end),
                    'link' => '/chi-tiet-cuoc-dua/'.$race->slug,
                    'status' => 0,
                    'step' => 3,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ],

                [
                    'id_athlete' => $athlete->id,
                    'id_race_detail' => $race_detail->id,
                    'title' => 'Bạn đã hoàn thành '.$type_mod.' <b>'.$race->title.'</b>',
                    'start' => $race->start,
                    'link' => '/chi-tiet-cuoc-dua/'.$race->slug,
                    'status' => 1,
                    'step' => 4,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ],

                [
                    'id_athlete' => $athlete->id,
                    'id_race_detail' => $race_detail->id,
                    'title' => 'Bạn chưa hoàn thành '.$type_mod.' <b>'.$race->title.'</b>',
                    'start' => $race->end,
                    'link' => '/chi-tiet-cuoc-dua/'.$race->slug,
                    'status' => 0,
                    'step' => 5,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]
            ]);

            $payurl = $this->onePayPayment->process([
                'code' => $order_no,
                'email' => $request->email,
                'phone' => $request->phone,
                'money' => $request->race_total,
                'return_url' => url('/') . '/api/frontend/reponse_payment/' . $order->id . '?order_no=' . $order_no
            ]);

            $race_type = "cuộc đua";
            if ($race->type == 1) {
                $race_type = "thử thách";
            }

            $obj = new \stdClass();
            $obj->sender = 'toprace.vn';
            $obj->receiver = $request->name;
            $obj->race_title = $race->title;
            $obj->race_type = $race_type;
            $obj->race_slug = $race->slug;

            Mail::to($request->email)->send(new OrderCreatedMailer($obj));

            $data['code'] = 200;
            $data['payurl'] = $payurl;
            $data['message'] = 'Đăng ký thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function checkout(Request $request)
    {
        try {

            // get user
            $session_user = $this->sessionUser->get();
            if (!$session_user && !isset($session_user->id)) {
                $data['code'] = 300;
                $data['error'] = 'Yêu cầu đăng nhập lại.';
                return response()->json($data, 200);
            }

            $order = DB::table('orders')
                ->select('*')
                ->where([
                    ['order_no', '=', $request->id],
                    ['athlete_id', '=', $session_user->id]
                ])
                ->first();

            if (!$order) {
                $data['code'] = 300;
                $data['error'] = 'không tìm thấy đơn hàng.';
                return response()->json($data, 200);
            }

            $payurl = $this->onePayPayment->process([
                'code' => $order->order_no,
                'email' => $order->name,
                'phone' => $order->phone,
                'money' => $order->race_total,
                'return_url' => url('/') . '/api/frontend/reponse_payment/' . $order->id . '?order_no=' . $order->order_no
            ]);
    
            $data['code'] = 200;
            $data['payurl'] = $payurl;
            $data['message'] = 'Lưu thành công';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    public function reponse_payment($id, Request $request)
    {

        $order = DB::table('orders')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['order_no', '=', $request->order_no]
            ])
            ->first();

        //thanh toan online onepage
        $checkpay = $this->onePayPayment->resultValidate([
            'vpc_TxnResponseCode' => $request->vpc_TxnResponseCode,
            'vpc_SecureHash' => $request->vpc_SecureHash
        ]);

        if ($checkpay == 1) {
            DB::table('orders')
                ->where('order_no', $request->order_no)
                ->update(['pay_status' => 1]);

            DB::table('alerts')
                ->where([
                    ['id_athlete', $order->athlete_id],
                    ['id_order', $order->id],
                    ['step', 1]
                ])
                ->update([
                    'status' => 0,
                    'updated_at' => date("Y-m-d H:i:s")
                ]);

            DB::table('alerts')
                ->where([
                    ['id_athlete', $order->athlete_id],
                    ['id_order', $order->id],
                    ['step','!=', 1]
                ])
                ->delete();

            return redirect('/chi-tiet-don-hang/' . $request->order_no);
        } else {

            DB::table('alerts')
                ->where([
                    ['id_athlete', $order->athlete_id],
                    ['id_order', $order->id],
                    ['step', 2]
                ])
                ->update(['status' => 0]);

            DB::table('alerts')
                ->where([
                    ['id_athlete', $order->athlete_id],
                    ['id_order', $order->id],
                    ['step','!=', 2]
                ])
                ->delete();

            return redirect('/chi-tiet-don-hang/' . $request->order_no);
        }
    }
}