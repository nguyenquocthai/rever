<?php

namespace App\Http\Controllers\Frontend\api;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use App\Exceptions\SessionUser;
use App\Exceptions\ErrorCodes;
use App\Model\athlete;
use App\Model\activitie;
use App\Model\plan_child;
use Session;
use DB;
use Illuminate\Http\Request;
use Mail;
use App\Mail\LostpassCreatedMailer;

class Api_athletes extends BaseFrontendController
{

    private $sessionUser;

    public function __construct(SessionUser $sessionUser)
    {
        $this->sessionUser = $sessionUser;
        $this->boot();
    }

    //-------------------------------------------------------------------------------
    public function strava_data(Request $request)
    {
        try {

            // get user
            $session_user = $this->sessionUser->get();
            if (!$session_user || !isset($session_user->id)) {
                $data['code'] = 300;
                $data['error'] = 'Yêu cầu đăng nhập lại.';
                return response()->json($data, 200);
            }

            $user = athlete::where([
                ['id', '=', $session_user->id],
                ['del_flg', '=', 0],
            ])->first();

            if (!$user) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            if($user->strava_code == null){
                $data['code'] = 300;
                $data['error'] = 'Bạn phải kết nối với strava trước.';
                return response()->json($data, 200);
            }

            $plan_childs = DB::table('plan_childs')
                ->select('*')
                ->where([
                    ['id_athlete', '=', $session_user->id],
                    ['status', '=', 1],
                    ['del_flg', '=', 0],
                    ['start_date_local', '<=', date('Y-m-d').' 00:00:00']
                ])
                ->orderBy('start_date_local', 'asc')
                ->get();

            if (count(@$plan_childs) == 0) {
                $data['code'] = 300;
                $data['error'] = 'Không có dữ liệu mới.';
                return response()->json($data, 200);
            }

            $save_activities = $this->save_activities($user->strava_code, $plan_childs[0]->start_date_local, $session_user->id);


            if ($save_activities['code'] == 300) {
                $data['code'] = 300;
                $data['error'] = 'Lỗi kết nối strava.';
                return response()->json($data, 200);
            }

            $update_lanchid = $this->update_plan_child($session_user->id);
            $update_lan = $this->update_plan($session_user->id);

            $data['code'] = 200;
            if($save_activities['new_data'] == 1){
                $data['message'] = 'Lưu thành công';
            } else {
                $data['message'] = 'Không có dữ liệu mới.';
            }
            return response()->json($data, 200);
            die();
    
            $data['code'] = 200;
            $data['message'] = 'Lưu thành công';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function send_race(Request $request)
    {
        try {

            // get user
            $session_user = $this->sessionUser->get();
            if (!$session_user || !isset($session_user->id)) {
                $data['code'] = 300;
                $data['error'] = 'Yêu cầu đăng nhập lại.';
                return response()->json($data, 200);
            }

            $user = athlete::where([
                ['id', '=', $session_user->id],
                ['del_flg', '=', 0],
            ])->first();

            if (!$user) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            if($user->strava_code == null){
                $data['code'] = 300;
                $data['error'] = 'Bạn phải kết nối với strava trước.';
                return response()->json($data, 200);
            }

            $token = $this->token_strava($user->strava_code);

            if($token['code'] == 300){
                $data['code'] = 300;
                $data['error'] = 'Xảy ra lỗi! vui lòng đăng nhập lại.';
                return response()->json($data, 200);
            }
            $token = $token['value'];

            $race = DB::table('races')
                ->select('*')
                ->where([
                    ['id', '=', $request->id_race],
                    ['start', '<=', date('Y-m-d').' 00:00:00'],
                    ['end', '>=', date('Y-m-d').' 00:00:00']
                ])
                ->first();

            if(!$race){
                $data['code'] = 300;
                $data['error'] = 'Hoạt động không tồn tại.';
                return response()->json($data, 200);
            }

            $armorial = DB::table('armorials')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $race->id_armorial]
                ])
                ->first();

            $race_detail = DB::table('race_details')
                ->select('*')
                ->where([
                    ['athlete_id', '=', $user->id],
                    ['race_id', '=', $race->id]
                ])
                ->first();

            if (!$race_detail) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy đăng ký.';
                return response()->json($data, 200);
            }

            if ($race_detail->status == 1) {
                $data['code'] = 300;
                $data['error'] = 'Kết quả đã được ghi nhận.';
                return response()->json($data, 200);
            }
            
            $activitys = $this->race_activity_strava($token, strtotime($race->start), strtotime($race->end));

            if($activitys['code'] == 300){
                $data['code'] = 300;
                $data['error'] = 'Lỗi kết nối';
                return response()->json($data, 200);
            }

            foreach ($activitys['value'] as $key => $activity) {
                $start = strtotime($activity->start_date);
                $find_activitie = DB::table('activities')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id_strava', '=', $activity->id]
                    ])
                    ->first();

                if (!$find_activitie) {
                    $table = new activitie;
                    $table->id_athlete = $session_user->id;
                    $table->id_race = $race->id;
                    $table->id_strava = $activity->id;

                    $table->title = $activity->name;
                    $table->start_date_local = date("Y-m-d H:i:s", $start);
                    $table->distance = $activity->distance;
                    $table->elapsed_time = $activity->elapsed_time;
                    $table->moving_time = $activity->moving_time;
                    $table->save();
                } else {
                    DB::table('activities')
                        ->where('id', $find_activitie->id)
                        ->update(['id_race' => $race->id]);
                }
            }

            if ($race_detail->type == 0) {
                $activitie = DB::table('activities')
                    ->select('*')
                    ->where([
                        ['start_date_local', '>=', $race->start],
                        ['start_date_local', '<=', $race->end],
                        ['distance', '>=', $race_detail->distance],
                        ['id_athlete', '=', $user->id]
                    ])
                    ->orderBy('start_date_local', 'asc')
                    ->first();

                if($activitie){
                    DB::table('race_details')
                        ->where('id', $race_detail->id)
                        ->update([
                            'real_distance' => $activitie->distance,
                            'elapsed_time' => $activitie->elapsed_time,
                            'status' => 1
                        ]);

                    DB::table('athlete_arms')->insert(
                        [
                            'athlete_id' => $user->id,
                            'athlete_name' => $user->name,
                            'race_id' => $race->id,
                            'race_title' => $race->title,
                            'armorial_id' => $armorial->id,
                            'armorial_avatar' => $armorial->avatar,
                            'real_distance' => $activitie->distance,
                            'elapsed_time' => $activitie->elapsed_time,
                            'type' => $race->type,
                        ]
                    );

                    DB::table('alerts')
                        ->where([
                            ['id_athlete', $user->id],
                            ['id_race_detail', $race_detail->id],
                            ['step', 4]
                        ])
                        ->update([
                            'status' => 0,
                            'updated_at' => date("Y-m-d H:i:s")
                        ]);

                    DB::table('alerts')
                        ->where([
                            ['id_athlete', $user->id],
                            ['id_race_detail', $race_detail->id],
                            ['step','!=', 4]
                        ])
                        ->delete();

                } else {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy kết quả hợp lệ.';
                    return response()->json($data, 200);
                }
            }

            if($race_detail->type == 1){
                $activities = DB::table('activities')
                    ->select('*')
                    ->where([
                        ['start_date_local', '>=', $race->start],
                        ['start_date_local', '<=', $race->end],
                        ['id_athlete', '=', $user->id]
                    ])
                    ->orderBy('start_date_local', 'asc')
                    ->get();

                $real_distance = 0;
                $elapsed_time = 0;

                if (count(@$activities) != 0) {
                    foreach ($activities as $key => $activitie) {
                        $elapsed_time = $elapsed_time + $activitie->elapsed_time;
                        $real_distance = $real_distance + $activitie->distance;
                        if ($real_distance >= $race_detail->distance) {
                            break;
                        }
                    }
                }

                if($real_distance >= $race_detail->distance){

                    DB::table('race_details')
                        ->where('id', $race_detail->id)
                        ->update([
                            'real_distance' => $real_distance,
                            'elapsed_time' => $elapsed_time,
                            'status' => 1
                        ]);

                    DB::table('athlete_arms')->insert(
                        [
                            'athlete_id' => $user->id,
                            'athlete_name' => $user->name,
                            'race_id' => $race->id,
                            'race_title' => $race->title,
                            'armorial_id' => $armorial->id,
                            'armorial_avatar' => $armorial->avatar,
                            'real_distance' => $activitie->distance,
                            'elapsed_time' => $activitie->elapsed_time,
                            'type' => $race->type,
                        ]
                    );

                    DB::table('alerts')
                    ->where([
                        ['id_athlete', $user->id],
                        ['id_race_detail', $race_detail->id],
                        ['step', 4]
                    ])
                    ->update(['status' => 0]);

                    DB::table('alerts')
                        ->where([
                            ['id_athlete', $user->id],
                            ['id_race_detail', $race_detail->id],
                            ['step','!=', 4]
                        ])
                        ->delete();

                } else {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy kết quả hợp lệ.';
                    return response()->json($data, 200);
                }
            }

            $data['code'] = 200;
            $data['message'] = 'Gửi kết quả thành công.';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function change_avatar(Request $request)
    {
        try {

            // get user
            $session_user = $this->sessionUser->get();
            if (!$session_user || !isset($session_user->id)) {
                $data['code'] = 300;
                $data['error'] = 'Yêu cầu đăng nhập lại.';
                return response()->json($data, 200);
            }

            $athlete = athlete::where([
                ['id', '=', $session_user->id],
                ['del_flg', '=', 0],
            ])->first();

            $avatar_name = $athlete->avatar;

            if (!$athlete) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            $avatar = $request->avatar;
            $avatar_link['path'] = config('general.athlete_path');
            $avatar_link['url'] = config('general.athlete_url');
            $options['file_name'] = $avatar_name;

            $upload = ShopUpload::upload($avatar, $avatar_link, $options);
            if ($upload == 'error') {
                $data['code'] = 300;
                $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                return response()->json($data, 200);
            }
            $file_name = $upload ? $upload['file_name'] : $avatar_name;

            // save
            $athlete = new athlete();
            $athlete->exists = true;
            $athlete->id = $session_user->id;
            $athlete->avatar = $file_name;
            $athlete->updated_at = date("Y-m-d H:i:s");
            $athlete->save();
    
            $data['code'] = 200;
            $data['message'] = 'Đã thay hình đại diện';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function update_profile(Request $request)
    {
        try {
    
            // get user
            $session_user = $this->sessionUser->get();
            if (!$session_user || !isset($session_user->id)) {
                $data['code'] = 300;
                $data['error'] = 'Yêu cầu đăng nhập lại.';
                return response()->json($data, 200);
            }

            $athlete = athlete::where([
                ['id', '=', $session_user->id],
                ['del_flg', '=', 0],
            ])->first();

            if (!$athlete) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            // save
            $athlete = new athlete();
            $athlete->exists = true;
            $athlete->id = $session_user->id;
            $athlete->name = $request->name;
            $athlete->phone = $request->phone;
            $athlete->gender = $request->gender;
            $athlete->birthday = $request->birthday;
            $athlete->slogan = $request->slogan;
            $athlete->address = $request->address;
            $athlete->inst = $request->inst;
            $athlete->face = $request->face;
            $athlete->updated_at = date("Y-m-d H:i:s");
            $athlete->save();
    
            $data['code'] = 200;
            $data['message'] = 'Cập nhật thành công';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function update_account(Request $request)
    {
        try {

            // get user
            $session_user = $this->sessionUser->get();
            if (!$session_user || !isset($session_user->id)) {
                $data['code'] = 300;
                $data['error'] = 'Yêu cầu đăng nhập lại.';
                return response()->json($data, 200);
            }

            $athlete = athlete::where([
                ['id', '=', $session_user->id],
                ['del_flg', '=', 0],
            ])->first();

            if (!$athlete) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            $live_email = DB::table('athletes')
                ->select('*')
                ->where([
                    ['id', '!=', $session_user->id],
                    ['email', '=', $request->email]
                ])
                ->first();

            if ($live_email) {
                $data['code'] = 300;
                $data['error'] = 'Email đã có người sử dụng.';
                return response()->json($data, 200);
            }

            if (!password_verify(@$request->old_password, $athlete->password)) {

                $data['code'] = 300;
                $data['error'] = 'Mật khẩu củ không đúng.';
                return response()->json($data, 200);
            }

            $tb_athlete = athlete::find($athlete->id);
            $tb_athlete->email = $request->email;
            $tb_athlete->password = bcrypt($request->password);
            $tb_athlete->updated_at = date("Y-m-d H:i:s");
            $tb_athlete->save();
    
            $data['code'] = 200;
            $data['message'] = 'Lưu thành công';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function race_filter(Request $request)
    {
        try {

            // get user
            $session_user = $this->sessionUser->get();
            if (!$session_user || !isset($session_user->id)) {
                $data['code'] = 300;
                $data['error'] = 'Lỗi kết nối';
                return response()->json($data, 200);
            }

            $user = athlete::where([
                ['id', '=', $session_user->id],
                ['del_flg', '=', 0],
            ])->first();

            if (!$user) {
                $data['code'] = 300;
                $data['error'] = 'Lỗi kết nối';
                return response()->json($data, 200);
            }
            
            if ($request->id == 1) {

                $race_joins = DB::table('races')
                    ->join('race_details', 'race_details.race_id', '=', 'races.id')
                    ->select(
                        'races.*',
                        'race_details.distance as dt_distance',
                        'race_details.real_distance as dt_real_distance'
                    )
                    ->where([
                        ['races.del_flg', '=', 0],
                        ['race_details.athlete_id', '=', $session_user->id],
                    ])
                    ->orderBy('id', 'desc')
                    ->get();

                $view = View::make('frontend/user/_races', ['race_joins' => $race_joins]);

                $data['code'] = 200;
                $data['value'] = $view->render();
                return response()->json($data, 200);

            } else if($request->id == 2){
                $race_joins = DB::table('races')
                    ->join('race_details', 'race_details.race_id', '=', 'races.id')
                    ->select(
                        'races.*',
                        'race_details.distance as dt_distance',
                        'race_details.real_distance as dt_real_distance'
                    )
                    ->where([
                        ['races.del_flg', '=', 0],
                        ['race_details.athlete_id', '=', $session_user->id],
                        ['race_details.status', '=', 1],
                    ])
                    ->orderBy('id', 'desc')
                    ->get();

                $view = View::make('frontend/user/_races', ['race_joins' => $race_joins]);

                $data['code'] = 200;
                $data['value'] = $view->render();
                return response()->json($data, 200);

            } else if($request->id == 3){
                $race_joins = DB::table('races')
                    ->join('race_likes', 'race_likes.race_id', '=', 'races.id')
                    ->select(
                        'races.*'
                    )
                    ->where([
                        ['races.del_flg', '=', 0],
                        ['race_likes.athlete_id', '=', $session_user->id],
                    ])
                    ->orderBy('id', 'desc')
                    ->get();

                $view = View::make('frontend/user/_races', ['race_joins' => $race_joins]);

                $data['code'] = 200;
                $data['value'] = $view->render();
                return response()->json($data, 200);

            } else {
                $data['code'] = 300;
                $data['error'] = 'Lỗi kết nối';
                return response()->json($data, 200);
            }
    
        }  catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}