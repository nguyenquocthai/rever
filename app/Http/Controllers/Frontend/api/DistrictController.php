<?php

namespace App\Http\Controllers\Frontend\api;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\MDistrict;

class DistrictController extends BaseFrontendController
{

	// ------------------------------------------------------------------
    public function list_district(Request $request)
    {
        try {

        	$shop_districts = MDistrict::where([
                ['m_province_id', '=', @$request->id_province],
            ])->get();
            /*foreach ($shop_districts as $shop_district) {

            }*/

            $data['data'] = $shop_districts;
            $data['code'] = 200;
            $data['msg'] = 'Cập nhật thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}