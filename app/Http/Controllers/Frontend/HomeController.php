<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class HomeController extends BaseFrontendController
{
    public function __construct()
    {
        $this->boot();

        $phuonghuongs = ['Đông','Tây','Nam','Bắc','Đông Đông Bắc','Đông Đông Nam','Tây Tây Bắc','Tây Tây Nam','Bắc Đông Bắc','Bắc Tây Bắc','Nam Đông Nam','Nam Tây Nam'];
        View::share('phuonghuongs', $phuonghuongs);
    }
    public function index()
    {

        
        $product_cat1s = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],

            ])
            ->orderBy('id', 'asc')
            ->get();
        $thues = DB::table('item_projects')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 0],

            ])
            ->orderBy('updated_at', 'desc')->limit(6)
            ->get();
        
        $bans = DB::table('item_projects')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1],

            ])
            ->orderBy('updated_at', 'desc')->limit(6)
            ->get();
        $blogs = DB::table('news')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],

            ])
            ->orderBy('id', 'desc')->limit(6)
            ->get();
        $intro_left = DB::table('intros')
            ->select('*')
            ->where([
                ['id', '=', 4],

            ])
            ->first();
        $intro_right = DB::table('intros')
            ->select('*')
            ->where([
                ['id', '=', 5],

            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        

        return view('frontend.home.index')->with(compact('product_cat1s','intro_left','intro_right','thues','bans','blogs'));
    }

    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
    //-------------------------------------------------------------------------------
    public function send_contact(Request $request)
    {
        try {
            $user_id = $this->get_domain();

            $time = date("Y-m-d H:i:s");
            DB::table('customers')->insert(
                [
                    // 'user_id' => $user_id,
                    'id_group' => @$request->id_group,
                    'id_project' => @$request->id_project,
                    'id_itemproject' => @$request->id_itemproject,
                    
                    'name_contact' => @$request->name_contact,
                    'phone_contact' => @$request->phone_contact,
                    'email_contact' => @$request->email_contact,

                    'negotiable_price' => @$request->negotiable_price,
                    'comment' => @$request->comment,
                    'status' => @$request->status,
                    'created_at' => $time,
                    'updated_at' => $time,
                    'del_flg' => 0
                ]
            );
    
            $data['code'] = 200;
            $data['message'] = 'Gửi liên hệ thành công';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function contact(Request $request)
    {
        return view('frontend.home.lienhe');
    }

    public function searchban(Request $request)
    {
        $offset = 0;
        $limit = 9;

        $query_serach = $request->all();
        $where[] = ['del_flg', '=', 0];
        $where[] = ['type', '=', 1];
        $key_slug = $this->slug($request->keyword);

        if ($request->keyword != '') {
            $where[] = ['search_slug', 'like', '%'.$key_slug.'%'];
        }

        if ($request->type != '') {
            $where[] = ['type', '=', $request->type];
        }

        if ($request->tinhthanh != '') {
            $where[] = ['tinhthanh', '=', $request->tinhthanh];
        }

        if ($request->category != '') {
            $where[] = ['category_id', '=', $request->category];
        }

        if ($request->price) {

            $price = substr($request->price, 0, -1); //bỏ dấu , cuối cùng
            $price = explode(",",$request->price); //Chuyển thành array
            $price_min = $price[0];
            $price_max = $price[1];

            $where[] = ['allprice', '>=', $price_min];
            $where[] = ['allprice', '<=', $price_max];
        }

        $p_max = DB::table('item_projects')
            ->where($where)
            ->count();

        $product1s = DB::table('item_projects')
            ->select('*')
            ->where($where)
           
            ->orderBy('updated_at', 'desc')
            ->paginate(9);

        $count_products = DB::table('item_projects')
            ->select('*')
            ->where($where)->count();
        $type = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1]
            ])->get();

        $db_searchs = DB::table('db_searchs')
            ->select('*')
            ->where([
                ['id_group', '=', 2],
            ])
            ->orderBy('value_min', 'asc')
            ->get();

        if ($p_max == 0) {
            $where = [];
            $where[] = ['del_flg', '=', 0];
            $where[] = ['active', '=', 0];
            $where[] = ['updated_at', '>', $this->afterday()];
            $key_slug = $this->slug($request->keyword);

            $arrays = explode('-', $key_slug);
            $db_search_tags = DB::table('db_search_tags')
                ->select('title', 'slug')
                ->Where(function ($query) use($arrays) {
                    foreach ($arrays as $array) {
                        $query->orwhere('slug', 'like', '%'.$array.'%');
                    }       
                })
                ->get();

            $array_slugs = [];
            foreach ($db_search_tags as $key => $value) {
                if (strpos($key_slug, $value->slug) !== false) {
                    $array_slugs[] = $value->slug;
                }
            }
            $array_slugs[] = $key_slug;

            if ($request->type != '') {
                $where[] = ['type', '=', $request->type];
            }

            if ($request->tinhthanh != '') {
                $where[] = ['tinhthanh', '=', $request->tinhthanh];
            }

            if ($request->category != '') {
                $where[] = ['category_id', '=', $request->category];
            }

            if ($request->price) {

                $price = substr($request->price, 0, -1); //bỏ dấu , cuối cùng
                $price = explode(",",$request->price); //Chuyển thành array
                $price_min = $price[0];
                $price_max = $price[1];

                $where[] = ['allprice', '>=', $price_min];
                $where[] = ['allprice', '<=', $price_max];
            }

            $p_max = DB::table('item_projects')
                ->where($where)
                ->Where(function ($query) use($array_slugs) {
                    foreach ($array_slugs as $array_slug) {
                        if ($array_slug != '') {
                            $query->orwhere('search_slug', 'like', '%'.$array_slug.'%');
                        }
                    }       
                })
                ->count();

            $product1s = DB::table('item_projects')
                ->select('*')
                ->where($where)
                ->Where(function ($query) use($array_slugs) {
                    foreach ($array_slugs as $array_slug) {
                        if ($array_slug != '') {
                            $query->orwhere('search_slug', 'like', '%'.$array_slug.'%');
                        }
                    }         
                })
                
                ->orderBy('updated_at', 'desc')
                ->paginate(9);
        }

        foreach ($product1s as $key => $value) {

            $yeuthich = DB::table('yeuthichs')
                ->where([
                    ['idsp', '=', $value->id],
                    ['iduser', '=', @session('userSession')->id],
                    ['group', '=', 1],
                ])
                ->count();

            if ($yeuthich == 0) {
                $product1s[$key]->liked = 0;
            } else {
                $product1s[$key]->liked = 1;
            }
        }

        return view('frontend.home.searchban')->with(compact('product1s', 'limit', 'p_max', 'query_serach','count_products','type','db_searchs'));
    }
    public function searchthue(Request $request)
    {
        $offset = 0;
        $limit = 9;
        

        $query_serach = $request->all();
        $where[] = ['del_flg', '=', 0];
        $where[] = ['type', '=', 0];
        $key_slug = $this->slug($request->keyword);

        if ($request->keyword != '') {
            $where[] = ['search_slug', 'like', '%'.$key_slug.'%'];
        }

        if ($request->type != '') {
            $where[] = ['type', '=', $request->type];
        }

        if ($request->tinhthanh != '') {
            $where[] = ['tinhthanh', '=', $request->tinhthanh];
        }

        if ($request->category != '') {
            $where[] = ['category_id', '=', $request->category];
        }

        if ($request->price) {

            $price = substr($request->price, 0, -1); //bỏ dấu , cuối cùng
            $price = explode(",",$request->price); //Chuyển thành array
            $price_min = $price[0];
            $price_max = $price[1];

            $where[] = ['allprice', '>=', $price_min];
            $where[] = ['allprice', '<=', $price_max];
        }

        $p_max = DB::table('item_projects')
            ->where($where)
            ->count();

        $product1s = DB::table('item_projects')
            ->select('*')
            ->where($where)
           
            ->orderBy('updated_at', 'desc')
            ->paginate(9);

        $count_products = DB::table('item_projects')
            ->select('*')
            ->where($where)->count();
        $type = DB::table('product_cat1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 0]
            ])->get();

        $db_searchs = DB::table('db_searchs')
            ->select('*')
            ->where([
                ['id_group', '=', 1],
            ])
            ->orderBy('value_min', 'asc')
            ->get();

        if ($p_max == 0) {
            $where = [];
            $where[] = ['del_flg', '=', 0];
            $where[] = ['active', '=', 0];
            $where[] = ['updated_at', '>', $this->afterday()];
            $key_slug = $this->slug($request->keyword);

            $arrays = explode('-', $key_slug);
            $db_search_tags = DB::table('db_search_tags')
                ->select('title', 'slug')
                ->Where(function ($query) use($arrays) {
                    foreach ($arrays as $array) {
                        $query->orwhere('slug', 'like', '%'.$array.'%');
                    }       
                })
                ->get();

            $array_slugs = [];
            foreach ($db_search_tags as $key => $value) {
                if (strpos($key_slug, $value->slug) !== false) {
                    $array_slugs[] = $value->slug;
                }
            }
            $array_slugs[] = $key_slug;

            if ($request->type != '') {
                $where[] = ['type', '=', $request->type];
            }

            if ($request->tinhthanh != '') {
                $where[] = ['tinhthanh', '=', $request->tinhthanh];
            }

            if ($request->category != '') {
                $where[] = ['category_id', '=', $request->category];
            }

            if ($request->price) {

                $price = substr($request->price, 0, -1); //bỏ dấu , cuối cùng
                $price = explode(",",$request->price); //Chuyển thành array
                $price_min = $price[0];
                $price_max = $price[1];

                $where[] = ['allprice', '>=', $price_min];
                $where[] = ['allprice', '<=', $price_max];
            }

            $p_max = DB::table('item_projects')
                ->where($where)
                ->Where(function ($query) use($array_slugs) {
                    foreach ($array_slugs as $array_slug) {
                        if ($array_slug != '') {
                            $query->orwhere('search_slug', 'like', '%'.$array_slug.'%');
                        }
                    }       
                })
                ->count();

            $product1s = DB::table('item_projects')
                ->select('*')
                ->where($where)
                ->Where(function ($query) use($array_slugs) {
                    foreach ($array_slugs as $array_slug) {
                        if ($array_slug != '') {
                            $query->orwhere('search_slug', 'like', '%'.$array_slug.'%');
                        }
                    }         
                })
                
                ->orderBy('updated_at', 'desc')
                ->paginate(9);
        }

        foreach ($product1s as $key => $value) {

            $yeuthich = DB::table('yeuthichs')
                ->where([
                    ['idsp', '=', $value->id],
                    ['iduser', '=', @session('userSession')->id],
                    ['group', '=', 1],
                ])
                ->count();

            if ($yeuthich == 0) {
                $product1s[$key]->liked = 0;
            } else {
                $product1s[$key]->liked = 1;
            }
        }

        return view('frontend.home.searchthue')->with(compact('product1s', 'limit', 'p_max', 'query_serach','count_products','type','db_searchs'));
    }
}