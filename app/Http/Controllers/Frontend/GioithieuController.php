<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;

class GioithieuController extends BaseFrontendController
{
	public function __construct()
    {
        $this->boot();
        View::share('gioithieu_active', 'active');
    }

    public function index()
    {
        return view('frontend.gioithieu.index');
    }
    public function detail($slug)
    {
    	$new = DB::table('intros')
            ->select('*')
            ->where([
                ['slug', '=', $slug],
            ])
            ->first();
        return view('frontend.gioithieu.detail')->with(compact('new'));
    }
}