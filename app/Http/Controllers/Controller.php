<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Exceptions\ErrorCodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\historys;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $components = ['RequestHandler'];
    protected $resp = [
        'code' => ErrorCodes::E_OK,
        'message' => '',
        'data' => [],
        'error' => [],
    ];

    public function resp($code = null, $msg = null, $data = [])
    {
        if (!empty($code)) {
            $this->resp['code'] = $code;
        }
        if (!empty($msg)) {
            $this->resp['message'] = $msg;
        }
        if ($this->resp['code'] == ErrorCodes::E_OK) {
            $this->resp['data'] = $data;
        } else {
            $this->resp['error'] = $data;
        }
    }

    public function response()
    {
        // $this->set(array(
        //     'response' => $this->resp,
        //     '_serialize' => array('response')
        // ));
        // $this->response->header();
        echo json_encode($this->resp);
        exit;
        // $this->response->body(json_encode($this->resp));
        // $this->response->send();
        // $this->_stop();
    }

    public function exception($e)
    {
        CakeLog::write('alert', AppError::HEADER_ERROR_LOG);
        if (method_exists($e, 'getTrace')) {
            $trace = $e->getTrace();
        }
        $class = isset($trace[0]['class'])? $trace[0]['class']: '';
        $func = isset($trace[0]['function'])? $trace[0]['function']: '';
        CakeLog::write('error', empty($e->getMessage()) ? 'E_SYSTEM_ERROR' : $e->getMessage() . " <###> " . $class . ':' . $func . ':' . $e->getLine());
        $code = empty($e->getCode()) ? ErrorCodes::E_SYSTEM_ERROR : $e->getCode();
        $msg = empty($e->getMessage()) ? 'E_SYSTEM_ERROR' : $e->getMessage();
        $this->resp($code, $msg);
    }

    //-------------------------------------------------------------------------------
    public function GetImg($option = "") {

        $image_path = config('general.'.@$option['data'].'_path').@$option['avatar'];

        if (file_exists($image_path)) {
            $src = config('general.'. $option['data'] .'_url') . $option['avatar'] . '?'.@$option['time'];
        } else{
            $src = '/public/img/no-image.png';
        }

        if (isset($option['type'])) {
            return $src;
        } else {
            return '<div class="table_img" data-path="'.$src.'"><img alt="..."></div>';
        } 
    }

	//-------------------------------------------------------------------------------
    public function priceFormat($price, $options = []) {
        $options['default'] = isset($options['default']) ? $options['default'] : '0';

        if (isset($price) && $price != '' && $price != 0) {
            return number_format($price, 0, ',', '.') . ' đ';
        }
        return $options['default'];
    }

    public function image($link = null, $options = []) {

        return $link . '?=' . Date('U');
    }

    //-------------------------------------------------------------------------------
    public function fullBaseUrl()
    {
        //$domain = env("HTTP_HOST");
        $domain = $_SERVER['SERVER_NAME'];
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
            // this is HTTPS
            return 'https://' . $domain;
        } else {
            // this is HTTP
            return 'http://' . $domain;
        }
    }

    //-------------------------------------------------------------------------------
    public static function dateTime($date_time) {

        $lang = session('lang');

        if ($lang == '_vn' || !$lang) {
            if (isset($date_time) && $date_time != '') {
                return date('d/m/Y H:i:s', strtotime($date_time));
            }
        } else {
            return $date_time;
        }

        return '';
    }

    //-------------------------------------------------------------------------------
    public static function date($date) {

        $lang = session('lang');

        if ($lang == '_vn' || !$lang) {
            if (isset($date) && $date != '') {
                return date('d/m/Y', strtotime($date));
            }
        } else {
            if (isset($date) && $date != '') {
                return date('Y-m-d', strtotime($date));
            }
        }

        return '';
    }

    //-------------------------------------------------------------------------------
    public static function limit_text($text, $limit) {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return $text;
    }

    //-------------------------------------------------------------------------------
    public static function checkrule_admin($list_urls, $url_main, $role) {
        $result = 300;
        if ($role == 1) {
            $result = 200;
        } else {
            foreach ($list_urls as $list_url) {
                if(strpos($list_url, $url_main) != false) {
                    $result = 200;
                    break;
                }
            }
        }
        return $result;
    }

    //-------------------------------------------------------------------------------
    public function GetPos($table, $where = "")
    {
        $query['where'] = [];

        if ($where != "") {
            foreach ($where as $value) {
                $query['where'][] = $value;
            }
        }

        $last_pos = DB::table($table)
            ->select('position')
            ->where($query['where'])
            ->orderBy('position', 'desc')
            ->first();

        if ($last_pos) {
            $position = $last_pos->position + 1;
        } else {
            $position = 1;
        }

        return $position;
    }

    //-------------------------------------------------------------------------------
    public function UpPos($table, $where = "", $id) {

        $query['where'] = [
            ['del_flg', '=', 0],
            ['id', '=', $id]
        ];

        if ($where != "") {
            foreach ($where as $value) {
                $query['where'][] = $value;
            }
        }

        $find_pos = DB::table($table)
            ->select('position')
            ->where($query['where'])
            ->first();

        $query['where2'] = [
            ['del_flg', '=', 0],
            ['position', '>', $find_pos->position]
        ];

        if ($where != "") {
            foreach ($where as $value) {
                $query['where2'][] = $value;
            }
        }

        $find_up = DB::table($table)
            ->select('id', 'position')
            ->where($query['where2'])
            ->orderBy('position', 'asc')
            ->first();

        if($find_up && $find_pos){

            // save
            DB::table($table)
                ->where('id', $id)
                ->update(['position' => $find_up->position]);

            // save
            DB::table($table)
                ->where('id', $find_up->id)
                ->update(['position' => $find_pos->position]);

            $data['code'] = 200;
            $data['idnew'] = $id;
            $data['posnew'] = $find_up->position;
            $data['idold'] = $find_up->id;
            $data['posold'] = $find_pos->position;
            return $data;
        }

        $data['code'] = 300;
        $data['error'] = 'Đã ở vị trí đầu.';
        return $data;
    }

    //-------------------------------------------------------------------------------
    public function DownPos($table, $where = "", $id) {

        $query['where'] = [
            ['del_flg', '=', 0],
            ['id', '=', $id]
        ];

        if ($where != "") {
            foreach ($where as $value) {
                $query['where'][] = $value;
            }
        }

        $find_pos = DB::table($table)
            ->select('position')
            ->where($query['where'])
            ->first();

        $query['where2'] = [
            ['del_flg', '=', 0],
            ['position', '<', $find_pos->position]
        ];

        if ($where != "") {
            foreach ($where as $value) {
                $query['where2'][] = $value;
            }
        }

        $find_down = DB::table($table)
            ->select('id', 'position')
            ->where($query['where2'])
            ->orderBy('position', 'desc')
            ->first();

        if($find_down && $find_pos){

            // save
            DB::table($table)
                ->where('id', $id)
                ->update(['position' => $find_down->position]);

            // save
            DB::table($table)
                ->where('id', $find_down->id)
                ->update(['position' => $find_pos->position]);

            $data['code'] = 200;
            $data['idnew'] = $id;
            $data['posnew'] = $find_down->position;
            $data['idold'] = $find_down->id;
            $data['posold'] = $find_pos->position;
            return $data;
        }

        $data['code'] = 300;
        $data['error'] = 'Đã ở vị trí cuối.';
        return $data;
    }
    
    //-------------------------------------------------------------------------------
    public function ChangePos($table, $id, $id_swap) {

        try {
            DB::beginTransaction();

            $pos_old = DB::table($table)
                ->select('position')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            $pos_new = DB::table($table)
                ->select('position')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id_swap]
                ])
                ->first();

            if ($pos_new && $pos_old) {
                // save
                DB::table($table)
                    ->where('id', $id)
                    ->update(['position' => $pos_new->position]);

                // save
                DB::table($table)
                    ->where('id', $id_swap)
                    ->update(['position' => $pos_old->position]);
            } else {
                $data['code'] = 300;
                $data['error'] = 'Lỗi kết nối';
                return $data;
            }

            DB::commit();
            $data['code'] = 200;
            return $data;

        } catch (Exception $e) {

            DB::rollback();
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return $data;
        }
    }

    //-------------------------------------------------------------------------------
    public function SaveDB($request, $table="")
    {
        if (!array_key_exists('noimg', $request)) {

            // Create folder
            if ( !file_exists(config('general.'.$table.'_path')) )
                mkdir(config('general.'.$table.'_path'), config('permission_folder'), true);

            // upload avatar
            if(!array_key_exists('files', $request)) {
                $data['code'] = 300;
                $data['error'] = 'Hãy chọn hình đại diện';
                return $data;
            }

            $avatar = $request['files'][0];
            $avatar_link['path'] = config('general.'.$table.'_path');
            $avatar_link['url'] = config('general.'.$table.'_url');

            $upload = ShopUpload::upload($avatar, $avatar_link);
            if ($upload == 'error') {
                $data['code'] = 300;
                $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                return $data;
            }

            $file_name = $upload ? $upload['file_name'] : '';
        } else {
            $file_name = "";
        }

        // Get position
        $position = $this->GetPos($table.'s',"");

        $request['value']['avatar'] = $file_name;
        $request['value']['position'] = $position;
        $request['value']['created_at'] = date("Y-m-d H:i:s");
        $request['value']['updated_at'] = date("Y-m-d H:i:s");
        $request['value']['del_flg'] = 0;

        DB::table($table.'s')->insert($request['value']);

        $data['code'] = 200;
        $data['message'] = 'Lưu thành công';

        return $data;
    }

    //-------------------------------------------------------------------------------
    public function EditDB($request, $table="", $id="", $where=[])
    {
        $tables = $table.'s';

        // check $id isset in database
        $db_table = DB::table($tables)
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', $id]
            ])
            ->first();

        if (!$db_table) {
            $data['code'] = 300;
            $data['error'] = 'Không tồn tại id.';
            return $data;
        }

        if ($request['status_code'] == "up") {
            $pos = $this->UpPos($tables, $where, $id);
            return $pos;
        }

        if ($request['status_code'] == "down") {
            $pos = $this->DownPos($tables, $where, $id);
            return $pos;
        }

        if ($request['status_code'] == "change") {
            $pos = $this->ChangePos($tables, $id, $request['id_swap']);
            return $pos;
        }

        if ($request['status_code'] == "show") {

            $where[] = ['del_flg', '=', 0];
            $positions = DB::table($tables)
                ->select('id','position')
                ->where($where)
                ->orderBy('position', 'desc')
                ->get();

            $view = View::make('Backend/'.$table.'/_edit', ['data' => $db_table, 'positions' => $positions, 'table' => $table]);
            $output = $view->render();

            $data['code'] = 200;
            $data['data'] = $output;
            return $data;
        }

        if ($request['status_code'] == "change_status") {

            if ($db_table->status == 0) {
                $status = 1;
            } else {
                $status = 0;
            }

            DB::table($tables)
                ->where('id', $id)
                ->update([
                    'status' => $status,
                    'updated_at' => date("Y-m-d H:i:s")
                ]);

            $view = View::make('Backend/'.$table.'/_status', ['status' => $status]);

            $data['code'] = 200;
            $data['status'] = $view->render();
            return $data;
        }

        $file_name = $db_table->avatar;

        // Create folder
        if ( !file_exists(config('general.'.$table.'_path')) )
            mkdir(config('general.'.$table.'_path'), config('permission_folder'), true);

        // upload avatar
        if(array_key_exists('files', $request)) {
            $avatar = $request['files'][0];
            $avatar_link['path'] = config('general.'.$table.'_path');
            $avatar_link['url'] = config('general.'.$table.'_url');
            $options['file_name'] = $file_name;

            $upload = ShopUpload::upload($avatar, $avatar_link, $options);
            if ($upload == 'error') {
                $data['code'] = 300;
                $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                return $data;
            }

            $file_name = $upload ? $upload['file_name'] : $db_table->avatar;
        }

        $request['value']['avatar'] = $file_name;
        $request['value']['updated_at'] = date("Y-m-d H:i:s");

        DB::table($tables)
            ->where('id', $id)
            ->update($request['value']);

        $row = DB::table($tables)->where('id', $id)->first();

        $data['code'] = 200;
        $data['row'] = $row;
        $data['message'] = 'Cập nhật thành công';
        return $data;
    }
    

    //--------------------------------------------------------------------------
    public function GetImg_seo($option = "") {
        $tail = '?'.$this->slug(@$option['time']);

        $host_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
        $image_path = config('general.'.@$option['data'].'_path').@$option['avatar'];

        if (file_exists($image_path) && $option['avatar'] != "") {
            $src = $host_link.config('general.'. $option['data'] .'_url') . $option['avatar'].$tail;
        } else{
            $src = $host_link.'/public/img/no-image.png';
        }

        return $src;
    }

    //-------------------------------------------------------------------------------
    public static function price_format($price, $options = []) {
        $options['default'] = isset($options['default']) ? $options['default'] : '0';

        if (isset($price) && $price != '' && $price != 0) {
            return number_format($price, 0, ',', '.') . ' đ';
        }
        return $options['default'];
    }

    //-------------------------------------------------------------------------------
    public static function RandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //-------------------------------------------------------------------------------
    public function RandID($id='', $length)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString.$id;
    }

    //-------------------------------------------------------------------------------
    public function ErrorList($array)
    {
        $array = json_decode($array, true);

        $list = '<ul>';

        foreach ($array as $key => $value) {
            foreach ($value as $key => $data) {
                $list = $list.'<li>- '.$data.'</li>';
            }
        }

        return $list.'</ul>';
    }

    //-------------------------------------------------------------------------------
    public function Datelist($from,$to)
    {
        $dates = array();
        $start = strtotime($from);
        $end = strtotime($to);
        while($start <= $end)
        {
            $dates[]=date("Y-m-d",$start);
            $start=strtotime("+1 day",$start);
        }

        return $dates;
    }

    //-------------------------------------------------------------------------------
    public static function datevn($date) {

        $lang = session('lang');

        if ($lang == '_vn' || !$lang) {
            if (isset($date) && $date != '') {
                return date('d/m/Y', strtotime($date));
            }
        } else {
            if (isset($date) && $date != '') {
                return date('Y-m-d', strtotime($date));
            }
        }

        return '';
    }

    //-------------------------------------------------------------------------------
    public static function changedate($date, $lang = "") {

        if ($lang == 'vn' || $lang == "") {
            if (isset($date) && $date != '') {
                return date('d/m/Y', strtotime($date));
            }
        } else {
            if (isset($date) && $date != '') {
                $date = str_replace('/', '-', $date);
                return date('Y-m-d', strtotime($date));
            }
        }

        return '';
    }

    //-------------------------------------------------------------------------------
    public function google_captcha($recaptcha)
    {
        $secret = config('general.secretkey');

        $repoin = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$recaptcha);

        $check = json_decode($repoin);

        if ($check->success != 1) {
            $data['code'] = 300;
            $data['value'] = $check->{'error-codes'}[0];
            $data['error'] = 'Captcha không hợp lệ. Vui lòng tải lại trang để tiếp tục.';
        } else {
            $data['code'] = 200;
        }

        return $data;
    }

    //-------------------------------------------------------------------------------
    public function dayispast($date)
    {
        if(strtotime($date) < strtotime("today")){
            return true;
        } elseif(strtotime($date) == strtotime("today")){
            return false;
        } else {
            return false;
        }
    }

    //-------------------------------------------------------------------------------
    public function timeispast($date)
    {
        if(strtotime($date) < time()){
            return true;
        } elseif(strtotime($date) == time()){
            return false;
        } else {
            return false;
        }
    }

    //-------------------------------------------------------------------------------
    public function timein($date1,$date2)
    {
        if(time() > strtotime($date1) && time() < strtotime($date2)){
            return true;
        }

        return false;
    }

    //-------------------------------------------------------------------------------
    public function change_his($sec)
    {
        return gmdate("H:i:s", $sec);
    }

    //-------------------------------------------------------------------------------
    public function changekm($km) {
        return number_format($km/1000, 2, '.', '') + 0;
    }

    //-------------------------------------------------------------------------------
    public function changetime($code, $date)
    {
        //code exm '+24 hour'
        $time1 = strtotime($date);
        $time2 = strtotime($code, $time1);
        return date("Y-m-d H:i:s", $time2);
    }

    //-------------------------------------------------------------------------------
    public function curlink($get = 0)
    {
        $linkpage = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $domanpage = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

        if ($get == 0) {
            return $domanpage;
        } else {
            return $linkpage;
        }
    }

    //-------------------------------------------------------------------------------
    public function slug($str) {
        $str = trim($str);
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        $str = preg_replace('/[^\p{L}\p{N}]/u', '-', $str);
        return strtolower($str);
    }

    //-------------------------------------------------------------------------------
    public function DB_insert($insert_values, $table)
    {
        $insert_values['created_at'] = date("Y-m-d H:i:s");
        $insert_values['updated_at'] = date("Y-m-d H:i:s");
        $insert_values['del_flg'] = 0;
        $id = DB::table($table)->insertGetId($insert_values);
        return $id;
    }

    //-------------------------------------------------------------------------------
    public function DB_update($update_values, $table, $id)
    {
        $update_values['updated_at'] = date("Y-m-d H:i:s");
        DB::table($table)
            ->where('id', $id)
            ->update($update_values);
    }

    //-------------------------------------------------------------------------------
    public function show_error($error)
    {
        $error2 = "";
        foreach ($error as $key => $value) {
            foreach ($error[$key] as $key2 => $value2) {
                $error2 = $error2.'<p>'.$value2.'</p>';
            }
        }
        return $error2;
    }
}
