<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Exceptions\ErrorCodes;
use App\Quotation;
use App\Model\Nguoidung;
use App\Model\User;

class BaseController extends Controller
{
	protected $resp = [
        'code' => ErrorCodes::E_OK,
        'message' => '',
        'data' => [],
        'error' => [],
    ];

    public function __construct()
    {
        $this->boot();
    }

    public function boot()
    {
        $this->middleware(function ($request, $next) {
            $lang = session('lang');
            if ($lang == null) {
                session(['lang' => '_vn']);
                $lang = session('lang');
            } else {
                $lang = session('lang');
            }
            if ($lang == '_vn') {
                $lang = '';
            }
            View::share('lang', $lang);

            $session_sosanh = session('sosanh');
            $id_sosanh = [];
            if ($session_sosanh !== null) {
                $id_sosanh = session('sosanh');
            }

            View::share('id_sosanh', $id_sosanh);
            
            return $next($request);
        });

        $setting = DB::table('settings')
            ->select('*')
            ->where('key', '=', 'info_website')
            ->first();
        $info_web = json_decode($setting->value, true);
        
        View::share('info_web', $info_web);
        View::share('google_analytics', $setting->google_analytics);
        View::share('facebook_code', $setting->facebook_code);

        $cat_duans = DB::table('product_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])
            ->orderBy('position', 'desc')
            ->get();
        View::share('cat_duans', $cat_duans);

        $product_cat1s = DB::table('product_cat1s')
            ->select('*')
            ->where('del_flg', '=', 0)
            ->orderBy('position', 'desc')
            ->get();
        View::share('product_cat1s', $product_cat1s);

        $product_cat2s = DB::table('product_cat2s')
            ->select('*')
            ->where('del_flg', '=', 0)
            ->orderBy('id', 'desc')
            ->get();
        View::share('product_cat2s', $product_cat2s);

        $popups = DB::table('popups')
            ->select('*')
            ->where('del_flg', '=', 0)
            ->orderBy('id', 'desc')
            ->get();
        View::share('popups', $popups);

        $new_cats = DB::table('new_cats')
            ->select('*')
            ->where('del_flg', '=', 0)
            ->orderBy('id', 'desc')
            ->get();
        View::share('new_cats', $new_cats);

        $intro = DB::table('intros')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', 4]
            ])
            ->first();

        View::share('intro', $intro);

        $pages = DB::table('pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', 4]
            ])
            ->get();

        $pages = DB::table('pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->offset(0)
            ->limit(3)
            ->orderBy('position', 'desc')
            ->get();

        View::share('pages', $pages);

        $langs = DB::table('langs')
            ->select('*')
            ->get()->toArray();

        View::share('langs', $langs);
    }

    public function getYoutubeEmbedUrl($url){
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
        $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

        if (preg_match($longUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }

        if (preg_match($shortUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
        return 'https://www.youtube.com/embed/' . @$youtube_id ;
    }

    public function resp($code = null, $msg = null, $data = [])
    {
        if (!empty($code)) {
            $this->resp['code'] = $code;
        }
        if (!empty($msg)) {
            $this->resp['message'] = $msg;
        }
        if ($this->resp['code'] == ErrorCodes::E_OK) {
            $this->resp['data'] = $data;
        } else {
            $this->resp['error'] = $data;
        }
    }

    public function response()
    {
        echo json_encode($this->resp);
        exit;
    }

    public function exception($e)
    {

    }

    public function error404()
    {
        $this->resp(ErrorCodes::E_NOT_FOUND, 'E_NOT_FOUND');
        return json_encode(['response' => $this->resp]);
    }

    //-------------------------------------------------------------------------------
    public function BaseUrl()
    {
        //$domain = env("HTTP_HOST");
        $domain = $_SERVER['SERVER_NAME'];
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
            // this is HTTPS
            return 'https://' . $domain;
        } else {
            // this is HTTP
            return 'http://' . $domain;
        }
    }

    public function fullBaseUrl($value='')
    {
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        return $actual_link;
    }

    //-------------------------------------------------------------------------------
    public static function date($date) {

        $lang = session('lang');

        if ($lang == '_vn') {
            if (isset($date) && $date != '') {
                return date('d/m/Y', strtotime($date));
            }
        } else {
            if (isset($date) && $date != '') {
                return date('Y-m-d', strtotime($date));
            }
        }

        return '';
    }

    //-------------------------------------------------------------------------------
    public function show_error($error)
    {
        $error2 = "";
        foreach ($error as $key => $value) {
            foreach ($error[$key] as $key2 => $value2) {
                $error2 = $error2.'<p>'.$value2.'</p>';
            }
        }
        return $error2;
    }

    //-------------------------------------------------------------------------------
    public function google_captcha($recaptcha)
    {
        $secret = config('general.secretkey');

        $repoin = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$recaptcha);

        $check = json_decode($repoin);

        if ($check->success != 1) {
            $data['code'] = 300;
            $data['value'] = $check->{'error-codes'}[0];
            $data['error'] = 'Captcha không hợp lệ. Vui lòng tải lại trang để tiếp tục.';
        } else {
            $data['code'] = 200;
        }

        return $data;
    }

    //-------------------------------------------------------------------------------
    public function bd_nice_number($n) {
        $lang = session('lang');
        // first strip any formatting;
        $n = (0+str_replace(",","",$n));
        
        // is this a number?
        if(!is_numeric($n)) return false;
        
        if ($lang == '_vn') {
            // now filter it;
            if($n>=1000000000000) return round(($n/1000000000000),1).'ngìn tỷ';
            else if($n>=1000000000) return round(($n/1000000000),1).'tỷ';
            else if($n>=1000000) return round(($n/1000000),1).'tr';
            else if($n>=1000) return round(($n/1000),1).'k';
        } else {
            if($n>=1000000000000) return round(($n/1000000000000),1).' TB';
            else if($n>=1000000000) return round(($n/1000000000),1).' Billion';
            else if($n>=1000000) return round(($n/1000000),1).' Million';
        }
        
        return number_format($n);
    }

    //-------------------------------------------------------------------------------
    public function get_db()
    {
        $lv = session('admin')->lv;
        if ($lv == 0) {
            $database = 'mysql';
        } else {
            $database = 'mysql_sub';
        }

        return $database;
    }

    //-------------------------------------------------------------------------------
    public function check_user()
    {
        $token_admin = session('admin');
        if ($token_admin != null) {

            if ($token_admin->lv == 0) {
                $user_find = User::where([
                    ['del_flg', '=', 0],
                    ['remember_token', '=', $token_admin->id],
                ])->first();

                if($user_find){
                    $data['code'] = 200;
                    $data['value'] = $user_find;
                    $data['message'] = 'Tồn tại user';
                } else {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy';
                }
            }

            if ($token_admin->lv == 1) {
                $user_find = Nguoidung::where([
                    ['del_flg', '=', 0],
                    ['id', '=', $token_admin->id],
                ])->first();
                if($user_find){
                    $data['code'] = 200;
                    $data['value'] = $user_find;
                    $data['message'] = 'Tồn tại user';
                } else {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy';
                }
            }

        } else {
            $data['code'] = 300;
            $data['error'] = 'Không tìm thấy';
        }

        return $data;
    }

    //-------------------------------------------------------------------------------
    public function DB_insert($insert_values, $table)
    {
        $insert_values['created_at'] = date("Y-m-d H:i:s");
        $insert_values['updated_at'] = date("Y-m-d H:i:s");
        $insert_values['del_flg'] = 0;
        $id = DB::table($table)->insertGetId($insert_values);
        return $id;
    }

    //-------------------------------------------------------------------------------
    public function DB_update($update_values, $table, $id)
    {
        $update_values['updated_at'] = date("Y-m-d H:i:s");
        DB::table($table)
            ->where('id', $id)
            ->update($update_values);
    }

    //-------------------------------------------------------------------------------
    public function slug($title) {
        $replacement = '-';
        $map = array();
        $quotedReplacement = preg_quote($replacement, '/');
        $default = array(
            '/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|å/' => 'a',
            '/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|ë/' => 'e',
            '/ì|í|ị|ỉ|ĩ|Ì|Í|Ị|Ỉ|Ĩ|î/' => 'i',
            '/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|ø/' => 'o',
            '/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|ů|û/' => 'u',
            '/ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ/' => 'y',
            '/đ|Đ/' => 'd',
            '/ç/' => 'c',
            '/ñ/' => 'n',
            '/ä|æ/' => 'ae',
            '/ö/' => 'oe',
            '/ü/' => 'ue',
            '/Ä/' => 'Ae',
            '/Ü/' => 'Ue',
            '/Ö/' => 'Oe',
            '/ß/' => 'ss',
            '/[^\s\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu' => ' ',
            '/\\s+/' => $replacement,
            sprintf('/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement) => '',
        );
        //Some URL was encode, decode first
        $title = urldecode($title);
        $map = array_merge($map, $default);
        return strtolower(preg_replace(array_keys($map), array_values($map), $title));
    }

    //-------------------------------------------------------------------------------
    public function afterday()
    {
        $day = date('Y-m-d H:i:s', strtotime('-7 day', strtotime(date("Y-m-d H:i:s"))));
        return $day;
    }
}
