WebWorldApp.controller('new_cats.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Thông tin';

        // Title block
        $scope.detail_block_title = 'Thông tin';
        $scope.detail_block_title2 = 'Thông tin';
        $scope.image_block_title = 'Hình ảnh';

        $id_group = 2;
        $scope.img_phong = [];

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // LOAD DATA
            commonService.requestFunction('show_new_cat/' + $routeParams.id, {}, function(e) {
                if (!e.data) {
                    $location.path('/admin/new_cats');
                    return false;
                }
                $scope.new_cat = e.data;
                $('#load_medias').html($scope.new_cat.medias);
                $('#load_tienich').html($scope.new_cat.tienichs);
                $('#load_tq').html($scope.new_cat.tongquans);


                $('.input-group.date').datepicker({
                    format: 'dd/mm/yyyy',
                });
                // load image avatar
                testImage(e.data['avatar'], attr_url_image);

                if($scope.new_cat.type == 1){
                    
                    $rootScope.app.title = 'Bất động sản bán';
                    $scope.detail_block_title = 'Details in transplant';
                    $scope.detail_block_title2 = 'Thông tin bán';
                }
                
                $('.tb_active').val($scope.new_cat.active).trigger('change');
                $('.metarobot').val($scope.new_cat.metarobot).trigger('change');
            
                select2s('#category_id',{
                    commonService: commonService,
                    name:'product_cat1s',
                    have_default: true,
                    selected: e.data.category_id,
                    //title: 'code',
                    //limit: 100,
                    //where: ['type,=,1'],
                });
                
                load_list_box('search_val',e.data.search_val);

                tinymce.get('tinymce').setContent(html_entity_decode($scope.new_cat.content) || '');

            });

            // Tiny mce
            tinymce.remove();
            load_tinymce('#tinymce', null);

            $.ajax({
                type: 'GET',
                url: '/datatable_store2?admin=1',

                success: function(e) {
                    $scope.datatable2 = $('#tbl-dataz2').DataTable({
                        order: [[ 1, 'desc' ]],
                        columnDefs: [
                            { sortable: false, searchable: false, targets: [ 0 ] },
                            { class: 'hidden', targets: [ 4 ] }
                        ],
                        displayStart: 0,
                        displayLength: 5,
                        data: e.value,
                        autoWidth: false
                    });
                    $scope.datatable2.search('').draw();
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.new_cat.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.search_datatable = function() {
            var keyword = $('#keyword').val() || '';
            $scope.datatable.search(keyword).draw();
        };

        //-------------------------------------------------------------------------------
        $scope.search_datatable2 = function() {
            var keyword = $('#keyword2').val() || '';
            $scope.datatable2.search(keyword).draw();
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };


        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            

            var files = [];
            if ($("#upload_input").get(0).files[0])
                files = files.concat($("#upload_input").get(0).files[0]);

            $scope.new_cat['_method'] = 'PUT';
            
            $scope.new_cat.content = tinymce.get('tinymce').getContent();
            
            $scope.new_cat.slug = slugify($scope.new_cat.title);
            
            $scope.new_cat.status_code = 'edit_b1';

            fileUpload.uploadFileToUrl(files, $scope.new_cat, 'update_new_cat/' + $routeParams.id, function(e) {
                switch (e.code) {
                    case 200:
                        
                        $location.path('/admin/new_cats');
                        
                        break;
                    default:
                        break;
                }
            });
        }
        //-------------------------------------------------------------------------------
        $(".fileUpload").change(readURL);
        $("form").on('click', '.delbtn', function (e) {
            reset_inputfile($(this));
            button.innerText = 'Chọn hình';
        });

        var button = document.getElementById('upload_button');
        var input  = document.getElementById('upload_input');


        input.style.display = 'none';
        button.style.display = 'initial';

        button.addEventListener('click', function (e) {
            e.preventDefault();
            input.click();
        });

        input.addEventListener('change', function () {
           if( this.value != '')
            button.innerText = 'Đổi hình';
        });

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

        //----------------------------------------------------------------------------------
        $('#modalMap').on('shown.bs.modal', function() {
            // GOOGLE MAP
            $scope.pointer = $scope.pointer || {};
            if ($scope.new_cat.map == "" || $scope.new_cat.map == null) {
               $scope.new_cat.map = "10.824758628439996,106.62956724047547";
            }
            var geo = $scope.new_cat.map.split(',');
            if (geo.length == 2) {
                $scope.pointer.latitude = geo[0];
                $scope.pointer.longitude = geo[1];
            }

            init_google_map($scope, {
                marker: $scope.marker,
                pointer: $scope.pointer,
            });
        });

        //-------------------------------------------------------------------------------
        $scope.apply_geolocation = function() {
            $scope.new_cat.map = $scope.pointer.latitude + ',' +$scope.pointer.longitude;
            $("#modalMap").modal('toggle');
        };


        //-------------------------------------------------------------------------------
        

        //-------------------------------------------------------------------------------
        delete_media = function(id) {
            confirmPopup('Xóa', 'Bạn muốn xóa hình này' , function() {
                commonService.requestFunction('delete_media/' + id, {}, function(e) {
                    switch (e.code) {
                        case 200:
                           $('.item-load-media[data-id="'+id+'"]').remove();
                        default:
                            break;
                    }
                });
            });
        };

        $('.tinhthanh').change(function(event) {
            var check = $(this).attr('data-ready');

            if (check == 1) {
                var tinhthanh = $(this).val();
                $('.quanhuyen').empty();
                // quanhuyen
                quanhuyen(null, {
                    commonService: commonService,
                    tinhthanh: tinhthanh,
                    have_default: true,
                    callback: function(e) {
                        e.val(0).trigger('change');
                    }
                });
            }
        });

        //-------------------------------------------------------------------------------
        $scope.add_user = function() {
            var list_ids = [];
            
            $('.check_player').each(function(i, e) {
                var value = {};
                var check_id = this.checked ? '1' : '0';
                if (check_id == '1') {
                    value['data'] = $(this).val();
                    value['id'] = $(this).attr('data-id');
                    value['title'] = $(this).attr('data-title');
                    value['title_en'] = $(this).attr('data-title_en');
                    value['value'] = $(this).attr('data-value');
                    value['value_en'] = $(this).attr('data-value_en');
                    list_ids.push(value);
                }
            });

            if (list_ids == '') {
                alert('Chưa chọn bất kỳ tổng quan...')
                return false;
            }

            $('#all_id_player').prop('checked', false);

            $('.check_player').prop('checked', false);
        };

        //-------------------------------------------------------------------------------
        $('#add_tienich').click(function(event) {
            var tienich = [];
            var file = [];

            var data = $('#data_icon').val();
            var title = $('#title_tienich').val();

            if (data == '') {
                alert('Chưa chọn icon');
                return false;
            }

            if (title == '') {
                alert('Chưa nhập tên tiện ích');
                return false;
            }

            $scope.tienich['data'] = data;
            $scope.tienich['title'] = title;
            $scope.tienich['id_product'] = $routeParams.id;
            $scope.tienich['id_group'] = $id_group;

            fileUpload.uploadFileToUrl(file, $scope.tienich, 'create_tienich', function(e) {
                switch (e.code) {
                    case 200:
                        $('#load_tienich').prepend('<li data-id="'+e.data.id+'" class="col-sm-3 item-tienich"><div class="item-mod"><span class="icon-mod"><i class="'+e.data.data+'"></i></span><span class="title-mod">'+e.data.title+'</span><span class="title-mod _en">'+e.data.title_en+'</span><i onclick="delete_tienich('+e.data.id+')" class="fas fa-times delete-mod"></i></div></li>');
                        $('#data_icon').val('');
                        $('#title_tienich').val('');
                        break;
                    default:
                        break;
                }
            });
        });

        
        // Tiện ích-------------------------------------------------------------------------
        //-------------------------------------------------------------------------------
        $('.themtienich').click(function(event) {
            $('#modal_tienich').modal('show');
        });

        $('#all_id_player2').change(function(){
            var check = this.checked ? '1' : '0';
            if (check == 1) {
                $('.check_player2').prop('checked', true);
            } else {
                $('.check_player2').prop('checked', false);
            }
        });

        //-------------------------------------------------------------------------------
        $scope.add_user2 = function() {
            var list_ids2 = [];
            
            $('.check_player2').each(function(i, e) {
                var value = {};
                var check_id = this.checked ? '1' : '0';
                if (check_id == '1') {
                    value['data'] = $(this).val();
                    value['id'] = $(this).attr('data-id');
                    value['title'] = $(this).attr('data-title');
                    value['title_en'] = $(this).attr('data-title_en');
                    list_ids2.push(value);
                }
            });

            if (list_ids2 == '') {
                alert('Chưa chọn bất kỳ tiện ích...')
                return false;
            }

            list_tienich(list_ids2);

            $('#modal_tienich').modal('hide');

            $('#all_id_player2').prop('checked', false);

            $('.check_player2').prop('checked', false);
        };

        


        function sort2() {

            // Do nothing if sort value did not change.
            var currentSort = sortField2.value;
            if (sortFieldValue2 === currentSort) {
                return;
            }

            // If we are changing from "order" sorting to something else
            // let's store the drag order.
            if (sortFieldValue2 === 'order') {
                dragOrder2 = grid2.getItems();
            }

            // Sort the items.
            grid2.sort2(
                currentSort === 'title' ? compareItemTitle2 :
                currentSort === 'color' ? compareItemColor2 :
                dragOrder2
            );

            // Update indices and active sort value.
            updateIndices2();
            sortFieldValue2 = currentSort;

        }

        function addItems2() {

            var arr2 = [];

            // Generate new_cat elements.
            var new_catElems2 = generateElements2(arr2);

            // Set the display of the new_cat elements to "none" so it will be hidden by
            // default.
            new_catElems2.forEach(function (item) {
                item.style.display = 'none';
            });

            // Add the elements to the grid.
            var new_catItems = grid2.add(new_catElems2);

            // Update UI indices.
            updateIndices2();

            // Sort the items only if the drag sorting is not active.
            if (sortFieldValue2 !== 'order') {
                grid2.sort2(sortFieldValue2 === 'title' ? compareItemTitle2 : compareItemColor2);
                dragOrder2 = dragOrder2.concat(new_catItems);
            }

            // Finally filter2 the items.
            filter2();

        }

        function removeItem2(e) {

            var elem = elementClosest2(e.target, '.item');
            grid2.hide(elem, {
                onFinish: function (items) {
                    var item = items[0];
                    grid2.remove(item, {
                        removeElements: true
                    });
                    if (sortFieldValue2 !== 'order') {
                        var itemIndex = dragOrder2.indexOf(item);
                        if (itemIndex > -1) {
                            dragOrder2.splice(itemIndex, 1);
                        }
                    }
                }
            });
            updateIndices2();

        }

        function changeLayout2() {

            layoutFieldValue2 = layoutField2.value;
            grid2._settings.layout = {
                horizontal: false,
                alignRight: layoutFieldValue2.indexOf('right') > -1,
                alignBottom: layoutFieldValue2.indexOf('bottom') > -1,
                fillGaps: layoutFieldValue2.indexOf('fillgaps') > -1
            };
            grid2.layout();

        }

        //
        // Generic helper functions
        //

        function generateElements2(arr2) {

            var ret = [];
            for (var i = 0; i < arr2.length; i++) {
                ret.push(generateElement2(
                    ++uuid2,
                    arr2[i].id,
                    arr2[i].data,
                    arr2[i].title,
                    arr2[i].title_en,
                ));
            }
            return ret;
        }

        function generateElement2(id, id_tq, data, title, title_en) {

            var itemElem = document.createElement('div');
            var classNames = 'item h1 w2';
            var itemTemplate = '' +
                '<div class="' + classNames + '" data-id="' + id + '" data-color="green" data-title="' + title + '">' +
                '<div class="item-content">' +
                '<div class="card">' +
                '<div class="card-id">' + id + '</div>' +
                '<div class="card_child">' +
                '<div class="card-id_tq">' + id_tq + '</div>' +
                '<div class="card-id_data"><i class="' + data + '"></i></div>' +
                '<div class="card-title">' + title + '</div>' +
                '<div class="card-title_en">' + title_en + '</div>' +
                '</div>' +
                '<div class="card-remove"><i class="fa fa-trash-o" aria-hidden="true"></i></div>' +
                '</div>' +
                '</div>' +
                '</div>';

            itemElem.innerHTML = itemTemplate;
            return itemElem.firstChild;
        }

        function getRandomItem2(collection) {

            return collection[Math.floor(Math.random() * collection.length)];

        }

        // https://stackoverflow.com/a/7228322
        function getRandomInt2(min, max) {

            return Math.floor(Math.random() * (max - min + 1) + min);

        }

        function generateRandomWord2(length) {

            var ret = '';
            for (var i = 0; i < length; i++) {
                ret += getRandomItem2(characters2);
            }
            return ret;

        }

        function compareItemTitle2(a, b) {

            var aVal = a.getElement().getAttribute('data-title') || '';
            var bVal = b.getElement().getAttribute('data-title') || '';
            return aVal < bVal ? -1 : aVal > bVal ? 1 : 0;

        }

        function compareItemColor2(a, b) {

            var aVal = a.getElement().getAttribute('data-color') || '';
            var bVal = b.getElement().getAttribute('data-color') || '';
            return aVal < bVal ? -1 : aVal > bVal ? 1 : compareItemTitle2(a, b);

        }

        function updateIndices2() {

            grid2.getItems().forEach(function (item, i) {
                item.getElement().setAttribute('data-id', i + 1);
                item.getElement().querySelector('.card-id').innerHTML = i + 1;
            });

        }

        function elementMatches2(element, selector) {

            var p = Element.prototype;
            return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector).call(element, selector);

        }

        function elementClosest2(element, selector) {

            if (window.Element && !Element.prototype.closest) {
                var isMatch = elementMatches2(element, selector);
                while (!isMatch && element && element !== document) {
                    element = element.parentNode;
                    isMatch = element && element !== document && elementMatches2(element, selector);
                }
                return element && element !== document ? element : null;
            } else {
                return element.closest(selector);
            }

        }

        //
        // Fire it up!
        //

        

    }
]);
