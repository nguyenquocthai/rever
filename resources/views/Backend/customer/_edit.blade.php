<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="next-label">Hình</label>
            <div class="file_upload_box">
                <input type="file" class="file_image">
                <img class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $data->avatar,'data' => $table, 'time' => $data->updated_at])}}" alt="...">
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="form-group">
            <label>Tên</label>
            <input value="{{$data->name_contact}}" type="text" class="form-control" name="name_contact" >
        </div>

        <div class="form-group">
            <label class="next-label">Trạng thái</label>
            <select class="form-control" ng-required="true" name="trang_thai">
                <option @if($data->trang_thai == 1) selected @endif value="1">Kích hoạt</option>
                <option @if($data->trang_thai == 0) selected @endif value="0">Tạm dừng</option>
            </select>
        </div>
        <div class="form-group">
            <label class="next-label">Email</label>
            <input type="text" class="form-control" name="email_contact"  value="{{$data->email_contact}}">
        </div>
        <div class="form-group">
            <label class="next-label">Điện thoại</label>
            <input type="text" class="form-control" name="phone_contact"  value="{{$data->phone_contact}}">
        </div>
        <div class="form-group">
            <label class="next-label">Comment</label>
            <textarea class="form-control" name="comment" rows="5">{{$data->comment}}</textarea>
        </div>

        <div class="form-group">
            <label class="next-label">Ghi chú</label>
            <textarea class="form-control" name="comment" rows="5">{{$data->comment}}</textarea>
        </div>
    </div>
</div>
