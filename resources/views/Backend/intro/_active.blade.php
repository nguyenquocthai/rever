<?php
    $class_name = '';
    $text = '';

    switch ($active) {
        case 0:
            $class_name = 'green';
            $text = 'Approved';
            break;

        case 1:
            $class_name = 'red';
            $text = 'Pending';
            break;

        case 2:
            $class_name = 'red';
            $text = 'Draft';
            break;

        case 4:
            $class_name = 'red';
            $text = 'Deleted';
            break;

        case 5:
            $class_name = 'red';
            $text = 'Do not browse';
            break;

        default:
            $class_name = 'red';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Error';
            break;
    }
?>
<span class="hidden">{{$active}}</span>
<span class="<?= $class_name ?>">
    <?= $text; ?>
</span>