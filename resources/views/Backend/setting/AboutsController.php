<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\ShopPartnerSupport;

class AboutsController extends BaseFrontendController
{
    public function index($slug)
    {
    	// support
        $support = ShopPartnerSupport::where([
            ['del_flg', 0],
            ['slug', $slug],
            ['status', 0]
        ])->first();

        $active_support = 'active_in';

        return view('frontend.abouts.index')->with(compact('support','active_support'));
    }

    public function detail($slug)
    {

    	$DetailTBabout = DB::table('TBabouts')
    	    ->select(
    	    	'title'.@session('lang').' as title',
    	    	'content'.@session('lang').' as content',
    	    	'slug'
    	    )
    	    ->where([
    	    	['del_flg', '=', 0],
    	    	['slug', '=', $slug],
    	    ])
    	    ->first();

    	if (!$DetailTBabout) {
    		return redirect('/404');
    	}

    	$TBabouts = DB::table('TBabouts')
    	    ->select(
    	    	'title'.@session('lang').' as title',
    	    	'summary'.@session('lang').' as summary',
    	    	'slug',
    	    	'avatar',
    	    	'created_at'
    	    )
    	    ->where('del_flg', '=', 0)
    	    ->orderBy('position', 'desc')
    	    ->get();

        $active_thongtin = "active";
        return view('frontend.abouts.detail')->with(compact('TBabouts', 'DetailTBabout', 'active_thongtin'));
    }
}