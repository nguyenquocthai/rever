@extends('frontend.layouts.main')
@section('content')
<div class="container-fluid">
    <div class="container">
        <div class="row row_top_use_all">
            <div class="col-md-12">
                <div class="">
                    <span class="span_head_title">
                        @if(@session('lang') == 'en')
                            Home
                        @else
                         Trang chủ
                        @endif ></span>
                    <span class="span_para_title">
                    @if(@session('lang') == 'en')
                            Visitor
                        @else
                         Khách tham quan
                        @endif </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-lg-3">
                <div class="large_about">
                    <ul class="ul_about">
                        @if(isset($regulations))
                            @foreach ($regulations as $regulation)
                            <li class="">
                                <a class="" href="/khach-tham-quan/{{$regulation->slug}}">{{BladeGeneral::lang($regulation, 'title')}}</a>
                                <i class="right_icon_about fas fa-caret-right"></i>
                            </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <div class="fair_information">
                    <div class="sec1_fair_information">
                        <i class="fas fa-user-friends"></i>
                        @if(@session('lang') == 'en')
                            Infomation
                        @else
                         Thông tin hội chợ
                        @endif
                    </div>
                    <div class="box_gay">
                        <div class="head_sec1_fair_information">
                            
                            @if(@session('lang') == 'en')
                                TIME
                            @else
                             THỜI GIAN
                            @endif
                        </div>
                        <div class="txt_sec1_fair_information">
                            {{$info_web['ngaymocua']}}
                        </div>
                        <div class="txt_sec1_fair_information">
                            {{$info_web['giomocua']}}
                        </div>
                        
                        <div class="head_sec1_fair_information">
                            
                            @if(@session('lang') == 'en')
                                ADDRESS
                            @else
                             ĐỊA CHỈ
                            @endif
                        </div>
                        <div class="txt_sec1_fair_information">
                            {{$info_web['diadiem']}}
                        </div>
                        <div class="txt_sec1_fair_information">
                            {{$info_web['address']}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-lg-9">
                <div class="box_top_content">
                    <div class="txt_top_content">{{BladeGeneral::lang($regulation, 'title')}}</div>
                </div>
                <div class="border_content">
                    <div class="content_why_txt">
                        {!!BladeGeneral::lang($regulation, 'content')!!}
                    </div>
                </div>
                <!-- <div class="border_content">
                    <div class="img_main_content pd-10-20">
                      <img width="100%" src="img/visit.jpg" alt="">
                    </div>
                    
                    <div class="content_why pd-10-20 txt_size_17">
                      <strong>An Ultimate Trade Fair to Maximize Your Travel Costs!</strong>
                    </div>
                    <div class="col-md-1">
                      <img class="ss" src="img/negotiation@3x.png" alt="">
                    </div>
                    <div class="content_why_txt">
                    <p>
                        • A perfect opportunity to shop for <strong>a wide selection of finest lifestyle quality products</strong> from more than 1,000 companies.
                        These products appeal to customers of different ages and demands, and most importantly, they come in unique and outstanding designs.<br>
                    </p>
                    </div>
                    <div class="col-md-1">
                      <img class="ss" src="img/seo@3x.png" alt="">
                    </div>
                    <div class="content_why_txt">
                    • The Expo is bringing the top exhibitors from  <strong>“Niche Markets”</strong>  including elderly care products, baby, kids and mum products, pet products,
                        the  <strong>“Demark”</strong> award-winning products and <br /><strong>“I+D Style Café”</strong> Prototype Café.<br>
                    </div>
                    <div class="col-md-1">
                      <img class="ss" src="img/shield@3x.png" alt="">
                    </div>
                    <div class="content_why_txt">
                    • Networking opportunities</strong> with the participants from ASEAN nations in CLMV zone and International Pavilion.<br>
                    </div>
                    <div class="col-md-1">
                      <img class="ss" src="img/team-1@3x.png" alt="">
                    </div>
                    <div class="content_why_txt">
                    • Up-and-coming topic <strong>Seminars / Business Matching</strong> services / Lifestyle product <strong> Fashion Shows</strong>
                    </div>
                    </div> -->
            </div>
        </div>
    </div>
</div>
@endsection