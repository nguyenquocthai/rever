<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\FunctionAuthority;
use App\Model\ShopRegulation;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminregulationsController extends BaseAdminController
{

    public function search_datatable (Request $request) {
        // rule
        if (Auth::user()->list_rule != null)
            $list_rules = json_decode(Auth::user()->list_rule, true);
        else
            $list_rules = [];

        $list_url = [];
        foreach($list_rules as $list_rule) {
            $function = FunctionAuthority::where([
                ['id', '=', $list_rule['id']],
            ])->first();

            if($function) {
                $list_url[] = $function->role_json;
            }
        }
        $role = Auth::user()->role;

        $regulations = DB::table('shop_regulations')
            ->select('*')
            ->where([
                ['del_flg', 0],
            ])
            ->orderBy('position', 'asc')
            ->get();

        $output = [];
        foreach ($regulations as $key => $regulation) {

            $vitri = $key+1;

            $row = [];

            $row[] = $regulation->id;

            $row[] = $regulation->position;

            $row[] = $vitri;

            $row[] = $regulation->title;

            $row[] = '<span class="hidden">'.$regulation->updated_at.'</span>'.date('d/m/Y', strtotime($regulation->updated_at));

            $view = View::make('Backend/Adminregulation/_status', ['status' => $regulation->status]);
            $row[] = $view->render();

            $view = View::make('Backend/Adminregulation/_actions', ['id' => $regulation->id, 'list_url' => $list_url, 'role' => $role, 'page' => 'regulation']);
            $row[] = $view->render();
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // Create folder
            if ( !file_exists(config('general.shop_regulation_path')) )
                mkdir(config('general.shop_regulation_path'), config('permission_folder'), true);

            // validate
            $id = 0;
            $error_validate = ShopRegulation::validate($id);
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $file_name = '';
            // upload avatar
            /*
            if($request->hasFile('files') != 1) {
                $data['code'] = 300;
                $data['error'] = 'Hãy chọn hình đại diện';
                return response()->json($data, 200);
            } else {
                $files = $request->file('files');
                $avatar = $files[0];
                $avatar_link['path'] = config('general.shop_regulation_path');
                $avatar_link['url'] = config('general.shop_regulation_url');

                $upload = ShopUpload::upload($avatar, $avatar_link);
                if ($upload == 'error') {
                    $data['code'] = 300;
                    $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                    return response()->json($data, 200);
                }

                $file_name = $upload ? $upload['file_name'] : '';
            }
            */

            // last_pos
            $last_pos = DB::table('shop_regulations')
                ->select('position')
                ->orderBy('position', 'desc')
                ->first();
            if ($last_pos) {
                $position = $last_pos->position + 1;
            } else {
                $position = 1;
            }

            // save
            $shop_regulation = new ShopRegulation;

            $shop_regulation->title = $request->title;
            $shop_regulation->title_en = $request->title_en;
            $shop_regulation->short_content = $request->short_content;
            $shop_regulation->short_content_en = $request->short_content_en;

            $shop_regulation->content = $request->content;
            $shop_regulation->content_en = $request->content_en;


            $shop_regulation->slug = $request->slug;
            $shop_regulation->meta_keyword = $request->meta_keyword;
            $shop_regulation->meta_description = $request->meta_description;
            $shop_regulation->status = $request->status;
            $shop_regulation->position = $position;

            $shop_regulation->avatar = @$file_name;

            $shop_regulation->del_flg = 0;

            $shop_regulation->save();

            $data['code'] = 200;
            $data['message'] = 'Lưu thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            $error_validate = ShopRegulation::validate($id);
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            // check $id isset in database
            $regulation = ShopRegulation::where([
                ['id', '=', $id],
                //['domain_id', Auth::guard('api')->id()]
            ])->first();

            if (!$regulation) {
                $data['code'] = 300;
                $data['error'] = 'Lưu thất bại';
                return response()->json($data, 200);
            }


            $file_name = $regulation['avatar'];
            // Create folder
            if ( !file_exists(config('general.shop_regulation_path')) )
                mkdir(config('general.shop_regulation_path'), config('permission_folder'), true);

            // upload avatar
            /*
            if($request->hasFile('files') == 1) {

                $files = $request->file('files');
                $avatar = $files[0];
                $avatar_link['path'] = config('general.shop_regulation_path');
                $avatar_link['url'] = config('general.shop_regulation_url');
                $options['file_name'] = $file_name;

                $upload = ShopUpload::upload($avatar, $avatar_link, $options);
                if ($upload == 'error') {
                    $data['code'] = 300;
                    $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                    return response()->json($data, 200);
                }

                $file_name = $upload ? $upload['file_name'] : $regulation['avatar'];

            }
            */


            // save
            $shop_regulation = ShopRegulation::find($id);

            $shop_regulation->title = $request->title;
            $shop_regulation->title_en = $request->title_en;
            $shop_regulation->short_content = $request->short_content;
            $shop_regulation->short_content_en = $request->short_content_en;

            $shop_regulation->content = $request->content;
            $shop_regulation->content_en = $request->content_en;

            $shop_regulation->slug = $request->slug;
            $shop_regulation->meta_keyword = $request->meta_keyword;
            $shop_regulation->meta_description = $request->meta_description;
            $shop_regulation->status = $request->status;

            $shop_regulation->avatar = @$file_name;

            $shop_regulation->save();

            $data['code'] = 200;
            $data['message'] = 'Cập nhật thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {
            $regulation = ShopRegulation::where([
                ['id', $id],
                ['del_flg', 0],
                //['domain_id', Auth::guard('api')->id()]
            ])->first();

            if ($regulation) {
                $regulation->avatar = config('general.shop_regulation_url') . $regulation->avatar;
            }

            $positions = DB::table('shop_regulations')
                ->select('id','position')
                ->where([
                    ['del_flg', '=', 0],
                ])
                ->orderBy('position', 'desc')
                ->get();

            $view = View::make('Backend/Adminregulation/_position', ['positions' => $positions, 'here' => $regulation->position]);
            $list_position = $view->render();

            $data['code'] = 200;
            $data['data'] = $regulation;
            $data['list_position'] = $list_position;
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {
        try {
            $regulation = ShopRegulation::where([
                ['id', $id],
                //['domain_id', Auth::guard('api')->id()]
            ])->update(['del_flg' => 1]);
            if ($regulation) {
                $data['code'] = 200;
                $data['message'] = 'Xóa thành công';
                return response()->json($data, 200);
            } else {
                $data['code'] = 300;
                $data['error'] = 'Xóa không thành công';
                return response()->json($data, 200);
            }
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function pos_regulation(Request $request)
    {

        $id = $request->id;

        if ($request->status_code == "up") {

            $find_pos = DB::table('shop_regulations')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id],
                ])
                ->first();

            $find_up = DB::table('shop_regulations')
                ->select('id', 'position')
                ->where([
                    ['del_flg', '=', 0],
                    ['position', '>', $find_pos->position],
                ])
                ->orderBy('position', 'asc')
                ->first();

            if($find_up && $find_pos){
                // save
                $up_pos = ShopRegulation::find($id);
                $up_pos->position = $find_up->position;
                $up_pos->save();

                // save
                $down_pos = ShopRegulation::find($find_up->id);
                $down_pos->position = $find_pos->position;
                $down_pos->save();

                $data['code'] = 200;

                $data['idnew'] = $id;
                $data['posnew'] = $find_up->position;

                $data['idold'] = $find_up->id;
                $data['posold'] = $find_pos->position;

                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            $data['code'] = 300;
            $data['error'] = 'Đã ở vị trí đầu tiên.';
            return response()->json($data, 200);
        }

        if ($request->status_code == "down") {

            $find_pos = DB::table('shop_regulations')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id],
                ])
                ->first();

            $find_up = DB::table('shop_regulations')
                ->select('id', 'position')
                ->where([
                    ['del_flg', '=', 0],
                    ['position', '<', $find_pos->position],
                ])
                ->orderBy('position', 'desc')
                ->first();

            if($find_up && $find_pos){

                // save
                $up_pos = ShopRegulation::find($id);
                $up_pos->position = $find_up->position;
                $up_pos->save();

                // save
                $down_pos = ShopRegulation::find($find_up->id);
                $down_pos->position = $find_pos->position;
                $down_pos->save();

                $data['code'] = 200;

                $data['idnew'] = $id;
                $data['posnew'] = $find_up->position;

                $data['idold'] = $find_up->id;
                $data['posold'] = $find_pos->position;

                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            $data['code'] = 300;
            $data['error'] = 'Đã ở vị trí cuối cùng.';
            return response()->json($data, 200);
        }

        if ($request->status_code == "change") {

            try {

                DB::beginTransaction();
                // save
                $change_regulation = ShopRegulation::find($id);
                $change_regulation->position = $request->position_new;
                $change_regulation->save();

                // save
                $swap_regulation = ShopRegulation::find($request->id_swap);
                $swap_regulation->position = $request->position_old;
                $swap_regulation->save();

                DB::commit();
                $data['code'] = 200;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);

            } catch (Exception $e) {

                DB::rollback();
                $data['code'] = 300;
                $data['error'] = 'Lỗi kết nối';
                return response()->json($data, 200);
            }
        }

        $data['code'] = 300;
        $data['error'] = 'Lỗi kết nối';
        return response()->json($data, 200);
    }
}
