<div class="form-group">
    <label class="next-label">Title</label>
    <input value="{{$data->title}}" type="text" class="form-control" name="title">
</div>

<div class="form-group">
    <label class="next-label">Describe</label>
    <input value="{{$data->value}}" type="text" class="form-control" name="value">
</div>

<div class="form-group load_icon_box">
    <input type="hidden" class="icon_input" value="{{$data->data}}" name="data">
    <label class="icon-review">Choose icon <i class="{{$data->data}}"></i></label>
    @include('Backend.store._icons')
</div>

<div class="form-group">
    <label class="next-label">Status</label>
    <select class="form-control" ng-required="true" name="status">
        <option @if($data->status == 0) selected @endif value="0">Activated</option>
        <option @if($data->status == 1) selected @endif value="1">Unactivated</option>
    </select>
</div>

<div class="form-group">
    <label class="next-label">Position</label>
    <div class="load_position">
        <div class="position_box" data-id="">
            @if (count($positions) != 0)
                @foreach ($positions as $key => $position)
                <span style="margin-bottom: 3px" data-value="{{$position->position}}" data-id="{{$position->id}}" class="item-position btn btn-icon-only grey-cascade @if($data->position == $position->position) active @endif">{{$key+1}}</span>
                @endforeach
            @else
            @endif
        </div>
    </div>
</div>