<style>
.myUL {
  list-style-type: none;
  padding: 0;
  margin: 0;
  height: 116px;
  overflow-x: hidden;
}

.myUL:after{
    content: "";
    display: block;
    clear: both;
}

.myUL li{
    width: 10%;
    
    float: left;
}

.myUL li a {
    border: 1px solid #ddd;
    background-color: #f6f6f6;
    text-decoration: none;
    font-size: 18px;
    color: black;
    display: block;
    padding-bottom: 100%;
    position: relative;
}

.myUL li a span{
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
}

.myUL li a:hover:not(.header) {
  background-color: #eee;
}
</style>

<input class="form-control myInput" type="text" placeholder="Search for icon.." title="Type in a name">
<ul class="myUL">
    <li><a data-name="icon2-facebook" href="#"><span class="icon2 icon2-facebook"></span></a></li>
    <li><a data-name="icon2-zalo" href="#"><span class="icon2 icon2-zalo"></span></a></li>
    <li><a data-name="icon2-google" href="#"><span class="icon2 icon2-google"></span></a></li>
    <li><a data-name="icon2-approved" href="#"><span class="icon2 icon2-approved"></span></a></li>
    <li><a data-name="icon2-furniture" href="#"><span class="icon2 icon2-furniture"></span></a></li>
    <li><a data-name="icon2-kitchen-fill" href="#"><span class="icon2 icon2-kitchen-fill"></span></a></li>
    <li><a data-name="icon2-kitchen" href="#"><span class="icon2 icon2-kitchen"></span></a></li>
    <li><a data-name="icon2-language-copy" href="#"><span class="icon2 icon2-language-copy"></span></a></li>
    <li><a data-name="icon2-language" href="#"><span class="icon2 icon2-language"></span></a></li>
    <li><a data-name="icon2-vr" href="#"><span class="icon2 icon2-vr"></span></a></li>
    <li><a data-name="icon2-attention" href="#"><span class="icon2 icon2-attention"></span></a></li>
    <li><a data-name="icon2-eye" href="#"><span class="icon2 icon2-eye"></span></a></li>
    <li><a data-name="icon2-thumbsdown" href="#"><span class="icon2 icon2-thumbsdown"></span></a></li>
    <li><a data-name="icon2-reset" href="#"><span class="icon2 icon2-reset"></span></a></li>
    <li><a data-name="icon2-checkbox-uncheck" href="#"><span class="icon2 icon2-checkbox-uncheck"></span></a></li>
    <li><a data-name="icon2-checkbox-check" href="#"><span class="icon2 icon2-checkbox-check"></span></a></li>
    <li><a data-name="icon2-elevator" href="#"><span class="icon2 icon2-elevator"></span></a></li>
    <li><a data-name="icon2-elevator-fill" href="#"><span class="icon2 icon2-elevator-fill"></span></a></li>
    <li><a data-name="icon2-hate" href="#"><span class="icon2 icon2-hate"></span></a></li>
    <li><a data-name="icon2-whisper" href="#"><span class="icon2 icon2-whisper"></span></a></li>
    <li><a data-name="icon2-whisper-fill" href="#"><span class="icon2 icon2-whisper-fill"></span></a></li>
    <li><a data-name="icon2-nearby" href="#"><span class="icon2 icon2-nearby"></span></a></li>
    <li><a data-name="icon2-nearby-active" href="#"><span class="icon2 icon2-nearby-active"></span></a></li>
    <li><a data-name="icon2-loft" href="#"><span class="icon2 icon2-loft"></span></a></li>
    <li><a data-name="icon2-loft-fill" href="#"><span class="icon2 icon2-loft-fill"></span></a></li>
    <li><a data-name="icon2-water-heater" href="#"><span class="icon2 icon2-water-heater"></span></a></li>
    <li><a data-name="icon2-water-heater-fill" href="#"><span class="icon2 icon2-water-heater-fill"></span></a></li>
    <li><a data-name="icon2-window" href="#"><span class="icon2 icon2-window"></span></a></li>
    <li><a data-name="icon2-window-fill" href="#"><span class="icon2 icon2-window-fill"></span></a></li>
    <li><a data-name="icon2-shield" href="#"><span class="icon2 icon2-shield"></span></a></li>
    <li><a data-name="icon2-bell-ring" href="#"><span class="icon2 icon2-bell-ring"></span></a></li>
    <li><a data-name="icon2-bus" href="#"><span class="icon2 icon2-bus"></span></a></li>
    <li><a data-name="icon2-coffee" href="#"><span class="icon2 icon2-coffee"></span></a></li>
    <li><a data-name="icon2-electric" href="#"><span class="icon2 icon2-electric"></span></a></li>
    <li><a data-name="icon2-external" href="#"><span class="icon2 icon2-external"></span></a></li>
    <li><a data-name="icon2-food" href="#"><span class="icon2 icon2-food"></span></a></li>
    <li><a data-name="icon2-minus" href="#"><span class="icon2 icon2-minus"></span></a></li>
    <li><a data-name="icon2-minus-fill" href="#"><span class="icon2 icon2-minus-fill"></span></a></li>
    <li><a data-name="icon2-sort" href="#"><span class="icon2 icon2-sort"></span></a></li>
    <li><a data-name="icon2-university" href="#"><span class="icon2 icon2-university"></span></a></li>
    <li><a data-name="icon2-water" href="#"><span class="icon2 icon2-water"></span></a></li>
    <li><a data-name="icon2-a-down-mini" href="#"><span class="icon2 icon2-a-down-mini"></span></a></li>
    <li><a data-name="icon2-g-lbgt" href="#"><span class="icon2 icon2-g-lbgt"></span></a></li>
    <li><a data-name="icon2-share" href="#"><span class="icon2 icon2-share"></span></a></li>
    <li><a data-name="icon2-location-fill" href="#"><span class="icon2 icon2-location-fill"></span></a></li>
    <li><a data-name="icon2-location1" href="#"><span class="icon2 icon2-location1"></span></a></li>
    <li><a data-name="icon2-airconditioner-fill" href="#"><span class="icon2 icon2-airconditioner-fill"></span></a></li>
    <li><a data-name="icon2-calendar" href="#"><span class="icon2 icon2-calendar"></span></a></li>
    <li><a data-name="icon2-camera-fill" href="#"><span class="icon2 icon2-camera-fill"></span></a></li>
    <li><a data-name="icon2-circle-check" href="#"><span class="icon2 icon2-circle-check"></span></a></li>
    <li><a data-name="icon2-circle-intersect" href="#"><span class="icon2 icon2-circle-intersect"></span></a></li>
    <li><a data-name="icon2-circle-linear" href="#"><span class="icon2 icon2-circle-linear"></span></a></li>
    <li><a data-name="icon2-cook-fill" href="#"><span class="icon2 icon2-cook-fill"></span></a></li>
    <li><a data-name="icon2-error" href="#"><span class="icon2 icon2-error"></span></a></li>
    <li><a data-name="icon2-fast" href="#"><span class="icon2 icon2-fast"></span></a></li>
    <li><a data-name="icon2-home-list-fill" href="#"><span class="icon2 icon2-home-list-fill"></span></a></li>
    <li><a data-name="icon2-image-fill" href="#"><span class="icon2 icon2-image-fill"></span></a></li>
    <li><a data-name="icon2-image-upload" href="#"><span class="icon2 icon2-image-upload"></span></a></li>
    <li><a data-name="icon2-information-fill" href="#"><span class="icon2 icon2-information-fill"></span></a></li>
    <li><a data-name="icon2-key" href="#"><span class="icon2 icon2-key"></span></a></li>
    <li><a data-name="icon2-more-horizontal" href="#"><span class="icon2 icon2-more-horizontal"></span></a></li>
    <li><a data-name="icon2-more-vertical" href="#"><span class="icon2 icon2-more-vertical"></span></a></li>
    <li><a data-name="icon2-nav-heart-2" href="#"><span class="icon2 icon2-nav-heart-2"></span></a></li>
    <li><a data-name="icon2-quote" href="#"><span class="icon2 icon2-quote"></span></a></li>
    <li><a data-name="icon2-settings" href="#"><span class="icon2 icon2-settings"></span></a></li>
    <li><a data-name="icon2-squaremeter-fill" href="#"><span class="icon2 icon2-squaremeter-fill"></span></a></li>
    <li><a data-name="icon2-squaremeter" href="#"><span class="icon2 icon2-squaremeter"></span></a></li>
    <li><a data-name="icon2-star-fill" href="#"><span class="icon2 icon2-star-fill"></span></a></li>
    <li><a data-name="icon2-star" href="#"><span class="icon2 icon2-star"></span></a></li>
    <li><a data-name="icon2-swipe-down" href="#"><span class="icon2 icon2-swipe-down"></span></a></li>
    <li><a data-name="icon2-swipe-up" href="#"><span class="icon2 icon2-swipe-up"></span></a></li>
    <li><a data-name="icon2-a-down" href="#"><span class="icon2 icon2-a-down"></span></a></li>
    <li><a data-name="icon2-a-left" href="#"><span class="icon2 icon2-a-left"></span></a></li>
    <li><a data-name="icon2-a-right" href="#"><span class="icon2 icon2-a-right"></span></a></li>
    <li><a data-name="icon2-a-up" href="#"><span class="icon2 icon2-a-up"></span></a></li>
    <li><a data-name="icon2-airconditioner-copy" href="#"><span class="icon2 icon2-airconditioner-copy"></span></a></li>
    <li><a data-name="icon2-airconditioner" href="#"><span class="icon2 icon2-airconditioner"></span></a></li>
    <li><a data-name="icon2-back" href="#"><span class="icon2 icon2-back"></span></a></li>
    <li><a data-name="icon2-bed-fill" href="#"><span class="icon2 icon2-bed-fill"></span></a></li>
    <li><a data-name="icon2-bed" href="#"><span class="icon2 icon2-bed"></span></a></li>
    <li><a data-name="icon2-bell-fill" href="#"><span class="icon2 icon2-bell-fill"></span></a></li>
    <li><a data-name="icon2-bell" href="#"><span class="icon2 icon2-bell"></span></a></li>
    <li><a data-name="icon2-call" href="#"><span class="icon2 icon2-call"></span></a></li>
    <li><a data-name="icon2-camera" href="#"><span class="icon2 icon2-camera"></span></a></li>
    <li><a data-name="icon2-clear" href="#"><span class="icon2 icon2-clear"></span></a></li>
    <li><a data-name="icon2-close-fill" href="#"><span class="icon2 icon2-close-fill"></span></a></li>
    <li><a data-name="icon2-close" href="#"><span class="icon2 icon2-close"></span></a></li>
    <li><a data-name="icon2-closet-fill" href="#"><span class="icon2 icon2-closet-fill"></span></a></li>
    <li><a data-name="icon2-closet" href="#"><span class="icon2 icon2-closet"></span></a></li>
    <li><a data-name="icon2-cook" href="#"><span class="icon2 icon2-cook"></span></a></li>
    <li><a data-name="icon2-create-home-fill" href="#"><span class="icon2 icon2-create-home-fill"></span></a></li>
    <li><a data-name="icon2-create-home" href="#"><span class="icon2 icon2-create-home"></span></a></li>
    <li><a data-name="icon2-edit" href="#"><span class="icon2 icon2-edit"></span></a></li>
    <li><a data-name="icon2-filter" href="#"><span class="icon2 icon2-filter"></span></a></li>
    <li><a data-name="icon2-fridge-fill" href="#"><span class="icon2 icon2-fridge-fill"></span></a></li>
    <li><a data-name="icon2-fridge" href="#"><span class="icon2 icon2-fridge"></span></a></li>
    <li><a data-name="icon2-g-female" href="#"><span class="icon2 icon2-g-female"></span></a></li>
    <li><a data-name="icon2-g-lgbt" href="#"><span class="icon2 icon2-g-lgbt"></span></a></li>
    <li><a data-name="icon2-g-male" href="#"><span class="icon2 icon2-g-male"></span></a></li>
    <li><a data-name="icon2-home-list" href="#"><span class="icon2 icon2-home-list"></span></a></li>
    <li><a data-name="icon2-image" href="#"><span class="icon2 icon2-image"></span></a></li>
    <li><a data-name="icon2-information" href="#"><span class="icon2 icon2-information"></span></a></li>
    <li><a data-name="icon2-location" href="#"><span class="icon2 icon2-location"></span></a></li>
    <li><a data-name="icon2-nav-chat-fill" href="#"><span class="icon2 icon2-nav-chat-fill"></span></a></li>
    <li><a data-name="icon2-nav-chat" href="#"><span class="icon2 icon2-nav-chat"></span></a></li>
    <li><a data-name="icon2-nav-heart-fill" href="#"><span class="icon2 icon2-nav-heart-fill"></span></a></li>
    <li><a data-name="icon2-nav-heart" href="#"><span class="icon2 icon2-nav-heart"></span></a></li>
    <li><a data-name="icon2-nav-home-fill" href="#"><span class="icon2 icon2-nav-home-fill"></span></a></li>
    <li><a data-name="icon2-nav-home" href="#"><span class="icon2 icon2-nav-home"></span></a></li>
    <li><a data-name="icon2-nav-roomate-fill" href="#"><span class="icon2 icon2-nav-roomate-fill"></span></a></li>
    <li><a data-name="icon2-nav-roomate" href="#"><span class="icon2 icon2-nav-roomate"></span></a></li>
    <li><a data-name="icon2-parking-fill" href="#"><span class="icon2 icon2-parking-fill"></span></a></li>
    <li><a data-name="icon2-parking" href="#"><span class="icon2 icon2-parking"></span></a></li>
    <li><a data-name="icon2-pets-fill" href="#"><span class="icon2 icon2-pets-fill"></span></a></li>
    <li><a data-name="icon2-pets" href="#"><span class="icon2 icon2-pets"></span></a></li>
    <li><a data-name="icon2-plus-fill" href="#"><span class="icon2 icon2-plus-fill"></span></a></li>
    <li><a data-name="icon2-plus" href="#"><span class="icon2 icon2-plus"></span></a></li>
    <li><a data-name="icon2-private-fill" href="#"><span class="icon2 icon2-private-fill"></span></a></li>
    <li><a data-name="icon2-private" href="#"><span class="icon2 icon2-private"></span></a></li>
    <li><a data-name="icon2-profile-fill" href="#"><span class="icon2 icon2-profile-fill"></span></a></li>
    <li><a data-name="icon2-profile" href="#"><span class="icon2 icon2-profile"></span></a></li>
    <li><a data-name="icon2-question" href="#"><span class="icon2 icon2-question"></span></a></li>
    <li><a data-name="icon2-recent" href="#"><span class="icon2 icon2-recent"></span></a></li>
    <li><a data-name="icon2-reply" href="#"><span class="icon2 icon2-reply"></span></a></li>
    <li><a data-name="icon2-search" href="#"><span class="icon2 icon2-search"></span></a></li>
    <li><a data-name="icon2-security_guard-fill" href="#"><span class="icon2 icon2-security_guard-fill"></span></a></li>
    <li><a data-name="icon2-security_guard" href="#"><span class="icon2 icon2-security_guard"></span></a></li>
    <li><a data-name="icon2-send" href="#"><span class="icon2 icon2-send"></span></a></li>
    <li><a data-name="icon2-television-fill" href="#"><span class="icon2 icon2-television-fill"></span></a></li>
    <li><a data-name="icon2-television" href="#"><span class="icon2 icon2-television"></span></a></li>
    <li><a data-name="icon2-time" href="#"><span class="icon2 icon2-time"></span></a></li>
    <li><a data-name="icon2-time-fill" href="#"><span class="icon2 icon2-time-fill"></span></a></li>
    <li><a data-name="icon2-toilet-fill" href="#"><span class="icon2 icon2-toilet-fill"></span></a></li>
    <li><a data-name="icon2-toilet" href="#"><span class="icon2 icon2-toilet"></span></a></li>
    <li><a data-name="icon2-trash-fill" href="#"><span class="icon2 icon2-trash-fill"></span></a></li>
    <li><a data-name="icon2-trash" href="#"><span class="icon2 icon2-trash"></span></a></li>
    <li><a data-name="icon2-upload" href="#"><span class="icon2 icon2-upload"></span></a></li>
    <li><a data-name="icon2-wash-fill" href="#"><span class="icon2 icon2-wash-fill"></span></a></li>
    <li><a data-name="icon2-wash" href="#"><span class="icon2 icon2-wash"></span></a></li>
    <li><a data-name="icon2-wifi-fill" href="#"><span class="icon2 icon2-wifi-fill"></span></a></li>
    <li><a data-name="icon2-wifi" href="#"><span class="icon2 icon2-wifi"></span></a></li>
    <li><a data-name="icon2-coin" href="#"><span class="icon2 icon2-coin"></span></a></li>
    <li><a data-name="icon2-flag" href="#"><span class="icon2 icon2-flag"></span></a></li>
    <li><a data-name="icon2-money" href="#"><span class="icon2 icon2-money"></span></a></li>
    <li><a data-name="icon2-money-fill" href="#"><span class="icon2 icon2-money-fill"></span></a></li>
    <li><a data-name="icon2-wallet" href="#"><span class="icon2 icon2-wallet"></span></a></li>
    <li><a data-name="icon2-wallet-fill" href="#"><span class="icon2 icon2-wallet-fill"></span></a></li>
    <li><a data-name="icon2-movein" href="#"><span class="icon2 icon2-movein"></span></a></li>
</ul>

<script>

    $('.myInput').keyup(function(event) {
        var input, filter, ul, li, a, i, txtValue;
        input = $(this);
        filter = input.val().toUpperCase();
        ul = $(this).closest('.load_icon_box').find('.myUL');
        li = ul.find('li');
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            txtValue = a.getAttribute("data-name");
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    });

    $('.myUL a').click(function(event) {
        var view = $(this).closest('.load_icon_box').find('.icon-review i');
        var input = $(this).closest('.load_icon_box').find('.icon_input');
        var data = $(this).attr('data-name');
        view.attr('class', data);
        input.val(data);
    });
</script>