WebWorldApp.controller('locations.index', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {

        $rootScope.app.title = $rootScope.app.title + ' | location';
        $scope.title = 'List location';
        $scope.form_add = $('#form_add').html();
        $scope.location = {};

        // DATATABLE
        commonService.requestFunction('index_location', {}, function(e) {

            $scope.form = $('#userForm').html();
            $scope.value = e.data;
            $scope.datatable = $('#tbl-data').DataTable({
                "order": [[ 2, "asc" ]],
                columnDefs: [
                    { sortable: false, searchable: false, targets: [ 4,7 ] },
                    { class: 'center-text', targets: [ 2,4,5,6,7 ] },
                    { class: 'hidden', targets: [ 0,1 ] }
                ],
                "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                    $(nRow).attr('data-id', aData[0]);
                },
                displayStart: 0,
                displayLength: 5,
                data: e.data,
                "autoWidth": false
            });
            $scope.datatable.columns().search('').draw();
            $scope.datatable.order([2, 'asc']).draw();
        });

        //-------------------------------------------------------------------------------
        $scope.search_datatable = function() {
            var title = $('.search_title').val() || '';
            console.log(title);
            $scope.datatable.columns(3).search(title)
                            .draw();
        };

        //-------------------------------------------------------------------------------
        $('.btn_addrow').click(function(event) {
            var request = {};
            var data = {};
            var files = [];
            if ($("#form_add .file_image").get(0).files[0])
                files = files.concat($("#form_add .file_image").get(0).files[0]);

            $("#form_add").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']); 

            request['value'] = data;
            fileUpload.uploadFileToUrl(files, request, 'create_location', function(e) {
                switch (e.code) {
                    case 200:
                        commonService.requestFunction('index_location', {}, function(d) {
                            $scope.datatable.clear().draw();
                            $scope.datatable.rows.add(d.data);
                            $scope.datatable.columns.adjust().draw();
                            $scope.value = d.data;
                        });
                        $('#modal_addrow').modal('hide');
                        $('#form_add').html($scope.form_add);
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.btn-edit-row', function(event) {
            var data = {};
            var curenpage = $scope.datatable.page.info().page;
            var id = $(this).attr('data-id');

            $('#form_edit').attr('data-curenpage', curenpage);
            $('#form_edit').attr('data-id', id);
            
            data['status_code'] = "show";
            data['_method'] = 'PUT';
            commonService.requestFunction('update_location/' + id, data, function(e) {
                $('#form_edit').html(e.data);
                $('#modal_editrow').modal('show');
            });
        });

        //-------------------------------------------------------------------------------
        $('.btn_editrow').click(function(event) {
            var request = {};
            var data = {};
            var files = [];

            var id_edit = $('#form_edit').attr('data-id');

            var curenpage_string = $('#form_edit').attr('data-curenpage');
            var curenpage = parseInt(curenpage_string, 10);

            var files = [];
            if ($("#form_edit .file_image").length != 0) {
                if ($("#form_edit .file_image").get(0).files[0])
                    files = files.concat($("#form_edit .file_image").get(0).files[0]);
            }

            $("#form_edit").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);

            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";
            request['vitri'] = parseInt($('.item-position.active').html(), 10);
            
            fileUpload.uploadFileToUrl(files, request, 'update_location/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $('#form_edit').html('');
                        $('#modal_editrow').modal('hide');
                        for (var i = 0; i < $scope.value.length; i++) {
                            if($scope.value[i][0] == id_edit){
                                $scope.value[i] = e.row;
                            }
                        }

                        $scope.datatable.clear().draw();
                        $scope.datatable.rows.add($scope.value); // Add new data
                        $scope.datatable.columns.adjust().draw(); // Redraw the DataTable
                        $scope.datatable.page( curenpage ).draw( false );
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.delete_row', function(event) {
            var row_id = $(this).attr('data-id');
            var row = $(this).closest('tr');
            confirmPopup('Delete location', 'Do you want to delete the location?', function() {
                commonService.requestFunction('delete_location/' + row_id + $rootScope.api_token, {}, function(e) {
                    switch (e.code) {
                        case 200:
                            $scope.datatable.row(row).remove().draw( false );
                            commonService.requestFunction('index_location', {}, function(e) {
                                $scope.value = e.data;
                            });
                            break;
                        default:
                            break;
                    }
                });
            });
        });

        //-------------------------------------------------------------------------------
        $('#form_edit').on('click', '.item-position', function(event) {

            var data = {};
            var here = $(this);
            var id = $('#form_edit').attr('data-id');
            var curenpage_string = $('#form_edit').attr('data-curenpage');
            var curenpage = parseInt(curenpage_string, 10);

            data['id_swap'] = $(this).attr('data-id');
            data['status_code'] = 'change';
            data['_method'] = 'PUT';

            if(data['id_swap'] == id){
                return false;
            }

            commonService.requestFunction('update_location/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        $('.item-position.active').attr('data-id',data['id_swap']);
                        here.attr('data-id',id);
                        $('.item-position').removeClass('active');
                        here.addClass('active');
                        commonService.requestFunction('index_location', {}, function(d) {
                            $scope.value = d.data;
                            $scope.datatable.clear().draw();
                            $scope.datatable.rows.add($scope.value); // Add new data
                            $scope.datatable.columns.adjust().draw(); // Redraw the DataTable
                            $scope.datatable.page( curenpage ).draw( false );
                        });
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.up', function(event) {

            var curenpage = $scope.datatable.page.info().page;
            var id = $(this).attr('data-id');

            var data = {};
            data['status_code'] = "up";
            data['_method'] = 'PUT';

            $('#tbl-data tbody tr').removeClass('active');
            commonService.requestFunction('update_location/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        for (var y = 0; y < $scope.value.length; y++) {
                            if($scope.value[y][0] == e.idnew){
                                var vtrinew = $scope.value[y][2];
                            }
                            if($scope.value[y][0] == e.idold){
                                var vtriold = $scope.value[y][2];
                            }
                        }
                        for (var i = 0; i < $scope.value.length; i++) {
                            if($scope.value[i][0] == e.idnew){
                                $scope.value[i][1] = e.posnew;
                                $scope.value[i][2] = vtriold;
                            }
                            if($scope.value[i][0] == e.idold){
                                $scope.value[i][1] = e.posold;
                                $scope.value[i][2] = vtrinew;
                            }
                        }

                        $scope.datatable.clear().draw();
                        $scope.datatable.rows.add($scope.value); // Add new data
                        $scope.datatable.columns.adjust().draw(); // Redraw the DataTable
                        $scope.datatable.page( curenpage ).draw( false );
                        $('#tbl-data tbody tr[data-id="'+id+'"]').addClass('active');
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.down', function(event) {
            
            var curenpage = $scope.datatable.page.info().page;
            var id = $(this).attr('data-id');

            var data = {};
            data['status_code'] = "down";
            data['_method'] = 'PUT';

            $('#tbl-data tbody tr').removeClass('active');
            commonService.requestFunction('update_location/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        for (var y = 0; y < $scope.value.length; y++) {
                            if($scope.value[y][0] == e.idnew){
                                var vtrinew = $scope.value[y][2];
                            }
                            if($scope.value[y][0] == e.idold){
                                var vtriold = $scope.value[y][2];
                            }
                        }
                        for (var i = 0; i < $scope.value.length; i++) {
                            if($scope.value[i][0] == e.idnew){
                                $scope.value[i][1] = e.posnew;
                                $scope.value[i][2] = vtriold;
                            }
                            if($scope.value[i][0] == e.idold){
                                $scope.value[i][1] = e.posold;
                                $scope.value[i][2] = vtrinew;
                            }
                        }

                        $scope.datatable.clear().draw();
                        $scope.datatable.rows.add($scope.value); // Add new data
                        $scope.datatable.columns.adjust().draw(); // Redraw the DataTable
                        $scope.datatable.page( curenpage ).draw( false );
                        $('#tbl-data tbody tr[data-id="'+id+'"]').addClass('active');
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on( 'draw.dt', function () {
            $('.table_img').each(function(index, el) {
                var path = $(this).attr('data-path');
                $(this).html('<img alt="..." src="'+path+'"></img>');
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.btn_status', function(event) {

            var here = $(this);
            var id = $(this).closest('tr').attr('data-id');
            var curenpage = $scope.datatable.page.info().page;

            var data = {};
            data['status_code'] = "change_status";
            data['_method'] = 'PUT';

            commonService.requestFunction('update_location/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        for (var i = 0; i < $scope.value.length; i++) {
                            if($scope.value[i][0] == id){
                                $scope.value[i][6] = e.status;
                            }
                        }

                        $scope.datatable.clear().draw();
                        $scope.datatable.rows.add($scope.value); // Add new data
                        $scope.datatable.columns.adjust().draw(); // Redraw the DataTable
                        $scope.datatable.page( curenpage ).draw( false );
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.table_img', function(event) {
            var path = $(this).attr('data-path');
            window.open(path);
        }); 
    }
]);
