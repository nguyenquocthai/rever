<div class="form-group">
    <label>Tên</label>
    <input value="{{$data->name}}" type="text" class="form-control" name="name" readonly>
</div>

<div class="form-group">
    <label class="next-label">Trạng thái</label>
    <select class="form-control" ng-required="true" name="status">
        <option @if($data->status == 0) selected @endif value="0">Đã xem</option>
        <option @if($data->status == 1) selected @endif value="1">Chưa xem</option>
    </select>
</div>
<div class="form-group">
    <label class="next-label">Số lượng</label>
    <input type="text" class="form-control" name="content" readonly value="{{$data->content}}">
</div>

<div class="form-group">
    <label class="next-label">Số điện thoại</label>
    <input type="text" class="form-control" name="sdt" readonly value="{{$data->sdt}}">
</div>

<div class="form-group">
    <label class="next-label">Địa chỉ</label>
    <input type="text" class="form-control" name="diachi" readonly value="{{$data->diachi}}">
</div>

<div class="form-group">
    <label class="next-label">Khách hàng ghi chú</label>
    <textarea class="form-control" name="note">{{$data->ghichu}}</textarea>
</div>

<div class="form-group">
    <label class="next-label">Ghi chú</label>
    <textarea class="form-control" name="note">{{$data->note}}</textarea>
</div>