<!DOCTYPE html>
<html ng-app="WebWorldApp" ng-controller="WebWorldCtrl">

    @include('Backend.Elements.head')

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            @include('Backend.Elements.header')

                <!-- BEGIN CONTAINER -->
                <div class="page-container">
                    @include('Backend.Elements.sidebar')
                    <!-- BEGIN CONTENT -->
                    <div class="page-content-wrapper">

                        <!-- BEGIN CONTENT BODY -->
                        <div class="page-content" ng-view>

                        </div>
                        <!-- END CONTENT BODY -->

                    </div>
                    <!-- END CONTENTx -->

                </div>
                <!-- END CONTAINER -->

            @include('Backend.Elements.footer')
        </div>
    </body>
    @include('Backend.Elements.js')

</html>