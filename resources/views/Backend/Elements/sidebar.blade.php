<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">

        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>

            <li class="nav-item start ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Admin home</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="/admin" class="nav-link">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Quản lý chung</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">Tổng quan trang web</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">

                    <li class="nav-item">
                        <a href="{{ url('admin/sliders') }}" class="nav-link ">
                            <span class="title">Slider</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('admin/seo_pages') }}" class="nav-link ">
                            <span class="title">Seo page</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('admin/settings') }}" class="nav-link ">
                            <span class="title">Thông tin trang web</span>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="heading">
                <h3 class="uppercase">Dữ liệu</h3>
            </li>

            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-star"></i>
                    <span class="title">Tin thuê</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    
                    <li class="nav-item">
                        <a href="{{ url('admin/product_cat1s') }}" class="nav-link ">Danh mục</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/tin-thue') }}" class="nav-link ">Bài viết</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-star"></i>
                    <span class="title">Tin bán</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    
                    <li class="nav-item">
                        <a href="{{ url('admin/product_cat1bans') }}" class="nav-link ">Danh mục</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/tin-ban') }}" class="nav-link ">Bài viết</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-star"></i>
                    <span class="title">Tin tức</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    
                    <li class="nav-item">
                        <a href="{{ url('admin/new_cats') }}" class="nav-link ">Danh mục</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/news') }}" class="nav-link ">Danh sách</a>
                    </li>
                </ul>
            </li> 
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-star"></i>
                    <span class="title">Giới thiệu</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    
            
                    <li class="nav-item">
                        <a href="{{ url('admin/intros') }}" class="nav-link ">Danh sách</a>
                    </li>
                </ul>
            </li>
            
             <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-drawer"></i>
                    <span class="title">Storage</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/tongquans') }}" class="nav-link ">Overview</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/tienichs') }}" class="nav-link ">Utilities</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a href="{{ url('admin/locations') }}" class="nav-link ">Location</a>
                    </li> -->
                </ul>
            </li> 
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-envelope-letter"></i>
                    <span class="title">Liên hệ @if($count_customer != 0)<span class="badge badge-success">{{$count_customer}}</span>@endif</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/customers') }}" class="nav-link ">Danh sách</a>
                    </li>
                    
                </ul>
            </li>
            <!-- <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-envelope-letter"></i>
                    <span class="title">Comment</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/customers') }}" class="nav-link ">Danh sách</a>
                    </li>
                    
                </ul>
            </li> -->
             <!-- <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-envelope-letter"></i>
                    <span class="title">Người dùng</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/nguoidungs') }}" class="nav-link ">Danh sách</a>
                    </li>
                    
                </ul>
            </li>  -->

        </ul>
        <!-- END SIDEBAR MENU -->

    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->