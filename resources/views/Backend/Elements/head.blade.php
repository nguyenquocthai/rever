<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8" />
	<title ng-bind="app.title"></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link rel="shortcut icon" type="image/x-icon" href="/public/img/logo.png">

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<link href="/public/assets/global/plugins/fontawesome/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">
	<link href="/public/assets/global/plugins/fontawesome-iconpicker/dist/css/fontawesome-iconpicker.min.css" rel="stylesheet">

	<link href="/public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />

	<link href="/public/vendor/icon/css/icon.css" rel="stylesheet" type="text/css" />

	<link href="/public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="/public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

	<link href="/public/assets/global/plugins/jquery-nestable/jquery.nestable.css" rel="stylesheet" type="text/css" />
	<!-- END GLOBAL MANDATORY STYLES -->

	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<link href="/public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
	<link href="/public/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
	<link href="/public/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
	<!-- END PAGE LEVEL PLUGINS -->

	<!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

	<!-- BEGIN THEME GLOBAL STYLES -->
	<link href="/public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
	<link href="/public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<!-- END THEME GLOBAL STYLES -->

	<!-- BEGIN THEME LAYOUT STYLES -->
	<link href="/public/assets/layouts/layout/css/layout.css" rel="stylesheet" type="text/css" />
	<link href="/public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
	<link href="/public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
	<!-- END THEME LAYOUT STYLES -->

	<link href="/public/assets/global/plugins/toastr/toastr.min.css" rel="stylesheet">
	<link href="/public/assets/global/plugins/select2-4.0.3/css/select2.min.css" rel="stylesheet">
	<link href="/public/assets/global/plugins/select2-4.0.3/css/select2-bootstrap.css" rel="stylesheet">
	<link href="/public/assets/global/plugins/dropzone/css/dropzone.css" rel="stylesheet">
	<link href="/public/assets/global/plugins/highcharts/modules/css/dcharts.css" rel="stylesheet">
	<link href="/public/assets/global/plugins/pace/pace-theme-flash.css" rel="stylesheet">
	<link href="/public/assets/global/css/animate.min.css" rel="stylesheet">

	<link href="/public/assets/global/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet">

	<link href="/public/assets/global/plugins/muuri/styles/demo-grid.css" rel="stylesheet">

	<!-- <link href="/public/assets/global/plugins/muuri/styles/main.css" rel="stylesheet"> -->
	<link href="/public/vendor/TableExport-master/dist/css/tableexport.css" rel="stylesheet">
	<link href="/public/css/backend/style.css" rel="stylesheet">
	<!-- <link href="{{ asset('css/plugins/custom.css') }}" rel="stylesheet"> -->
	<!-- <link href="{{ asset('css/plugins/c_tempcss/style.css') }}" rel="stylesheet"> -->
	<!-- <link href="{{ asset('css/plugins/kstyle.css') }}" rel="stylesheet"> -->

	<base href="/" />
</head>