<div class="position_box">
    @if (count($positions) != 0)
        @foreach ($positions as $key => $position)
        <span style="margin-bottom: 3px" data-value="{{$position->position}}" data-id="{{$position->id}}" class="item-position btn btn-icon-only grey-cascade @if($here == $position->position) active @endif">{{$key+1}}</span>
        @endforeach
    @else
    @endif 
</div>