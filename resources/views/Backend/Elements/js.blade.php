<script src="https://maps.google.com/maps/api/js?key=AIzaSyCQiaMU8DTCCDi90sT1ApqUYM737YYXymU&libraries=geometry,places" async></script>
<!-- BEGIN CORE PLUGINS -->
<script src="/public/assets/global/plugins/jquery.min.js"></script>

<script>
    var baseURL = '';
    var unit_price = "";
    var unit_weight = "";
    var ip_server = "";
	var json_name_apps = '{!!$json_name_apps!!}';
    var name_apps = $.parseJSON(json_name_apps);
</script>

<script src="/public/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/public/assets/global/plugins/js.cookie.min.js"></script>
<script src="/public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="/public/assets/global/plugins/jquery.blockui.min.js"></script>
<script src="/public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/public/assets/global/plugins/moment.min.js"></script>
<script src="/public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"></script>
<script src="/public/assets/global/plugins/morris/morris.min.js"></script>
<script src="/public/assets/global/plugins/morris/raphael-min.js"></script>
<script src="/public/assets/global/plugins/counterup/jquery.waypoints.min.js"></script>
<script src="/public/assets/global/plugins/counterup/jquery.counterup.js"></script>
<script src="/public/assets/global/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="/public/assets/global/plugins/horizontal-timeline/horizontal-timeline.js"></script>
<script src="/public/assets/global/plugins/flot/jquery.flot.min.js"></script>
<script src="/public/assets/global/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="/public/assets/global/plugins/flot/jquery.flot.categories.min.js"></script>
<script src="/public/assets/global/plugins/jquery.sparkline.min.js"></script>

<script src="/public/assets/global/scripts/datatable.js"></script>
<script src="/public/assets/global/plugins/datatables/datatables.min.js"></script>
<script src="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>

<script src="/public/assets/global/plugins/toastr/toastr.min.js"></script>
<script src="/public/assets/global/plugins/tinymce/tinymce.min.js"></script>
<script src="/public/assets/global/plugins/select2-4.0.3/js/select2.full.min.js"></script>
<script src="/public/assets/global/plugins/select2-4.0.3/js/i18n/vi.js"></script>
<script src="/public/assets/global/plugins/dropzone/dropzone.min.js"></script>
<script src="/public/assets/global/plugins/bootbox/bootbox.js"></script>
<script src="/public/assets/global/plugins/lazyload/lazyload.min.js"></script>

<!-- datepicker -->
<link href="/public/vendor/air-datepicker-master/dist/css/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="/public/vendor/air-datepicker-master/dist/js/datepicker.min.js"></script>
<script src="/public/vendor/air-datepicker-master/dist/js/i18n/datepicker.en.js"></script>
<!-- /datepicker -->

<script src="/public/assets/global/plugins/jquery-nestable/jquery.nestable.js"></script>

<script src="/public/assets/global/plugins/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.js"></script>

<script src="/public/assets/global/plugins/muuri/scripts/vendor/web-animations-2.3.1.min.js"></script>
<script src="/public/assets/global/plugins/muuri/scripts/vendor/hammer-2.0.8.min.js"></script>
<script src="/public/assets/global/plugins/muuri/scripts/vendor/muuri-0.5.4.js"></script>

<script src="/public/vendor/TableExport-master/dist/js/FileSaver.min.js"></script>
<script src="/public/vendor/TableExport-master/dist/js/Blob.min.js"></script>
<script src="/public/vendor/TableExport-master/dist/js/xls.core.min.js"></script>
<script src="/public/vendor/TableExport-master/dist/js/tableexport.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="/public/assets/global/scripts/app.min.js"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="/public/assets/layouts/layout/scripts/layout.min.js"></script>
<script src="/public/assets/layouts/global/scripts/quick-sidebar.min.js"></script>
<script src="/public/assets/layouts/global/scripts/quick-nav.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<script src="/public/js/angularjs/lib/angular.min.js"></script>
<script src="/public/js/angularjs/lib/angular-route.min.js"></script>
<script src="/public/js/angularjs/lib/dirPagination.js"></script>
<script src="/public/js/html2canvas.js"></script>
<script src="/public/js/script.js"></script>
<script src="/public/js/common_function.js"></script>
<script src="/public/js/angularjs/app.js"></script>
<script src="/public/js/angularjs/route/route.js"></script>
<script src="/public/js/angularjs/config/config.js"></script>
<script src="/public/js/angularjs/service/common.js"></script>
<script src="/public/js/angularjs/service/fileupload.js"></script>
<script src="/public/js/angularjs/directive/directive.js"></script>
<script src="/public/js/angularjs/controller/WebWorldCtrl.js"></script>
<script src="/public/js/angularjs/controller/DashboardCtrl.js"></script>

@if (count($name_apps) != 0)
@foreach ($name_apps as $name)
<!-- {{$name}} -->
<script src="/resources/views/Backend/{{$name}}/new.js"></script>
<script src="/resources/views/Backend/{{$name}}/edit.js"></script>
<script src="/resources/views/Backend/{{$name}}/index.js"></script>
@endforeach
@else
@endif

<script src="/resources/views/Backend/item_project/index_thue.js"></script>
<script src="/resources/views/Backend/item_project/index_ban.js"></script>