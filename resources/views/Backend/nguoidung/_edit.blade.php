<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="next-label">Hình</label>
            <div class="file_upload_box">
                <input type="file" class="file_image">
                <img class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $data->avatar,'data' => $table, 'time' => $data->updated_at])}}" alt="...">
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="form-group">
            <label>Tên</label>
            <input value="{{$data->email}}" type="text" class="form-control" name="email">
        </div>
        
        <div class="form-group">
            <label class="lang_label">
                <span>Mô tả ngắn</span>
                
            </label>
        
            <textarea data-lang="vn" placeholder="" class="form-control value_lang active" name="intro" rows="5">{{$data->intro}}</textarea>
        
            
        </div>
        <div class="form-group">
            <label class="next-label">Trạng thái</label>
            <select class="form-control" ng-required="true" name="status">
                <option @if($data->status == 0) selected @endif value="0">Kích hoạt</option>
                <option @if($data->status == 1) selected @endif value="1">Tạm dừng</option>
            </select>
        </div>

    </div>
    
</div>