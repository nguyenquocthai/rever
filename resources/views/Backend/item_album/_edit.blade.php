<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="next-label">Image</label>
            <div class="file_upload_box">
                <input type="file" class="file_image">
                <img class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $data->avatar,'data' => $table, 'time' => $data->updated_at])}}" alt="...">
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="form-group">
            <label>Title</label>
            <input value="{{$data->title}}" type="text" class="form-control" name="title">
        </div>

        <div class="form-group">
            <label class="next-label">Status</label>
            <select id="active_post" class="form-control" ng-required="true" name="active">
                <option value="0">Approved</option>
                <option value="1">Pending</option>
                <option value="2">Draft</option>
                <option value="4">Deleted</option>
                <option value="5">Do not browse</option>
            </select>
            <script>
                $('#active_post').val('{{$data->active}}').trigger('change');
            </script>
        </div>

        <div class="form-group">
            <label class="next-label">Location</label>
            <div class="load_position">
                <div class="position_box" data-id="">
                    @if (count($positions) != 0)
                        @foreach ($positions as $key => $position)
                        <span style="margin-bottom: 3px" data-value="{{$position->position}}" data-id="{{$position->id}}" class="item-position btn btn-icon-only grey-cascade @if($data->position == $position->position) active @endif">{{$key+1}}</span>
                        @endforeach
                    @else
                    @endif
                </div>
            </div>
        </div>

    </div>
    
</div>