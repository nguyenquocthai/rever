WebWorldApp.controller('item_projects.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Dự án';

        // Title block
        $scope.detail_block_title = 'Chi tiết dự án';
        $scope.image_block_title = 'Hình ảnh';
        $scope.category_block_title = 'Phân loại';
        $scope.seo_block_title = 'Tối ưu SEO';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            // INIT DATA

            $scope.item_project = {};
            $scope.item_project.tienich = [];
            $rootScope.app.title = $scope.item_project.title_page || $rootScope.app.title;

            $('.input-group.date').datepicker({
                format: 'dd/mm/yyyy',
            });

            if (document.getElementById('avatar')) {
                $scope.dz_avatar = new Dropzone("#avatar", {
                    maxFiles: 1,
                    addRemoveLinks: true,
                    acceptedFiles: 'image/jpeg,image/png',
                    autoProcessQueue: false,
                    init: function() {
                        this.hiddenFileInput.removeAttribute('multiple');

                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    }
                });
            }

            // Init select2 for show_item_project_cat
            select2s('#product_cat1',{
                    commonService: commonService,
                    name:'product_cat1s',
                    have_default: true,
                    //selected: e.data.category_id,
                    //title: 'code',
                    //limit: 100,
                    //where: ['type,=,1'],
                });



            // show tongquan
            // select2_tongquan(null, {
            //     commonService: commonService,
            //     have_default: true
            // });

            // load_tongquan
            commonService.requestFunction('datatable_store/' , {}, function(e) {
                $scope.datatable = $('#tbl-dataz').DataTable({
                    order: [[ 1, 'desc' ]],
                    columnDefs: [
                        { sortable: false, searchable: false, targets: [ 0 ] },
                    ],
                    displayStart: 0,
                    displayLength: 5,
                    data: e.data
                });
                $scope.datatable.search('').draw();
            });
            // End load_tongquan

            

            // Tiny mce
            tinymce.remove();
            load_tinymce('#tinymce', null);
            load_tinymce('#tinymce2', null);
            load_tinymce('#tinymce3', null);

            load_tinymce('#tinymce_en', null);
            load_tinymce('#tinymce2_en', null);
            load_tinymce('#tinymce3_en', null);
        }


        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.item_project.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.search_datatable = function() {
            var keyword = $('#keyword').val() || '';
            $scope.datatable.search(keyword).draw();
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
         $scope.submit = function () {
            var tongquans = [];
            $('.tongquan .card').each(function(index, el) {
                var vitri = $(this).find('.card-id').html();
                var id_tq = $(this).find('.card-id_tq').html();
                var item = {
                    vitri:vitri,
                    id_tq:id_tq,
                };
                tongquans.push(item);
            });
            $scope.item_project.tongquans = tongquans;

            // edit data
            var landmarks = [];

            //avatar
            landmarks = landmarks.concat($scope.dz_avatar.files.filter(function(e) {
                return e.accepted == true;
            }));

            if (landmarks.length != 0) {
                $scope.item_project.have_avatar = true;
            } else {
                $scope.item_project.have_avatar = false;
            }

            $scope.item_project.id_item_project_cat = $('.select2_item_project_cat').val();

            $scope.item_project.content = tinymce.get('tinymce').getContent();
            $scope.item_project.summary = tinymce.get('tinymce2').getContent();
            $scope.item_project.vitri = tinymce.get('tinymce3').getContent();

            $scope.item_project.content_en = tinymce.get('tinymce_en').getContent();
            $scope.item_project.summary_en = tinymce.get('tinymce2_en').getContent();
            $scope.item_project.vitri_en = tinymce.get('tinymce3_en').getContent();

            $scope.item_project.slug = slugify($scope.item_project.title);

            fileUpload.uploadFileToUrl(landmarks, $scope.item_project, 'create_item_project', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/item_projects');
                        break;
                    default:
                        break;
                }
            });

        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

        //-------------------------------------------------------------------------------
        $('#add_tienich').click(function(event) {
            var tienich = [];

            var id = 'key'+$.now();
            var data = $('#data_icon').val();
            var title = $('#title_tienich').val();
            var value = $('#value_tienich').val();

            var title_en = $('#title_tienich_en').val();
            var value_en = $('#value_tienich_en').val();

            if (data == '') {
                alert('Chưa chọn icon');
                return false;
            }

            if (title == '') {
                alert('Chưa nhập tên tổng quan');
                return false;
            }

            if (value == '') {
                alert('Chưa nhập giá trị tổng quan');
                return false;
            }

            if (title_en == '') {
                alert('Chưa nhập tên tổng quan (English)');
                return false;
            }

            if (value_en == '') {
                alert('Chưa nhập giá trị tổng quan (English)');
                return false;
            }

            tienich['id'] = id;
            tienich['data'] = data;
            tienich['title'] = title;
            tienich['value'] = value;

            tienich['title_en'] = title_en;
            tienich['value_en'] = value_en;

            $scope.item_project.tienich.push(tienich);
            $('.load-add-tienich').prepend('<li data-id="'+id+'" class="col-sm-3 item-tienich"><div class="item-mod"><span class="icon-mod"><i class="'+data+'"></i></span><span class="title-mod">'+title+'</span><span class="title-mod _en">'+title_en+'</span><span class="title-mod">'+value+'</span><span class="title-mod _en">'+value_en+'</span><i onclick="delete_tienich(&#39;'+id+'&#39;)" class="fas fa-times delete-mod"></i></div></li>');

            $('#data_icon').val('');
            $('#title_tienich').val('');
            $('#value_tienich').val('');
            $('#title_tienich_en').val('');
            $('#value_tienich_en').val('');
        });

        delete_tienich = function(id) {
            $('.item-tienich[data-id="'+id+'"]').remove();
            $scope.item_project.tienich = $scope.item_project.tienich.filter(function( obj ) {
                return obj.id !== id;
            });
        };

        //-------------------------------------------------------------------------------
        $('.icp-auto').iconpicker();

        //-------------------------------------------------------------------------------
        $('#load_tongquan').click(function(event) {
            var id_tongquan = $('.select2_tongquan').val();
            if (id_tongquan == 0) {
                alert('Bạn chưa chọn bất kỳ tổng quan nào ?');
                return false;
            }
            commonService.requestFunction('show_store/' + id_tongquan, {}, function(e) {
                $('#data_icon').val(e.data.data);
                $('#title_tienich').val(e.data.title);
                $('#value_tienich').val(e.data.value);
                $('#title_tienich_en').val(e.data.title_en);
                $('#value_tienich_en').val(e.data.value_en);
                $('.load_icon_tq').html('<i class="'+ e.data.data +'"></i>')
            }); // END LOAD DATA
        });

        //-------------------------------------------------------------------------------
        $('.themtq').click(function(event) {
            $('#modal_tongquan').modal('show');
        });

        $('#all_id_player').change(function(){
            var check = this.checked ? '1' : '0';
            if (check == 1) {
                $('.check_player').prop('checked', true);
            } else {
                $('.check_player').prop('checked', false);
            }
        });

        //-------------------------------------------------------------------------------
        $scope.add_user = function() {
            var list_ids = [];
            
            $('.check_player').each(function(i, e) {
                var value = {};
                var check_id = this.checked ? '1' : '0';
                if (check_id == '1') {
                    value['data'] = $(this).val();
                    value['id'] = $(this).attr('data-id');
                    value['title'] = $(this).attr('data-title');
                    value['title_en'] = $(this).attr('data-title_en');
                    value['value'] = $(this).attr('data-value');
                    value['value_en'] = $(this).attr('data-value_en');
                    list_ids.push(value);
                }
            });

            if (list_ids == '') {
                alert('Chưa chọn bất kỳ ai...')
                return false;
            }

            list_tongquan(list_ids);

            $('#modal_tongquan').modal('hide');

            $('#all_id_player').prop('checked', false);

            $('.check_player').prop('checked', false);
        };

        //
        // Initialize stuff
        //

        var grid = null;
        var docElem = document.documentElement;
        var demo = document.querySelector('.grid-demo');
        var gridElement = demo.querySelector('.grid');
        var filterField = demo.querySelector('.filter-field');
        var searchField = demo.querySelector('.search-field');
        var sortField = demo.querySelector('.sort-field');
        var layoutField = demo.querySelector('.layout-field');
        var addItemsElement = demo.querySelector('.add-more-items');
        var characters = 'abcdefghijklmnopqrstuvwxyz';
        var filterOptions = ['red', 'blue', 'green'];
        var dragOrder = [];
        var uuid = 0;
        var filterFieldValue;
        var sortFieldValue;
        var layoutFieldValue;
        var searchFieldValue;

        //
        // Grid helper functions
        //

        function list_tongquan(arr) {
            // Generate new elements.
            var newElems = generateElements(arr);

            // Set the display of the new elements to "none" so it will be hidden by
            // default.
            newElems.forEach(function (item) {
                item.style.display = 'none';
            });

            // Add the elements to the grid.
            var newItems = grid.add(newElems);

            // Update UI indices.
            updateIndices();

            // Sort the items only if the drag sorting is not active.
            if (sortFieldValue !== 'order') {
                grid.sort(sortFieldValue === 'title' ? compareItemTitle : compareItemColor);
                dragOrder = dragOrder.concat(newItems);
            }

            // Finally filter the items.
            filter();
        }

        function initDemo() {

            initGrid();

            // Reset field values.
            searchField.value = '';
            [sortField, filterField, layoutField].forEach(function (field) {
                field.value = field.querySelectorAll('option')[0].value;
            });

            // Set inital search query, active filter, active sort value and active layout.
            searchFieldValue = searchField.value.toLowerCase();
            filterFieldValue = filterField.value;
            sortFieldValue = sortField.value;
            layoutFieldValue = layoutField.value;

            // Search field binding.
            searchField.addEventListener('keyup', function () {
                var newSearch = searchField.value.toLowerCase();
                if (searchFieldValue !== newSearch) {
                    searchFieldValue = newSearch;
                    filter();
                }
            });

            // Filter, sort and layout bindings.
            filterField.addEventListener('change', filter);
            sortField.addEventListener('change', sort);
            layoutField.addEventListener('change', changeLayout);

            // Add/remove items bindings.
            addItemsElement.addEventListener('click', addItems);

            gridElement.addEventListener('click', function (e) {
                if (elementMatches(e.target, '.card-remove, .card-remove i')) {
                    removeItem(e);
                }
            });

        }

        function initGrid() {

            var dragCounter = 0;
            var arr = [];

            grid = new Muuri(gridElement, {
                    items: generateElements(arr),
                    layoutDuration: 400,
                    layoutEasing: 'ease',
                    dragEnabled: true,
                    dragSortInterval: 50,
                    dragContainer: document.body,
                    dragStartPredicate: function (item, event) {
                        var isDraggable = sortFieldValue === 'order';
                        var isRemoveAction = elementMatches(event.target, '.card-remove, .card-remove i');
                        return isDraggable && !isRemoveAction ? Muuri.ItemDrag.defaultStartPredicate(item, event) : false;
                    },
                    dragReleaseDuration: 400,
                    dragReleseEasing: 'ease'
                })
                .on('dragStart', function () {
                    ++dragCounter;
                    docElem.classList.add('dragging');
                })
                .on('dragEnd', function () {
                    if (--dragCounter < 1) {
                        docElem.classList.remove('dragging');
                    }
                })
                .on('move', updateIndices)
                .on('sort', updateIndices);

        }

        function filter() {

            filterFieldValue = filterField.value;
            grid.filter(function (item) {
                var element = item.getElement();
                var isSearchMatch = !searchFieldValue ? true : (element.getAttribute('data-title') || '').toLowerCase().indexOf(searchFieldValue) > -1;
                var isFilterMatch = !filterFieldValue ? true : (element.getAttribute('data-color') || '') === filterFieldValue;
                return isSearchMatch && isFilterMatch;
            });

        }

        function sort() {

            // Do nothing if sort value did not change.
            var currentSort = sortField.value;
            if (sortFieldValue === currentSort) {
                return;
            }

            // If we are changing from "order" sorting to something else
            // let's store the drag order.
            if (sortFieldValue === 'order') {
                dragOrder = grid.getItems();
            }

            // Sort the items.
            grid.sort(
                currentSort === 'title' ? compareItemTitle :
                currentSort === 'color' ? compareItemColor :
                dragOrder
            );

            // Update indices and active sort value.
            updateIndices();
            sortFieldValue = currentSort;

        }

        function addItems() {

            var arr = [];

            // Generate new elements.
            var newElems = generateElements(arr);

            // Set the display of the new elements to "none" so it will be hidden by
            // default.
            newElems.forEach(function (item) {
                item.style.display = 'none';
            });

            // Add the elements to the grid.
            var newItems = grid.add(newElems);

            // Update UI indices.
            updateIndices();

            // Sort the items only if the drag sorting is not active.
            if (sortFieldValue !== 'order') {
                grid.sort(sortFieldValue === 'title' ? compareItemTitle : compareItemColor);
                dragOrder = dragOrder.concat(newItems);
            }

            // Finally filter the items.
            filter();

        }

        function removeItem(e) {

            var elem = elementClosest(e.target, '.item');
            grid.hide(elem, {
                onFinish: function (items) {
                    var item = items[0];
                    grid.remove(item, {
                        removeElements: true
                    });
                    if (sortFieldValue !== 'order') {
                        var itemIndex = dragOrder.indexOf(item);
                        if (itemIndex > -1) {
                            dragOrder.splice(itemIndex, 1);
                        }
                    }
                }
            });
            updateIndices();

        }

        function changeLayout() {

            layoutFieldValue = layoutField.value;
            grid._settings.layout = {
                horizontal: false,
                alignRight: layoutFieldValue.indexOf('right') > -1,
                alignBottom: layoutFieldValue.indexOf('bottom') > -1,
                fillGaps: layoutFieldValue.indexOf('fillgaps') > -1
            };
            grid.layout();

        }

        //
        // Generic helper functions
        //

        function generateElements(arr) {

            var ret = [];
            for (var i = 0; i < arr.length; i++) {
                ret.push(generateElement(
                    ++uuid,
                    arr[i].id,
                    arr[i].data,
                    arr[i].title,
                    arr[i].title_en,
                    arr[i].value,
                    arr[i].value_en,
                ));
            }
            return ret;
        }

        function generateElement(id, id_tq, data, title, title_en, value, value_en) {

            var itemElem = document.createElement('div');
            var classNames = 'item h1 w2';
            var itemTemplate = '' +
                '<div class="' + classNames + '" data-id="' + id + '" data-color="green" data-title="' + title + '">' +
                '<div class="item-content">' +
                '<div class="card">' +
                '<div class="card-id">' + id + '</div>' +
                '<div class="card_child">' +
                '<div class="card-id_tq">' + id_tq + '</div>' +
                '<div class="card-id_data"><i class="' + data + '"></i></div>' +
                '<div class="card-title">' + title + '</div>' +
                '<div class="card-title_en">' + title_en + '</div>' +
                '<div class="card-value">' + value + '</div>' +
                '<div class="card-value_en">' + value_en + '</div>' +
                '</div>' +
                '<div class="card-remove"><i class="fa fa-trash-o" aria-hidden="true"></i></div>' +
                '</div>' +
                '</div>' +
                '</div>';

            itemElem.innerHTML = itemTemplate;
            return itemElem.firstChild;
        }

        function getRandomItem(collection) {

            return collection[Math.floor(Math.random() * collection.length)];

        }

        // https://stackoverflow.com/a/7228322
        function getRandomInt(min, max) {

            return Math.floor(Math.random() * (max - min + 1) + min);

        }

        function generateRandomWord(length) {

            var ret = '';
            for (var i = 0; i < length; i++) {
                ret += getRandomItem(characters);
            }
            return ret;

        }

        function compareItemTitle(a, b) {

            var aVal = a.getElement().getAttribute('data-title') || '';
            var bVal = b.getElement().getAttribute('data-title') || '';
            return aVal < bVal ? -1 : aVal > bVal ? 1 : 0;

        }

        function compareItemColor(a, b) {

            var aVal = a.getElement().getAttribute('data-color') || '';
            var bVal = b.getElement().getAttribute('data-color') || '';
            return aVal < bVal ? -1 : aVal > bVal ? 1 : compareItemTitle(a, b);

        }

        function updateIndices() {

            grid.getItems().forEach(function (item, i) {
                item.getElement().setAttribute('data-id', i + 1);
                item.getElement().querySelector('.card-id').innerHTML = i + 1;
            });

        }

        function elementMatches(element, selector) {

            var p = Element.prototype;
            return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector).call(element, selector);

        }

        function elementClosest(element, selector) {

            if (window.Element && !Element.prototype.closest) {
                var isMatch = elementMatches(element, selector);
                while (!isMatch && element && element !== document) {
                    element = element.parentNode;
                    isMatch = element && element !== document && elementMatches(element, selector);
                }
                return element && element !== document ? element : null;
            } else {
                return element.closest(selector);
            }

        }

        //
        // Fire it up!
        //

        initDemo();
        var arr = [];
        list_tongquan(arr);
    }
]);
