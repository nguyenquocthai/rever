WebWorldApp.controller('item_projects.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'cho thuê';

        // Title block
        $scope.detail_block_title = 'Chi tiết cho thuê';
        $scope.detail_block_title2 = 'Thông tin cho thuê';
        $scope.image_block_title = 'Hình ảnh';

        $id_group = 2;
        $scope.img_phong = [];

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // LOAD DATA
            commonService.requestFunction('show_item_project/' + $routeParams.id, {}, function(e) {
                if (!e.data) {
                    $location.path('/admin/item_projects');
                    return false;
                }
                $scope.item_project = e.data;
                $('#load_medias').html($scope.item_project.medias);
                $('#load_tienich').html($scope.item_project.tienichs);
                $('#load_tq').html($scope.item_project.tongquans);

                list_tongquan($scope.item_project.tongquans);
                list_tienich($scope.item_project.tienichs);

                $('.input-group.date').datepicker({
                    format: 'dd/mm/yyyy',
                });
                if($scope.item_project.type == 0){
                // select2_item_project_cat
                    select2s('#product_cat1s',{
                        commonService: commonService,
                        name:'product_cat1s',
                        have_default: true,
                        selected: e.data.category_id,
                        title: 'title',
                        //limit: 100,
                        //where: ['type,=,1'],
                    });
                }
                if($scope.item_project.type == 1){
                    $('.donvi_mod').html('/m<sup>2</sup>');
                    $('.type_mark').html('Giá bán');
                    $rootScope.app.title = 'Bất động sản bán';
                    $scope.detail_block_title = 'Chi tiết bán';
                    $scope.detail_block_title2 = 'Thông tin bán';

                    select2s('#product_cat1s',{
                        commonService: commonService,
                        name:'product_cat1s',
                        have_default: true,
                        selected: e.data.category_id,
                        title: 'title',
                        //limit: 100,
                        where: ['type,=,1'],
                    });
                }
                
                $('.tb_active').val($scope.item_project.active).trigger('change');
                $('.metarobot').val($scope.item_project.metarobot).trigger('change');

                // load image avatar
                testImage(e.data['avatar'], attr_url_image);
            
                

                // tinhthanh
                select2s('#tinhthanh',{
                    commonService: commonService,
                    name:'za_provinces',
                    have_default: true,
                    selected: e.data.tinhthanh,
                    title: 'name',
                });

                // quanhuyen
                select2s('#quanhuyen',{
                    commonService: commonService,
                    name:'zb_districts',
                    have_default: true,
                    selected: e.data.quanhuyen,
                    title: 'name',
                    where: ['province_id,=,'+e.data.tinhthanh],
                });

                select2_tags(null, {
                    commonService: commonService,
                    multiple: true,
                    callback: function(e) {
                        setTimeout(function() {
                            if ($scope.item_project.tags != null) {
                                e.val($scope.item_project.tags.map(function(e) { return e; })).trigger('change');
                            }
                        });
                    }
                });

                tinymce.get('tinymce').setContent(html_entity_decode($scope.item_project.content) || '');
                tinymce.get('tinymce1').setContent(html_entity_decode($scope.item_project.short_content) || '');

            });

            // Tiny mce
            tinymce.remove();
            load_tinymce('#tinymce', null);
            load_tinymce('#tinymce1', null);

            // load_tongquan
            commonService.requestFunction('datatable_store/' , {}, function(e) {
                $scope.datatable = $('#tbl-dataz').DataTable({
                    order: [[ 1, 'desc' ]],
                    columnDefs: [
                        { sortable: false, searchable: false, targets: [ 0 ] },
                    ],
                    displayStart: 0,
                    displayLength: 5,
                    data: e.data
                });
                $scope.datatable.search('').draw();
            });
            // End load_tongquan

            // load_tienich
            commonService.requestFunction('datatable_store2/' , {}, function(e) {
                $scope.datatable2 = $('#tbl-dataz2').DataTable({
                    order: [[ 1, 'desc' ]],
                    columnDefs: [
                        { sortable: false, searchable: false, targets: [ 0 ] },
                    ],
                    displayStart: 0,
                    displayLength: 5,
                    data: e.data
                });
                $scope.datatable2.search('').draw();
            });
            // End load_tienich
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.item_project.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.search_datatable = function() {
            var keyword = $('#keyword').val() || '';
            $scope.datatable.search(keyword).draw();
        };

        //-------------------------------------------------------------------------------
        $scope.search_datatable2 = function() {
            var keyword = $('#keyword2').val() || '';
            $scope.datatable2.search(keyword).draw();
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {
            // get tong quan
            var tongquans = [];
            $('.tongquan .card').each(function(index, el) {
                var vitri = $(this).find('.card-id').html();
                var id_tq = $(this).find('.card-id_tq').html();
                var data = $(this).find('.card-id_data i').attr('class');
                var title = $(this).find('.card-title').html();
                var title_en = $(this).find('.card-title_en').html();
                var value = $(this).find('.card-value').html();
                var value_en = $(this).find('.card-value_en').html();

                var item_tq = {
                    vitri:vitri,
                    id_tq:id_tq,
                    data:data,
                    title:title,
                    title_en:title_en,
                    value:value,
                    value_en:value_en
                };
                tongquans.push(item_tq);
            });
            var jsontg = JSON.stringify(tongquans);

            // get tien ich
            var tienichs = [];
            $('.tienich .card').each(function(index, el) {
                var vitri = $(this).find('.card-id').html();
                var id_tq = $(this).find('.card-id_tq').html();
                var data = $(this).find('.card-id_data i').attr('class');
                var title = $(this).find('.card-title').html();
                var title_en = $(this).find('.card-title_en').html();

                var item_ti = {
                    vitri:vitri,
                    id_tq:id_tq,
                    data:data,
                    title:title,
                    title_en:title_en,
                };
                tienichs.push(item_ti);
            });
            var jsonti = JSON.stringify(tienichs);

            var files = [];
            if ($("#upload_input").get(0).files[0])
                files = files.concat($("#upload_input").get(0).files[0]);

            $scope.item_project['_method'] = 'PUT';
            $scope.item_project.content = tinymce.get('tinymce').getContent();
            $scope.item_project.short_content = tinymce.get('tinymce1').getContent();
            $scope.item_project.category_id = $('.select2_item_project_cat').val();
            $scope.item_project.tinhthanh = $('.tinhthanh').val();
            $scope.item_project.quanhuyen = $('.quanhuyen').val();
            $scope.item_project.active = $('.tb_active').val();
            $scope.item_project.metarobot = $('.metarobot').val();
            $scope.item_project.id_project = $('.duan').val();
            $scope.item_project.tags = JSON.stringify($('.select2_tags').val());
            $scope.item_project.slug = slugify($scope.item_project.title);
            $scope.item_project.medias = "";
            $scope.item_project.tienichs = jsonti;
            $scope.item_project.tongquans = jsontg;

            $scope.item_project.tinhthanh = $('#tinhthanh').val();
            $scope.item_project.quanhuyen = $('#quanhuyen').val();

            fileUpload.uploadFileToUrl(files, $scope.item_project, 'update_item_project/' + $routeParams.id, function(e) {
                switch (e.code) {
                    case 200:
                        if($scope.item_project.type == 0){
                            $location.path('/admin/item_projects');
                        } else {
                            $location.path('/admin/item_project_2s');
                        }
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

        //----------------------------------------------------------------------------------
        $('#modalMap').on('shown.bs.modal', function() {
            // GOOGLE MAP
            $scope.pointer = $scope.pointer || {};
            if ($scope.item_project.vitri == "" || $scope.item_project.vitri == null) {
               $scope.item_project.vitri = "10.824758628439996,106.62956724047547";
            }
            var geo = $scope.item_project.vitri.split(',');
            if (geo.length == 2) {
                $scope.pointer.latitude = geo[0];
                $scope.pointer.longitude = geo[1];
            }

            init_google_map($scope, {
                marker: $scope.marker,
                pointer: $scope.pointer,
            });
        });

        //-------------------------------------------------------------------------------
        $scope.apply_geolocation = function() {
            $scope.item_project.vitri = $scope.pointer.latitude + ',' +$scope.pointer.longitude;
            $("#modalMap").modal('toggle');
        };

        //-------------------------------------------------------------------------------
        $(".fileUpload").change(readURL);
        $("form").on('click', '.delbtn', function (e) {
            reset_inputfile($(this));
            button.innerText = 'Chọn hình';
        });

        var button = document.getElementById('upload_button');
        var input  = document.getElementById('upload_input');


        input.style.display = 'none';
        button.style.display = 'initial';

        button.addEventListener('click', function (e) {
            e.preventDefault();
            input.click();
        });

        input.addEventListener('change', function () {
           if( this.value != '')
            button.innerText = 'Đổi hình';
        });

        //-------------------------------------------------------------------------------
        $("#gallery_player").change(function() {
            var list_files = [];
            var files = this.files;

            $('.item-gallery').remove();
            for (var i = 0; i < files.length; i++) {
                var file = this.files[i];
                var fileType = file["type"];
                var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(fileType, ValidImageTypes) > 0) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#list-gallery').append('<div class="item-gallery col-sm-2"><div class="img-mod"><img src="' + e.target.result +'" ></div></div>').show().fadeIn("slow");
                        //$('.item-gallery:last').after('<img class="item-gallery" src="' + e.target.result +'" >').show().fadeIn("slow");
                    }
                    reader.readAsDataURL(file);
                    list_files.push(file);
                }
            }

            var files = [];
            var list_image = $("#gallery_player").get(0).files;

            for (var i = 0; i < list_image.length; i++) {
                files = files.concat($("#gallery_player").get(0).files[i]);
            }
            $scope.item_project.album = files;
        });

        //-------------------------------------------------------------------------------
        delete_media = function(id) {
            confirmPopup('Xóa', 'Bạn muốn xóa hình này' , function() {
                commonService.requestFunction('delete_media/' + id, {}, function(e) {
                    switch (e.code) {
                        case 200:
                           $('.item-load-media[data-id="'+id+'"]').remove();
                        default:
                            break;
                    }
                });
            });
        };

        $('.tinhthanh').change(function(event) {
            var check = $(this).attr('data-ready');

            if (check == 1) {
                var tinhthanh = $(this).val();
                $('.quanhuyen').empty();
                // quanhuyen
                quanhuyen(null, {
                    commonService: commonService,
                    tinhthanh: tinhthanh,
                    have_default: true,
                    callback: function(e) {
                        e.val(0).trigger('change');
                    }
                });
            }
        });

        //-------------------------------------------------------------------------------
        $scope.add_user = function() {
            var list_ids = [];
            
            $('.check_player').each(function(i, e) {
                var value = {};
                var check_id = this.checked ? '1' : '0';
                if (check_id == '1') {
                    value['data'] = $(this).val();
                    value['id'] = $(this).attr('data-id');
                    value['title'] = $(this).attr('data-title');
                    value['title_en'] = $(this).attr('data-title_en');
                    value['value'] = $(this).attr('data-value');
                    value['value_en'] = $(this).attr('data-value_en');
                    list_ids.push(value);
                }
            });

            if (list_ids == '') {
                alert('Chưa chọn bất kỳ tổng quan...')
                return false;
            }

            list_tongquan(list_ids);

            $('#modal_tongquan').modal('hide');

            $('#all_id_player').prop('checked', false);

            $('.check_player').prop('checked', false);
        };

        //-------------------------------------------------------------------------------
        $('#add_tienich').click(function(event) {
            var tienich = [];
            var file = [];

            var data = $('#data_icon').val();
            var title = $('#title_tienich').val();

            if (data == '') {
                alert('Chưa chọn icon');
                return false;
            }

            if (title == '') {
                alert('Chưa nhập tên tiện ích');
                return false;
            }

            $scope.tienich['data'] = data;
            $scope.tienich['title'] = title;
            $scope.tienich['id_product'] = $routeParams.id;
            $scope.tienich['id_group'] = $id_group;

            fileUpload.uploadFileToUrl(file, $scope.tienich, 'create_tienich', function(e) {
                switch (e.code) {
                    case 200:
                        $('#load_tienich').prepend('<li data-id="'+e.data.id+'" class="col-sm-3 item-tienich"><div class="item-mod"><span class="icon-mod"><i class="'+e.data.data+'"></i></span><span class="title-mod">'+e.data.title+'</span><span class="title-mod _en">'+e.data.title_en+'</span><i onclick="delete_tienich('+e.data.id+')" class="fas fa-times delete-mod"></i></div></li>');
                        $('#data_icon').val('');
                        $('#title_tienich').val('');
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('#add_tq').click(function(event) {
            var tq = [];
            var file = [];

            var data = $('#data_tq').val();
            var title = $('#title_tq').val();
            var value = $('#value_tq').val();

            if (data == '') {
                alert('Chưa chọn icon');
                return false;
            }

            if (title == '') {
                alert('Chưa nhập tên tổng quan');
                return false;
            }

            if (value == '') {
                alert('Chưa nhập giá trị tổng quan');
                return false;
            }

            $scope.tienich['data'] = data;
            $scope.tienich['title'] = title;
            $scope.tienich['value'] = value;
            $scope.tienich['id_product'] = $routeParams.id;
            $scope.tienich['id_group'] = 22;

            fileUpload.uploadFileToUrl(file, $scope.tienich, 'create_tienich', function(e) {
                switch (e.code) {
                    case 200:
                        $('#load_tq').prepend('<li data-id="'+e.data.id+'" class="col-sm-3 item-tq"><div class="item-mod"><span class="icon-mod"><i class="'+e.data.data+'"></i></span><span class="title-mod">'+e.data.title+'</span><span class="title-mod _en">'+e.data.title_en+'</span><span class="title-mod">'+e.data.value+'</span><span class="title-mod _en">'+e.data.value_en+'</span><i onclick="delete_tq('+e.data.id+')" class="fas fa-times delete-mod"></i></div></li>');
                        $('#data_tq').val('');
                        $('#title_tq').val('');
                        $('#value_tq').val('');
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('.themtq').click(function(event) {
            $('#modal_tongquan').modal('show');
        });

        $('#all_id_player').change(function(){
            var check = this.checked ? '1' : '0';
            if (check == 1) {
                $('.check_player').prop('checked', true);
            } else {
                $('.check_player').prop('checked', false);
            }
        });

        //
        // Initialize stuff
        //

        var grid = null;
        var docElem = document.documentElement;
        var demo = document.querySelector('.grid-demo');
        var gridElement = demo.querySelector('.grid');
        var filterField = demo.querySelector('.filter-field');
        var searchField = demo.querySelector('.search-field');
        var sortField = demo.querySelector('.sort-field');
        var layoutField = demo.querySelector('.layout-field');
        var addItemsElement = demo.querySelector('.add-more-items');
        var characters = 'abcdefghijklmnopqrstuvwxyz';
        var filterOptions = ['red', 'blue', 'green'];
        var dragOrder = [];
        var uuid = 0;
        var filterFieldValue;
        var sortFieldValue;
        var layoutFieldValue;
        var searchFieldValue;

        //
        // Grid helper functions
        //

        function list_tongquan(arr) {
            // Generate new elements.
            var newElems = generateElements(arr);

            // Set the display of the new elements to "none" so it will be hidden by
            // default.
            newElems.forEach(function (item) {
                item.style.display = 'none';
            });

            // Add the elements to the grid.
            var newItems = grid.add(newElems);

            // Update UI indices.
            updateIndices();

            // Sort the items only if the drag sorting is not active.
            if (sortFieldValue !== 'order') {
                grid.sort(sortFieldValue === 'title' ? compareItemTitle : compareItemColor);
                dragOrder = dragOrder.concat(newItems);
            }

            // Finally filter the items.
            filter();
        }

        function initDemo() {

            initGrid();

            // Reset field values.
            searchField.value = '';
            [sortField, filterField, layoutField].forEach(function (field) {
                field.value = field.querySelectorAll('option')[0].value;
            });

            // Set inital search query, active filter, active sort value and active layout.
            searchFieldValue = searchField.value.toLowerCase();
            filterFieldValue = filterField.value;
            sortFieldValue = sortField.value;
            layoutFieldValue = layoutField.value;

            // Search field binding.
            searchField.addEventListener('keyup', function () {
                var newSearch = searchField.value.toLowerCase();
                if (searchFieldValue !== newSearch) {
                    searchFieldValue = newSearch;
                    filter();
                }
            });

            // Filter, sort and layout bindings.
            filterField.addEventListener('change', filter);
            sortField.addEventListener('change', sort);
            layoutField.addEventListener('change', changeLayout);

            // Add/remove items bindings.
            addItemsElement.addEventListener('click', addItems);

            gridElement.addEventListener('click', function (e) {
                if (elementMatches(e.target, '.card-remove, .card-remove i')) {
                    removeItem(e);
                }
            });

        }

        function initGrid() {

            var dragCounter = 0;
            var arr = [];

            grid = new Muuri(gridElement, {
                    items: generateElements(arr),
                    layoutDuration: 400,
                    layoutEasing: 'ease',
                    dragEnabled: true,
                    dragSortInterval: 50,
                    dragContainer: document.body,
                    dragStartPredicate: function (item, event) {
                        var isDraggable = sortFieldValue === 'order';
                        var isRemoveAction = elementMatches(event.target, '.card-remove, .card-remove i');
                        return isDraggable && !isRemoveAction ? Muuri.ItemDrag.defaultStartPredicate(item, event) : false;
                    },
                    dragReleaseDuration: 400,
                    dragReleseEasing: 'ease'
                })
                .on('dragStart', function () {
                    ++dragCounter;
                    docElem.classList.add('dragging');
                })
                .on('dragEnd', function () {
                    if (--dragCounter < 1) {
                        docElem.classList.remove('dragging');
                    }
                })
                .on('move', updateIndices)
                .on('sort', updateIndices);

        }

        function filter() {

            filterFieldValue = filterField.value;
            grid.filter(function (item) {
                var element = item.getElement();
                var isSearchMatch = !searchFieldValue ? true : (element.getAttribute('data-title') || '').toLowerCase().indexOf(searchFieldValue) > -1;
                var isFilterMatch = !filterFieldValue ? true : (element.getAttribute('data-color') || '') === filterFieldValue;
                return isSearchMatch && isFilterMatch;
            });

        }

        function sort() {

            // Do nothing if sort value did not change.
            var currentSort = sortField.value;
            if (sortFieldValue === currentSort) {
                return;
            }

            // If we are changing from "order" sorting to something else
            // let's store the drag order.
            if (sortFieldValue === 'order') {
                dragOrder = grid.getItems();
            }

            // Sort the items.
            grid.sort(
                currentSort === 'title' ? compareItemTitle :
                currentSort === 'color' ? compareItemColor :
                dragOrder
            );

            // Update indices and active sort value.
            updateIndices();
            sortFieldValue = currentSort;

        }

        function addItems() {

            var arr = [];

            // Generate new elements.
            var newElems = generateElements(arr);

            // Set the display of the new elements to "none" so it will be hidden by
            // default.
            newElems.forEach(function (item) {
                item.style.display = 'none';
            });

            // Add the elements to the grid.
            var newItems = grid.add(newElems);

            // Update UI indices.
            updateIndices();

            // Sort the items only if the drag sorting is not active.
            if (sortFieldValue !== 'order') {
                grid.sort(sortFieldValue === 'title' ? compareItemTitle : compareItemColor);
                dragOrder = dragOrder.concat(newItems);
            }

            // Finally filter the items.
            filter();

        }

        function removeItem(e) {

            var elem = elementClosest(e.target, '.item');
            grid.hide(elem, {
                onFinish: function (items) {
                    var item = items[0];
                    grid.remove(item, {
                        removeElements: true
                    });
                    if (sortFieldValue !== 'order') {
                        var itemIndex = dragOrder.indexOf(item);
                        if (itemIndex > -1) {
                            dragOrder.splice(itemIndex, 1);
                        }
                    }
                }
            });
            updateIndices();

        }

        function changeLayout() {

            layoutFieldValue = layoutField.value;
            grid._settings.layout = {
                horizontal: false,
                alignRight: layoutFieldValue.indexOf('right') > -1,
                alignBottom: layoutFieldValue.indexOf('bottom') > -1,
                fillGaps: layoutFieldValue.indexOf('fillgaps') > -1
            };
            grid.layout();

        }

        //
        // Generic helper functions
        //

        function generateElements(arr) {

            var ret = [];
            for (var i = 0; i < arr.length; i++) {
                ret.push(generateElement(
                    ++uuid,
                    arr[i].id,
                    arr[i].data,
                    arr[i].title,
                    arr[i].title_en,
                    arr[i].value,
                    arr[i].value_en,
                ));
            }
            return ret;
        }

        function generateElement(id, id_tq, data, title, title_en, value, value_en) {

            var itemElem = document.createElement('div');
            var classNames = 'item h1 w2';
            var itemTemplate = '' +
                '<div class="' + classNames + '" data-id="' + id + '" data-color="green" data-title="' + title + '">' +
                '<div class="item-content">' +
                '<div class="card">' +
                '<div class="card-id">' + id + '</div>' +
                '<div class="card_child">' +
                '<div class="card-id_tq">' + id_tq + '</div>' +
                '<div class="card-id_data"><i class="' + data + '"></i></div>' +
                '<div class="card-title">' + title + '</div>' +
                '<div class="card-title_en">' + title_en + '</div>' +
                '<div class="card-value">' + value + '</div>' +
                '<div class="card-value_en">' + value_en + '</div>' +
                '</div>' +
                '<div class="card-remove"><i class="fa fa-trash-o" aria-hidden="true"></i></div>' +
                '</div>' +
                '</div>' +
                '</div>';

            itemElem.innerHTML = itemTemplate;
            return itemElem.firstChild;
        }

        function getRandomItem(collection) {

            return collection[Math.floor(Math.random() * collection.length)];

        }

        // https://stackoverflow.com/a/7228322
        function getRandomInt(min, max) {

            return Math.floor(Math.random() * (max - min + 1) + min);

        }

        function generateRandomWord(length) {

            var ret = '';
            for (var i = 0; i < length; i++) {
                ret += getRandomItem(characters);
            }
            return ret;

        }

        function compareItemTitle(a, b) {

            var aVal = a.getElement().getAttribute('data-title') || '';
            var bVal = b.getElement().getAttribute('data-title') || '';
            return aVal < bVal ? -1 : aVal > bVal ? 1 : 0;

        }

        function compareItemColor(a, b) {

            var aVal = a.getElement().getAttribute('data-color') || '';
            var bVal = b.getElement().getAttribute('data-color') || '';
            return aVal < bVal ? -1 : aVal > bVal ? 1 : compareItemTitle(a, b);

        }

        function updateIndices() {

            grid.getItems().forEach(function (item, i) {
                item.getElement().setAttribute('data-id', i + 1);
                item.getElement().querySelector('.card-id').innerHTML = i + 1;
            });

        }

        function elementMatches(element, selector) {

            var p = Element.prototype;
            return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector).call(element, selector);

        }

        function elementClosest(element, selector) {

            if (window.Element && !Element.prototype.closest) {
                var isMatch = elementMatches(element, selector);
                while (!isMatch && element && element !== document) {
                    element = element.parentNode;
                    isMatch = element && element !== document && elementMatches(element, selector);
                }
                return element && element !== document ? element : null;
            } else {
                return element.closest(selector);
            }

        }

        //
        // Fire it up!
        //

        initDemo();
        var arr = [];
        list_tongquan(arr);


        // Tiện ích-------------------------------------------------------------------------
        //-------------------------------------------------------------------------------
        $('.themtienich').click(function(event) {
            $('#modal_tienich').modal('show');
        });

        $('#all_id_player2').change(function(){
            var check = this.checked ? '1' : '0';
            if (check == 1) {
                $('.check_player2').prop('checked', true);
            } else {
                $('.check_player2').prop('checked', false);
            }
        });

        //-------------------------------------------------------------------------------
        $scope.add_user2 = function() {
            var list_ids2 = [];
            
            $('.check_player2').each(function(i, e) {
                var value = {};
                var check_id = this.checked ? '1' : '0';
                if (check_id == '1') {
                    value['data'] = $(this).val();
                    value['id'] = $(this).attr('data-id');
                    value['title'] = $(this).attr('data-title');
                    value['title_en'] = $(this).attr('data-title_en');
                    list_ids2.push(value);
                }
            });

            if (list_ids2 == '') {
                alert('Chưa chọn bất kỳ tiện ích...')
                return false;
            }

            list_tienich(list_ids2);

            $('#modal_tienich').modal('hide');

            $('#all_id_player2').prop('checked', false);

            $('.check_player2').prop('checked', false);
        };

        var grid2 = null;
        var docElem2 = document.documentElement;
        var demo2 = document.querySelector('.grid-demo2');
        var gridElement2 = demo2.querySelector('.grid2');
        var filter2Field2 = demo2.querySelector('.filter2-field2');
        var searchField2 = demo2.querySelector('.search-field2');
        var sortField2 = demo2.querySelector('.sort-field2');
        var layoutField2 = demo2.querySelector('.layout-field2');
        var addItemsElement2 = demo2.querySelector('.add-more-items2');
        var characters2 = 'abcdefghijklmnopqrstuvwxyz';
        var filter2Options2 = ['red', 'blue', 'green'];
        var dragOrder2 = [];
        var uuid2 = 0;
        var filter2FieldValue2;
        var sortFieldValue2;
        var layoutFieldValue2;
        var searchFieldValue2;

        function list_tienich(arr2) {
            // Generate new elements.
            var newElems2 = generateElements2(arr2);

            // Set the display of the new elements to "none" so it will be hidden by
            // default.
            newElems2.forEach(function (item) {
                item.style.display = 'none';
            });

            // Add the elements to the grid.
            var newItems = grid2.add(newElems2);

            // Update UI indices.
            updateIndices2();

            // Sort the items only if the drag sorting is not active.
            if (sortFieldValue2 !== 'order') {
                grid2.sort2(sortFieldValue2 === 'title' ? compareItemTitle2 : compareItemColor2);
                dragOrder2 = dragOrder2.concat(newItems);
            }

            // Finally filter2 the items.
            filter2();
        }

        function initDemo2() {

            initGrid2();

            // Reset field values.
            searchField2.value = '';
            [sortField2, filter2Field2, layoutField2].forEach(function (field) {
                field.value = field.querySelectorAll('option')[0].value;
            });

            // Set inital search query, active filter2, active sort value and active layout.
            searchFieldValue2 = searchField2.value.toLowerCase();
            filter2FieldValue2 = filter2Field2.value;
            sortFieldValue2 = sortField2.value;
            layoutFieldValue2 = layoutField2.value;

            // Search field binding.
            searchField2.addEventListener('keyup', function () {
                var newSearch = searchField2.value.toLowerCase();
                if (searchFieldValue2 !== newSearch) {
                    searchFieldValue2 = newSearch;
                    filter2();
                }
            });

            // Filter, sort and layout bindings.
            filter2Field2.addEventListener('change', filter2);
            sortField2.addEventListener('change', sort2);
            layoutField2.addEventListener('change', changeLayout2);

            // Add/remove items bindings.
            addItemsElement2.addEventListener('click', addItems2);

            gridElement2.addEventListener('click', function (e) {
                if (elementMatches2(e.target, '.card-remove, .card-remove i')) {
                    removeItem2(e);
                }
            });

        }

        function initGrid2() {

            var dragCounter = 0;
            var arr2 = [];

            grid2 = new Muuri(gridElement2, {
                    items: generateElements2(arr2),
                    layoutDuration: 400,
                    layoutEasing: 'ease',
                    dragEnabled: true,
                    dragSortInterval: 50,
                    dragContainer: document.body,
                    dragStartPredicate: function (item, event) {
                        var isDraggable = sortFieldValue2 === 'order';
                        var isRemoveAction = elementMatches2(event.target, '.card-remove, .card-remove i');
                        return isDraggable && !isRemoveAction ? Muuri.ItemDrag.defaultStartPredicate(item, event) : false;
                    },
                    dragReleaseDuration: 400,
                    dragReleseEasing: 'ease'
                })
                .on('dragStart', function () {
                    ++dragCounter;
                    docElem2.classList.add('dragging');
                })
                .on('dragEnd', function () {
                    if (--dragCounter < 1) {
                        docElem2.classList.remove('dragging');
                    }
                })
                .on('move', updateIndices2)
                .on('sort2', updateIndices2);

        }

        function filter2() {

            filter2FieldValue2 = filter2Field2.value;
            grid2.filter(function (item) {
                var element = item.getElement();
                var isSearchMatch = !searchFieldValue2 ? true : (element.getAttribute('data-title') || '').toLowerCase().indexOf(searchFieldValue2) > -1;
                var isFilterMatch = !filter2FieldValue2 ? true : (element.getAttribute('data-color') || '') === filter2FieldValue2;
                return isSearchMatch && isFilterMatch;
            });

        }

        function sort2() {

            // Do nothing if sort value did not change.
            var currentSort = sortField2.value;
            if (sortFieldValue2 === currentSort) {
                return;
            }

            // If we are changing from "order" sorting to something else
            // let's store the drag order.
            if (sortFieldValue2 === 'order') {
                dragOrder2 = grid2.getItems();
            }

            // Sort the items.
            grid2.sort2(
                currentSort === 'title' ? compareItemTitle2 :
                currentSort === 'color' ? compareItemColor2 :
                dragOrder2
            );

            // Update indices and active sort value.
            updateIndices2();
            sortFieldValue2 = currentSort;

        }

        function addItems2() {

            var arr2 = [];

            // Generate new elements.
            var newElems2 = generateElements2(arr2);

            // Set the display of the new elements to "none" so it will be hidden by
            // default.
            newElems2.forEach(function (item) {
                item.style.display = 'none';
            });

            // Add the elements to the grid.
            var newItems = grid2.add(newElems2);

            // Update UI indices.
            updateIndices2();

            // Sort the items only if the drag sorting is not active.
            if (sortFieldValue2 !== 'order') {
                grid2.sort2(sortFieldValue2 === 'title' ? compareItemTitle2 : compareItemColor2);
                dragOrder2 = dragOrder2.concat(newItems);
            }

            // Finally filter2 the items.
            filter2();

        }

        function removeItem2(e) {

            var elem = elementClosest2(e.target, '.item');
            grid2.hide(elem, {
                onFinish: function (items) {
                    var item = items[0];
                    grid2.remove(item, {
                        removeElements: true
                    });
                    if (sortFieldValue2 !== 'order') {
                        var itemIndex = dragOrder2.indexOf(item);
                        if (itemIndex > -1) {
                            dragOrder2.splice(itemIndex, 1);
                        }
                    }
                }
            });
            updateIndices2();

        }

        function changeLayout2() {

            layoutFieldValue2 = layoutField2.value;
            grid2._settings.layout = {
                horizontal: false,
                alignRight: layoutFieldValue2.indexOf('right') > -1,
                alignBottom: layoutFieldValue2.indexOf('bottom') > -1,
                fillGaps: layoutFieldValue2.indexOf('fillgaps') > -1
            };
            grid2.layout();

        }

        //
        // Generic helper functions
        //

        function generateElements2(arr2) {

            var ret = [];
            for (var i = 0; i < arr2.length; i++) {
                ret.push(generateElement2(
                    ++uuid2,
                    arr2[i].id,
                    arr2[i].data,
                    arr2[i].title,
                    arr2[i].title_en,
                ));
            }
            return ret;
        }

        function generateElement2(id, id_tq, data, title, title_en) {

            var itemElem = document.createElement('div');
            var classNames = 'item h1 w2';
            var itemTemplate = '' +
                '<div class="' + classNames + '" data-id="' + id + '" data-color="green" data-title="' + title + '">' +
                '<div class="item-content">' +
                '<div class="card">' +
                '<div class="card-id">' + id + '</div>' +
                '<div class="card_child">' +
                '<div class="card-id_tq">' + id_tq + '</div>' +
                '<div class="card-id_data"><i class="' + data + '"></i></div>' +
                '<div class="card-title">' + title + '</div>' +
                '<div class="card-title_en">' + title_en + '</div>' +
                '</div>' +
                '<div class="card-remove"><i class="fa fa-trash-o" aria-hidden="true"></i></div>' +
                '</div>' +
                '</div>' +
                '</div>';

            itemElem.innerHTML = itemTemplate;
            return itemElem.firstChild;
        }

        function getRandomItem2(collection) {

            return collection[Math.floor(Math.random() * collection.length)];

        }

        // https://stackoverflow.com/a/7228322
        function getRandomInt2(min, max) {

            return Math.floor(Math.random() * (max - min + 1) + min);

        }

        function generateRandomWord2(length) {

            var ret = '';
            for (var i = 0; i < length; i++) {
                ret += getRandomItem2(characters2);
            }
            return ret;

        }

        function compareItemTitle2(a, b) {

            var aVal = a.getElement().getAttribute('data-title') || '';
            var bVal = b.getElement().getAttribute('data-title') || '';
            return aVal < bVal ? -1 : aVal > bVal ? 1 : 0;

        }

        function compareItemColor2(a, b) {

            var aVal = a.getElement().getAttribute('data-color') || '';
            var bVal = b.getElement().getAttribute('data-color') || '';
            return aVal < bVal ? -1 : aVal > bVal ? 1 : compareItemTitle2(a, b);

        }

        function updateIndices2() {

            grid2.getItems().forEach(function (item, i) {
                item.getElement().setAttribute('data-id', i + 1);
                item.getElement().querySelector('.card-id').innerHTML = i + 1;
            });

        }

        function elementMatches2(element, selector) {

            var p = Element.prototype;
            return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector).call(element, selector);

        }

        function elementClosest2(element, selector) {

            if (window.Element && !Element.prototype.closest) {
                var isMatch = elementMatches2(element, selector);
                while (!isMatch && element && element !== document) {
                    element = element.parentNode;
                    isMatch = element && element !== document && elementMatches2(element, selector);
                }
                return element && element !== document ? element : null;
            } else {
                return element.closest(selector);
            }

        }

        //
        // Fire it up!
        //

        initDemo2();
        var arr2 = [];
        list_tienich(arr2);

    }
]);
