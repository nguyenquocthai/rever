<style>
	body{
		margin: 0px;
		font-family: Arial, Helvetica, sans-serif;
	}
	.error-store{
		position: fixed;
		width: 100%;
		height: 100vh;
		text-align: center;
		background: #fff;
	}

	a{
		text-decoration: none;
	}

	.error-store .error{
		position: absolute;
		max-width: 100%;
		max-height: 100%;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
	}
	.error-store .home{
		position: absolute;
		left: 15px;
		top: 15px;
		max-width: 100%;
		width: 204px;
	}
	.error-store .home img{
		width: 100%;
	}
</style>
<!-- main content -->
<section class="error-store">
	<div class="error">
		<h1 class="title mb-30">Trang này không khả dụng</h1>
		<p>Liên kết bạn truy cập có thể bị hỏng hoặc trang có thể bị xóa</p>
		<p><a href="/" title="">Quay lại trang chủ</a></p>

		<figure class="img">
			<img src="/public/img/not-found.jpg" alt="">
		</figure>
	</div>
</section>