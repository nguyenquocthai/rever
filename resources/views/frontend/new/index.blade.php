@extends('frontend.layouts.main')
@section('content')
<main class="main">
  <!-- NEWS-->
  @include('frontend.new._banner2')

  <section class="section-spacing">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- 10 items / page-->
          @foreach($news as $new)
          <article class="news-item hor transUp">
            <figure class="img embed-responsive hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $new->avatar,'data' => 'new', 'time' => $new->updated_at])}}" alt=""><a class="link" href="/tin/{{$new->slug}}" title="{{$new->title}}"></a></figure>
            <div class="content"><a class="name link" href="/tin/{{$new->slug}}" title="{{$new->slug}}">{{$new->title}}</a><a class="cate link cl_blue" href="/tin/{{$new->slug}}">Chi tiết</a>
              <p class="date">{{date('d/m/Y', strtotime(@$new->created_at))}}</p>
            </div>
          </article>
          @endforeach
          <nav class="paginationk center">
            {{ $news->links() }}
          </nav>
        </div>
        <!-- <aside class="col-12 col-lg-3 aside aside-news">
          <div class="aside-wrap">
            <div class="aside-block"></div>
          </div>
        </aside> -->
      </div>
    </div>
  </section>
</main>
@endsection