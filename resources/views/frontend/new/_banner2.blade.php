@if(count($banner_subs) == 0)

<section class="news-banner" style="background-image: url(/public/img/upload/sliders/{{$sliderlast->avatar}})">
  <div class="caption">
    <div class="title-main">Tin tức</div>
    <p class="fs_20 mb-0">Thông tin nhanh chóng, chuyên nghiệp</p>
  </div>
  <nav class="categories">
    <btn class="reset-btn categories-btn collapsed" data-toggle="collapse" data-target="#collapseNewsNav">Tất cả chuyên mục</btn>
    <div class="categories-wrap collapse" id="collapseNewsNav">
      <ul class="reset-list categories-list">
        <li class="item"><a class="link @if(!@$find_cat) active @endif" href="/tin-tuc">Tất cả</a></li>

        @if (count($new_cats) != 0)
            @foreach ($new_cats as $new_cat)
            <li class="item"><a class="link @if(@$find_cat->id == $new_cat->id) active @endif" href="/loai-tin-tuc/{{$new_cat->slug}}">{{$new_cat->title}}</a></li>
            @endforeach
        @else
        
        @endif
      </ul>
    </div>
  </nav>
</section>
@else
  @if(@$banner_subs[2]->status == 0)
  <section class="news-banner" style="background-image: url(/public/img/upload/sliders/{{$sliderlast->avatar}})">

    <div class="caption">
      <div class="title-main">Tin tức</div>
      <p class="fs_20 mb-0"></p>
    </div>
    <nav class="categories">
      <btn class="reset-btn categories-btn collapsed" data-toggle="collapse" data-target="#collapseNewsNav">Tất cả chuyên mục</btn>
      <div class="categories-wrap collapse" id="collapseNewsNav">
        <ul class="reset-list categories-list">
          <li class="item"><a class="link @if(!@$find_cat) active @endif" href="/tin-tuc">Tất cả</a></li>

          @if (count($new_cats) != 0)
              @foreach ($new_cats as $new_cat)
              <li class="item"><a class="link @if(@$find_cat->id == $new_cat->id) active @endif" href="/loai-tin-tuc/{{$new_cat->slug}}">{{$new_cat->title}}</a></li>
              @endforeach
          @else
          
          @endif
        </ul>
      </div>
    </nav>
  </section>
  @else
  <section class="news-banner" style="background-image: url({{BladeGeneral::GetImg(['avatar' => @$banner_subs[2]->avatar,'data' => 'banner_sub', 'time' => @$banner_subs[2]->updated_at])}});">
    <div class="caption">
      <div class="title-main">{{@$banner_subs[2]->name}}</div>
      <p class="fs_20 mb-0">{!!@$banner_subs[2]->content!!}</p>
    </div>
    <nav class="categories">
      <btn class="reset-btn categories-btn collapsed" data-toggle="collapse" data-target="#collapseNewsNav">Tất cả chuyên mục</btn>
      <div class="categories-wrap collapse" id="collapseNewsNav">
        <ul class="reset-list categories-list">
          <li class="item"><a class="link @if(!@$find_cat) active @endif" href="/tin-tuc">Tất cả</a></li>

          @if (count($new_cats) != 0)
              @foreach ($new_cats as $new_cat)
              <li class="item"><a class="link @if(@$find_cat->id == $new_cat->id) active @endif" href="/loai-tin-tuc/{{$new_cat->slug}}">{{$new_cat->title}}</a></li>
              @endforeach
          @else
          
          @endif
        </ul>
      </div>
    </nav>
  </section>
  @endif
@endif