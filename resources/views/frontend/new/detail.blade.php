@extends('frontend.layouts.main')
@section('content')
<main class="main">
  <!-- NEWS DETAIL-->
  <section class="section-spacing">
    <div class="news-detail">
      <div class="content">
        <div class="box box-750">
          <h1 class="title-main">{{$new->title}}</h1>
          <nav class="breadcrumbk center">
            <!--
            <ul class="reset-list breadcrumbk-list">
              <li class="item"><a class="link cl_blue" href="tin-tuc.html">Thị trường</a></li>
            </ul>
          -->
          </nav>
        </div>
        <div class="box box-945 mb-4 mb-5"><img class="w-100" src="{{BladeGeneral::GetImg(['avatar' => $new->avatar,'data' => 'new', 'time' => $new->updated_at])}}" alt=""></div>
        <div class="box box-750">
          <div class="author">
            <!-- <div class="avatar">
              <figure class="img hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $nguoidung->avatar,'data' => 'shop_user', 'time' => $nguoidung->updated_at])}}" alt=""><a class="link" href="/gioi-thieu"></a></figure><span class="name">{{$nguoidung->name}}</span>
            </div> -->
            <p class="date">{{date('d/m/Y', strtotime(@$new->created_at))}}</p>
          </div>
          <article class="content fs_18">
            {!! $new->content !!}
          </article>
        </div>
      </div>
      <aside class="aside-ads">
        <div class="aside-wrap">

          @include('frontend.new._banner4')

          @include('frontend.new._banner5')

        </div>
      </aside>
    </div>
    <!-- RELATED NEWS-->
    <div class="container news-related">
      <h2 class="title-main text-center mb-4">Bài viết liên quan</h2>
      <div class="row">
        <!--each item in kid.news-->
        @foreach($relate_news as $relate_new)
        <article class="col-12 col-lg-6">
          <div class="news-item hor transUp">
            <figure class="img embed-responsive hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $relate_new->avatar,'data' => 'new', 'time' => $relate_new->updated_at])}}" alt=""><a class="link" href="/tin/{{$relate_new->slug}}" title="{{$relate_new->title}}"></a></figure>
            <div class="content"><a class="name link" href="/tin/{{$relate_new->slug}}" title="{{$relate_new->title}}">{{$relate_new->title}}</a>
              <p class="date">{{date('d/m/Y', strtotime(@$relate_new->created_at))}}</p>
            </div>
          </div>
        </article>
        @endforeach
      </div>
    </div>
  </section>
</main>
@endsection