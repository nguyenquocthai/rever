@extends('frontend.layouts.main')
@section('content')
@include('frontend.search._head')
<main class="main">
    <span class="js-pageVal" id="project"></span>
    <!-- PROJECTS-->
    <section class="project">
        <div class="filter">
            <div class="container">
                <nav class="breadcrumbk">
                    <ul class="reset-list breadcrumbk-list">
                        <li class="item"><a class="link" href="/">Home</a></li>
                        <li class="item active">{{$product_cat1->title}}</li>
                    </ul>
                </nav>
                @include('frontend.search._fillter')
                
            </div>
        </div>
        <!-- PROJECT CONTENT-->
        <div class="project-content d-lg-flex">
            <div class="project-map">
                <div class="map" id="map"></div>
                <div class="infowindow-content" id="infowindow-content">
                    <figure class="img"><img src="" alt="" height="80" width="110"></figure>
                    <div class="content">
                        <a class="name link" id="title" href=""></a>
                        <p id="para"></p>
                        <div class="info"><span><i class="zmdi zmdi-airline-seat-individual-suite"></i></span><span><i class="zmdi zmdi-photo-size-select-small"></i></span><span><i class="zmdi zmdi-money"></i></span></div>
                    </div>
                </div>
            </div>
            <div class="project-list" id="project-list">
                @include('frontend.search._fillterbox')
                @include('frontend.search._itemsearch')

            </div>
        </div>
    </section>
    <script>
        // Note: This example requires that you consent to location sharing when prompted by your browser. If you see the error "The Geolocation service failed.", it means you probably did not give permission for the browser to locate you.
        @php $lcmap = ['43.654217', '-79.382413']; @endphp
        var map,
            infoWindow,
            service;
        
        var places = [
            @if (count($item_projects) != 0)
                @php $lcmap = explode(",",$item_projects[0]->map); @endphp
                @foreach ($item_projects as $key => $item_project)
                @php 
                    $map =  explode(",",$item_project->map); 
                @endphp
                {
                    id: "{{$item_project->id}}",
                    title: "{{$item_project->title}}",
                    lat: '{{$map[0]}}',
                    lng: '{{$map[1]}}',
                    content: "{{$item_project->address}}",
                    image: "{{BladeGeneral::GetImg(['avatar' => $item_project->avatar,'data' => 'item_project', 'time' => $item_project->updated_at])}}",
                    room: "{{$item_project->phongngu}}",
                    area: "{{$item_project->area}}",
                    price: "{{$item_project->price}}"
                },
                @endforeach
            @else
            
            @endif
        ];
        
        function setMarkers(map, org) {
            var infoWindow = new google.maps.InfoWindow({maxWidth: 320});
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
        
            var image = {
                url: '/public/theme/img/root/map-pin.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(27, 43),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(13, 43)
            };
            // Shapes define the clickable region of the icon. The type defines an HTML
            // <area> element 'poly' which traces out a polygon as a series of X,Y points.
            // The final coordinate closes the poly by connecting to the first coordinate.
            var shape = {
                //- coords: [1, 1, 1, 20, 18, 20, 18, 1],
                coords: [13.5,0,4,3.75,0,13.5,13.5,43,27,13.5,23,3.75],
                type: 'poly'
            };
        
            var pos = navigator.geolocation.getCurrentPosition(function(position) {
                return pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
            });
        
            Array.prototype.forEach.call(places, function(markerElem) {
                var point = new google.maps.LatLng(
                    parseFloat(markerElem.lat),
                    parseFloat(markerElem.lng)
                );
        
                var info = infoContent(markerElem);
        
                var marker = new google.maps.Marker({
                    map: map,
                    position: point,
                    //- icon: image,
                    label: {text: markerElem.id + '', color: "white"}
                });
                marker.addListener('click', function() {
                    let id = markerElem.id;
                    $(".project-item").removeClass("active2");
                    //- document.getElementsByClassName("project-item").classList.remove("active");
                    document.getElementById(id).classList.add("active2");
                    moveTo(id);
                    
                    infoWindow.setContent(info);
                    infoWindow.open(map, marker);
                    calculateAndDisplayRoute(directionsService, directionsDisplay, pos, point);
                    directionsDisplay.setMap(map);
                });
                google.maps.event.addListener(marker, "mouseover", function(evt) {
                    let id = markerElem.id;
                    document.getElementById(id).classList.add("active");
                    moveTo(id,);
                });
                google.maps.event.addListener(marker, "mouseout", function(evt) {
                    let id = markerElem.id;
                    document.getElementById(id).classList.remove("active");
                });
            });
        }
        
        function moveTo(item) {
            let pos = document.getElementById(item).offsetTop,
                parent = document.getElementById('project-list'),
                parentPos = parent.offsetTop;
            parent.scrollTop = pos - parentPos - 16;
        }
        
        function infoContent(item) {
            var content = [
                '<div class="infowindow-content">',
                    '<figure class="img">',
                        '<img src="'+item.image+'" alt="'+item.title+'" height=80 width=110 />',
                    '</figure>',
                    '<div class="content">',
                        '<a class="name" href="" title="'+item.title+'">'+item.title+'</a>',
                        '<p>'+item.content+'</p>',
                        '<div class="info">',
                            '<span class="item"><i class="zmdi zmdi-airline-seat-individual-suite"></i>'+item.room+'</span>',
                            '<span class="item"><i class="zmdi zmdi-photo-size-select-small"></i>'+item.area+' sqft</span>',
                            '<span class="item"><i class="zmdi zmdi-money"></i>'+item.price+'</span>',
                        '</div>',
                    '</div>',
                '</div>'
            ].join("");
            return content;
        }
        
        function initMap() {
            var origin = {
                lat: {{$lcmap[0]}},
                lng: {{$lcmap[1]}}
            };
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 10,
                center: origin,
                disableDefaultUI: true,
            });
        
            var request = {
                location: origin,
                radius: '5000',
                type: ['gym']
            };
        
            setMarkers(map, origin);
        
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    map.setCenter(pos);
                    var infoWindow = new google.maps.InfoWindow,
                        infowincontent = "Vị trí hiện tại.";
        
                    var marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        title: 'You\'re here!'
                    });
                    marker.addListener('click', function() {
                        infoWindow.setContent(infowincontent);
                        infoWindow.open(map, marker);
                    });
                }, function() {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }
        
            //- var getCurPos = getCurPos();
            //- var getLocation = document.getElementById('getLocation');
            //- getLocation.addEventListener('click', function() {
            //-     navigator.permissions.query({name:'geolocation'})
            //-     .then(function(permissionStatus) {
            //-         console.log(permissionStatus.state);
            //-         if (permissionStatus.state != 'granted') {
            //-             alert('Vui lòng cho phép truy cập vị trí hiện tại!');
            //-         }
            //-     });
            //- });
        }
        
        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
        }
        
        function calculateAndDisplayRoute(directionsService, directionsDisplay, org, des) {
            directionsService.route({
                origin: org,
                destination: des,
                travelMode: 'DRIVING'
            }, function(response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });
        }
        
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNLTNZjRk7mkXbQgxu82LUJYCBKby6cZ4&amp;libraries=places&amp;callback=initMap" async defer></script>
    <script type="text/javascript">
        $('.reset_value').click(function(event) {
            $(this).closest('.filter-item').find('li input').prop("checked", false);
            $('#form_serach').submit();
        });

        $('#collapseFilter1 .checkbox').click(function(event) {
            var txt_val = $(this).find('.checkbox-text').html();
            $('.tag_ft1 .txt_val').html(txt_val);
            $('.tag_ft1').addClass('active');
            $('.filter-box').addClass('active');
        });
        $('.tag_ft1 .btn_del').click(function(event) {
            $('#collapseFilter1 input').prop("checked", false);
            $('.tag_ft1 .txt_val').html('');
            $('.tag_ft1').removeClass('active');
        });

        $('#collapseFilter2 .checkbox').click(function(event) {
            var txt_val = $(this).find('.checkbox-text').html();
            $('.tag_ft2 .txt_val').html(txt_val);
            $('.tag_ft2').addClass('active');
            $('.filter-box').addClass('active');
        });
        $('.tag_ft2 .btn_del').click(function(event) {
            $('#collapseFilter2 input').prop("checked", false);
            $('.tag_ft2 .txt_val').html('');
            $('.tag_ft2').removeClass('active');
        });

        $('#collapseFilter3 .checkbox').click(function(event) {
            var txt_val = $(this).find('.checkbox-text').html();
            $('.tag_ft3 .txt_val').html(txt_val);
            $('.tag_ft3').addClass('active');
            $('.filter-box').addClass('active');
        });
        $('.tag_ft3 .btn_del').click(function(event) {
            $('#collapseFilter3 input').prop("checked", false);
            $('.tag_ft3 .txt_val').html('');
            $('.tag_ft3').removeClass('active');
        });

        $('#collapseFilter4 .checkbox').click(function(event) {
            var txt_val = $(this).find('.checkbox-text').html();
            $('.tag_ft4 .txt_val').html(txt_val);
            $('.tag_ft4').addClass('active');
            $('.filter-box').addClass('active');
        });
        $('.tag_ft4 .btn_del').click(function(event) {
            $('#collapseFilter4 input').prop("checked", false);
            $('.tag_ft4 .txt_val').html('');
            $('.tag_ft4').removeClass('active');
        });

        $('.close-filter-box').click(function(event) {
            $('.filter-box').removeClass('active');
        });

        $('.apply-filter-box').click(function(event) {
            $('#form_serach').submit();
        });

    </script>
</main>
<style>
    .project-list .project-item.ver .list .item {
        font-size: .8rem;
    }
</style>
@endsection