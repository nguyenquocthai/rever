@extends('frontend.layouts.main')
@section('content')
<style type="text/css" media="screen">
  .project .author .name{
    color: #4877f8;
    font-weight: bold;
  }
  .reset-list.list{
    margin-bottom: 15px;
  }
  .reset-list.list li{
    display: inline-block;
  }

  .project .detail .list .item .icon{
    display: inline-block;
    padding-top: 5px;
    margin-top: -5px;
  }
</style>
<main class="main"><span class="js-pageVal" id="project"></span><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css" integrity="sha256-BtbhCIbtfeVWGsqxk1vOHEYXS6qcvQvLMZqjtpWUEx8=" crossorigin="anonymous" />
      <!-- PROJECTS-->
      <section class="project">
        <div class="project-gallery js-magnificPopup">
          @if (count($albums) != 0)
              @foreach ($albums as $key => $album)
              <a @if($key == 4 && count($albums) > 5) data-plus="+{{count($albums)-5}}" @endif class="item embed-responsive" href="{{BladeGeneral::GetImg(['avatar' => $album->name,'data' => 'item_album', 'time' => $album->updated_at])}}"><img src="{{BladeGeneral::GetImg(['avatar' => $album->name,'data' => 'item_album', 'time' => $album->updated_at])}}" alt="{{$album->id}}"></a>
              @endforeach
          @else
          
          @endif
        </div>
        <h1 class="project-title">{{$item_project->title}}</h1>
        <ul class="reset-list project-address">
          <li class="item active">{{$item_project->address}}</li>
        </ul>
        <ul class="reset-list list">
            <li class="item">{{$item_project->phongngu}} BD</li>
            <li class="item">| {{$item_project->phongtam}} BA</li>
            <li class="item">{{$item_project->baixe}} Parking</li>
            <li class="item">| {{$item_project->area}} sqft</li>
        </ul>
        <div class="row">
          <div class="col-lg-7 col-xl-8">
            <section class="block">
              <h3 class="block-title"><i class="icon icon-nav-home-fill cl_main"></i>Room information</h3>
              <div class="detail">
                <ul class="reset-list list">
                  
                  <li class="item">
                    <P class="mb-1 fs_12">TYPE OF PROPERTY</P>
                    <p class="mb-1">{{@$product_cat1->title}}</p>
                  </li>
                  <li class="item">
                    <P class="mb-1 fs_12">MONTHLY PAYMENT</P>
                    <p class="mb-1">${{BladeGeneral::priceFormat($item_project->price)}} / month</p>
                  </li>
                  <li class="item">
                    <P class="mb-1 fs_12">SIZE</P>
                    <p class="mb-1">{{$item_project->area}} sqft</p>
                  </li>
                  <li class="item">
                    <P class="mb-1 fs_12">DEPOSIT</P>
                    <p class="mb-1">${{$item_project->datcoc}}</p>
                  </li>
                  <li class="item">
                    <P class="mb-1 fs_12">CAPACITY</P>
                    <p class="mb-1">{{$item_project->succhua}} person</p>
                  </li>
                  <li class="item">
                    <P class="mb-1 fs_12">AVAILABILITY</P>
                    <p class="mb-1 cl_green">{{$item_project->sangco_time}}</p>
                  </li>
                </ul>
                <div class="address">
                  <p class="fs_12 mb-1">ADDRESS</p>
                  <p class="mb-1">{{$item_project->address}}</p>
                </div>
              </div>
            </section>
            <section class="block">
              <h3 class="block-title"><i class="icon icon-furniture cl_main"></i>Utilities</h3>
              <div class="detail">
                <ul class="reset-list list">
                  @foreach($tienichs as $tienich)
                  <li class="item"><span class="icon {{$tienich->data}}"></span><span>{{$tienich->title}}</span></li>
                  @endforeach
                </ul>
              </div>
            </section>
            <section class="block">
              <h3 class="block-title"><i class="icon icon-information-fill cl_main"></i>Additional description</h3>
              <div class="detail">
                {!!$item_project->short_content!!}
                
              </div>
            </section>
          </div>
          <div class="col-lg-5 col-xl-4">
            <section class="block">
              <h4 class="block-title"><i class="icon icon-profile-fill cl_blue"></i>Property owner information</h4>
              <div class="author-wrap">
                <div class="left author">
                  <figure class="avatar"><img src="{{BladeGeneral::GetImg(['avatar' => @$nguoidung->avatar,'data' => 'nguoidung', 'time' => @$nguoidung->updated_at])}}" alt=""></figure>
                  <div class="content">
                    <div class="name">{{@$nguoidung->name}}</div>
                    <p class="date mb-0"><span class="d-inline-block">Date submitted:&nbsp;</span><span class="d-inline-block"><?php if($item_project->updated_at) echo date('d-m-Y', strtotime($item_project->updated_at)); else echo date('d-m-Y', strtotime($item_project->created_at)) ?></span></p>
                  </div>
                </div>
                <?php 
                  $gioithieu = [];
                  if($nguoidung->gioithieu !== null){
                    $gioithieu = json_decode($nguoidung->gioithieu);
                  }
                ?>
                <div class="right">
                  <div>Rating for user</div>
                  <div class="rating without-caption">
                    <input class="d-none kv-fa js-ratingStars rating-loading" type="text" readonly="readonly" value="4.5">
                  </div>
                  <ul class="reset-list list">
                    <li class="item"><i class="icon-eye mr-2"></i>{{$item_project->luotxem}}</li>
                    <li class="item">
                      <a class="link" target="_blank" href="{{@$gioithieu->facebook}}"><i class="fab fa-facebook"></i></a>
                      
                    </li>

                    <li class="item"><a class="link" target="_blank" href="{{@$gioithieu->facebook}}"><i class="fab fa-facebook-messenger"></i></a></li>
                  </ul>
                </div>
                <button class="reset-btn btn-main mx-auto mt-3" data-toggle="modal" data-target="#<?php if(session('userSession')) echo 'modal_comment';else echo 'modal_login' ?>"><i class="zmdi zmdi-border-color mr-2"></i>Booking</button>
              </div>
            </section>
            <section class="block p-2">
              <div class="mapbox embed-responsive">
                <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCQiaMU8DTCCDi90sT1ApqUYM737YYXymU&q={!!@$item_project->map!!}" width="600" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
              </div>
            </section>
          </div>
        </div>
      </section>
    </main>
    <style>
      .block-title{
        font-size: 1.4rem;
      }
    </style>

    <!-- The Modal comment -->
<div class="modal fade" id="modal_comment">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
    
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Sign up to receive messages</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      
      <!-- Modal body -->
      <form action="" id="form_comment">
        <div class="modal-body">
          @if(session('userSession'))
          <input class="form-control mb-2" type="text" name="name_contact" placeholder="Full name..." value="{{session('userSession')->name}}">
          <input class="form-control mb-2" type="number" name="phone_contact" placeholder="Phone number..." value="{{session('userSession')->phone}}">
          <input class="form-control mb-2" type="email" name="email_contact" placeholder="Email..." value="{{session('userSession')->email}}">
          <textarea name="comment" class="form-control mb-2" placeholder="Comment..."></textarea>
          <input type="hidden" name="id_chu" value="{{@$nguoidung->id}}">
          <input type="hidden" name="id_khach" value="{{session('userSession')->id}}">
          <input type="hidden" name="id_itemproject" value="{{$item_project->id}}">
          @endif
        </div>
      
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-secondary" style="background-color: #4877f8;border: 0;">Send</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The modal_info -->
<div class="modal fade" id="modal_info">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
    
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Property owner information</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      
      <!-- Modal body -->
      <div class="modal-body">
        <table class="table table-bordered">
          <tbody>
              <tr>
                <td>Name</td>
                <td><i>{{@$nguoidung->name}}</i></td>
              </tr>
              
              <tr>
                <td>Hometown</td>
                <td><i>{{@$gioithieu->hometown}}</i></td>
              </tr>

              <tr>
                <td>First language</td>
                <td><i>{{@$gioithieu->first_language}}</i></td>
              </tr>

              <tr>
                <td>Relationship status</td>
                <td><i>{{@$gioithieu->relationship_status}}</i></td>
              </tr>

              <tr>
                <td>Email</td>
                <td><i>{{@$nguoidung->email}}</i></td>
              </tr>

              <tr>
                <td>Phone number</td>
                <td><i>{{@$nguoidung->phone}}</i></td>
              </tr>

              <tr>
                <td>Facebook (optional)</td>
                <td><i>{{@$gioithieu->facebook}}</i></td>
              </tr>
          </tbody>
        </table>
      </div>
    
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- The Modal login -->
<div class="modal fade" id="modal_login">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
    
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Login</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      
      <!-- Modal body -->
      <form action="" id="form_dangnhap">
        <div class="modal-body">
          <input class="form-control mb-2" type="email" name="email" placeholder="Email...">
          <input class="form-control mb-2" type="password" name="password" placeholder="Password...">
        </div>
      
        <!-- Modal footer -->
        <div class="modal-footer">
          <button  type="submit" class="btn btn-secondary" style="background-color: #4877f8;border: 0;">Login</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
    <script>
      var form_dangnhap = $('#form_dangnhap').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
        },
        rules: {
            email:{
                required: true
            },
            password:{
                required: true
            }
        },
        messages: {
            email:{
                required: 'No email entered yet.'
            },
            password:{
                required: 'Password has not been entered.'
            }
        },
        submitHandler: function (form) {
    
            var data = {};
            $("#form_dangnhap").serializeArray().map(function(x){data[x.name] = x.value;});
           
            $('.block-page-all').addClass('active');
    
            $.ajax({
                type: 'POST',
                url: '/dangnhap',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    switch (result.code) {
                        
                        case 200:
                            location.reload();
                            break;

                        case 300:
                            toastr.error(result.error);
                            $('.block-page-all').removeClass('active');
                            break;

                        default:
                            break;
                    }
                }
            });
            return false;
        }
    });
    var form_comment = $('#form_comment').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
        },
        rules: {
            name_contact:{
                required: true,
            },
            phone_contact:{
                required: true,
            },
            email_contact:{
                required: true,
            },
            comment:{
                required: true,
            }
        },
        messages: {
            name_contact:{
                required: 'Name must not be blank.',
            },
            phone_contact:{
                required: 'Phone must not be blank.',
            },
            email_contact:{
                required: 'Email is not vacant.',
            },
            
            comment:{
                required: 'The comment must not be empty.',
            }
        },
        submitHandler: function (form) {
            var data = {};
            $("#form_comment").serializeArray().map(function(x){data[x.name] = x.value;});
            $('.block-page-all').addClass('active');
            $.ajax({
                type: "POST",
                url: '/add_comment',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error('Data error');
                },
                success: function(result) {
                    $('.block-page-all').removeClass('active');
                    switch (result.code) {
                        case 200:
                            $('#form_comment').find("textarea").val("");
                            $('#modal_comment').modal('hide');
                            toastr.success('Send contact successfully.');
                            break;
                        default:
                            toastr.error('Data error');
                            break;
                    }
                }
            });
            return false;
        }
    });

    $('.left.author').click(function(event) {
      $('#modal_info').modal('show');
    });
    </script>


    <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3&appId=1256277581215699&autoLogAppEvents=1"></script>
@endsection