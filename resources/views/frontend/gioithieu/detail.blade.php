@extends('frontend.layouts.main')
@section('content')
<main class="main">
  <!-- NEWS DETAIL-->
  <section class="section-spacing">
    <div class="news-detail">
      <div class="content">
        <div class="box box-750">
          <h1 class="title-main">{{$new->title}}</h1>
          <nav class="breadcrumbk center">
            <!--
            <ul class="reset-list breadcrumbk-list">
              <li class="item"><a class="link cl_blue" href="tin-tuc.html">Thị trường</a></li>
            </ul>
          -->
          </nav>
        </div>
        <div class="box box-945 mb-4 mb-5"></div>
        <div class="box box-750">
          
          <article class="content fs_18">
            {!! $new->content !!}
          </article>
        </div>
      </div>
      <aside class="aside-ads">
        
      </aside>
    </div>
    <!-- RELATED NEWS-->
    
  </section>
</main>
@endsection