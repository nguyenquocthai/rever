@extends('frontend.layouts.main')
@section('content')
<main class="main">
  <!-- ABOUT-->
  @include('frontend.gioithieu._banner3')
  <section class="about section-spacing">
    <div class="container about-container">
      
      <div class="content">
        
        
        <div class="title-main mb-3">Giới thiệu</div>{!!$nguoidung->intro!!}</div>
    </div>
  </section>
  <!-- MODAL CONTACT-->
  <section class="modal contact-modal" id="modalContact">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title text-center">Liên hệ tư vấn</div>
          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form id="contac_form" class="contact-form">
            <input type="hidden" name="">
            <div class="item mb-3">
              <input name="ho" class="form-control" type="text" placeholder="Họ">
            </div>
            <div class="item mb-3">
              <input name="ten" class="form-control" type="text" placeholder="Tên">
            </div>
            <div class="item mb-3">
              <input name="phone_contact" class="form-control" type="text" placeholder="Số điện thoại">
            </div>
            <div class="item mb-3">
              <input name="email_contact" class="form-control" type="email" placeholder="Email">
            </div>
            <div class="item mb-3">
              <textarea name="comment" class="form-control" rows="3" placeholder="Lời nhắn"></textarea>
            </div>
            <div class="item mb-3">
              <button class="reset-btn btn-red w-100" type="submit">Gửi thông tin</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</main>

<script>
  jQuery(document).ready(function($) {
      var html_contac_form = $('#contac_form').html();
      var contac_form = $('#contac_form').validate({
          highlight: function(element, errorClass, validClass) {
              $(element).removeClass(errorClass);
          },
          rules: {
              ho:{
                  required: true
              },
              ten:{
                  required: true
              },
              phone_contact:{
                  required: true
              },
              email_contact:{
                  required: true
              },
              comment: {
                  required: true
              }
          },
          messages: {
              ho:{
                  required: 'Bạn chưa nhập họ.'
              },
              ten:{
                  required: 'Bạn chưa nhập tên.'
              },
              phone_contact:{
                  required: 'Bạn chưa nhập số điện thoại.'
              },
              email_contact:{
                  required: 'Bạn chưa nhập email.'
              },
              comment: {
                  required: 'Bạn chưa nhập lời nhắn.'
              }
          },
          submitHandler: function (form) {
      
              var data = {};
              $("#contac_form").serializeArray().map(function(x){data[x.name] = x.value;});
              data['status'] = 5;
              data['name_contact'] = data['ho']+' '+data['ten'];
             
              $('.block-page-all').addClass('active');
      
              $.ajax({
                  type: 'POST',
                  url: '/send_contact',
                  data: data,
                  dataType: 'json',
                  error: function(){
                      $('.block-page-all').removeClass('active');
                      toastr.error(result.error);
                  },
                  success: function(result) {
                      if (result.code == 300) {
                          toastr.error(result.error);
                          $('.block-page-all').removeClass('active');
                          return false
                      }
                      $('#modalContact').modal('hide')
                      $('#contac_form').html(html_contac_form);
                      $('.block-page-all').removeClass('active');
                      toastr.success(result.message);
                  }
              });
              return false;
          }
      });
  });  
</script>
@endsection