@if(count($banner_subs) == 0)
<figure class="about-banner" style="background-image: url(/public/theme_sub/img/root/bg-banner-about.jpg)"></figure>
@else
  @if(@$banner_subs[3]->status == 0)
  <figure class="about-banner" style="background-image: url(/public/theme_sub/img/root/bg-banner-about.jpg)"></figure>
  @else
  <figure class="about-banner" style="background-image: url({{BladeGeneral::GetImg(['avatar' => @$banner_subs[3]->avatar,'data' => 'banner_sub', 'time' => @$banner_subs[3]->updated_at])}});"></figure>
  @endif
@endif