<!DOCTYPE html>
<html lang="en">
	<head>
		@include('frontend.elements.head')
    </head>

    <body >
    	
		@include('frontend.elements.header')
        @yield('content')
        @include('frontend.elements.footer')
        
    </body>
</html>