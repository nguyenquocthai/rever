@extends('frontend.layouts.main')
@section('content')
<main class="main">
  <!-- PROJECTS-->
  <section class="section-spacing project">
    <nav class="breadcrumbk">
      <div class="container">
        <ul class="reset-list breadcrumbk-list">
          <li class="item"><a class="link" href="/">Trang chủ</a></li>
          @if($product->type == 1)
            <li class="item"><a class="link" href="/nha-dat-ban">Nhà đất bán</a></li>
          @else
            <li class="item"><a class="link" href="/nha-dat-thue">Nhà đất thuê</a></li>
          @endif
          <li class="item active">{{$product->title}}</li>
        </ul>
      </div>
    </nav>
    <div class="project-gallery js-magnificPopup js-sliderProject">
        @foreach ($medias as $key => $media)
        @if($key ==0 )
        <div class="item"><a class="img embed-responsive" href="/public/img/upload/item_albums/{{@$media->name}}"><img src="/public/img/upload/item_albums/{{@$media->name}}" alt=""></a></div>
        @endif
        @break
        @endforeach
        @foreach ($medias as $key => $media)
        @if($key>=1 && $key <= 4)
        <div class="item"><a class="img embed-responsive" href="/public/img/upload/item_albums/{{@$media->name}}"><img src="/public/img/upload/item_albums/{{@$media->name}}" alt=""></a></div>
        @endif
        @endforeach
        
      </div>
    <div class="container">
      <div class="project-info">
        <div class="info">
          <h1 class="title-main mb-3">{{$product->title}}</h1>
          <p class="address"><i class="zmdi zmdi-pin cl_red mr-2"></i>{{$product->address}}</p>
        </div>
        <div class="price"><span class="cl_red mr-2">{{$product->allprice_mark}}</div>
      </div>
    </div>
    <nav class="project-nav js-projectNav">
      <div class="container">
        <ul class="reset-list list">
          <li class="item"><a class="link active" href="#tong-quan">Tổng quan</a></li>
          <li class="item"><a class="link" href="#thong-tin">Thông tin</a></li>
          <li class="item"><a class="link" href="#noi-that">Nội thất</a></li>
          <li class="item"><a class="link" href="#tien-nghi">Tiện nghi</a></li>
          <li class="item"><a class="link" href="#uu-diem">Ưu điểm</a></li>
          <li class="item"><a class="link" href="#vi-tri">Vị trí</a></li>
        </ul>
      </div>
    </nav>
    <div class="container section-spacing">
      <div class="row">
        <div class="col-12 col-lg-8 project-content">
          <button class="reset-btn aside-open js-asideTrigger mb-3"><i class="zmdi zmdi-phone mr-2"></i>Đăng ký tư vấn</button>
          
          <section class="project-content__block" id="tong-quan">
            <div class="title"><span>TỔNG QUAN</span><span></span></div>
            <div class="content">
              {!!@$product->short_content!!}
            </div>
          </section>
          <section class="project-content__block" id="thong-tin">
            <div class="title"><span>THÔNG TIN CƠ BẢN</span><span></span></div>
            <div class="content">
              <div class="row">
                <div class="col-12 col-md-6 d-flex justify-content-between mb-2"><span>Phòng ngủ</span><span class="fw-6">{{@$thongtin->phongngu}}</span></div>
                <div class="col-12 col-md-6 d-flex justify-content-between mb-2"><span>Phòng tắm</span><span class="fw-6">{{@$thongtin->phongtam}}</span></div>
                <div class="col-12 col-md-6 d-flex justify-content-between mb-2"><span>Diện tích</span><span class="fw-6">{{@$product->area}} m²</span></div>
                <div class="col-12 col-md-6 d-flex justify-content-between mb-2"><span>Diện tích sử dụng</span><span class="fw-6">{{@$thongtin->dientichsd}} m²</span></div>
                <div class="col-12 col-md-6 d-flex justify-content-between mb-2"><span>Vỉa hè</span><span class="fw-6">{{@$thongtin->viahe}}</span></div>
                <div class="col-12 col-md-6 d-flex justify-content-between mb-2"><span>Hướng</span><span class="fw-6">{{@$thongtin->huongnha}}</span></div>
              </div>
            </div>
          </section>

          <section class="project-content__block" id="tien-nghi">
            <div class="title"><span>TIỆN NGHI</span><span></span></div>
            <div class="content">
              <div class="row">
                @foreach(@$tienichs as $tienich)
                <div class="col-12 col-md-6 d-flex justify-content-between mb-2"><span>{{@$tienich->title}}</span><i class="zmdi zmdi-check fw-6"></i></div>
                @endforeach
              </div>
            </div>
          </section>
          <section class="project-content__block" id="uu-diem">
            <div class="title"><span>ƯU ĐIỂM NGÔI NHÀ</span><span></span></div>
            <div class="content">
              {!!@$product->content!!}
            </div>
          </section>
          <section class="project-content__block" id="vi-tri">
            <div class="title"><span>VỊ TRÍ</span><span></span></div>
            <div class="content">
              <div class="mapbox embed-responsive"><iframe width="100%" frameborder="0" style="border:0" allowfullscreen src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCQiaMU8DTCCDi90sT1ApqUYM737YYXymU&q={!!@$product->map!!}"></iframe></div>
            </div>
          </section>
        </div>
        <aside class="aside js-aside col-12 col-lg-4">
          <button class="reset-btn aside-close"><i class="zmdi zmdi-chevron-left mr-2"></i>Trở về</button>
          <div class="aside-wrap js-blurOff">
            <div class="aside-block">
              @include('frontend.product._userinfo')
            </div>
            <!-- <div class="aside-block">
              <p class="title fw-6">TAGS:</p>
              <div class="tags">
                @if (count($tags) != 0)
                    @foreach ($tags as $tag)
                    <a href="/tags-ban/{{$tag->slug}}" class="link">{{$tag->title}}</a>
                    @endforeach
                @else

                @endif
              </div>
            </div> -->
          </div>
        </aside>
      </div>
    </div>
    <!-- <div class="section-spacing bg-gray">
      <div class="container">
        <div class="title-main mb-3"><span class="d-inline-block mr-2">NHÀ ĐẤT</span><span class="d-inline-block cl_red">CÙNG DỰ ÁN</span>
        </div>
        <div class="js-slider3">

            @if (count($cungduans) != 0)
                @foreach ($cungduans as $sale_product)
                <div class="item">
                @include('frontend.product._itemban')
                </div>
                @endforeach
            @else
            
            @endif
          </div>

        </div>
      </div>
    </div> -->
    <div class="section-spacing bg-white">
      <div class="container">
        <div class="title-main mb-3"><span class="d-inline-block mr-2">NHÀ ĐẤT</span><span class="d-inline-block cl_red">TƯƠNG TỰ</span>
        </div>
        <div class="js-slider3">

          @if (count($cungloais) != 0)
                @foreach ($cungloais as $sale_product)
                <div class="item">
                @include('frontend.product._itemban')
                </div>
                @endforeach
            @else
            
            @endif

        </div>
      </div>
    </div>
  </section>

  <!-- MODAL ADVISORY-->
  <section class="modal project-modal" id="modalAdvisory">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title text-center">Liên hệ</div>
          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-12 col-lg-6">
              <div class="project-item ver style2">
                <figure class="img hasLink embed-responsive"><img src="{{BladeGeneral::GetImg(['avatar' => $product->avatar,'data' => 'item_project', 'time' => $product->updated_at])}}" alt=""></figure>
                <div class="content"><a class="name link fs_24" href="#" title="Empire City">{{$product->title}}</a>
                  <p class="address fs_14">{{$product->address}}</p>
                  <div class="content-block">
                    <?php $thongtin = json_decode($product->thongtin);?>
                    <ul class="reset-list list">
                      <li class="item"><span>Phòng ngủ: </span><strong>{{@$thongtin->phongngu}}</strong></li>
                      <li class="item"><span>Diện tích: </span><strong>{{@$thongtin->dientich}} m²</strong></li>
                      <li class="item"><span>Phòng tắm: </span><strong>{{@$thongtin->phongtam}}</strong></li>
                      <li class="item"><span>Giá: </span><strong class="cl_red">{{$product->allprice_mark}}</strong></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6">
              <form class="contact-form" id="formView">
                <input type="hidden" name="id_group" value="3">
                <input type="hidden" name="status" value="1">
                <input type="hidden" name="id_itemproject" value="{{$product->id}}">
                <p class="title fs_18 text-center"><strong>THÔNG TIN KHÁCH HÀNG</strong></p>
                <div class="item mb-3">
                  <input name="name_contact" class="form-control" type="text" placeholder="Họ Tên">
                </div>
                <div class="item mb-3">
                  <input name="phone_contact" class="form-control" type="text" placeholder="Số điện thoại">
                </div>
                <div class="item mb-3">
                  <input name="email_contact" class="form-control" type="email" placeholder="Email">
                </div>
                <div class="item mb-3">
                  <button class="reset-btn btn-red w-100" type="submit">Gửi thông tin</button>
                </div>
              </form>

              <form class="contact-form" id="formNegotiate">
                <input type="hidden" name="id_group" value="3">
                <input type="hidden" name="status" value="2">
                <input type="hidden" name="id_itemproject" value="{{$product->id}}">

                <p class="title fs_18 text-center"><strong>THÔNG TIN KHÁCH HÀNG</strong></p>
                <div class="item mb-3">
                  <input name="name_contact" class="form-control" type="text" placeholder="Họ Tên">
                </div>
                <div class="item mb-3">
                  <input name="phone_contact" class="form-control" type="text" placeholder="Số điện thoại">
                </div>
                <div class="item mb-3">
                  <input name="email_contact" class="form-control" type="email" placeholder="Email">
                </div>
                <div class="item mb-3">
                  <input name="negotiable_price" class="form-control" type="number" placeholder="Giá thương lượng">
                </div>
                <div class="item mb-3">
                  <button class="reset-btn btn-red w-100" type="submit">Gửi thông tin</button>
                </div>
              </form>

              <form class="contact-form" id="formQuestion">
                <input type="hidden" name="id_group" value="3">
                <input type="hidden" name="status" value="3">
                <input type="hidden" name="id_itemproject" value="{{$product->id}}">

                <p class="title fs_18 text-center"><strong>THÔNG TIN KHÁCH HÀNG</strong></p>
                <div class="item mb-3">
                  <input name="name_contact" class="form-control" type="text" placeholder="Họ Tên">
                </div>
                <div class="item mb-3">
                  <input name="phone_contact" class="form-control" type="text" placeholder="Số điện thoại">
                </div>
                <div class="item mb-3">
                  <input name="email_contact" class="form-control" type="email" placeholder="Email">
                </div>
                <div class="item mb-3">
                  <textarea name="comment" class="form-control" rows="3" placeholder="Lời nhắn"></textarea>
                </div>
                <div class="item mb-3">
                  <button class="reset-btn btn-red w-100" type="submit">Gửi thông tin</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>

<script>
  jQuery(document).ready(function($) {
    var html_formView = $('#formView').html();
    var formView = $('#formView').validate({
      highlight: function(element, errorClass, validClass) {
          $(element).removeClass(errorClass);
      },
      rules: {
        name_contact:{
            required: true
        },
        phone_contact:{
            required: true,
            number:true,
            minlength: 10,
            maxlength: 10,
        },
        email_contact:{
            required: true,
            email:true
        }
      },
      messages: {
        name_contact:{
            required: 'Bạn chưa nhập tên.'
        },
        phone_contact:{
            required: 'Bạn chưa nhập số điện thoại.',
            number: 'Số điện thoại phải là số.',
            minlength: 'Số điện thoại phải là 10 số.',
            maxlength: 'Số điện thoại phải là 10 số.',
        },
        email_contact:{
            required: 'Bạn chưa nhập email.',
            email: 'Email chưa đúng.',
        }
      },
      submitHandler: function (form) {
  
          var data = {};
          $("#formView").serializeArray().map(function(x){data[x.name] = x.value;});
         
          $('.block-page-all').addClass('active');
  
          $.ajax({
              type: 'POST',
              url: '/send_contact',
              data: data,
              dataType: 'json',
              error: function(){
                  $('.block-page-all').removeClass('active');
                  toastr.error(result.error);
              },
              success: function(result) {
                  if (result.code == 300) {
                      toastr.error(result.error);
                      $('.block-page-all').removeClass('active');
                      return false
                  }
                  $('#modalAdvisory').modal('hide');
                  $('#formView').html(html_formView);
                  $('.block-page-all').removeClass('active');
                  toastr.success(result.message);
              }
          });
          return false;
      }
    });

    var html_formNegotiate = $('#formNegotiate').html();
    var formNegotiate = $('#formNegotiate').validate({
      highlight: function(element, errorClass, validClass) {
          $(element).removeClass(errorClass);
      },
      rules: {
        name_contact:{
            required: true
        },
        phone_contact:{
            required: true
        },
        email_contact:{
            required: true
        },
        comment: {
            required: true
        },
        negotiable_price: {
            required: true
        }
      },
      messages: {
        name_contact:{
            required: 'Bạn chưa nhập tên.'
        },
        phone_contact:{
            required: 'Bạn chưa nhập số điện thoại.'
        },
        email_contact:{
            required: 'Bạn chưa nhập email.'
        },
        negotiable_price: {
            required: 'Bạn chưa nhập giá.'
        }
      },
      submitHandler: function (form) {
  
          var data = {};
          $("#formNegotiate").serializeArray().map(function(x){data[x.name] = x.value;});
         
          $('.block-page-all').addClass('active');
  
          $.ajax({
              type: 'POST',
              url: '/send_contact',
              data: data,
              dataType: 'json',
              error: function(){
                  $('.block-page-all').removeClass('active');
                  toastr.error(result.error);
              },
              success: function(result) {
                  if (result.code == 300) {
                      toastr.error(result.error);
                      $('.block-page-all').removeClass('active');
                      return false
                  }
                  $('#modalAdvisory').modal('hide');
                  $('#formNegotiate').html(html_formNegotiate);
                  $('.block-page-all').removeClass('active');
                  toastr.success(result.message);
              }
          });
          return false;
      }
    });

    var html_formQuestion = $('#formQuestion').html();
    var formQuestion = $('#formQuestion').validate({
      highlight: function(element, errorClass, validClass) {
          $(element).removeClass(errorClass);
      },
      rules: {
        name_contact:{
            required: true
        },
        phone_contact:{
            required: true
        },
        email_contact:{
            required: true
        },
        comment: {
            required: true
        },
        comment: {
            required: true
        }
      },
      messages: {
        name_contact:{
            required: 'Bạn chưa nhập tên.'
        },
        phone_contact:{
            required: 'Bạn chưa nhập số điện thoại.'
        },
        email_contact:{
            required: 'Bạn chưa nhập email.'
        },
        comment: {
            required: 'Bạn chưa nhập nội dung.'
        }
      },
      submitHandler: function (form) {
  
          var data = {};
          $("#formQuestion").serializeArray().map(function(x){data[x.name] = x.value;});
         
          $('.block-page-all').addClass('active');
  
          $.ajax({
              type: 'POST',
              url: '/send_contact',
              data: data,
              dataType: 'json',
              error: function(){
                  $('.block-page-all').removeClass('active');
                  toastr.error(result.error);
              },
              success: function(result) {
                  if (result.code == 300) {
                      toastr.error(result.error);
                      $('.block-page-all').removeClass('active');
                      return false
                  }
                  $('#modalAdvisory').modal('hide');
                  $('#formQuestion').html(html_formQuestion);
                  $('.block-page-all').removeClass('active');
                  toastr.success(result.message);
              }
          });
          return false;
      }
    });
  });
</script>
@endsection