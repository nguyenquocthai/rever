<div class="avatar">
  <figure class="img mb-0"><img src="/public/img/upload/settings/{{@$info_web[logo]}}" alt=""></figure>
  <div class="content">
    <div class="name fs_18">{{@$nguoidung->callname}}</div>
    <div class="position fs_14">{{@$info_web['name']}}</div>
  </div>
</div><a class="reset-btn btn-red w-100 text-center px-3" href="tel:{{@$nguoidung->phone}}"><i class="zmdi zmdi-phone mr-2"></i>{{@$info_web['phone2']}}</a>
<p class="my-3 text-center fs_14 cl_83">Hoặc</p>
  <div class="mb-2">
    <button class="reset-btn btn-white-trans w-100 js-projectModalOpen" data-toggle="modal" data-target="#modalAdvisory" data-tab="#formView">Đăng ký xem nhà</button>
  </div>
  <!-- <div class="mb-2">
    <button class="reset-btn btn-white-trans w-100 js-projectModalOpen" data-toggle="modal" data-target="#modalAdvisory" data-tab="#formNegotiate">Thương lượng giá</button>
  </div>
  <div class="mb-2">
    <button class="reset-btn btn-white-trans w-100 js-projectModalOpen" data-toggle="modal" data-target="#modalAdvisory" data-tab="#formQuestion">Hỏi thêm thông tin</button>
  </div> -->
<style>
  .aside-block .avatar .img {
    height: auto;
    max-width: 100px;
    flex: 0 0 100px;
    border-radius: 0;
    overflow: hidden;
}
</style>