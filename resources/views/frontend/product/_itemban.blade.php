<?php $thongtin = json_decode($sale_product->thongtin) ?>
<div class="project-item ver transUp">
  <figure class="img embed-responsive hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $sale_product->avatar,'data' => 'item_project', 'time' => $sale_product->updated_at])}}" alt=""><a class="link" href="/ban/{{$sale_product->slug}}" title="{{$sale_product->title}}"></a></figure>
  <div class="content fs_14"><a class="name link" href="/ban/{{$sale_product->slug}}" title="{{$sale_product->title}}">{{$sale_product->title}}</a>
    <p class="address">{{$sale_product->address}}</p>
    <ul class="reset-list list">
      <li class="item"><i class="zmdi zmdi-airline-seat-individual-suite"></i>{{@$thongtin->phongngu}}
      </li>
      <li class="item"><i class="zmdi zmdi-photo-size-select-small"></i>{{@$thongtin->dientich}} m²
      </li>
      <li class="item price">{{$sale_product->allprice_mark}}</li>
    </ul>
  </div>
</div>