@extends('frontend.layouts.main')
@section('content')
<main class="main">
  <!-- PROJECTS-->
  <section class="section-spacing bg-gray project">
    <div class="container">
      <h2 class="title-main fs_30">Nhà đất thuê<span>({{$count_products}})</span></h2>
      <div class="filter-container">
        <button class="reset-btn filter-open js-filterTrigger"><i class="zmdi zmdi-filter-list"></i>Bộ lọc</button>
        <form id="filter_thue" class="filter-wrap js-filterCollapse" method="GET"
        action="/tim-nha-dat-thue">
        <input type="hidden" name="tags" value="{{@$data_request['tags']}}">
          <button class="reset-btn filter-close" type="button"><i class="zmdi zmdi-chevron-left"></i>Trở về</button>
          <div class="filter-ctrl">
            <button class="reset-btn button button-black js-collapseHide" type="button">Hủy</button>
            <button class="reset-btn button button-red" name="collapseFilterundefined">Áp dụng</button>
          </div>
          <div class="filter-list js-blurOff">
            <div class="filter-item">
              <button class="reset-btn btn-collapse collapsed" type="button" data-toggle="collapse" data-target="#collapseFilter0">Chọn loại hình</button>
              <div class="filter-collapse collapse js-filterContent" id="collapseFilter0">
                <div class="inner">
                  <p class="title">Chọn loại bất động</p>
                  <input type="hidden" name="category_id" class="id_cat" value="{{@$data_request['category_id']}}">
                  <ul class="reset-list list">

                    @if (count($type) != 0)
                        @foreach ($type as $product_cat1)
                        <li class="item">
                          <label class="checkbox cat_box">
                            <input <?php if(in_array($product_cat1->id, $idcats)) echo 'checked'; ?> value="{{$product_cat1->id}}" class="checkbox-input" type="checkbox"><span class="checkbox-text">{{$product_cat1->title}}</span>
                          </label>
                        </li>
                        @endforeach
                    @else
                    
                    @endif
                  </ul>

                  <script>
                    $('.cat_box').click(function(event) {
                      var listid = [];
                      $('.cat_box .checkbox-input').each(function(index, el) {
                        if ($(this).prop('checked')==true){ 
                          listid.push($(this).val());
                        }
                      });

                      $('.id_cat').val(JSON.stringify(listid))
                    });
                  </script>

                  <div class="ctrl">
                    <button class="reset-btn button button-black js-collapseHide" type="button">Hủy</button>
                    <button class="reset-btn button button-red" name="collapseFilter0">Áp dụng</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="filter-item">
              <button class="reset-btn btn-collapse collapsed" type="button" data-toggle="collapse" data-target="#collapseFilter1">Phòng ngủ</button>
              <div class="filter-collapse collapse js-filterContent" id="collapseFilter1">
                <div class="inner">
                  <p class="title">Chọn số phòng ngủ</p>

                  <input type="hidden" name="number_bedroom" class="number_bedroom" value="{{@$data_request['number_bedroom']}}">
                  <ul class="reset-list list">
                    @for ($i=0; $i < 6; $i++)
                      <li class="item">
                        <label class="checkbox bedroom_box">
                          <input <?php if(in_array($i+1, $number_bedrooms)) echo 'checked'; ?> value="{{$i+1}}" class="checkbox-input" type="checkbox"><span class="checkbox-text">{{$i+1}}@if($i == 5)+@endif phòng ngủ</span>
                        </label>
                      </li>
                    @endfor
                  </ul>

                  <script>
                    $('.bedroom_box').click(function(event) {
                      var listid = [];
                      $('.bedroom_box .checkbox-input').each(function(index, el) {
                        if ($(this).prop('checked')==true){ 
                          listid.push($(this).val());
                        }
                      });

                      $('.number_bedroom').val(JSON.stringify(listid))
                    });
                  </script>

                  <div class="ctrl">
                    <button class="reset-btn button button-black js-collapseHide" type="button">Hủy</button>
                    <button class="reset-btn button button-red" name="collapseFilter1">Áp dụng</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="filter-item">
              <button class="reset-btn btn-collapse collapsed" type="button" data-toggle="collapse" data-target="#collapseFilter2">Khoảng giá</button>
              <div class="filter-collapse collapse js-filterContent" id="collapseFilter2">
                <div class="inner">
                  <p class="title">Chọn tầm giá</p>
                  <ul class="reset-list list">

                    @if (count($db_searchs) != 0)
                        @foreach ($db_searchs as $key => $db_search)
                        <?php $seachpriec =  $db_search->value_min.','.$db_search->value_max;?>
                        <li class="item">
                          <label class="checkbox radio">
                            <input <?php if($seachpriec == @$data_request['allprice']) echo 'checked'; ?> value="{{$db_search->value_min}},{{$db_search->value_max}}" class="checkbox-input" type="radio" name="allprice">
                            @if($db_search->value_min == 0)
                            <span class="checkbox-text"> Dưới {{BladeGeneral::bd_nice_number($db_search->value_max)}}</span>
                            @else
                            <span class="checkbox-text">Từ {{BladeGeneral::bd_nice_number($db_search->value_min)}} đến {{BladeGeneral::bd_nice_number($db_search->value_max)}}</span>
                            @endif
                          </label>
                        </li>

                        @if($key+1 == count($db_searchs))
                        <li class="item">
                          <label class="checkbox radio">
                            <input value=">,{{$db_search->value_max}}" class="checkbox-input" type="radio" name="allprice"><span class="checkbox-text">Trên {{BladeGeneral::bd_nice_number($db_search->value_max)}}</span>
                          </label>
                        </li>
                        @endif
                        @endforeach
                    @else
                    
                    @endif
                  </ul>
                  <div class="ctrl">
                    <button class="reset-btn button button-black js-collapseHide" type="button">Hủy</button>
                    <button class="reset-btn button button-red" name="collapseFilter2">Áp dụng</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="filter-item">
              <button class="reset-btn btn-collapse collapsed" type="button" data-toggle="collapse" data-target="#collapseFilter3">Thêm</button>
              <div class="filter-collapse collapse style2 js-filterContent" id="collapseFilter3">
                <div class="inner">
                  <div class="column">
                    <p class="title">Chọn số phòng tắm</p>
                    <ul class="reset-list list">

                      @for ($i=0; $i < 5; $i++) 
                      <li class="item">
                        <label class="checkbox radio">
                          <input <?php if($i+1 == @$data_request['number_bathroom']) echo 'checked'; ?> value="{{$i+1}}" class="checkbox-input" type="radio" name="number_bathroom"><span class="checkbox-text">{{$i+1}} phòng tắm</span>
                        </label>
                      </li>
                      @endfor
                      <li class="item">
                        <label class="checkbox radio">
                          <input <?php if(@$data_request['number_bathroom'] == "") echo 'checked'; ?> value="" class="checkbox-input" type="radio" name="number_bathroom"><span class="checkbox-text">Tất cả</span>
                        </label>
                      </li>
                      
                    </ul>
                  </div>
                  <div class="column">
                    <p class="title">Chọn hướng</p>
                    <input type="hidden" name="huongnha" class="huongnha" value="{{@$data_request['huongnha']}}">
                    <ul class="reset-list list">

                      @if (count($phuonghuongs) != 0)
                          @foreach ($phuonghuongs as $phuonghuong)
                          <li class="item">
                            <label class="checkbox huongnha_box">
                              <input <?php if(in_array($phuonghuong, $huongnhas)) echo 'checked'; ?> value="{{$phuonghuong}}" class="checkbox-input" type="checkbox"><span class="checkbox-text">{{$phuonghuong}}</span>
                            </label>
                          </li>
                          @endforeach
                      @else
                      
                      @endif

                    </ul>

                    <script>
                    $('.huongnha_box').click(function(event) {
                      var listid = [];
                      $('.huongnha_box .checkbox-input').each(function(index, el) {
                        if ($(this).prop('checked')==true){ 
                          listid.push($(this).val());
                        }
                      });

                      $('.huongnha').val(JSON.stringify(listid))
                    });
                  </script>

                  </div>
                  <div class="ctrl">
                    <button class="reset-btn button button-black js-collapseHide" type="button">Hủy</button>
                    <button class="reset-btn button button-red" name="collapseFilter3">Áp dụng</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <input type="hidden" name="orderBy" class="order_by">
        </form>
        <label class="selectbox">
          <select class="select_orderby">
            <option @if($orderby_mark == 'id,desc') selected @endif value="id,desc">Mới nhất</option>
            <option @if($orderby_mark == 'allprice,asc') selected @endif value="allprice,asc">Giá tăng dần</option>
            <option @if($orderby_mark == 'allprice,desc') selected @endif value="allprice,desc">Giá giảm dần</option>
          </select>
        </label>

        <script>
          $('.select_orderby').change(function(event) {
            $('.order_by').val($('.select_orderby').val());
            $('#filter_thue').submit();
          });
        </script>
      </div>
      <div class="row">
        <!-- 18 items / page -->
        @foreach($sale_products as  $sale_product)
        <article class="col-12 col-md-6 col-lg-4">
          @include('frontend.product._itemthue')
        </article>
        @endforeach
      </div>
      {{ $sale_products->appends([
        'category_id' => @$data_request['category_id'],
        'number_bedroom' => @$data_request['number_bedroom'],
        'number_bathroom' => @$data_request['number_bathroom'],
        'allprice' => @$data_request['allprice'],
        'huongnha' => @$data_request['huongnha'],
        'orderBy' => $orderby_mark,
        'tags' => @$data_request['tags']
      ])->links() }}
    </div>
  </section>
  
</main>
@endsection