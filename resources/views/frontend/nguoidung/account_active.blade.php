@extends('frontend.layouts.main')
@section('content')
@include('frontend.nguoidung._plugin')
<style>
    @media screen and (min-width: 767px){
        .thanhcong{
            max-width: 600px !important;
            text-align: center;
        }
    }
    
</style>
<!--/head-block-->
<div class="content-index">
    <div class="bg-gray py-3">
        <div class="container thanhcong">
            <div class="row">

                <div class="col-12" id="formdoimatkhau ">
                    <div class="collapse show" data-toggle="collapse" data-parent="#loginForm">
                        <h3 class="title-main cl_orange text-uppercase">Successful activation</h3>
                        <form class="bg-white p-3 p-lg-4 form-login">   

                            <div class="alert alert-success" role="alert"> <strong>Hi: {{$nguoidung->name}}.</strong> You have successfully activated your account. Please login to continue.
                            </div>

                            <a href="/create-account" class="btn btn-main text-uppercase text-center">Log in</a>
                        </form>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection