@extends('frontend.layouts.main')
@section('content')

@include('frontend.nguoidung._plugin')
<style type="text/css" media="screen">
    .title-main{
        font-family: "Nunito Sans";
    }    
</style>
<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>Home</a></li>
                <li class="item active">Create Account</li>
            </ul>
        </div>
    </nav>
    <!-- ACCOUNT-->
    <section class="section-spacing">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 mb-3">
                    <h3 class="title-main mb-3">Create Account</h3>
                    <form id="form_nguoidung" class="form-hor">
                        <div class="form-hor__row mb-3">
                            <div class="text"><span>Sex </span><span class="cl_red">*</span></div>
                            <div class="input">
                                <label class="mr-3">
                                <input value="1" class="mr-2" type="radio" name="gioitinh" checked>Male
                                </label>

                                <label class="mr-3">
                                <input value="0" class="mr-2" type="radio" name="gioitinh">Female
                                </label>

                                <label class="mr-3">
                                <input value="2" class="mr-2" type="radio" name="gioitinh">Other
                                </label>
                            </div>
                        </div>
                        <div class="form-hor__row mb-3">
                            <div class="text"><span>Full name </span><span class="cl_red">*</span></div>
                            <div class="input">
                                <input class="form-control" type="text" name="name">
                            </div>
                        </div>
                        <div class="form-hor__row mb-3">
                            <div class="text"><span>Email </span><span class="cl_red">*</span></div>
                            <div class="input">
                                <input class="form-control" type="email" name="email">
                            </div>
                        </div>
                        <div class="form-hor__row mb-3">
                            <div class="text"><span>Tel </span><span class="cl_red">*</span></div>
                            <div class="input">
                                <input class="form-control" type="number" name="phone">
                            </div>
                        </div>
                        
                        <div class="form-hor__row mb-3">
                            <div class="text"><span>Password </span><span class="cl_red">*</span></div>
                            <div class="input">
                                <input id="password" class="form-control" type="password" name="password">
                            </div>
                        </div>
                        <div class="form-hor__row mb-3">
                            <div class="text"><span>Re password </span><span class="cl_red">*</span></div>
                            <div class="input">
                                <input class="form-control" type="password" name="repassword">
                            </div>
                        </div>
                        <div class="form-hor__row mb-3">
                            <div class="text d-none d-md-block">&nbsp;</div>
                            <div class="input">
                                <button class="reset-btn btn-main w-100 fs_14" type="submit">CREATE ACCOUNT</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-12 col-lg-6 mb-3">
                    <h3 class="title-main mb-3">Log in</h3>
                    <form id="form_dangnhap" class="form-hor">
                        <div class="form-hor__row mb-3">
                            <div class="text"><span>Email </span><span class="cl_red">*</span></div>
                            <div class="input">
                                <input class="form-control" type="email" name="email">
                            </div>
                        </div>
                        <div class="form-hor__row mb-3">
                            <div class="text"><span>Password </span><span class="cl_red">*</span></div>
                            <div class="input">
                                <input class="form-control" type="password" name="password">
                            </div>
                        </div>
                        <div class="form-hor__row mb-3">
                            <div class="text d-none d-md-block">&nbsp;</div>
                            <div class="input">
                                <p class="mb-2">Forgot password? <a class="link" href="/forgot-password">Click here!</a></p>
                                <button class="reset-btn btn-main w-100 fs_14 mb-2" type="submit">LOG IN</button>
                                <button class="reset-btn btn-blue w-100 fs_14 login-facebook" type="submit"><i class="zmdi zmdi-facebook mr-2"></i>LOG IN WITH FACEBOOK</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>

<script>
    var form_nguoidung = $('#form_nguoidung').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
        },
        rules: {
            name:{
                required: true,
                minlength : 3,
                maxlength: 15
            },
            email:{
                required: true,
                email: true
            },
            phone:{
                required: true,
                minlength : 3,
                maxlength: 15
            },
            
            password:{
                required: true,
                minlength : 3,
                maxlength: 15
            },
            repassword : {
                equalTo : "#password",
            }
        },
        messages: {
            name:{
                required: 'Name cannot be empty.',
                minlength: 'Please enter 3 - 15 characters',
                maxlength: 'Please enter 3 - 15 characters'
            },
            email:{
                required: 'Email cannot be empty.',
                email: 'Incorrect email format'
            },
            phone:{
                required: 'Phone number cannot be empty.',
                minlength: 'Please enter 3 - 15 characters',
                maxlength: 'Please enter 3 - 15 characters'
            },
            
            password:{
                required: 'Password cannot be empty.',
                minlength: 'Please enter 3 - 15 characters',
                maxlength: 'Please enter 3 - 15 characters'
            },
            repassword: {
                required:'Re password cannot be empty.',
                equalTo: 'Please re-enter the same value.'
            }
        },
        submitHandler: function (form) {
    
            var data = {};
            $("#form_nguoidung").serializeArray().map(function(x){data[x.name] = x.value;});
            
            delete data["repassword"];
           
            $('.block-page-all').addClass('active');
    
            $.ajax({
                type: 'POST',
                url: '/add_nguoidung',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    document.location.href = '/register-success';
                }
            });
            return false;
        }
    });

    var form_dangnhap = $('#form_dangnhap').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
        },
        rules: {
            email:{
                required: true
            },
            password:{
                required: true
            }
        },
        messages: {
            email:{
                required: 'No email entered yet.'
            },
            password:{
                required: 'Password has not been entered.'
            }
        },
        submitHandler: function (form) {
    
            var data = {};
            $("#form_dangnhap").serializeArray().map(function(x){data[x.name] = x.value;});
           
            $('.block-page-all').addClass('active');
    
            $.ajax({
                type: 'POST',
                url: '/dangnhap',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    switch (result.code) {
                        
                        case 200:
                            window.location.href = "/info-account";
                            break;

                        case 201:
                            window.location.href = "/register-success";
                            break;

                        case 300:
                            toastr.error(result.error);
                            $('.block-page-all').removeClass('active');
                            break;

                        default:
                            break;
                    }
                }
            });
            return false;
        }
    });
</script>

<script>
    // login facebook
    function statusChangeCallback(response) {
        if (accessToken) {
            var accessToken = response.authResponse.accessToken;
        }
        if (response.status === 'connected') {
            graph_api(accessToken);
        } else if (response.status === 'not_authorized') {
            Alert.show('Có lỗi xảy ra , Vui lòng thử lại', 'error');
        } else {
            Alert.show('Có lỗi xảy ra , Vui lòng thử lại', 'error');
        }
    }

    window.fbAsyncInit = function () {
        FB.init({
          appId      : '1223865444468019',
          xfbml      : true,
          version    : 'v3.2'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_EN/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $(".login-facebook").click(function(){
        FB.login(statusChangeCallback, {scope: 'email,public_profile', return_scopes: true});
    });

    function graph_api() {
        FB.api('/me', { fields: 'id,name,email' },
            function (response) {
                var data = {
                    id: response.id,
                    name: response.name,
                    email: response.email,
                };
                $('.block-page-all').addClass('active');
                $.ajax({
                    type: 'POST',
                    url: '/login_social',
                    data: data,
                    dataType: 'json',
                    error: function(){
                        $('.block-page-all').removeClass('active');
                        toastr.error(result.error);
                    },
                    success: function(result) {
                        if (result.code == 300) {
                            toastr.error(result.error);
                            $('.block-page-all').removeClass('active');
                            return false
                        }
                        location.reload();
                    }
                });
            }
        );
    }
</script>
@endsection