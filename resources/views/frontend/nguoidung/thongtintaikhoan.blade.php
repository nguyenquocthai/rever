@extends('frontend.layouts.main')
@section('content')
@include('frontend.nguoidung._plugin')

<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>Home</a></li>
                <li class="item active">Edit account</li>
            </ul>
        </div>
    </nav>
    <!-- NEWS-->
    <div class="profile-block">
        <div class="content-index bg-gray">
            <div class="container">
                <div class="bg-white panel-wrap my-3 my-md-5">
                    @include('frontend.nguoidung._widget')

                    <section class="panel-content">
                        <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>News Board</button>

                        <h2 class="title text-uppercase">Change personal information</h2>
                        
                        <form autocomplete="off" id="user_form" class="panel-form" enctype="multipart/form-data">
                            <div class="d-flex flex-wrap block">
                                <div class="left">
                                    <h4 class="title-line"><span class="text"><i class="far fa-image"></i>Avatar</span></h4>
                                    
                                    <figure class="img-upload file_upload_box">
                                        <label class="img">
                                            <img class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $nguoidung->avatar,'data' => 'nguoidung', 'time' => $nguoidung->updated_at])}}" alt="">
                                            <input accept="image/*" id="avatar_user" type="file" class="d-none file_image" name="avatar">
                                        </label>
                                        <button type="button" class="reset-btn btn-main text-uppercase upload_trigir">Upload</button>
                                    </figure>
                                </div>

                                <script>
                                    $('.upload_trigir').click(function(event) {
                                        $('#avatar_user').trigger('click');
                                    });
                                </script>

                                <div class="right">
                                    <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Personal information</span> <span class="badge badge-info">ID: {{$nguoidung->id}}</span></h4>

                                    <div class="panel-form__row">
                                        <label class="text mb-md-0">Full name <span class="cl_red">(*)</span></label>
                                        <div class="input">
                                            <input type="text" name="name" class="form-control" value="{{$nguoidung->name}}">
                                        </div>
                                    </div>
                                    <div class="panel-form__row">
                                        <label class="text mb-md-0">Birthday</label>
                                        <div class="input">
                                            <input name="ngaysinh" value="{{$nguoidung->ngaysinh}}" type="text" class="form-control ngaysinh">
                                        </div>
                                    </div>
                                    <div class="panel-form__row">
                                        <label class="text mb-md-0">Sex</label>
                                        <div class="input">
                                            @if($nguoidung->gioitinh == 3)
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="male" value="1"> Male
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="female" value="0"> Female
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="other" value="2"> Other
                                            </label>
                                            @endif

                                            @if($nguoidung->gioitinh == 1)
                                            <label class="mt-2 mr-3">
                                                <input checked type="radio" name="gioitinh" id="male" value="1"> Male
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="female" value="0"> Female
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="other" value="2"> Other
                                            </label>
                                            @endif

                                            @if($nguoidung->gioitinh == 0)
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="male" value="1"> Male
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input checked type="radio" name="gioitinh" id="female" value="0"> Female
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="other" value="2"> Other
                                            </label>
                                            @endif

                                            @if($nguoidung->gioitinh == 2)
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="male" value="1"> Male
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="female" value="0"> Female
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input checked type="radio" name="gioitinh" id="other" value="2"> Other
                                            </label>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="block">
                                <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Additional information</span></h4>
                                <?php
                                    $gioithieu = [];
                                    if ($nguoidung->gioithieu != null) {
                                        $gioithieu = json_decode($nguoidung->gioithieu);
                                    }
                                ?>
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Occupation</label>
                                    <div class="input">
                                        <input name="occupation" value="{{@$gioithieu->occupation}}" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Workplace</label>
                                    <div class="input">
                                        <input name="workplace" value="{{@$gioithieu->workplace}}" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Hometown <span class="cl_red">(*)</span></label>
                                    <div class="input">
                                        <input name="hometown" value="{{@$gioithieu->hometown}}" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="panel-form__row">
                                    <label class="text mb-md-0">First language <span class="cl_red">(*)</span></label>
                                    <div class="input">
                                        <input name="first_language" value="{{@$gioithieu->first_language}}" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Other languages</label>
                                    <div class="input">
                                        <input name="other_languages" value="{{@$gioithieu->other_languages}}" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Relationship status <span class="cl_red">(*)</span></label>
                                    <div class="input">
                                        <input name="relationship_status" value="{{@$gioithieu->relationship_status}}" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="panel-form__row flex-100 order-2">
                                    <label class="text mb-md-0">Bio</label>
                                    <div class="input">
                                        <textarea placeholder="Add a short bio to tell people more about yourself" name="intro" rows="5" class="form-control">{{$nguoidung->intro}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="block">
                                <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Contact information</span></h4>

                                <!-- <div class="panel-form__row">
                                    <label class="text mb-md-0">Tên thường gọi</label>
                                    <div class="input">
                                        <input name="callname" value="{{$nguoidung->callname}}" type="text" class="form-control">
                                    </div>
                                </div> -->
                                
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Phone number <span class="cl_red">(*)</span></label>
                                    <div class="input">
                                        <input name="phone" value="{{$nguoidung->phone}}" type="text" class="form-control">
                                    </div>
                                </div>
    <!--                             <div class="panel-form__row">
                                    <label class="text mb-md-0">Điện thoại bàn</label>
                                    <div class="input">
                                        <input name="phone2" value="{{$nguoidung->phone2}}" type="text" class="form-control">
                                    </div>
                                </div> -->
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Email <span class="cl_red">(*)</span></label>
                                    <div class="input">
                                        <input name="email" value="{{$nguoidung->email}}" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Facebook</label>
                                    <div class="input">
                                        <input name="facebook" value="{{@$gioithieu->facebook}}" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Address <span class="cl_red">(*)</span></label>
                                    <div class="input">
                                        <input name="address" value="{{$nguoidung->address}}" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="panel-form--footer">
                                <button type="submit" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Save</span></button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>

        <script>
            $('.ngaysinh').datepicker({
                autoClose:true,
                dateFormat: 'yyyy-mm-dd',
                language: 'en'
            })
            var user_form = $('#user_form').validate({
                highlight: function(element, errorClass, validClass) {
                    $(element).removeClass(errorClass);
                },
                rules: {
                    name:{
                        required: true
                    },
                    email:{
                        required: true
                    },
                    address:{
                        required: true
                    },
                    phone:{
                        required: true
                    },
                    hometown:{
                        required: true
                    },
                    first_language:{
                        required: true
                    },
                    relationship_status:{
                        required: true
                    }
                },
                messages: {
                    name:{
                        required: 'No name entered.'
                    },
                    email:{
                        required: 'No email entered yet'
                    },
                    address:{
                        required: 'No address entered'
                    },
                    phone:{
                        required: 'Phone number not entered'
                    },
                    hometown:{
                        required: 'Hometown not entered'
                    },
                    first_language:{
                        required: 'First language not entered'
                    },
                    relationship_status:{
                        required: 'Relationship status not entered'
                    }
                },
                submitHandler: function (form) {
            
                    var data = {};
                    $("#user_form").serializeArray().map(function(x){data[x.name] = x.value;});

                    $('.block-page-all').addClass('active');
                    $.ajax({
                        type: 'POST',
                        url: '/suataikhoan',
                        data: data,
                        dataType: 'json',
                        error: function(){
                            $('.block-page-all').removeClass('active');
                            toastr.error(result.error);
                        },
                        success: function(result) {
                            if (result.code == 300) {
                                toastr.error(result.error);
                                $('.block-page-all').removeClass('active');
                                return false;
                            }
                            $('.block-page-all').removeClass('active');
                            toastr.success(result.message);
                        }
                    });
                    return false;
                }
            });

            $('#avatar_user').change(function(event) {
                var file = this.files[0];
                if (typeof(file) === 'undefined') {
                    return false;
                }
                var fileType = file["type"];
                var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(fileType, ValidImageTypes) < 0) {
                    toastr.error('Hình ảnh không hợp lệ', null, {timeOut: 4000});
                    return false;
                }
                if (file.size > 1000000) {
                    toastr.error('The right shape is smaller 1MB', null, {timeOut: 4000});
                    return false;
                }

                var images = $(this).closest('.file_upload_box').find('.image_review');
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        images.attr('src', e.target.result);
                    }

                    reader.readAsDataURL(this.files[0]);
                }

                var fd = new FormData();
                fd.append("status_code", "change_avatar");
                if ($(".file_image").get(0)){
                    fd.append("avatar", $(".file_image").get(0).files[0]);
                }

                $('.block-page-all').addClass('active');
                $.ajax({
                    type: 'POST',
                    url: '/suataikhoan',
                    data: fd,
                    dataType: 'json',
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false, // NEEDED, DON'T OMIT THIS
                    success: function(result) {
                        $('.block-page-all').removeClass('active');
                    }
                });
            });
        </script>
    </div>
</main>
@endsection