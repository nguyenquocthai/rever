@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>Home</a></li>
                <li class="item active">Forgot password</li>
            </ul>
        </div>
    </nav>
    <!-- ACCOUNT-->
    <section class="section-spacing">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 col-xl-6 mb-3">
                    <div class="block">
                        <h3 class="block-title mb-3">Forgot password</h3>
                        <form class="form-hor" id="form_forget_pass">
                            <div class="form-hor__row mb-3">
                                <div class="text"><span>Email </span><span class="cl_red">*</span></div>
                                <div class="input">
                                    <input class="form-control" type="email" name="email">
                                </div>
                            </div>
                            <div class="form-hor__row mb-3">
                                <div class="text d-none d-md-block">&nbsp;</div>
                                <div class="input">
                                    <button class="reset-btn btn-main w-100 fs_14" type="submit">SEND</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script>
    // form forget password
    var form_forget_pass = $('#form_forget_pass').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
        },
        rules: {
            email:{
                required: true,
                email: true
            }
        },
        messages: {
            email:{
                required: 'Please enter Email',
                email: 'Invalid email.',
            },
            
        },
        submitHandler: function (form) {
            $('.block-page-all').addClass('active');
            var data = {};
            $("#form_forget_pass").serializeArray().map(function(x){data[x.name] = x.value;});
            $.ajax({
                type: 'POST',
                url: '/mail_forgot_pass',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error('Email has not registered an account');
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    $('.block-page-all').removeClass('active');
                    toastr.success('Check your email to get your password back.');
                }
            });

            return false;
        }
    });
</script>
@endsection