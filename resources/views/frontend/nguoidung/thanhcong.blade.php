@extends('frontend.layouts.main')
@section('content')
@include('frontend.nguoidung._plugin')
<style>
    @media screen and (min-width: 767px){
        .thanhcong{
            max-width: 600px !important;
            text-align: center;
        }
    }
     
</style>
<!--/head-block-->
    <div class="content-index">
        <div class="bg-gray py-3">
            <div class="container thanhcong">
                <div class="row">

                    <div class="col-12" id="formdoimatkhau ">
                        <div class="collapse show" data-toggle="collapse" data-parent="#loginForm">
                            <h3 class="title-main cl_orange text-uppercase">Sign Up Success</h3>
                            <form class="bg-white p-3 p-lg-4 form-login">   

                                <div class="alert alert-success" role="alert"> <strong>Hi: {{$nguoidung->name}}</strong>. You have successfully registered an account. Check email <b>{{$nguoidung->email}}</b> to activate.
                                    <p><i>If you have not received an email, please click send again.</i></p>
                                </div>

                                <button type="button" class="btn btn-main text-uppercase text-center reactive">Send</button>
                            </form>
                        </div>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>


<script>
    $('.reactive').click(function(event) {
        var data = {};
        data['status_code'] = 'reactive';
        $('.block-page-all').addClass('active');
        $.ajax({
            type: 'POST',
            url: '/add_nguoidung',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                if (result.code == 300) {
                    toastr.error(result.error);
                    $('.block-page-all').removeClass('active');
                    return false
                }
                $('.block-page-all').removeClass('active');
                toastr.success(result.message);
            }
        });
    });
</script>
@endsection