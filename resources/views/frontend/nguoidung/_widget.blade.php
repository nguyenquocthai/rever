<aside class="panel-aside">
    <button class="reset-btn panel-aside__close d-lg-none js-panelAsideTrigger"><i class="fa fa-times"></i></button>

    <div class="inner js-blurOff">
        <h4 class="title-line">
            <span class="text"><i class="far fa-newspaper"></i>Manage your listings</span>
        </h4>
        <ul class="panel-nav reset-list">
            
            <li class="item"><a class="{{@$dangtinthue}}" href="/post-rental"><i class="fa fa-angle-right"></i>My listings</a></li>

            <li class="item"><a class="{{@$lienhebds}}" href="/contact-rental"><i class="fa fa-angle-right"></i>Inbox</a></li>
            
        </ul>

        <h4 class="title-line">
            <span class="text"><i class="far fa-newspaper"></i>Profile</span>
        </h4>
        <ul class="panel-nav reset-list">
            <li class="item"><a class="{{@$active_thongtintaikhoan}}" href="/info-account"><i class="fa fa-angle-right"></i>Personal information</a></li>
            <li class="item"><a class="<?php if($_SERVER['REQUEST_URI']=="/change-pass") echo "active"; ?>" href="/change-pass"><i class="fa fa-angle-right"></i>Change password</a></li>
            <li class="item"><a href="/logout"><i class="fa fa-angle-right"></i>Log out</a></li>
        </ul>
    </div>
</aside>