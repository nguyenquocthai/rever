<h3>Xin chào <i>{{ $forgetpass->receiver }}</i></h3>
<p>Bạn đã gửi đến chúng tôi yêu cầu lấy lại mật khẩu đã quên.</p>

<p><b>Vui lòng nhấn vào đường link bên dưới để đổi mật khẩu mới.</b></p>

<div>
<p><b>Liên kết đổi mật khẩu:</b>&nbsp;{{ $forgetpass->forgetpass_one }}</p>
</div>

Thank You,
<br/>
<i>{{ $forgetpass->receiver }}</i>