<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,viewport-fit=cover">
<link rel="icon" type="image/png" sizes="96x96" href="/public/img/upload/settings/{{@$info_web['favicon']}}">
{!!$info_web['google_analytics']!!}
@include('frontend.elements._seo')
@include('frontend.elements._plugin')
@include('frontend.elements._loading')

<style>
	 .pagination {
	    justify-content: center;display: flex;
	    flex-wrap: wrap;padding-left: 0;
	    margin-bottom: 0;
	    list-style: none;
	}

	.pagination .page-item .page-link, .pagination li a, .pagination li span {
	    display: flex;
	    height: 1.75rem;
	    width: 1.75rem;
	    overflow: hidden;
	    justify-content: center;
	    align-items: center;
	    border-radius: .25rem;
	    color: #222;
	    font-size: 1rem;
	    line-height: 1;
	    border: none;
	}
	
	.pagination .page-item,.pagination li {
		margin: 0 .25rem .25rem;
	}
	@media screen and (min-width: 576px)
	{.pagination .page-item .page-link,.pagination li a, .pagination li span {
	    height: 2.25rem;
	    width: 2.25rem;
	    font-size: 1.125rem;
	}}
	.page-item.disabled .page-link,.pagination li.disabled a,.pagination li.disabled span,
	.pagination .page-item .page-link[rel="prev"],.pagination li a[rel="prev"],.pagination li span[rel="prev"],
	.pagination .page-item .page-link[rel="next"],.pagination li a[rel="next"],.pagination li span[rel="next"]{
		font-size: 2rem;
	}
	.pagination .page-item.active .page-link,.pagination li.active a,.pagination li.active span, .pagination .page-item:hover .page-link,.pagination li:hover a,.pagination li:hover span {
	    color: #fff;
	    background-color: #222;
	}
	.page-item.disabled:hover .page-link,li.disabled:hover a,li.disabled:hover span{
		color: #222;
	    background-color: #fff;
	}
	.pagination .page-item.disabled,.pagination li.disabled {
	    opacity: .5;
	}
</style>
