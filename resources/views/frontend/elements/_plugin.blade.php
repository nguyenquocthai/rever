

	<link rel="stylesheet" href="/public/theme/vendors/bootstrap-4/bootstrap.min.css">
    <link rel="stylesheet" href="/public/theme/vendors/material-design-iconic-font/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="/public/theme/vendors/slick-1.8.1/slick.css">
    <link rel="stylesheet" href="/public/theme/vendors/js/jquery-ui.min.css">
    <link rel="stylesheet" href="/public/theme/vendors/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" href="/public/theme/vendors/footer.min-1569223378600.css">
    <link rel="stylesheet" href="/public/theme/css/main.css">

	

	@include('frontend.elements._jstheme')

	<!-- fontawesome -->
	<link rel="stylesheet" href="/public/assets/global/plugins/fontawesome/web-fonts-with-css/css/fontawesome-all.css">
	<!-- /fontawesome -->

	<!-- validate -->
	<script src="/public/vendor/jquery-validation-1.16.0/jquery.validate.min.js"></script>
	<script src="/public/vendor/jquery-validation-1.16.0/additional-methods.min.js"></script>
	<!-- /validate -->

	<!-- toastr -->
	<link href="/public/assets/global/plugins/toastr/toastr.min.css" rel="stylesheet">
	<script src="/public/assets/global/plugins/toastr/toastr.min.js"></script>
	<!-- /toastr -->

	<!-- datepicker -->
	<link href="/public/vendor/air-datepicker-master/dist/css/datepicker.min.css" rel="stylesheet">
	<script src="/public/vendor/air-datepicker-master/dist/js/datepicker.min.js"></script>
	<script src="/public/vendor/air-datepicker-master/dist/js/i18n/datepicker.en.js"></script>
	<!-- /datepicker -->

	<!-- DataTables -->
	<link href="/public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" />
	<link href="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" />
	<script src="/public/assets/global/scripts/datatable.js"></script>
	<script src="/public/assets/global/plugins/datatables/datatables.min.js" ></script>
	<script src="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
	<!-- /DataTables -->

	<!-- select2 -->
	<link href="/public/assets/global/plugins/select2-4.0.3/css/select2.min.css" rel="stylesheet">
	<link href="/public/assets/global/plugins/select2-4.0.3/css/select2-bootstrap.css" rel="stylesheet">
	<script src="/public/assets/global/plugins/select2-4.0.3/js/select2.full.min.js"></script>
	<script src="/public/assets/global/plugins/select2-4.0.3/js/i18n/en.js"></script>
	<!-- /select2 -->

	<!-- profile -->
	<link rel="stylesheet" href="/public/css/profile/style.css">
	<link rel="stylesheet" href="/public/css/profile/custom.css">

	
