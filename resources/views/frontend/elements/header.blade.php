<header class="header">
      <div class="header-container">
        <div class="header-avatar">
          <figure class="img hasLink"><img src="/public/img/upload/settings/{{@$info_web['logo']}}" alt=""><a class="link" href="/"></a></figure>
        </div><a class="reset-btn btn-red header-contact" href="lien-he.html">Liên hệ</a>
        <button class="reset-btn navigation-open js-navTrigger"><i class="zmdi zmdi-menu"></i></button>
        <nav class="navigation js-nav">
          <button class="reset-btn navigation-close"><i class="zmdi zmdi-chevron-left"></i>Trở về</button>
          <div class="navigation-wrap js-blurOff">
            <ul class="reset-list navigation-list">
              <li class="item"><a class="link" id="index" href="/">Tổng quan</a></li>
              <li class="item"><a class="link" href="/nha-dat-ban">Nhà đất bán</a></li>
              <li class="item"><a class="link" href="/nha-dat-cho-thue">Nhà cho thuê</a></li>
              <li class="item"><a class="link" href="/tin-tuc">Tin tức</a></li>
              <li class="item"><a class="link" href="/lien-he">Liên hệ</a></li>
            </ul>
          </div>
        </nav>
        <!-- <figure class="header-logo hasLink"><img src="/public/theme/img/logo.png" alt="sosanhnhadat.com"><a class="link" href="#!" title="sosanhnhadat.com"></a></figure> -->
      </div>
    </header>

<style>
  .header-avatar .img {
    height: auto;
     max-width: 200px;
    flex: 0 0 200px;
     border-radius: 0; 
    overflow: hidden;
    margin-bottom: 0;
}
</style>