<!-- js theme -->

<script src="/public/theme/vendors/js/jquery-3.3.1.min.js"></script>
<script src="/public/theme/vendors/js/popper.min.js"></script>
<script src="/public/theme/vendors/bootstrap-4/bootstrap.min.js"></script>
<script src="/public/theme/vendors/slick-1.8.1/slick.min.js"></script>
<script src="/public/theme/vendors/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="/public/theme/js/main.min.js"></script>