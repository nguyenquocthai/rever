<footer class="footer" id="collapseFooterParent">
      <div class="footer-container">
        <ul class="reset-list hotline-info">
          <li class="d-flex"><span class="font-icon fone"><i class="zmdi zmdi-phone"></i></span>
            <div>
              <p class="mb-0 wt">Hotline (24/7)</p>
              <p class="mb-0 cyt"><a href="tel:0901 777 667" style="font-weight: 700;">{{$info_web['phone2']}}</a></p>
            </div>
          </li>
          <li class="d-flex"><span class="font-icon fone"><i class="zmdi zmdi-phone"></i></span>
            <div>
              <p class="mb-0 wt">Khếu nại, phản hồi</p>
              <p class="mb-0 cyt"><a href="tel:0901 862 898" style="font-weight: 700;">{{$info_web['phone']}}</a></p>
            </div>
          </li>
          <li class="d-flex"><span class="font-icon email-icon-pro"><i class="zmdi zmdi-email-open"></i></span>
            <div>
              <p class="mb-0 wt">Bộ phận kinh doanh</p>
              <p class="mb-0 cyt"><a href="mailto:sales@rever.vn" style="font-weight: 700;">{{$info_web['email_kinhdoanh']}}</a></p>
            </div>
          </li>
          <li class="d-flex"><span class="font-icon email-icon-pro"><i class="zmdi zmdi-email-open"></i></span>
            <div>
              <p class="mb-0 wt">Phòng dự án</p>
              <p class="mb-0 cyt"><a href="mailto:phongduan@rever.vn" style="font-weight: 700;">{{$info_web['email']}}</a></p>
            </div>
          </li>
          <li class="d-flex"><span class="font-icon email-icon-pro"><i class="zmdi zmdi-email-open"></i></span>
            <div>
              <p class="mb-0 wt">Chăm sóc khách hàng</p>
              <p class="mb-0 cyt"><a href="mailto:support@rever.vn" style="font-weight: 700;">{{$info_web['email_support']}}</a></p>
            </div>
          </li>
        </ul>
        <div class="box-about-ft">
          <ul class="reset-list right-ft">
            <li class="left-ft"><a href="/" style="padding:0 0 10px"><img src="/public/img/upload/settings/{{@$info_web['logo']}}" alt="rever" title="rever"></a>
              <p>{{$info_web['address']}}</p>
              <div class="social">
                <a class="font-icon facebook" target="_blank" title="Facebook" href="{{$info_web['facebook']}}"><i class="zmdi zmdi-facebook-box"></i></a>
                <a class="font-icon twitter" target="_blank" title="Twitter" href="{{$info_web['twitter']}}"><i class="zmdi zmdi-twitter-box"></i></a>
                <a class="font-icon in" target="_blank" title="Linkedin" href="{{$info_web['linkedin']}}"><i class="zmdi zmdi-linkedin"></i></a>
                <a class="font-icon instagram" target="_blank" title="Instagram" href="{{$info_web['instagram']}}"><i class="zmdi zmdi-instagram"></i></a>
                <a class="font-icon youtube" target="_blank" title="Youtube" href="{{$info_web['google']}}"><i class="zmdi zmdi-youtube-play"></i></a>
              </div>
            </li>
            <li>
              <button class="reset-btn link-cty collapse-btn collapsed" data-toggle="collapse" data-target="#mn-01"><strong>CÔNG TY</strong></button>
              <div class="collapse" id="mn-01">
                <a href="/gioi-thieu" title="Về Rever">Về Rever</a>
                <a href="/lien-he" title="Liên hệ">Liên hệ</a>
            </div>
            </li>
            <li>
              <button class="reset-btn link-dv collapse-btn collapsed" data-toggle="collapse" data-target="#mn-02"><strong>DỊCH VỤ</strong></button>
              <div class="collapse" id="mn-02">
                <a href="/nha-dat-ban">Bán</a>
                <a href="/nha-dat-cho-thue">Cho thuê</a></div>
            </li>
            <li>
              <button class="reset-btn link-tt collapse-btn collapsed" data-toggle="collapse" data-target="#mn-03"><strong>THÔNG TIN</strong></button>
              <div class="collapse" id="mn-03">
                @foreach($new_cats as $new_cat)
                <a href="/loai-tin-tuc/{{$new_cat->slug}}" target="_blank">{{$new_cat->title}}</a>
                @endforeach
                </div>
            </li>
            <li>
              <button class="reset-btn link-ud collapse-btn collapsed" data-toggle="collapse" data-target="#mn-04"><strong>ỨNG DỤNG</strong></button>
              <div class="collapse" id="mn-04"><a href="#!" target="_blank">Rever trên iOS</a><a href="#!" target="_blank">Rever trên Android</a><a href="#!" target="_blank">Rever Agents trên iOS</a><a href="#!" target="_blank">Rever Agents trên Android</a></div>
            </li>
          </ul>
        </div>
        <div class="footer-row last-row">
          <div class="left"><span>Copyright &copy; 2019 - Rever. All Rights Reserved.</span></div>
          <div class="right"><a href="/gioi-thieu/{{$chinhsach->slug}}">Chính sách bảo mật</a><a href="/gioi-thieu/{{$dieukhoan->slug}}">Điều khoản sử dụng</a><a href="/gioi-thieu/{{$phanhoi->slug}}">Phản hồi</a></div>
        </div>
      </div>
    </footer>

    <style>
      .project-info .price{
        font-weight: 400;
      }
    </style>