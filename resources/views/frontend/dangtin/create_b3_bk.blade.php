@extends('frontend.layouts.application')
@section('content')
@include('frontend.dangtin._head')
<div class="content-index bg-gray store-dang-tin">
    <div class="container-fluid t-video-rp project-video without-video"></div>

    <div class="container">
        <div class="bg-white panel-wrap my-3 my-md-5">
            @include('frontend.elements.sidebar_dangtin')

            <section class="panel-content dangtin_b3">
                <h2 class="title text-uppercase">Đăng tin dự án <small class="d-block"><b>Chú ý: </b>Hoàn thành những mục được cảnh báo (<i class="fas fa-exclamation-triangle"></i>) bên dưới để bài đăng của bạn tốt nhất. Xin cám ơn</small></h2>
                @include('frontend.dangtin._step')
                @if($project->step == 3)
                <div class="alert alert-success" role="alert">
                    Dự án của bạn đã gửi đến admin. Vui lòng chờ duyệt.
                </div>
                @endif
                <div class="block">
                    <h4 class="title-line"><span class="text"><i class="fas fa-database"></i> Thông tin khởi tạo</span></h4>
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Mục tiêu</th>
                                <th scope="col">Nội dung</th>
                                <th scope="col">Tình trạng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Tên dự án</td>
                                <td>{{$project->title}}</td>
                                <td>
                                    @if($project->title != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Loại dự án</td>
                                <td>{{$project->catname}}</td>
                                <td>
                                    @if($project->category_id != 0) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Tỉnh / Thành phố</td>
                                <td>{{$project->tinhthanh}}</td>
                                <td>@if($project->tinhthanh != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Quận / Huyện</td>
                                <td>{{$project->quanhuyen}}</td>
                                <td>@if($project->quanhuyen != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Địa chỉ</td>
                                <td>{{$project->address}}</td>
                                <td>@if($project->address != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Bản đồ</td>
                                <td>{{$project->map}}</td>
                                <td>@if($project->map != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Ngày bắt đầu</td>
                                <td>{{$project->start_day}}</td>
                                <td>@if($project->start_day != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Ngày kết thúc</td>
                                <td>{{$project->end_day}}</td>
                                <td>@if($project->end_day != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Nội dung tin đăng</td>
                                <td>{{$project->short_content}}</td>
                                <td>@if($project->short_content != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>

                <div class="block">
                    <h4 class="title-line"><span class="text"><i class="fas fa-user"></i> Thông tin liên hệ</span></h4>
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Mục tiêu</th>
                                <th scope="col">Nội dung</th>
                                <th scope="col">Tình trạng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Tên liên hệ</td>
                                <td>{{$project->name_contact}}</td>
                                <td>
                                    @if($project->name_contact != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Địa chỉ</td>
                                <td>{{$project->address_contact}}</td>
                                <td>
                                    @if($project->address_contact != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Di động</td>
                                <td>{{$project->phone_contact}}</td>
                                <td>@if($project->phone_contact != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Emai</td>
                                <td>{{$project->email_contact}}</td>
                                <td>@if($project->email_contact != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Nhận email phản hồi</td>
                                <td>{{$project->email_flg}}</td>
                                <td><i class="fas fa-check"></i></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="block">
                    <h4 class="title-line"><span class="text"><i class="fas fa-image"></i> Hình ảnh dự án</span></h4>
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Mục tiêu</th>
                                <th scope="col">Nội dung</th>
                                <th scope="col">Tình trạng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Hình đại diện</td>
                                <td><a target="_blank" href="{{BladeGeneral::GetImg(['avatar' => $project->avatar,'data' => 'project', 'time' => $project->updated_at])}}" title="">
                                    <img width="100" class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $project->avatar,'data' => 'project', 'time' => $project->updated_at])}}" alt="...">
                                </a></td>
                                <td>
                                    @if($project->avatar != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Album dự án</td>
                                <td><a target="_blank" href="/tao-dang-tin-b2/{{$project->id}}" title="">Có {{$project->album_count}} hình trong album</a></td>
                                <td>
                                    @if($project->album_count != 0) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Mặt bằng các tầng</td>
                                <td><a target="_blank" href="/tao-dang-tin-b2/{{$project->id}}" title="">Có {{$project->flats}} mặt bằng tầng</a></td>
                                <td>@if($project->flats != 0) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="block">
                    <h4 class="title-line"><span class="text"><i class="fab fa-audible"></i> Thông tin tổng quan</span></h4>
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Mục tiêu</th>
                                <th scope="col">Nội dung</th>
                                <th scope="col">Tình trạng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Chủ đầu tư</td>
                                <td>{{$project->investor}}</td>
                                <td>
                                    @if($project->investor != null && $project->investor != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Nhà thầu</td>
                                <td>{{$project->contractors}}</td>
                                <td>
                                    @if($project->contractors != null && $project->contractors != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Số tháp</td>
                                <td>{{$project->tower}}</td>
                                <td>
                                    @if($project->tower != null && $project->tower != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Số căn hộ</td>
                                <td>{{$project->apartments}}</td>
                                <td>
                                    @if($project->apartments != null && $project->apartments != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Kiến trúc và cảnh quan</td>
                                <td>{{$project->landscape_architecture}}</td>
                                <td>
                                    @if($project->landscape_architecture != null && $project->landscape_architecture != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Quy mô dự án</td>
                                <td>{{$project->project_scale}}</td>
                                <td>
                                    @if($project->project_scale != null && $project->project_scale != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Số Tầng</td>
                                <td>{{$project->floors}}</td>
                                <td>
                                    @if($project->floors != null && $project->floors != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Giá bán</td>
                                <td>{!!$project->price!!}</td>
                                <td>
                                    @if($project->price != 'null') 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Giá thuê</td>
                                <td>{!!$project->thue_price!!}</td>
                                <td>
                                    @if($project->thue_price != 'null') 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Thêm tổng quan dự án</td>
                                <td><a target="_blank" href="/tao-dang-tin-b2/{{$project->id}}" title="">Đã thêm {{$project->tongquans}} tổng quan vào dự án</a></td>
                                <td>
                                    @if($project->tongquans != 0) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Thêm tiện ích dự án</td>
                                <td><a target="_blank" href="/tao-dang-tin-b2/{{$project->id}}" title="">Đã thêm {{$project->tienichs}} tiện ích vào dự án</a></td>
                                <td>
                                    @if($project->tienichs != 0) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>

                <div class="block">
                    <h4 class="title-line"><span class="text"><i class="fas fa-star"></i> Chương trình bán hàng</span></h4>
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Mục tiêu</th>
                                <th scope="col">Nội dung</th>
                                <th scope="col">Tình trạng</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>Hình bán hàng</td>
                                <td><a target="_blank" href="{{BladeGeneral::GetImg(['avatar' => $project->sale_image,'data' => 'sale', 'time' => $project->updated_at])}}" title=""><img width="100" class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $project->sale_image,'data' => 'sale', 'time' => $project->updated_at])}}" alt="..."></a></td>
                                <td>
                                    @if($project->sale_image != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Nội dung bán hàng</td>
                                <td>{{$project->content_sale}}</td>
                                <td>
                                    @if($project->content_sale != null && $project->content_sale != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>File download</td>
                                <td>{{$project->sale_file}}</td>
                                <td>
                                    @if($project->sale_file != null && $project->content_sale != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>

                @if($project->step != 3)
                <div class="panel-form--footer">
                    <button data-id="{{$project->id}}" type="button" id="submit-project" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Hoàng thành</span></button>
                </div>
                @endif

            </section>
        </div>
    </div>
</div>


<script>
    $('#submit-project').click(function(event) {
        var data = {};
        $('.block-page-all').addClass('active');
        data['status_code'] = 'submit_project';
        data['id'] = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            url: '/api_dangtin',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                if (result.code == 300) {
                    toastr.error(result.error);
                    $('.block-page-all').removeClass('active');
                    return false
                }
                document.location.href = '/tao-dang-tin-b3/'+result.id;
            }
        });
    });
</script>
@endsection