<!-- BEGIN CORE PLUGINS -->
<script src="/public/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/public/assets/global/plugins/js.cookie.min.js"></script>
<script src="/public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="/public/assets/global/plugins/jquery.blockui.min.js"></script>
<script src="/public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/public/assets/global/plugins/moment.min.js"></script>
<script src="/public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"></script>
<script src="/public/assets/global/plugins/morris/morris.min.js"></script>
<script src="/public/assets/global/plugins/morris/raphael-min.js"></script>
<script src="/public/assets/global/plugins/counterup/jquery.waypoints.min.js"></script>
<script src="/public/assets/global/plugins/counterup/jquery.counterup.js"></script>
<script src="/public/assets/global/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="/public/assets/global/plugins/horizontal-timeline/horizontal-timeline.js"></script>
<script src="/public/assets/global/plugins/flot/jquery.flot.min.js"></script>
<script src="/public/assets/global/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="/public/assets/global/plugins/flot/jquery.flot.categories.min.js"></script>
<script src="/public/assets/global/plugins/jquery.sparkline.min.js"></script>

<script src="/public/assets/global/scripts/datatable.js"></script>
<script src="/public/assets/global/plugins/datatables/datatables.min.js"></script>
<script src="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>

<script src="/public/assets/global/plugins/toastr/toastr.min.js"></script>
<script src="/public/assets/global/plugins/tinymce/tinymce.min.js"></script>
<script src="/public/assets/global/plugins/select2-4.0.3/js/select2.full.min.js"></script>
<script src="/public/assets/global/plugins/select2-4.0.3/js/i18n/vi.js"></script>
<script src="/public/assets/global/plugins/dropzone/dropzone.min.js"></script>
<script src="/public/assets/global/plugins/bootbox/bootbox.js"></script>
<script src="/public/assets/global/plugins/lazyload/lazyload.min.js"></script>

<script src="/public/assets/global/plugins/jquery-nestable/jquery.nestable.js"></script>

<script src="/public/assets/global/plugins/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.js"></script>

<script src="/public/assets/global/plugins/muuri/scripts/vendor/web-animations-2.3.1.min.js"></script>
<script src="/public/assets/global/plugins/muuri/scripts/vendor/hammer-2.0.8.min.js"></script>
<script src="/public/assets/global/plugins/muuri/scripts/vendor/muuri-0.5.4.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="/public/assets/global/scripts/app.min.js"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="/public/assets/layouts/layout/scripts/layout.min.js"></script>
<script src="/public/assets/layouts/global/scripts/quick-sidebar.min.js"></script>
<script src="/public/assets/layouts/global/scripts/quick-nav.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<!--[ JQUERY ADMIN ]-->
<script src="/public/assets/global/plugins/jquery-validation-1.17.0/dist/jquery.validate.min.js"></script>
<script src="/public/assets/global/plugins/jquery-validation-1.17.0/dist/additional-methods.js"></script>
<script src="/public/js/script.js"></script>
<script src="/public/js/common_function.js"></script>