<table id="tbl-dataz" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
    <thead>
        <tr>
            <th class="text-uppercase">Tin đăng</th>
            <th class="text-uppercase">Hình</th>
            <th class="text-uppercase">Giá</th>
            <th class="text-uppercase">Ngày tạo</th>
            <th class="text-uppercase">Thao tác</th>
        </tr>
    </thead>
    <tbody class="load_seach">
        @foreach (@$item_projects as $project)
        <tr>
            <td>
                <p class="mb-0"><b>{{@$project->title}}</b></p>
                <p class="mb-0">Ngày cập nhật: {{BladeGeneral::dateTime($project->updated_at)}}</p>
            </td>

            <td><figure class="img"><img src="/public/img/upload/items/{{$project->avatar}}" alt=""></figure></td>

            <td>
                <span class="price_table">
                    @if($project->allprice_mark == 0)
                        Thỏa thuận
                    @else
                        @if($project->allprice_mark == "Thỏa thuận")
                        {{@$project->allprice_mark}}
                        @else
                        {{@$project->allprice_mark}}
                        @endif
                    @endif
                </span>
            </td>

            <td>
                <span class="d-inline-block"><span class="hidden">{{$project->created_at}}</span>{{date('d/m/Y', strtotime(@$project->created_at))}}</span>
            </td>

            @if($project->active == 4)
            <td>
                <a href="javascript:;" class="reset-btn btn-ctrl delete_thue_ban"  data-id="{{@$project->id}}"><i class="far fa-trash-alt"></i> Xóa tin</a>
                <a href="javascript:;" class="reset-btn btn-ctrl resum_thue_ban"  data-id="{{@$project->id}}"><i class="fas fa-undo-alt"></i> Phục hồi</a>
            </td>
            @elseif($project->active == 0)
            <td>
                @if($project->updated_at < BladeGeneral::afterday())
                <span data-id="{{$project->id}}" class="reset-btn btn-ctrl lammoi">Làm mới</span>
                @else
                <a target="_blank" href="/cho-thue/{{$project->slug}}" class="reset-btn btn-ctrl xuatban">Đang chạy</a>
                @endif
                <a href="javascript:;" class="reset-btn btn-ctrl delete_thue_ban"  data-id="{{@$project->id}}"><i class="far fa-trash-alt"></i> Xóa tin</a>
            </td>
            @else
            <td>
                <a href="/tao-tin-ban-b1/{{$project->id}}" class="reset-btn btn-ctrl"><i class="far fa-edit"></i>Sửa tin</a>
                <a href="javascript:;" class="reset-btn btn-ctrl delete_thue_ban"  data-id="{{@$project->id}}"><i class="far fa-trash-alt"></i> Xóa tin</a>
            </td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    var datatable = $('#tbl-dataz').DataTable({
        order: [[ 3, 'desc' ]],
        columnDefs: [
            { sortable: false, searchable: false, targets: [ 0,1,2,4 ] },
        ],
        displayStart: 0,
        displayLength: 5,
        "autoWidth": false
    });
    datatable.search('').draw();
</script>