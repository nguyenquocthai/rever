<select name="donvi" id="donvi" class="form-control">
    <option value="0">Thỏa thuận</option>
    <option value="1">Nghìn/tháng</option>
    <option value="2">Triệu/tháng</option>
    <option value="3">Nghìn/m2/tháng</option>
    <option value="4">Triệu/m2/tháng</option>
</select>

@if(isset($item_project->donvi))
<script>
	$('#donvi').val('{{@$item_project->donvi}}').trigger('change');
</script>
@endif