<div class="row">
    <div class="col-sm-3 btn_img_flat item_img_flat">
        <div class="item">
            <i class="fas fa-upload"></i>
            <input type="file" data-id="{{$id_flat}}" id="upload_img_flat">
        </div>
    </div>

    @if (count($img_flats) != 0)
        @foreach ($img_flats as $img_flat)
        @include('frontend.dangtin._itemimg_flats')
        @endforeach
    @else
    
    @endif
</div>