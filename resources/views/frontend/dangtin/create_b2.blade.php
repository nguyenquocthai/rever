<!DOCTYPE html>
<html>
    @include('backend.elements.head')
    @include('backend.elements.js')
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            @include('backend.elements.header')

                <!-- BEGIN CONTAINER -->
                <div class="page-container">
                    @include('backend.elements.sidebar')
                    <!-- BEGIN CONTENT -->
                    <div class="page-content-wrapper">

                        <!-- BEGIN CONTENT BODY -->
                        <div class="page-content store-dang-tin">
                            @include('frontend.dangtin._head')
                            @include('frontend.dangtin._step')
                            <div class="row">
                                <div class="col-sm-3 avatar_box">
                                    <label class="d-block caption_img">Hình đại diện</label>
                                    <div class="file_upload_box">
                                        <input id="avatar_project" type="file" class="file_image">
                                        <img class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $project->avatar,'data' => 'project', 'time' => $project->updated_at])}}" alt="...">
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <label class="d-block caption_img">Album dự án</label>
                                    <div class="load_album_project">
                                        <div class="row">
                                            <div class="col-sm-3 btn_album_project item_album_project">
                                                <form enctype="multipart/form-data" class="item">
                                                    <i class="fas fa-upload"></i>
                                                    <input name="album[]" type="file" multiple id="upload_album_project">
                                                </form>
                                            </div>
                                            @include('frontend.dangtin._albumproject')
                                        </div>
                                    </div>

                                    <div class="error_log">
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="portlet light bordered">
                                <div class="portlet-title flats_title">
                                    <div class="caption font-green-sharp">
                                        <i class="fas fa-images font-green-sharp"></i>
                                        <span class="caption-subject bold uppercase" ng-bind="detail_block_title">Mặt bằng các tầng</span>
                                    </div>

                                    <button class="btn_modal_flats" type="button"><i class="fas fa-plus"></i> Thêm tầng</button>
                                </div>
                                <div class="portlet-body form">
                                    <div class="load_album_flats">
                                        <div class="row">
                                            @if (count($album_flats) != 0)
                                                @foreach ($album_flats as $album_flat)
                                                @include('frontend.dangtin._oneflats')
                                                @endforeach
                                            @else
                                            
                                            @endif
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-green-sharp">
                                        <i class="icon-settings font-green-sharp"></i>
                                        <span class="caption-subject bold uppercase" ng-bind="detail_block_title">Chi tiết dự án</span>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form id="form_dangtin_b2" autocomplete="off">
                                        <input type="hidden" value="{{$project->id}}" id="key_edit_project">
                                        <div class="block">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Chủ đầu tư</label>
                                                        <div class="input">
                                                            <input value="{{$project->investor}}" type="text" name="investor" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Nhà Thầu</label>
                                                        <div class="input">
                                                            <input value="{{$project->contractors}}" type="text" name="contractors" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Quy mô dự án</label>
                                                        <div class="input">
                                                            <input value="{{$project->project_scale}}"" type="text" name="project_scale" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Số Tầng</label>
                                                        <div class="input">
                                                            <input value="{{$project->floors}}" type="text" name="floors" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Số tháp</label>
                                                        <div class="input">
                                                            <input value="{{$project->tower}}" type="text" name="tower" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Số căn hộ</label>
                                                        <div class="input">
                                                            <input value="{{$project->apartments}}" type="text" name="apartments" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4">
                                                    
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Kiến trúc và cảnh quan</label>
                                                        <div class="input">
                                                            <textarea rows="5" name="landscape_architecture" class="form-control">{{$project->landscape_architecture}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    
                                                    <div class="form-group">
                                                        <label>Giá bán</label>
                                                        <div class="input">
                                                            <div class="price_box">
                                                                <input value="{{$project->fromprice}}" type="text" name="fromprice" class="form-control">
                                                                 Đến 
                                                                <input value="{{$project->toprice}}" type="text" name="toprice" class="form-control">
                                                                <span class="donvi_mod">/m<sup>2</sup></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Giá thuê</label>
                                                        <div class="input">
                                                            <div class="price_box">
                                                                <input value="{{$project->thue_fromprice}}" type="text" name="thue_fromprice" class="form-control">
                                                                 Đến 
                                                                <input value="{{$project->thue_toprice}}" type="text" name="thue_toprice" class="form-control">
                                                                <span class="donvi_mod">/tháng</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption font-green-sharp">
                                                    <i class="icon-settings font-green-sharp"></i>
                                                    <span class="caption-subject bold uppercase">Thêm tổng quan</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <div class="block">

                                                    <div class="code-box-gid">
                                                        <section class="grid-demo content-store">
                                                            <div class="controls cf">
                                                                <div class="control search">
                                                                    <div class="control-icon">
                                                                        <i class="material-icons">&#xE8B6;</i>
                                                                    </div>
                                                                    <input class="control-field search-field form-control " type="text" placeholder="Search..." />
                                                                </div>
                                                                <div class="control filter">
                                                                    <div class="control-icon">
                                                                        <i class="material-icons">&#xE152;</i>
                                                                    </div>
                                                                    <div class="select-arrow">
                                                                        <i class="material-icons">&#xE313;</i>
                                                                    </div>
                                                                    <select class="control-field filter-field form-control">
                                                                        <option value="" selected>All</option>
                                                                        <option value="red">Red</option>
                                                                        <option value="blue">Blue</option>
                                                                        <option value="green">Green</option>
                                                                    </select>
                                                                </div>
                                                                <div class="control sort">
                                                                    <div class="control-icon">
                                                                        <i class="material-icons">&#xE164;</i>
                                                                    </div>
                                                                    <div class="select-arrow">
                                                                        <i class="material-icons">&#xE313;</i>
                                                                    </div>
                                                                    <select class="control-field sort-field form-control">
                                                                        <option value="order" selected>Drag</option>
                                                                        <option value="title">Title (drag disabled)</option>
                                                                        <option value="color">Color (drag disabled)</option>
                                                                    </select>
                                                                </div>
                                                                <div class="control layout">
                                                                    <div class="control-icon">
                                                                        <i class="material-icons">&#xE871;</i>
                                                                    </div>
                                                                    <div class="select-arrow">
                                                                        <i class="material-icons">&#xE313;</i>
                                                                    </div>
                                                                    <select class="control-field layout-field form-control">
                                                                        <option value="left-top" selected>Left Top</option>
                                                                        <option value="left-top-fillgaps">Left Top (fill gaps)</option>
                                                                        <option value="right-top">Right Top</option>
                                                                        <option value="right-top-fillgaps">Right Top (fill gaps)</option>
                                                                        <option value="left-bottom">Left Bottom</option>
                                                                        <option value="left-bottom-fillgaps">Left Bottom (fill gaps)</option>
                                                                        <option value="right-bottom">Right Bottom</option>
                                                                        <option value="right-bottom-fillgaps">Right Bottom (fill gaps)</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="grid load_tongquan">

                                                            </div>
                                                            <div class="grid-footer1">
                                                                <button class="btn btn-primary themtq" type="button"><i class="fas fa-search-plus"></i> Load</button>
                                                                <button data-toggle="modal" data-target="#modal_newtq" class="btn btn-primary" type="button"><i class="fas fa-plus"></i> New</button>
                                                            </div>
                                                            <div class="grid-footer">
                                                                <button class="add-more-items btn btn-primary"><i class="fas fa-plus"></i> Add more items</button>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption font-green-sharp">
                                                    <i class="icon-settings font-green-sharp"></i>
                                                    <span class="caption-subject bold uppercase">Thêm tiện ích dự án</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <div class="block">
                                                    <div class="code-box-gid">
                                                        <section class="grid-demo2 content-store">
                                                            <div class="controls cf">
                                                                <div class="control search">
                                                                    <div class="control-icon">
                                                                        <i class="material-icons">&#xE8B6;</i>
                                                                    </div>
                                                                    <input class="control-field search-field2 form-control " type="text" placeholder="Search..." />
                                                                </div>
                                                                <div class="control filter">
                                                                    <div class="control-icon">
                                                                        <i class="material-icons">&#xE152;</i>
                                                                    </div>
                                                                    <div class="select-arrow">
                                                                        <i class="material-icons">&#xE313;</i>
                                                                    </div>
                                                                    <select class="control-field filter2-field2 form-control">
                                                                        <option value="" selected>All</option>
                                                                        <option value="red">Red</option>
                                                                        <option value="blue">Blue</option>
                                                                        <option value="green">Green</option>
                                                                    </select>
                                                                </div>
                                                                <div class="control sort">
                                                                    <div class="control-icon">
                                                                        <i class="material-icons">&#xE164;</i>
                                                                    </div>
                                                                    <div class="select-arrow">
                                                                        <i class="material-icons">&#xE313;</i>
                                                                    </div>
                                                                    <select class="control-field sort-field2 form-control">
                                                                        <option value="order" selected>Drag</option>
                                                                        <option value="title">Title (drag disabled)</option>
                                                                        <option value="color">Color (drag disabled)</option>
                                                                    </select>
                                                                </div>
                                                                <div class="control layout">
                                                                    <div class="control-icon">
                                                                        <i class="material-icons">&#xE871;</i>
                                                                    </div>
                                                                    <div class="select-arrow">
                                                                        <i class="material-icons">&#xE313;</i>
                                                                    </div>
                                                                    <select class="control-field layout-field2 form-control">
                                                                        <option value="left-top" selected>Left Top</option>
                                                                        <option value="left-top-fillgaps">Left Top (fill gaps)</option>
                                                                        <option value="right-top">Right Top</option>
                                                                        <option value="right-top-fillgaps">Right Top (fill gaps)</option>
                                                                        <option value="left-bottom">Left Bottom</option>
                                                                        <option value="left-bottom-fillgaps">Left Bottom (fill gaps)</option>
                                                                        <option value="right-bottom">Right Bottom</option>
                                                                        <option value="right-bottom-fillgaps">Right Bottom (fill gaps)</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="grid2 load_tienich">

                                                            </div>
                                                            <div class="grid-footer1">
                                                                <button class="btn btn-primary themtienich" type="button"><i class="fas fa-search-plus"></i> Load</button>
                                                                <button data-toggle="modal" data-target="#modal_newti" class="btn btn-primary" type="button"><i class="fas fa-plus"></i> New</button>
                                                            </div>
                                                            <div class="grid-footer">
                                                                <button class="add-more-items2 btn btn-primary"><i class="fas fa-plus"></i> Add more items</button>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption font-green-sharp">
                                                    <i class="icon-settings font-green-sharp"></i>
                                                    <span class="caption-subject bold uppercase">Chương trình bán hàng</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">

                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="sale-left">
                                                            <label class="d-block caption_img">Hình bán hàng</label>
                                                            <div class="file_upload_box">
                                                                <input id="sale_image" type="file" class="file_image">
                                                                <img class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $project->sale_image,'data' => 'sale', 'time' => $project->updated_at])}}" alt="...">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-9">
                                                        <div class="sale-right">
                                                            
                                                            <div class="form-group">
                                                                <label>Link google drive</label>
                                                                <input name="sale_file" class="form-control" type="text" value="{{$project->sale_file}}">
                                                            </div>
                                                           
                                                            <textarea placeholder="Nội dung" name="content_sale" id="content_sale" rows="10" class="form-control">{{$project->content_sale}}</textarea>
                                                        </div>
                                                        <script>
                                                            $('.mark-inputfile').click(function(event) {
                                                                $( ".file-ban-hang" ).trigger( "click" );
                                                            });

                                                            $('.file-ban-hang').change(function() {
                                                                var filename = $(this).val();
                                                                if (filename == "") {
                                                                    $('.load_file_name').html('Tải file lên');
                                                                } else {
                                                                    $('.load_file_name').html(filename);
                                                                }
                                                            });
                                                        </script>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="panel-form--footer">
                                            <button type id="submit-project" class="btn green"><span class="icon"><i class="fas fa-arrow-right"></i></span><span class="text text-uppercase"> Tiếp tục</span></button>
                                        </div>

                                    </form>

                                </div>

                            </div>

                            <div class="modal fade" id="modal_flats" tabindex="-1" role="dialog" aria-labelledby="modal_flatsLabel">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modal_flatsLabel">Thêm mặt bằng tầng</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form id="form_flats">
                                                <div class="form-group">
                                                    <label class="control-label">Tên Tầng</label>
                                                    <input type="text" class="form-control title_flats">
                                                </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="control-label">Hình đại diện</label>
                                                    <div class="file_upload_box">
                                                        <input type="file" class="file_image">
                                                        <img class="image_review" src="/public/img/no-image.png" alt="...">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                            <button type="button" class="btn btn-primary btn_add_flats">Thêm</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="modal_edit_flats" tabindex="-1" role="dialog" aria-labelledby="modal_edit_flatsLabel">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modal_edit_flatsLabel">Sửa mặt bằng tầng</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form id="form_edit_flats">
                                                
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                            <button type="button" class="btn btn-primary btn_edit_flats">Sửa</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="img_flat" tabindex="-1" role="dialog" aria-labelledby="img_flatLabel">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="img_flatLabel">Thêm hình</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="load_img_flat">
                                                
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="modal_newtq" role="dialog" aria-labelledby="modal_newtqLabel">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modal_newtqLabel">Thêm tổng quan</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form id="form_newtq" autocomplete="off">
                                                <input type="hidden" name="id" value="0">
                                                <div class="form-group">
                                                    <label class="control-label">Tên</label>
                                                    <input name="title" type="text" class="form-control">
                                                    <input type="hidden" name="title_en" value="">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Nội dung</label>
                                                    <input name="value" type="text" class="form-control">
                                                    <input type="hidden" name="value_en" value="">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Icon</label>
                                                    <div class="input-group iconpicker-container">
                                                        <input name="data" readonly id="data_icon" data-placement="bottomRight" class="form-control icp icp-auto iconpicker-element iconpicker-input" value="fas fa-archive" type="text">
                                                        <span class="input-group-addon load_icon_tq"><i class="fas fa-archive"></i></span>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                            <button type="button" class="btn btn-primary btn_newtq">Thêm</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="modal_newti" role="dialog" aria-labelledby="modal_newtiLabel">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modal_newtiLabel">Thêm tổng quan</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form id="form_newti" autocomplete="off">
                                                <input type="hidden" name="id" value="0">
                                                <div class="form-group">
                                                    <label class="control-label">Tên</label>
                                                    <input name="title" type="text" class="form-control">
                                                    <input type="hidden" name="title_en" value="">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Icon</label>
                                                    <div class="input-group iconpicker-container">
                                                        <input name="data" readonly id="data_icon" data-placement="bottomRight" class="form-control icp icp-auto iconpicker-element iconpicker-input" value="fas fa-archive" type="text">
                                                        <span class="input-group-addon load_icon_ti"><i class="fas fa-archive"></i></span>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                            <button type="button" class="btn btn-primary btn_newti">Thêm</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <script src="/public/js/cmuuri.js"></script>
                            @include('frontend.dangtin._food')
                            <script>

                                $('#date_delivery').datepicker({
                                    autoClose:true,
                                    dateFormat: 'dd/mm/yyyy',
                                    language: 'en'
                                })

                                $('#date_delivery').val('{{$project->date_delivery}}');

                                var idproject = '{{$project->id}}';
                                var form_flats = $('#form_flats').html();
                                var form_newtq = $('#form_newtq').html();
                                var form_newti = $('#form_newti').html();

                                $('#avatar_project').change(function(event) {
                                    var file = this.files[0];
                                    if (typeof(file) === 'undefined') {
                                        return false;
                                    }
                                    var fileType = file["type"];
                                    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
                                    if ($.inArray(fileType, ValidImageTypes) < 0) {
                                        toastr.error('Hình ảnh không hợp lệ', null, {timeOut: 4000});
                                        return false;
                                    }
                                    if (file.size > 1000000) {
                                        toastr.error('Dung lượng hình phải bé hơn 1MB', null, {timeOut: 4000});
                                        return false;
                                    }

                                    var images = $(this).closest('.file_upload_box').find('.image_review');
                                    if (this.files && this.files[0]) {
                                        var reader = new FileReader();
                                        reader.onload = function(e) {
                                            images.attr('src', e.target.result);
                                        }
                                        reader.readAsDataURL(this.files[0]);
                                    }

                                    var fd = new FormData();
                                    fd.append("status_code", "change_avatar");
                                    fd.append("id", idproject);
                                    if (typeof(file) !== 'undefined') {
                                        fd.append("avatar", file);
                                    }

                                    showPageLoading();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/api_dangtin',
                                        data: fd,
                                        dataType: 'json',
                                        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                                        processData: false, // NEEDED, DON'T OMIT THIS
                                        error: function(){
                                            hidePageLoading();
                                            toastr.error(result.error);
                                        },
                                        success: function(result) {
                                            hidePageLoading();
                                        }
                                    });
                                });

                                $('#sale_image').change(function(event) {
                                    var file = this.files[0];
                                    if (typeof(file) === 'undefined') {
                                        return false;
                                    }
                                    var fileType = file["type"];
                                    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
                                    if ($.inArray(fileType, ValidImageTypes) < 0) {
                                        toastr.error('Hình ảnh không hợp lệ', null, {timeOut: 4000});
                                        return false;
                                    }
                                    if (file.size > 1000000) {
                                        toastr.error('Dung lượng hình phải bé hơn 1MB', null, {timeOut: 4000});
                                        return false;
                                    }

                                    var images = $(this).closest('.file_upload_box').find('.image_review');
                                    if (this.files && this.files[0]) {
                                        var reader = new FileReader();
                                        reader.onload = function(e) {
                                            images.attr('src', e.target.result);
                                        }
                                        reader.readAsDataURL(this.files[0]);
                                    }

                                    var fd = new FormData();
                                    fd.append("status_code", "sale_image");
                                    fd.append("id", idproject);
                                    if (typeof(file) !== 'undefined') {
                                        fd.append("avatar", file);
                                    }

                                    showPageLoading();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/api_dangtin',
                                        data: fd,
                                        dataType: 'json',
                                        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                                        processData: false, // NEEDED, DON'T OMIT THIS
                                        error: function(){
                                            hidePageLoading();
                                            toastr.error(result.error);
                                        },
                                        success: function(result) {
                                            hidePageLoading();
                                        }
                                    });
                                });

                                // $('#sale_file').change(function(event) {
                                //     var file = this.files[0];
                                //     if (typeof(file) === 'undefined') {
                                //         return false;
                                //     }
                                //     if (file.size > 10000000) {
                                //         toastr.error('Dung lượng hình phải bé hơn 10MB', null, {timeOut: 4000});
                                //         return false;
                                //     }

                                //     var fd = new FormData();
                                //     fd.append("status_code", "sale_file");
                                //     fd.append("id", idproject);
                                //     if (typeof(file) !== 'undefined') {
                                //         fd.append("avatar", file);
                                //     }

                                //     showPageLoading();
                                //     $.ajax({
                                //         type: 'POST',
                                //         url: '/api_dangtin',
                                //         data: fd,
                                //         dataType: 'json',
                                //         contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                                //         processData: false, // NEEDED, DON'T OMIT THIS
                                //         error: function(){
                                //             hidePageLoading();
                                //             toastr.error(result.error);
                                //         },
                                //         success: function(result) {
                                //             hidePageLoading();
                                //         }
                                //     });
                                // });

                                $('#upload_album_project').change(function(event) {
                                    var files = $(this).get(0).files;
                                    var size = 0;
                                    for (i = 0; i < files.length; i++)
                                    {
                                        size = size + files[i].size;
                                    }

                                    if (size == 0) {
                                        return false;
                                    }

                                    if (size >= 10000000) {
                                        toastr.error('<p>Tổng dung lượng: '+bytesToSize(size)+'</p><p>Tối đa: '+bytesToSize(10000000)+'</p>');
                                        return false;
                                    }

                                    var fd = new FormData($(this).parents('form')[0]);
                                    fd.append("status_code", "upload_album_project");
                                    fd.append("id", idproject);

                                    showPageLoading();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/api_dangtin',
                                        data: fd,
                                        dataType: 'json',
                                        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                                        processData: false, // NEEDED, DON'T OMIT THIS
                                        error: function(){
                                            hidePageLoading();
                                            toastr.error(result.error);
                                        },
                                        success: function(result) {
                                            if (result.code == 300) {
                                                toastr.error(result.error);
                                                hidePageLoading();
                                                return false;
                                            }
                                            $('.btn_album_project').after(result.value);
                                            if (result.error_log != "") {
                                                $('.error_log').html(result.error_log);
                                            } else {
                                                $('.error_log').html("");
                                            }
                                            hidePageLoading();
                                        }
                                    });
                                });

                                $(document).on('click', '.delete_album_project', function(event) {
                                    var here = $(this).closest('.item_album_project');
                                    var data = {};
                                    data['status_code'] = 'delete_album_project';
                                    data['id'] = $(this).attr('data-id');
                                    showPageLoading();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/api_dangtin',
                                        data: data,
                                        dataType: 'json',
                                        error: function(){
                                            hidePageLoading();
                                            toastr.error(result.error);
                                        },
                                        success: function(result) {
                                            if (result.code == 300) {
                                                toastr.error(result.error);
                                                hidePageLoading();
                                                return false
                                            }
                                            here.remove();
                                            hidePageLoading();
                                        }
                                    });
                                });

                                $('.btn_modal_flats').click(function(event) {
                                    $('#modal_flats').modal('show')
                                });

                                $('.btn_add_flats').click(function(event) {
                                    var fd = new FormData();
                                    var file = $('#form_flats .file_image').get(0).files[0];

                                    fd.append("status_code", "add_flats");
                                    fd.append("id", idproject);
                                    fd.append("title", $('#form_flats .title_flats').val());
                                    if (typeof(file) !== 'undefined') {
                                        fd.append("avatar", file);
                                    }

                                    showPageLoading();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/api_dangtin',
                                        data: fd,
                                        dataType: 'json',
                                        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                                        processData: false, // NEEDED, DON'T OMIT THIS
                                        error: function(){
                                            hidePageLoading();
                                            toastr.error(result.error);
                                        },
                                        success: function(result) {
                                            if (result.code == 300) {
                                                toastr.error(result.error);
                                                hidePageLoading();
                                                return false
                                            }
                                            $('.load_album_flats .row').prepend(result.value);
                                            $('#modal_flats').modal('hide');
                                            $('#form_flats').html(form_flats);
                                            hidePageLoading();
                                        }
                                    });
                                });

                                $('.btn_edit_flats').click(function(event) {
                                    var fd = new FormData();
                                    var file = $('#form_edit_flats .file_image').get(0).files[0];

                                    fd.append("status_code", "edit_flats");
                                    fd.append("id", $('#form_edit_flats .id_flats').val());
                                    fd.append("title", $('#form_edit_flats .title_flats').val());
                                    if (typeof(file) !== 'undefined') {
                                        fd.append("avatar", file);
                                    }

                                    showPageLoading();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/api_dangtin',
                                        data: fd,
                                        dataType: 'json',
                                        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                                        processData: false, // NEEDED, DON'T OMIT THIS
                                        error: function(){
                                            hidePageLoading();
                                            toastr.error(result.error);
                                        },
                                        success: function(result) {
                                            if (result.code == 300) {
                                                toastr.error(result.error);
                                                hidePageLoading();
                                                return false
                                            }
                                            $( '.item_album_flats[data-id="'+result.id+'"]' ).replaceWith(result.value);
                                            $('#modal_edit_flats').modal('hide');
                                            hidePageLoading();
                                        }
                                    });
                                });

                                $(document).on('click', '.modal_edit_flat', function(event) {
                                    var data = {};
                                    data['status_code'] = 'modal_edit_flat';
                                    data['id'] = $(this).attr('data-id');

                                    showPageLoading();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/api_dangtin',
                                        data: data,
                                        dataType: 'json',
                                        error: function(){
                                            hidePageLoading();
                                            toastr.error(result.error);
                                        },
                                        success: function(result) {
                                            if (result.code == 300) {
                                                toastr.error(result.error);
                                                hidePageLoading();
                                                return false
                                            }
                                            $('#form_edit_flats').html(result.value);
                                            $('#modal_edit_flats').modal('show');
                                            hidePageLoading();
                                        }
                                    });
                                });

                                $(document).on('click', '.img_flat', function(event) {
                                    var data = {};
                                    data['status_code'] = 'img_flat';
                                    data['id'] = $(this).attr('data-id');

                                    showPageLoading();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/api_dangtin',
                                        data: data,
                                        dataType: 'json',
                                        error: function(){
                                            hidePageLoading();
                                            toastr.error(result.error);
                                        },
                                        success: function(result) {
                                            if (result.code == 300) {
                                                toastr.error(result.error);
                                                hidePageLoading();
                                                return false
                                            }
                                            $('.load_img_flat').html(result.value);
                                            $('#img_flat').modal('show');
                                            hidePageLoading();
                                        }
                                    });
                                });

                                $(document).on('change', '#upload_img_flat', function(event) {
                                    var file = this.files[0];
                                    if (typeof(file) === 'undefined') {
                                        return false;
                                    }
                                    var fileType = file["type"];
                                    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
                                    if ($.inArray(fileType, ValidImageTypes) < 0) {
                                        toastr.error('Hình ảnh không hợp lệ', null, {timeOut: 4000});
                                        return false;
                                    }
                                    if (file.size > 1000000) {
                                        toastr.error('Dung lượng hình phải bé hơn 1MB', null, {timeOut: 4000});
                                        return false;
                                    }

                                    var fd = new FormData();
                                    var id = $(this).attr('data-id');
                                    fd.append("status_code", "upload_img_flat");
                                    fd.append("id", id);

                                    if (typeof(file) !== 'undefined') {
                                        fd.append("avatar", file);
                                        fd.append("size", file.size);
                                    }

                                    showPageLoading();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/api_dangtin',
                                        data: fd,
                                        dataType: 'json',
                                        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                                        processData: false, // NEEDED, DON'T OMIT THIS
                                        error: function(){
                                            hidePageLoading();
                                            toastr.error(result.error);
                                        },
                                        success: function(result) {
                                            if (result.code == 300) {
                                                toastr.error(result.error);
                                                hidePageLoading();
                                            }
                                            $('.btn_img_flat').after(result.value);
                                            $('.count_img_flats[data-id="'+id+'"]').html(result.count);
                                            hidePageLoading();
                                        }
                                    });
                                });

                                $(document).on('click', '.delete_img_flat', function(event) {
                                    var here = $(this).closest('.item_img_flat');
                                    var data = {};
                                    data['status_code'] = 'delete_img_flat';
                                    data['id'] = $(this).attr('data-id');
                                    showPageLoading();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/api_dangtin',
                                        data: data,
                                        dataType: 'json',
                                        error: function(){
                                            hidePageLoading();
                                            toastr.error(result.error);
                                        },
                                        success: function(result) {
                                            if (result.code == 300) {
                                                toastr.error(result.error);
                                                hidePageLoading();
                                                return false
                                            }
                                            here.remove();
                                            $('.count_img_flats[data-id="'+result.id+'"]').html(result.count);
                                            hidePageLoading();
                                        }
                                    });
                                });

                                $(document).on('click', '.delete_flat', function(event) {
                                    if (confirm("Xác nhận xóa ?")) {
                                        var here = $(this).closest('.item_album_flats');
                                        var data = {};
                                        data['status_code'] = 'delete_album_flats';
                                        data['id'] = $(this).attr('data-id');
                                        showPageLoading();
                                        $.ajax({
                                            type: 'POST',
                                            url: '/api_dangtin',
                                            data: data,
                                            dataType: 'json',
                                            error: function(){
                                                hidePageLoading();
                                                toastr.error(result.error);
                                            },
                                            success: function(result) {
                                                if (result.code == 300) {
                                                    toastr.error(result.error);
                                                    hidePageLoading();
                                                    return false
                                                }
                                                here.remove();
                                                hidePageLoading();
                                            }
                                        });
                                    }
                                    return false;
                                });

                                var form_dangtin_b2 = $('#form_dangtin_b2').validate({
                                    highlight: function(element, errorClass, validClass) {
                                        $(element).removeClass(errorClass);
                                        $(element).addClass("error_input");
                                        $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
                                    },
                                    unhighlight: function (element, errorClass, validClass) {
                                        $(element).removeClass("error_input");
                                        $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
                                    },
                                    rules: {
                                        tower:{
                                            digits: true
                                        },
                                        apartments:{
                                            digits: true
                                        },
                                        project_scale:{
                                            digits: true
                                        },
                                        floors:{
                                            digits: true
                                        },
                                        fromprice:{
                                            digits: true,
                                            //required: true
                                        },
                                        toprice:{
                                            digits: true,
                                            //required: true
                                        },

                                        thue_fromprice:{
                                            digits: true,
                                            //required: true
                                        },
                                        thue_toprice:{
                                            digits: true,
                                            //required: true
                                        },

                                        date_delivery:{
                                            australianDate : true
                                        },
                                    },
                                    messages: {
                                        tower:{
                                            digits: 'Chỉ nhập số.'
                                        },
                                        apartments:{
                                            digits: 'Chỉ nhập số.'
                                        },
                                        project_scale:{
                                            digits: 'Chỉ nhập số.'
                                        },
                                        floors:{
                                            digits: 'Chỉ nhập số.'
                                        },
                                        fromprice:{
                                            digits: 'Chỉ nhập số.',
                                            //required: 'false'
                                        },
                                        toprice:{
                                            digits: 'Chỉ nhập số.',
                                            //required: 'false'
                                        },
                                        thue_fromprice:{
                                            digits: 'Chỉ nhập số.',
                                            //required: 'false'
                                        },
                                        thue_toprice:{
                                            digits: 'Chỉ nhập số.',
                                            //required: 'false'
                                        },
                                        date_delivery:{
                                            australianDate : 'Sai định dạng ngày tháng.'
                                        },
                                    },
                                    submitHandler: function (form) {
                                        // get tong quan
                                        var tongquans = [];
                                        $('.load_tongquan .card').each(function(index, el) {
                                            var vitri = $(this).find('.card-id').html();
                                            var id_tq = $(this).find('.card-id_tq').html();
                                            var data = $(this).find('.card-id_data i').attr('class');
                                            var title = $(this).find('.card-title').html();
                                            var title_en = $(this).find('.card-title_en').html();
                                            var value = $(this).find('.card-value').html();
                                            var value_en = $(this).find('.card-value_en').html();

                                            var item_tq = {
                                                vitri:vitri,
                                                id_tq:id_tq,
                                                data:data,
                                                title:title,
                                                title_en:title_en,
                                                value:value,
                                                value_en:value_en
                                            };
                                            tongquans.push(item_tq);
                                        });
                                        var jsontg = JSON.stringify(tongquans);

                                        // get tien ich
                                        var tienichs = [];
                                        $('.load_tienich .card').each(function(index, el) {
                                            var vitri = $(this).find('.card-id').html();
                                            var id_tq = $(this).find('.card-id_tq').html();
                                            var data = $(this).find('.card-id_data i').attr('class');
                                            var title = $(this).find('.card-title').html();
                                            var title_en = $(this).find('.card-title_en').html();

                                            var item_ti = {
                                                vitri:vitri,
                                                id_tq:id_tq,
                                                data:data,
                                                title:title,
                                                title_en:title_en,
                                            };
                                            tienichs.push(item_ti);
                                        });
                                        var jsonti = JSON.stringify(tienichs);

                                        var data = {};
                                        $("#form_dangtin_b2").serializeArray().map(function(x){data[x.name] = x.value;});
                                        data['status_code'] = 'dangtin_b2';
                                        data['id'] = idproject;
                                        data['tongquans'] = jsontg;
                                        data['tienichs'] = jsonti;

                                        showPageLoading();
                                        $.ajax({
                                            type: 'POST',
                                            url: '/api_dangtin',
                                            data: data,
                                            dataType: 'json',
                                            error: function(){
                                                hidePageLoading();
                                                toastr.error(result.error);
                                            },
                                            success: function(result) {
                                                if (result.code == 300) {
                                                    toastr.error(result.error);
                                                    hidePageLoading();
                                                    return false
                                                }
                                                document.location.href = '/tao-dang-tin-b3/'+result.id;
                                            }
                                        });
                                        return false;
                                    }
                                });

                                $('.btn_newtq').click(function(event) {
                                    var list_ids = [];
                                    var value = {};
                                    $("#form_newtq").serializeArray().map(function(x){value[x.name] = x.value;});
                                    if (value['title'] == "") {
                                        toastr.error('Bạn chưa nhập tên tổng quan.');
                                        return false;
                                    } else if(value['value'] == ""){
                                        toastr.error('Bạn chưa nhập nội dung tổng quan.');
                                        return false;
                                    } else if(value['data'] == ""){
                                        toastr.error('Bạn chưa chọn icon tổng quan.');
                                        return false;
                                    }
                                    list_ids.push(value);
                                    list_tongquan(list_ids);

                                    $('#form_newtq input[name="title"]').val('');
                                    $('#form_newtq input[name="value"]').val('');
                                    $('#form_newtq input[name="data"]').val('');
                                    $('#modal_newtq').modal('hide');
                                });

                                $('.btn_newti').click(function(event) {
                                    var list_ids = [];
                                    var value = {};
                                    $("#form_newti").serializeArray().map(function(x){value[x.name] = x.value;});
                                    if (value['title'] == "") {
                                        toastr.error('Bạn chưa nhập tên tiện ích.');
                                        return false;
                                    } else if(value['data'] == ""){
                                        toastr.error('Bạn chưa chọn icon tiện ích.');
                                        return false;
                                    }
                                    list_ids.push(value);
                                    list_tienich(list_ids);

                                    $('#form_newti input[name="title"]').val('');
                                    $('#form_newti input[name="data"]').val('');
                                    $('#modal_newti').modal('hide');
                                });

                                $(document).on('click', '.clear_error', function(event) {
                                    $(this).closest('.error_log').html("");
                                });

                            </script>
                        </div>
                        <!-- END CONTENT BODY -->
                        
                    </div>
                    <!-- END CONTENTx -->

                </div>
                <!-- END CONTAINER -->

            @include('backend.elements.footer')
        </div>
    </body>
</html>