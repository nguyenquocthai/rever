<link href="/public/css/frontend/cmuuri.css" rel="stylesheet">

<link href="/public/assets/global/plugins/fontawesome-iconpicker/dist/css/fontawesome-iconpicker.min.css" rel="stylesheet">

<!-- muuri -->
<script src="/public/assets/global/plugins/muuri/scripts/vendor/web-animations-2.3.1.min.js"></script>
<script src="/public/assets/global/plugins/muuri/scripts/vendor/hammer-2.0.8.min.js"></script>
<script src="/public/assets/global/plugins/muuri/scripts/vendor/muuri-0.5.4.js"></script>
<script src="/public/assets/global/plugins/dropzone/dropzone.min.js"></script>
<script src="/public/assets/global/plugins/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.js"></script>

<script src="/public/assets/global/plugins/jquery-validation-1.17.0/dist/jquery.validate.min.js"></script>
<script src="/public/assets/global/plugins/jquery-validation-1.17.0/dist/additional-methods.js"></script>