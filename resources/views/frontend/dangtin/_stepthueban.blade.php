@if($item_project->step == 1)
<div class="container-fluid">
    <br /><br />
    <ul class="list-unstyled multi-steps">
        <li class="{{@$b1}} ready"><a href="/tao-tin-thue-b1/{{$item_project->id}}" title="">Khởi tạo</a></li>
        <li class="{{@$b2}}"><a href="/tao-tin-thue-b2/{{$item_project->id}}" title="">Chi tiết</a></li>
        <li class="{{@$b3}}"><a href="/tao-tin-thue-b3/{{$item_project->id}}" title="">Hoàn thành</a></li>
    </ul>
</div>
@endif

@if($item_project->step == 2)
<div class="container-fluid">
    <br /><br />
    <ul class="list-unstyled multi-steps">
        <li class="{{@$b1}} ready"><a href="/tao-tin-thue-b1/{{$item_project->id}}" title="">Khởi tạo</a></li>
        <li class="{{@$b2}} ready"><a href="/tao-tin-thue-b2/{{$item_project->id}}" title="">Chi tiết</a></li>
        <li class="{{@$b13}}"><a href="/tao-tin-thue-b3/{{$item_project->id}}" title="">Hoàn thành</a></li>
    </ul>
</div>
@endif

@if($item_project->step == 3)
<div class="container-fluid">
    <br /><br />
    <ul class="list-unstyled multi-steps">
        <li class="{{@$b1}} ready"><a href="/tao-tin-thue-b1/{{$item_project->id}}" title="">Khởi tạo</a></li>
        <li class="{{@$b2}} ready"><a href="/tao-tin-thue-b2/{{$item_project->id}}" title="">Chi tiết</a></li>
        <li class="{{@$b3}} ready"><a href="/tao-tin-thue-b3/{{$item_project->id}}" title="">Hoàn thành</a></li>
    </ul>
</div>
@endif