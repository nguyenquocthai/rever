<input type="hidden" name="id" class="id_flats" value="{{$album_flat->id}}">
<div class="form-group">
    <label class="control-label">Tên Tầng</label>
    <input type="text" class="form-control title_flats" value="{{$album_flat->title}}">
</div>
<div class="form-group">
    <label for="message-text" class="control-label">Hình đại diện</label>
    <div class="file_upload_box">
        <input type="file" class="file_image">
        <img class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $album_flat->avatar,'data' => 'flat', 'time' => $album_flat->updated_at])}}" alt="...">
    </div>
</div>