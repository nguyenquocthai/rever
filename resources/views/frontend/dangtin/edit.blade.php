@extends('frontend.layouts.application')
@section('content')
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCQiaMU8DTCCDi90sT1ApqUYM737YYXymU&libraries=geometry,places" async></script>
<link href="/public/css/frondend/cmuuri.css" rel="stylesheet">
<link href="/public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="/public/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- muuri -->
<script src="/public/assets/global/plugins/muuri/scripts/vendor/web-animations-2.3.1.min.js"></script>
<script src="/public/assets/global/plugins/muuri/scripts/vendor/hammer-2.0.8.min.js"></script>
<script src="/public/assets/global/plugins/muuri/scripts/vendor/muuri-0.5.4.js"></script>
<!-- End muuri -->
<!--/head-block-->
    <div class="content-index bg-gray store-dang-tin">
        <input type="hidden" id="key_edit_project" value="{{$id_project}}">
        <div class="container-fluid t-video-rp project-video without-video"></div>

        <div class="container">
            <div class="bg-white panel-wrap my-3 my-md-5">
                @include('frontend.elements.sidebar_dangtin')

                <section class="panel-content">
                    <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>Bảng thông tin</button>

                    <h2 class="title text-uppercase">Đăng tin dự án <small class="d-block">Quý khách nhập thông tin nhà đất  cần bán hoặc cho thuê vào các mục dưới đây</small></h2>

                    <!-- <form id="form-create-project" action="" class="panel-form"> -->
                        <div class="block">
                            <div class="panel-form__row">
                                <label class="text mb-md-0">Tiêu đề <span class="cl_red">(*)</span></label>
                                <div class="input">
                                    <input type="text" name="title" value="{{ $project->title}}" class="form-control">
                                </div>
                            </div>
                            <div class="panel-form__row">
                                <label class="text mb-md-0">Địa Chỉ </label>
                                <div class="input">
                                    <input type="text" name="address" value="{{ $project->address}}" class="form-control">
                                </div>
                            </div>

                            <div class="panel-form__row toadobando_box">
                                <label class="text mb-md-0">Bản đồ </label>
                                <div class="input">
                                    <div class="input-group"> <input value="{{ $project->map }}" readonly name="map" id="map" type="text" class="form-control toadobando"> <span class="input-group-btn"> <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modalMap">Chọn vị trí</button> </span> </div>
                                </div>
                            </div>
                        </div>

                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="far fa-calendar-alt"></i>Lịch đăng tin</span></h4>

                            <div class="row row3">
                                <div class="col-12 col-md-4 col-xl-3 item">
                                    <label class="d-block">Loại dự án</label>
                                    <label class="select-box form-control">
                                        <select name="category_id" id="category_id">
                                            @foreach($categories as $category)
                                            <option value="{{$category->id}}" {{ $category->id == $project->category_id ? 'selected' : '' }} >{{$category->title}}</option>
                                            @endforeach
                                        </select>
                                        <i class="fa fa-angle-down"></i>
                                    </label>
                                </div>
                                <div class="col-12 col-md-4 col-xl-3 item">
                                    <label class="d-block">Ngày bắt đầu</label>
                                    <label class="select-box" style="background: #f5f5f5">
                                        <!-- <select name="" id="">
                                            <option value="">15/05/2015</option>
                                        </select>
                                        <i class="fa fa-angle-down"></i> -->
                                        <input type="text" name="start_day" value="{{ $project-> start_day}}" class="form-control">
                                    </label>
                                </div>
                                <div class="col-12 col-md-4 col-xl-3 item">
                                    <label class="d-block">Ngày kết thúc</label>
                                    <label class="select-box" style="background: #f5f5f5">
                                        <!-- <select name="" id="">
                                            <option value="">15/07/2015</option>
                                        </select>
                                        <i class="fa fa-angle-down"></i> -->
                                        <input type="text" name="end_day" value="{{ $project-> end_day}}" class="form-control">
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="fa fa-info-circle"></i>Giới thiệu</span></h4>

                            <div class="panel-form__row">
                                <label class="text mb-md-0">Nội dung tin đăng <span class="d-block cl_red">(*) Tối đa 3000 kí tự</span></label>
                                <div class="input">
                                    <textarea name="short_content" id="short_content" rows="3" class="form-control">{{ $project->short_content }}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="block border-top">
                            <div class="panel-form__row">
                                <label class="text mb-md-0">Hình ảnh <span class="d-block cl_red">(*) Tối đa 16 ảnh. Mỗi ảnh tối đa 2MB</span></label>
                                <div class="input d-flex flex-wrap img-upload__text">
                                    <!-- <button class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Tải ảnh lên</span></button> -->
                                    <p class="description">Lựa chọn những hình ảnh phù hợp với nội dung bài đăng của bạn để đạt được hiệu quả tốt nhất.</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                @if($project->avatar != null)
                                    <img src="/public/img/upload/projects/{{$project->avatar}}" alt="">
                                @endif
                                </div>
                                <div class="col-sm-9">
                                    <div class="slick-cus panel-slider js-slider4 without-arrows">
                                        @foreach($project->project_album as $album)
                                        <div class="item div-album" data-id="{{$album->id}}">
                                            <figure class="img">
                                                <img src="/public/img/upload/project_albums/{{$album->name}}" alt="">
                                            </figure>
                                            <button class="remove-album"><i class="far fa-trash-alt"></i> xóa</button>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="d-block caption_img">Hình đại diện</label>
                                    <form class="dropzone" id="avatar-project" action="/upload">
                                        <div class="dz-message "><i class="far fa-image"></i></div>
                                    </form>
                                </div>
                                <div class="col-sm-9">
                                    <label class="d-block caption_img">Album dự án</label>
                                    <form class="dropzone" id="album-project" action="/upload">
                                        <div class="dz-message"><i class="far fa-images"></i></div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="fa fa-info-circle"></i>Tổng quan</span></h4>

                            <div class="row row3">
                                <div class="col-12 col-md-6 col-lg-12 col-xl-6 item">
                                    <div class="panel-form__row style2">
                                        <label class="text mb-md-0">Chủ đầu tư</label>
                                        <div class="input">
                                            <input type="text" name="investor" value="{{ $project->investor }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="panel-form__row style2">
                                        <label class="text mb-md-0">Nhà Thầu</label>
                                        <div class="input">
                                            <input type="text" name="contractors" value="{{ $project->contractors }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="panel-form__row style2">
                                        <label class="text mb-md-0">Số tháp</label>
                                        <div class="input">
                                            <input type="text" name="tower" value="{{ $project->tower }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="panel-form__row style2">
                                        <label class="text mb-md-0">Số căn hộ</label>
                                        <div class="input">
                                            <input type="text" name="apartments" value="{{ $project->apartments }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="panel-form__row style2">
                                        <label class="text mb-md-0">Ngân hàng</label>
                                        <div class="input">
                                            <label class="select-box form-control">
                                                <select name="" id="">
                                                    <option value="">Ngân hàng bảo lãnh</option>
                                                </select>
                                                <i class="fa fa-angle-down"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 col-lg-12 col-xl-6 item">
                                    <div class="panel-form__row style2">
                                        <label class="text mb-md-0">Quy mô dự án</label>
                                        <div class="input">
                                            <input type="text" name="project_scale" value="{{ $project->project_scale }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="panel-form__row style2">
                                        <label class="text mb-md-0">Kiến trúc và cảnh quan</label>
                                        <div class="input">
                                            <input type="text" name="landscape_architecture" value="{{ $project->landscape_architecture }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="panel-form__row style2">
                                        <label class="text mb-md-0">Số Tầng</label>
                                        <div class="input">
                                            <input type="text" name="floors" value="{{ $project->floors }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="panel-form__row style2">
                                        <label class="text mb-md-0">Giá bán</label>
                                        <div class="input">
                                            <input type="text" name="price" value="{{ $project->price }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="panel-form__row style2">
                                        <label class="text mb-md-0">Ngày bàn giao</label>
                                        <div class="input">
                                            <!-- <label class="select-box form-control">
                                                <select name="" id="">
                                                    <option value="">15/07/2015</option>
                                                </select>
                                                <i class="fa fa-angle-down"></i>
                                            </label> -->
                                            <input type="text" name="date_delivery" value="{{ $project->date_delivery }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="fa fa-list-ul"></i>Tổng quan dự án</span></h4>

                            <div class="code-box-gid">
                                <section class="grid-demo content-store">
                                    <div class="controls cf">
                                        <div class="control search">
                                            <div class="control-icon">
                                                <i class="material-icons">&#xE8B6;</i>
                                            </div>
                                            <input class="control-field search-field form-control " type="text" name="search" placeholder="Search..." />
                                        </div>
                                        <div class="control filter">
                                            <div class="control-icon">
                                                <i class="material-icons">&#xE152;</i>
                                            </div>
                                            <div class="select-arrow">
                                                <i class="material-icons">&#xE313;</i>
                                            </div>
                                            <select class="control-field filter-field form-control">
                                                <option value="" selected>All</option>
                                                <option value="red">Red</option>
                                                <option value="blue">Blue</option>
                                                <option value="green">Green</option>
                                            </select>
                                        </div>
                                        <div class="control sort">
                                            <div class="control-icon">
                                                <i class="material-icons">&#xE164;</i>
                                            </div>
                                            <div class="select-arrow">
                                                <i class="material-icons">&#xE313;</i>
                                            </div>
                                            <select class="control-field sort-field form-control">
                                                <option value="order" selected>Drag</option>
                                                <option value="title">Title (drag disabled)</option>
                                                <option value="color">Color (drag disabled)</option>
                                            </select>
                                        </div>
                                        <div class="control layout">
                                            <div class="control-icon">
                                                <i class="material-icons">&#xE871;</i>
                                            </div>
                                            <div class="select-arrow">
                                                <i class="material-icons">&#xE313;</i>
                                            </div>
                                            <select class="control-field layout-field form-control">
                                                <option value="left-top" selected>Left Top</option>
                                                <option value="left-top-fillgaps">Left Top (fill gaps)</option>
                                                <option value="right-top">Right Top</option>
                                                <option value="right-top-fillgaps">Right Top (fill gaps)</option>
                                                <option value="left-bottom">Left Bottom</option>
                                                <option value="left-bottom-fillgaps">Left Bottom (fill gaps)</option>
                                                <option value="right-bottom">Right Bottom</option>
                                                <option value="right-bottom-fillgaps">Right Bottom (fill gaps)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="grid load_tongquan">

                                    </div>
                                    <div class="grid-footer1">
                                        <button class="btn btn-primary themtq" type="button"><i class="fas fa-search-plus"></i> Thêm</button>
                                    </div>
                                    <div class="grid-footer">
                                        <button class="add-more-items btn btn-primary"><i class="fas fa-plus"></i> Add more items</button>
                                    </div>
                                </section>
                            </div>
                        </div>

                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="fa fa-list-ul"></i>Tiện ích dự án</span></h4>

                            <div class="code-box-gid">
                                <section class="grid-demo2 content-store">
                                    <div class="controls cf">
                                        <div class="control search">
                                            <div class="control-icon">
                                                <i class="material-icons">&#xE8B6;</i>
                                            </div>
                                            <input class="control-field search-field2 form-control " type="text" name="search" placeholder="Search..." />
                                        </div>
                                        <div class="control filter">
                                            <div class="control-icon">
                                                <i class="material-icons">&#xE152;</i>
                                            </div>
                                            <div class="select-arrow">
                                                <i class="material-icons">&#xE313;</i>
                                            </div>
                                            <select class="control-field filter2-field2 form-control">
                                                <option value="" selected>All</option>
                                                <option value="red">Red</option>
                                                <option value="blue">Blue</option>
                                                <option value="green">Green</option>
                                            </select>
                                        </div>
                                        <div class="control sort">
                                            <div class="control-icon">
                                                <i class="material-icons">&#xE164;</i>
                                            </div>
                                            <div class="select-arrow">
                                                <i class="material-icons">&#xE313;</i>
                                            </div>
                                            <select class="control-field sort-field2 form-control">
                                                <option value="order" selected>Drag</option>
                                                <option value="title">Title (drag disabled)</option>
                                                <option value="color">Color (drag disabled)</option>
                                            </select>
                                        </div>
                                        <div class="control layout">
                                            <div class="control-icon">
                                                <i class="material-icons">&#xE871;</i>
                                            </div>
                                            <div class="select-arrow">
                                                <i class="material-icons">&#xE313;</i>
                                            </div>
                                            <select class="control-field layout-field2 form-control">
                                                <option value="left-top" selected>Left Top</option>
                                                <option value="left-top-fillgaps">Left Top (fill gaps)</option>
                                                <option value="right-top">Right Top</option>
                                                <option value="right-top-fillgaps">Right Top (fill gaps)</option>
                                                <option value="left-bottom">Left Bottom</option>
                                                <option value="left-bottom-fillgaps">Left Bottom (fill gaps)</option>
                                                <option value="right-bottom">Right Bottom</option>
                                                <option value="right-bottom-fillgaps">Right Bottom (fill gaps)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="grid2 load_tienich">

                                    </div>
                                    <div class="grid-footer1">
                                        <button class="btn btn-primary themtienich" type="button"><i class="fas fa-search-plus"></i> Thêm</button>
                                    </div>
                                    <div class="grid-footer">
                                        <button class="add-more-items2 btn btn-primary"><i class="fas fa-plus"></i> Add more items</button>
                                    </div>
                                </section>
                            </div>
                        </div>

                        <div class="block load_dropzone">
                            <h4 class="title-line"><span class="text"><i class="fa fa-list-ul"></i>Mặt bằng các tầng</span></h4> <button class="add_html_flat"><i class="fas fa-plus"></i> Thêm</button>
                            <p class="total-count" count-dropzone="2"></p>
                            @foreach($project->album_flat as $key => $album)
                            <div class="html-flat section-dropzone<?= $key ?>" data-id="{{$album->id}}" avatar="{{$album->avatar}}" json_album="{{$album->json_album}}">
                                <div class="col-12">
                                    <button class="remove-all-flat"><i class="fas fa-broom"></i> xóa mặt bằng</button>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                    <?php if($album['avatar'] != null): ?>
                                        <img src="/public/img/upload/flats/<?= $album['avatar'] ?>" alt="">
                                    <?php endif; ?>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="slick-cus panel-slider js-slider4 without-arrows">
                                            <?php
                                                if($album['json_album'] != null)
                                                    $flats = json_decode($album['json_album'], true);
                                                else
                                                    $flats = [];
                                            ?>
                                            <?php foreach($flats as $flat): ?>
                                            <div class="item div-album">
                                                <figure class="img">
                                                    <img src="/public/img/upload/flats/<?= $flat ?>" alt="">
                                                </figure>
                                                <button data-image="<?= $flat ?>" class="remove-flat<?= $key ?> btn_delte_mod"><i class="far fa-trash-alt"></i> xóa</button>
                                            </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-sm-3 item">
                                        <label class="d-block">Mặt bằng các tầng</label>

                                        <form class="dropzone count-element" id="avatar-dropzone{{$key}}" action="/upload">
                                            <div class="dz-message "><i class="far fa-image"></i></div>
                                        </form>
                                    </div>
                                    <div class="col-sm-9 item">
                                        <label class="d-block">Mặt bằng căn hộ</label>
                                        <form class="dropzone" id="demo-upload{{$key}}" action="/upload">
                                            <div class="dz-message"><i class="far fa-images"></i></div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                            @endforeach
                            <!-- <div class="html-dropzone">
                            </div> -->
                        </div>

                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="fa fa-list-ul"></i>Chương trình bán hàng</span></h4>
                            <div class="row list_hinh_ban_hang">
                                @if($project->sale_image != '')
                                <div class="col-sm-3">
                                    <div class="item-mod">
                                        <img src="/public/img/upload/sales/{{$project->sale_image}}" alt="">
                                        <!-- <span class="icon-mod"><i class="far fa-trash-alt"></i></span> -->
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-sm-3 item">
                                    <div class="sale-left">
                                        <label class="d-block caption_img">hình bán hàng</label>
                                        <form class="dropzone" id="avatar-sale" action="/upload">
                                        <div class="dz-message"><i class="far fa-image"></i></div>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-sm-9 item">
                                    <div class="sale-right">
                                        <!-- <div class="panel-form__row">
                                            <div class="d-flex flex-wrap img-upload__text">
                                                <button class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Tải tài liệu</span></button>
                                                <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga rerum deleniti doloremque ipsa harum deserunt, vel quidem.</p>
                                            </div>
                                        </div> -->
                                        <button class="reset-btn btn-save mark-inputfile"><span class="icon"><i class="fas fa-file"></i></span><span class="text text-uppercase load_file_name">@if($project->sale_file != null) {{ $project->sale_file }} @else Tải file lên @endif</span></button>
                                        <input id="load_file_name" class="file-ban-hang d-none" type="file" name="file_">
                                        <textarea name="content_sale" id="content_sale" rows="10" class="form-control"></textarea>
                                        <input type="hidden" id="file_name_old" value="@if($project->sale_file != null) {{ $project->sale_file }} @else Tải file lên @endif">
                                    </div>

                                    <script>
                                        $('.mark-inputfile').click(function(event) {
                                            $( ".file-ban-hang" ).trigger( "click" );
                                        });

                                        $('.file-ban-hang').change(function() {
                                            var filename = $(this).val();
                                            if (filename == "") {
                                                $('.load_file_name').html($('#file_name_old').val());
                                            } else {
                                                $('.load_file_name').html(filename);
                                            }
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>

                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Liên hệ</span></h4>

                            <div class="panel-form__row">
                                <label class="text mb-md-0">Tên liên hệ</label>
                                <div class="input">
                                    <input type="text" name="name_contact" value="{{ $project-> name_contact}}" class="form-control">
                                </div>
                            </div>
                            <div class="panel-form__row">
                                <label class="text mb-md-0">Địa chỉ <span class="cl_red">(*)</span></label>
                                <div class="input">
                                    <input type="text" name="address_contact" value="{{ $project-> address_contact}}" class="form-control">
                                </div>
                            </div>
                            <!-- <div class="panel-form__row">
                                <label class="text mb-md-0">Điện thoại bàn</label>
                                <div class="input">
                                    <input type="text"  class="form-control">
                                </div>
                            </div> -->
                            <div class="panel-form__row">
                                <label class="text mb-md-0">Di động</label>
                                <div class="input">
                                    <input type="text" name="phone_contact" value="{{ $project->phone_contact}}" class="form-control">
                                </div>
                            </div>
                            <div class="panel-form__row">
                                <label class="text mb-md-0">Emai <span class="cl_red">(*)</span></label>
                                <div class="input">
                                    <input type="text" name="email_contact" value="{{ $project-> email_contact}}" class="form-control">

                                    <label class="mt-2"><input @if($project->email_flg == 1) checked @endif id="email_flg" type="checkbox"> Nhận email phản hồi</label>
                                </div>
                            </div>
                        </div>

                        <div class="panel-form--footer">
                            <button id="submit-project" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Đăng tin</span></button>
                        </div>
                    <!-- </form> -->
                </section>
            </div>
        </div>
    </div>
    <script src="/public/assets/global/plugins/dropzone/dropzone.min.js"></script>
    <script type="text/javascript">
        Dropzone.autoDiscover = false;

        $('.remove-all-flat').click(function() {
            var id = $(this).closest('.html-flat').attr('data-id');
            $(this).closest('.html-flat').remove();
            $('.block-page-all').addClass('active');
            $.ajax({
                url: '/ajax/remove_all_flat',
                data: {id: id},
                type: 'POST',
                dataType: 'json',
                beforeSend: function(e) {
                },
                success: function(e) {
                    $('.block-page-all').removeClass('active');
                }
            });
        });

        $('.add_html_flat').click(function() {
            var count = $(".total-count").attr("count-dropzone");
            var id_project = <?= $slug ?>;
            $('.block-page-all').addClass('active');
            $.ajax({
                url: '/ajax/them_hinh_load',
                data: {count: count, id_project: id_project},
                type: 'POST',
                dataType: 'json',
                beforeSend: function(e) {
                },
                success: function(e) {
                    $(".total-count").attr("count-dropzone", count + 1);
                    $(".load_dropzone").append(e.data).show().fadeIn("slow");
                    $('.block-page-all').removeClass('active');
                }
            });
        });

        $('.remove-album').click(function() {
            var id = $(this).closest('.div-album').attr('data-id');
            $(this).closest('.div-album').remove();
            $('.block-page-all').addClass('active');
            $.ajax({
                url: '/ajax/remove_album',
                data: {id: id},
                type: 'POST',
                dataType: 'json',
                beforeSend: function(e) {
                },
                success: function(e) {
                    $('.block-page-all').removeClass('active');
                }
            });
        });

        $('#submit-project').click(function() {

            var title = $("input[name=title]").val();
            if (title == '') {
                toastr.error('Vui lòng điền tiêu đề', null, {timeOut: 4000});
                return false;
            }
            var category_id = $('#category_id').find(":selected").val();
            if (category_id == undefined) {
                toastr.error('Vui lòng chọn danh mục dự án', null, {timeOut: 4000});
                return false;
            }
            var slug = ChangeToSlug(title);
            var short_content = document.getElementById("short_content").value;
            var content_sale = document.getElementById("content_sale").value;
            var map = document.getElementById("map").value;
            var name_contact = $("input[name=name_contact]").val();
            var address_contact = $("input[name=address_contact]").val();
            var phone_contact = $("input[name=phone_contact]").val();
            var email_contact = $("input[name=email_contact]").val();

            var fd = new FormData();

            // avatar
            var files = [];
            files = files.concat(avatar_project.files.filter(function(e) {
                return e.accepted == true;
            }));

            for (var i = 0; i < files.length; i++) {
                if (files[i].constructor.name == 'File')
                    fd.append('avatar', files[i]);
                else if (files[i].files && files[i].files[0] && files[i].files[0].constructor.name == 'File')
                    fd.append('avatar', files[i].files[0]);
                else
                    fd.append('avatar', 'uploaded');
            }

            // album
            var files = [];
            files = files.concat(album_project.files.filter(function(e) {
                return e.accepted == true;
            }));

            for (var i = 0; i < files.length; i++) {
                if (files[i].constructor.name == 'File')
                    fd.append('files[' + i + ']', files[i]);
                else if (files[i].files && files[i].files[0] && files[i].files[0].constructor.name == 'File')
                    fd.append('files[' + i + ']', files[i].files[0]);
                else
                    fd.append('files[' + i + ']', 'uploaded');
            }

            // image sale
            var files = [];
            files = files.concat(avatar_sale.files.filter(function(e) {
                return e.accepted == true;
            }));

            for (var i = 0; i < files.length; i++) {
                if (files[i].constructor.name == 'File')
                    fd.append('avatar_sale', files[i]);
                else if (files[i].files && files[i].files[0] && files[i].files[0].constructor.name == 'File')
                    fd.append('avatar_sale', files[i].files[0]);
                else
                    fd.append('avatar_sale', 'uploaded');
            }

            // file sale
            var file_sale = $('#load_file_name').prop('files')[0];
            if (typeof file_sale !== 'undefined') {
                fd.append('file_save', file_sale);
            }

            // get tong quan
            var tongquans = [];
            $('.load_tongquan .card').each(function(index, el) {
                var vitri = $(this).find('.card-id').html();
                var id_tq = $(this).find('.card-id_tq').html();
                var item_tq = {
                    vitri:vitri,
                    id_tq:id_tq,
                };
                tongquans.push(item_tq);
            });
            var jsontg = JSON.stringify(tongquans);

            // get tien ich
            var tienichs = [];
            $('.load_tienich .card').each(function(index, el) {
                var vitri = $(this).find('.card-id').html();
                var id_tq = $(this).find('.card-id_tq').html();
                var item_ti = {
                    vitri:vitri,
                    id_tq:id_tq,
                };
                tienichs.push(item_ti);
            });
            var jsonti = JSON.stringify(tienichs);

            var email_flg = 0;
            if($("#email_flg").prop('checked') == true){
                var email_flg = 1;
            }
            
            var form = {};
            form['id'] = <?= $slug ?>;
            form['title'] = title;
            form['category_id'] = category_id;
            form['address'] = $("input[name=address]").val();
            form['start_day'] = $("input[name=start_day]").val();
            form['end_day'] = $("input[name=end_day]").val();
            form['investor'] = $("input[name=investor]").val();
            form['contractors'] = $("input[name=contractors]").val();
            form['tower'] = $("input[name=tower]").val();
            form['apartments'] = $("input[name=apartments]").val();
            form['project_scale'] = $("input[name=project_scale]").val();
            form['landscape_architecture'] = $("input[name=landscape_architecture]").val();
            form['floors'] = $("input[name=floors]").val();
            form['price'] = $("input[name=price]").val();
            form['date_delivery'] = $("input[name=date_delivery]").val();
            form['slug'] = slug;
            form['content_sale'] = content_sale;
            form['map'] = map;
            form['short_content'] = short_content;
            form['name_contact'] = name_contact;
            form['address_contact'] = address_contact;
            form['phone_contact'] = phone_contact;
            form['email_contact'] = email_contact;

            form['tongquans'] = jsontg;
            form['tienichs'] = jsonti;
            form['email_flg'] = email_flg;

            fd = objectToFormData(form, fd);
            $('.block-page-all').addClass('active');
            $.ajax({
                url: '/ajax/ajax_edit',
                type: 'POST',
                data: fd,
                contentType: false,
                 processData: false,
                beforeSend: function(e) {
                },
                success: function(e) {
                    switch (e.code) {
                        case 200:
                            toastr.success('Cập nhật thành công', null, {timeOut: 4000});
                            $('.block-page-all').removeClass('active');
                            window.location.href = "/dang-tin";
                            break;
                        default:
                            toastr.error(e.error, null, {timeOut: 4000});
                            $('.block-page-all').removeClass('active');
                            break;
                    }
                }
            });
        });

        function objectToFormData(obj, form, namespace) {
            var fd = form || new FormData();
            var formKey;

            for(var property in obj) {
                if(obj.hasOwnProperty(property)) {
                    if(namespace) {
                        formKey = namespace + '[' + property + ']';
                    } else {
                        formKey = property;
                    }
                    if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                        objectToFormData(obj[property], fd, formKey);
                    } else {
                        fd.append(formKey, obj[property]);
                    }
                }
            }
            return fd;
        };

        function ChangeToSlug(title)
        {
            var slug;
            //Đổi chữ hoa thành chữ thường
            slug = title.toLowerCase();
            //Đổi ký tự có dấu thành không dấu
            slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            slug = slug.replace(/đ/gi, 'd');
            //Xóa các ký tự đặt biệt
            slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
            //Đổi khoảng trắng thành ký tự gạch ngang
            slug = slug.replace(/ /gi, "-");
            //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
            //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
            slug = slug.replace(/\-\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-/gi, '-');
            slug = slug.replace(/\-\-/gi, '-');
            //Xóa các ký tự gạch ngang ở đầu và cuối
            slug = '@' + slug + '@';
            slug = slug.replace(/\@\-|\-\@|\@/gi, '');
            //In slug ra textbox có id “slug”
            return slug;
        }

        var avatar_project = new Dropzone('#avatar-project', {
            maxFiles: 1,
            addRemoveLinks: true,
            acceptedFiles: '.jpeg,.jpg,.png,.gif',
            autoProcessQueue: false,
        });

        var album_project = new Dropzone('#album-project', {
            maxFiles: 10,
            addRemoveLinks: true,
            acceptedFiles: '.jpeg,.jpg,.png,.gif',
            autoProcessQueue: false,
        });

        // sale image
        var avatar_sale = new Dropzone('#avatar-sale', {
            maxFiles: 1,
            addRemoveLinks: true,
            acceptedFiles: '.jpeg,.jpg,.png,.gif',
            autoProcessQueue: false,
        });

        // flat
        <?php foreach($project->album_flat as $key => $album): ?>
            var arr_dropzone_avatar = [];
            arr_dropzone_avatar[<?= $key ?>] = new Dropzone('#avatar-dropzone' + <?= $key ?>, {
                maxFiles: 1,
                acceptedFiles: '.jpeg,.jpg,.png',
                autoProcessQueue: false,

                init: function() {
                    this.hiddenFileInput.removeAttribute('multiple');

                    this.on("maxfilesexceeded", function(file) {
                        this.removeAllFiles();
                        this.addFile(file);
                    });

                    this.on("selectedfiles", function(files) {
                        var count = Number(files.length - 1);
                        if (files[count].type != 'image/jpeg' && files[count].type != 'image/png') {
                            toastr.error('Hình không đúng định dạng', null, {timeOut: 4000});
                            this.removeFile(file);
                            return false;
                        }

                        var avatar = $('.section-dropzone' + <?= $key ?>).attr('avatar');
                        var id = $('.section-dropzone' + <?= $key ?>).attr('data-id');

                        var fd = new FormData();
                        fd.append('file', files[0]);
                        var form = {};
                        form['id'] = id;
                        form['avatar'] = avatar;
                        fd = objectToFormData(form, fd);
                        $('.block-page-all').addClass('active');
                        $.ajax({
                            type: 'POST',
                            url: '/ajax/add_avatar',
                            data: fd,
                            contentType: false,
                            processData: false,
                            beforeSend: function(e) {
                            },
                            success: function(e) {
                                $(".section-dropzone" + <?= $key ?>).attr("avatar", e.avatar);
                                $('.block-page-all').removeClass('active');
                            }
                        });
                    });

                    /*this.on("removedfile", function() {
                        var avatar = $('.section-dropzone' + <?= $key ?>).attr('avatar');
                        var id = $('.section-dropzone' + <?= $key ?>).attr('data-id');

                        var fd = new FormData();
                        var form = {};
                        form['id'] = id;
                        form['avatar'] = avatar;
                        fd = objectToFormData(form, fd);
                        $('.block-page-all').addClass('active');
                        $.ajax({
                            type: 'POST',
                            url: '/ajax/remove_avatar',
                            data: fd,
                            contentType: false,
                            processData: false,
                            beforeSend: function(e) {
                            },
                            success: function(e) {
                                $(".section-dropzone" + <?= $key ?>).attr("avatar", '');
                                $('.block-page-all').removeClass('active');
                            }
                        });
                    });*/
                }
            });

            var arr_dropzone = [];
            arr_dropzone[<?= $key ?>] = new Dropzone('#demo-upload' + <?= $key ?>, {
                maxFiles: 30,
                addRemoveLinks: true,
                acceptedFiles: '.jpeg,.jpg,.png,.gif',
                autoProcessQueue: false,
                init: function() {

                    this.on('selectedfiles', function(files) {

                        var flats = $('.section-dropzone' + <?= $key ?>).attr('json_album');
                        var id = $('.section-dropzone' + <?= $key ?>).attr('data-id');
                        var fd = new FormData();
                        var check_arr = [];
                        for (i = 0; i < files.length; i++) {
                            if (files[i].type == 'image/jpeg' || files[i].type == 'image/png') {
                                var name_image = makeid();
                                files[i]['name_image'] = name_image;

                                fd.append('file['+ i +'][name_image]', name_image);
                                fd.append('file['+ i +'][file]', files[i]);
                                check_arr[i] = i;
                            } else {
                                files[i]['error_image'] = 'error';
                            }

                        }

                        if (check_arr.length > 0) {
                            var form = {};
                            form['id'] = id;
                            form['flats'] = flats;
                            fd = objectToFormData(form, fd);
                            $('.block-page-all').addClass('active');
                            $.ajax({
                                type: 'POST',
                                url: '/ajax/add_flats',
                                data: fd,
                                contentType: false,
                                processData: false,
                                beforeSend: function(e) {
                                },
                                success: function(e) {
                                    $(".section-dropzone" + <?= $key ?>).attr("data-id", e.id);
                                    $(".section-dropzone" + <?= $key ?>).attr("json_album", e.json_album);
                                    $('.block-page-all').removeClass('active');
                                }
                            });
                        }
                    });

                    this.on("removedfile", function(file) {

                        if (typeof file.error_image !== 'undefined') {

                        } else {
                            var flats = $('.section-dropzone' + <?= $key ?>).attr('json_album');
                            var id = $('.section-dropzone' + <?= $key ?>).attr('data-id');

                            var fd = new FormData();
                            var form = {};
                            form['id'] = id;
                            form['flats'] = flats;
                            form['name_image'] = file.name_image + '.png';
                            fd = objectToFormData(form, fd);
                            $('.block-page-all').addClass('active');
                            $.ajax({
                                type: 'POST',
                                url: '/ajax/remove_flat',
                                data: fd,
                                contentType: false,
                                processData: false,
                                beforeSend: function(e) {
                                },
                                success: function(e) {
                                    $(".section-dropzone" + <?= $key ?>).attr("json_album", e.json_album);
                                    $('.block-page-all').removeClass('active');
                                }
                            });
                        }
                    });
                }
            });

            $('.remove-flat' + <?= $key ?>).click(function() {
                var flats = $('.section-dropzone' + <?= $key ?>).attr('json_album');
                var id = $('.section-dropzone' + <?= $key ?>).attr('data-id');
                var image = $(this).attr('data-image');

                var fd = new FormData();
                var form = {};
                form['id'] = id;
                form['flats'] = flats;
                form['name_image'] = $(this).attr('data-image');
                fd = objectToFormData(form, fd);
                $('.block-page-all').addClass('active');
                $.ajax({
                    type: 'POST',
                    url: '/ajax/remove_flat',
                    data: fd,
                    contentType: false,
                    processData: false,
                    beforeSend: function(e) {
                    },
                    success: function(e) {
                        $(".section-dropzone" + <?= $key ?>).attr("json_album", e.json_album);
                        //$(".section-dropzone" + <?= $key ?>).find("item-image-flat").remove();
                        $('.block-page-all').removeClass('active');
                    }
                });
                $(this).closest(".item-image-flat").remove();
            });
            <?php endforeach; ?>

            // ajax load
            var ajax_dropzone_avatar = [];
            var ajax_arr_dropzone = [];

            function makeid() {
                var text = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                for (var i = 0; i < 32; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

              return text;
            }

    </script>
<!-- Modal add -->
<div id="modal_tongquan" class="modal list_store fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thông tin tổng quan</h4>
            </div>
            <div class="modal-body custom-list-data1">
                <table id="tbl-dataz" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
                    <thead>
                        <tr>
                            <th><label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" value="1" name="test" id="all_id_player"><span></span></label></th>
                            <th>id</th>
                            <th>data</th>
                            <th>title</th>
                            <th>title_en</th>
                            <th>value</th>
                            <th>value_en</th>
                        </tr>
                    </thead>

                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button onclick="addstore()" type="button" class="btn btn-primary">Thêm</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal add -->
<div id="modal_tienich" class="modal list_store fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thông tin tiện ích</h4>
            </div>
            <div class="modal-body custom-list-data1">
                <table id="tbl-dataz2" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
                    <thead>
                        <tr>
                            <th><label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" value="1" name="test" id="all_id_player2"><span></span></label></th>
                            <th>id</th>
                            <th>data</th>
                            <th>title</th>
                            <th>title_en</th>
                        </tr>
                    </thead>

                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button onclick="addstore2()" type="button" class="btn btn-primary">Thêm</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="modalMap" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Chọn vị trí bất động sản</h4>
            </div>

            <div class="modal-body">
                <input id="pac-input" class="controls" type="text" placeholder="Nhập địa chỉ cần tìm...">
                <div id="googleMap" style="width: 100%; height: 500px;"></div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-close width90" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                    <span>Đóng</span>
                </button>

                <button type="button" class="btn btn-primary width90 apply_geolocation">
                    <i class="fa fa-check"></i>
                    <span>Chọn</span>
                </button>
            </div>
        </div>

    </div>
</div>

<script src="/public/js/cmuuri.js"></script>

<script>
    var $scope = {};
    //-------------------------------------------------------------------------------
    $('#modalMap').on('shown.bs.modal', function() {
        // GOOGLE MAP
        var map = $('.toadobando').val();
        $scope.pointer = $scope.pointer || {};

        var geo = map.split(',');
        if (geo.length == 2) {
            $scope.pointer.latitude = geo[0];
            $scope.pointer.longitude = geo[1];
        }

        init_google_map($scope, {
            marker: $scope.marker,
            pointer: $scope.pointer,
        });
    });

    //----------------------------------------------------------------------------
    function init_google_map($scope, options) {
        if (!$scope.map) {
            var p = { lat: parseFloat(options.pointer.latitude), lng: parseFloat(options.pointer.longitude) };

            if (isNaN(options.pointer.latitude) || isNaN(options.pointer.longitude))
                p = { lat: 10.8230989, lng: 106.6296638 };

            $scope.map = new google.maps.Map($('#googleMap')[0], {
                zoom: 17,
                center: p,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            placeMarker(p);

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');

            var searchBox = new google.maps.places.SearchBox(input);

            $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        return;
                    }

                    placeMarker(place.geometry.location);

                    options.pointer.latitude = place.geometry.location.lat();
                    options.pointer.longitude = place.geometry.location.lng();
                    options.pointer.description = place.formatted_address;

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                $scope.map.fitBounds(bounds);
            });
        }
        google.maps.event.addListener($scope.map, 'click', function(event) {
            placeMarker(event.latLng);

            options.pointer.latitude = event.latLng.lat();
            options.pointer.longitude = event.latLng.lng();
        });

        function placeMarker(location) {
            if (options.marker == null) {
            options.marker = new google.maps.Marker({
                position: location,
                map: $scope.map
            });
            } else {
                options.marker.setPosition(location);
            }
        }
    };

    $('.apply_geolocation').click(function(event) {
        $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude)
        $("#modalMap").modal('toggle');
    });

</script> 
@endsection