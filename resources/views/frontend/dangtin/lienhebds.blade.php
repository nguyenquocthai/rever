@extends('frontend.layouts.application')
@section('content')
<link href="/public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="/public/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

<!--/head-block-->
<div class="content-index bg-gray">
    <div class="container-fluid t-video-rp project-video without-video"></div>

    <div class="container">
        <div class="bg-white panel-wrap my-3 my-md-5">
            @include('frontend.elements.sidebar_dangtin')

            <section class="panel-content">
                <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>Bảng thông tin</button>

                <h2 class="title text-uppercase">Danh sách liên hệ</h2>

                <div class="tab-container">
                    <div class="table-responsive load_thue_ban">
                        @include('frontend.dangtin._lienhebds')
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_lienhe" tabindex="-1" role="dialog" aria-labelledby="modal_lienheLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal_lienheLabel">Thông tin liên hệ</h4>
            </div>
            <div class="modal-body">
                <div class="load_lienhebds">
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('.load_thue_ban').on('click', '.xem_lienhe', function(event) {
        event.preventDefault();
        var id = $(this).attr('data-id');

        var data = {};
        data['id'] = id;
        $('.block-page-all').addClass('active');
        $.ajax({
            type: 'POST',
            url: '/api_lienhebds',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                if (result.code == 300) {
                    toastr.error(result.error);
                    $('.block-page-all').removeClass('active');
                    return false
                }
                $('.block-page-all').removeClass('active');
                $('.load_lienhebds').html(result.data);
                $('#modal_lienhe').modal('show');
            }
        });
    });
</script>
@endsection