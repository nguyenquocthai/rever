<!DOCTYPE html>
<html>

    <script src="https://maps.google.com/maps/api/js?key=AIzaSyCQiaMU8DTCCDi90sT1ApqUYM737YYXymU&libraries=geometry,places" async></script>
    @include('Backend.Elements.head')
    <script src="/public/assets/global/plugins/jquery.min.js"></script>

    <style>
        #dangtin .error,
        #create_item .error {
          display: none !important; }
        #dangtin .error_select,
        #create_item .error_select {
          border-color: red !important; }
        #dangtin .form-control,
        #create_item .form-control {
          border: 1px solid #ddd; }
          #dangtin .form-control.error_input,
          #create_item .form-control.error_input {
            border-color: red; }
    </style>
    
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            @include('Backend.Elements.header')

                <!-- BEGIN CONTAINER -->
                <div class="page-container">
                    @include('Backend.Elements.sidebar')
                    <!-- BEGIN CONTENT -->
                    <div class="page-content-wrapper">

                        <!-- BEGIN CONTENT BODY -->
                        <div class="page-content">

                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-red-sunglo">
                                        <i class="icon-settings font-red-sunglo"></i>
                                        <span class="caption-subject bold uppercase">Thông tin cho thuê</span>
                                    </div>
                                    <div class="actions box_lang_muti"></div>
                                </div>
                                <div class="portlet-body form">
                                    <form id="create_item" autocomplete="off" class="panel-form">
                                        <input type="hidden" name="type" value="0">

                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label>Tiêu đề</label>
                                                    <div class="input">
                                                        <input type="text" name="title" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="text mb-md-0">Danh mục</label>
                                                    <div class="input">
                                                        <select class="form-control" name="category_id" id="category_id">
                                                            <option value="">-- Danh mục --</option>
                                                            @foreach($categories as $category)
                                                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text mb-md-0">Tỉnh / Thành phố</label>
                                                    <div class="input">
                                                        <select id="tinhthanh" name="tinhthanh" class="form-control local">
                                                            <option value="">-- Tỉnh / Thành phố --</option>
                                                            @if (count($provinces) != 0)
                                                                @foreach ($provinces as $province)
                                                                <option value="{{$province->id}}">{{$province->name}}</option>
                                                                @endforeach
                                                            @else
                                                            
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text mb-md-0">Quận / Huyện</label>
                                                    <div class="input load_quanhuyen">
                                                        <select id="quanhuyen" name="quanhuyen" class="form-control">
                                                            <option value="">-- Quận / Huyện --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text mb-md-0">Phường / Xã</label>
                                                    <div class="input load_phuongxa">
                                                        <select id="phuongxa" name="phuongxa" class="form-control">
                                                            <option value="">-- Phường / Xã --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text mb-md-0">Đường</label>
                                                    <div class="input load_duong">
                                                        <select id="duong" name="duong" class="form-control">
                                                            <option value="">-- Đường --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="text mb-md-0">Địa chỉ</label>
                                            <div class="input">
                                                <input id="address" type="text" name="address" class="form-control">
                                                <input type="hidden" name="map" class="toadobando">
                                            </div>
                                        </div>

                                        <div class="form-group toadobando_box">
                                            <label class="text mb-md-0">Bản đồ</label>
                                            <div class="input">
                                                <input id="pac-input" class="controls" type="text" placeholder="Nhập địa chỉ cần tìm...">
                                                <div id="googleMap" style="width: 100%; height: 300px;"></div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text mb-md-0">Giá thuê</label>
                                                    <div class="input">
                                                        <input id="price" value="" type="number" name="price" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text mb-md-0">Diện tích / m<sup>2</sup></label>
                                                    <input type="number" class="form-control" name="area" id="area">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text mb-md-0">Đơn vị</label>
                                                    <div class="input">
                                                        @include('frontend.dangtin._donvithue')
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text mb-md-0">Tổng tiền / tháng</label>
                                                    <input value="Thỏa thuận" id="allprice_mark" readonly type="text" class="form-control" name="allprice_mark">

                                                    <input id="allprice" type="hidden" name="allprice">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="text mb-md-0">Mô tả ngắn</label>
                                            <textarea name="short_content" id="short_content" class="form-control"></textarea>
                                        </div>

                                        <div class="panel-form--footer">
                                            <button id="submit-project" class="btn green">Tiếp tục</button>
                                        </div>
                                    </form>

                                </div>

                            </div>

                        </div>
                        <!-- END CONTENT BODY -->

                    </div>
                    <!-- END CONTENTx -->

                </div>
                <!-- END CONTAINER -->

            @include('Backend.Elements.footer')
        </div>
    </body>
    @include('frontend.dangtin.admin._food')

    <script>
        $('#price').blur(function(event) {
            allprice();
        });

        $('#area').blur(function(event) {
            allprice();
        });

        $('#donvi').change(function(event) {
            allprice();
        });

        function allprice(argument) {
            var price = parseFloat($('#price').val());
            var area = parseFloat($('#area').val());
            var donvi = parseInt($('#donvi').val());
            var allprice = 0;
            var allprice_mark = "";

            if (donvi == 0) {
                allprice = 0;
                allprice_mark = "Thỏa thuận";
            } else if (donvi == 1){
                if (isNaN(price)) {
                    $('#price').addClass('error_input');
                } else {
                    allprice = price*1000;
                }
            } else if (donvi == 2){
                if (isNaN(price)) {
                    $('#price').addClass('error_input');
                } else {
                    allprice = price*1000000;
                }
            } else if (donvi == 3){
                if (isNaN(area)) {
                    $('#area').addClass('error_input');
                } else if(isNaN(price)){
                    $('#price').addClass('error_input');
                } else {
                    allprice = area*price*1000;
                }
            } else if (donvi == 4){
                if (isNaN(area)) {
                    $('#area').addClass('error_input');
                } else if(isNaN(price)){
                    $('#price').addClass('error_input');
                } else {
                    allprice = area*price*1000000;
                }
            }
            if (allprice != "Thỏa thuận" && allprice != "") {
                allprice_mark = nFormatter(allprice,2);
            }
            $('#allprice_mark').val(allprice_mark);
            $('#allprice').val(allprice);
        }
    </script>

    <script>
        var $scope = {};

        tinymce.init({
            selector: '#short_content'
        });

        jQuery(document).ready(function($) {
            // GOOGLE MAP
            var map = "10.8230989,106.6296638"
            $scope.pointer = $scope.pointer || {};

            var geo = map.split(',');
            if (geo.length == 2) {
                $scope.pointer.latitude = geo[0];
                $scope.pointer.longitude = geo[1];
            }

            init_google_map($scope, {
                marker: $scope.marker,
                pointer: $scope.pointer,
            });
        });

        //----------------------------------------------------------------------------
        function init_google_map($scope, options) {
            if (typeof google === "undefined") {
                location.reload();
            }

            if (!$scope.map) {
                var p = { lat: parseFloat(options.pointer.latitude), lng: parseFloat(options.pointer.longitude) };

                if (isNaN(options.pointer.latitude) || isNaN(options.pointer.longitude))
                    p = { lat: 10.8230989, lng: 106.6296638 };

                $scope.map = new google.maps.Map($('#googleMap')[0], {
                    zoom: 17,
                    center: p,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                placeMarker(p);

                // Create the search box and link it to the UI element.
                var input = document.getElementById('pac-input');

                var searchBox = new google.maps.places.SearchBox(input);

                $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function() {
                    var places = searchBox.getPlaces();

                    if (places.length == 0) {
                        return;
                    }

                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();
                    places.forEach(function(place) {
                        if (!place.geometry) {
                            return;
                        }

                        placeMarker(place.geometry.location);

                        options.pointer.latitude = place.geometry.location.lat();
                        options.pointer.longitude = place.geometry.location.lng();
                        options.pointer.description = place.formatted_address;

                        if (place.geometry.viewport) {
                            // Only geocodes have viewport.
                            bounds.union(place.geometry.viewport);
                        } else {
                            bounds.extend(place.geometry.location);
                        }
                    });
                    $scope.map.fitBounds(bounds);
                    $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude);
                });
            }
            google.maps.event.addListener($scope.map, 'click', function(event) {
                placeMarker(event.latLng);

                options.pointer.latitude = event.latLng.lat();
                options.pointer.longitude = event.latLng.lng();
                $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude);
            });

            function placeMarker(location) {
                if (options.marker == null) {
                options.marker = new google.maps.Marker({
                    position: location,
                    map: $scope.map
                });
                } else {
                    options.marker.setPosition(location);
                }
                $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude);
            }
        };

        $('#create_item').on('change', '.local', function(event) {
            var tinhthanh = $('#tinhthanh option:selected').html();

            var quanhuyen = $('#quanhuyen option:selected').html();
            if (quanhuyen == '-- Quận / Huyện --') {
                quanhuyen = '';
            } else {
                quanhuyen = quanhuyen + ', ';
            }

            var phuongxa = $('#phuongxa option:selected').html();
            if (phuongxa == '-- Phường / Xã --') {
                phuongxa = '';
            } else {
                phuongxa = phuongxa + ', ';
            }

            var duong = $('#duong option:selected').html();
            if (duong == '-- Đường --') {
                duong = '';
            } else {
                duong = duong + ', ';
            }

            var address = duong + phuongxa + quanhuyen + tinhthanh;
            $('#address').val(address);

            var input = document.getElementById('pac-input');
            $('#pac-input').val($('#address').val());
            google.maps.event.trigger(input, 'focus', {});
            google.maps.event.trigger(input, 'keydown', { keyCode: 13 });
            google.maps.event.trigger(this, 'focus', {});
        });

        $("#address").blur(function(){
            var input = document.getElementById('pac-input');
            $('#pac-input').val($(this).val());
            google.maps.event.trigger(input, 'focus', {});
            google.maps.event.trigger(input, 'keydown', { keyCode: 13 });
            google.maps.event.trigger(this, 'focus', {});
        });

    </script>   

    <script>
        $('#tinhthanh, #category_id, #id_project, #donvi').select2({
            theme: "bootstrap",
            width: '100%'
        });

        $('#tinhthanh, #category_id, #id_project').change(function(event) {
            if ($(this).val() != "") {
                $(this).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
            } else {
                $(this).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
            }
        });

        $('#tinhthanh').change(function(event) {
            $('#phuongxa').val('');
            $('#duong').val('');
            var data = {};
            data['status_code'] = "quanhuyen";
            data['id'] = $(this).val();

            showPageLoading();
            $.ajax({
                type: 'POST',
                url: '/api_dangtin',
                data: data,
                dataType: 'json',
                error: function(){
                    hidePageLoading();
                    toastr.error(result.error);
                },
                success: function(result) {
                    $('.load_quanhuyen').html(result.value);
                    $('#quanhuyen').select2({
                        theme: "bootstrap",
                        width: '100%'
                    });
                    $('#quanhuyen').val('').trigger('change');
                    hidePageLoading();
                }
            });
        });

        $('#create_item').on('change', '#quanhuyen', function(event) {
            var data = {};
            data['status_code'] = "phuongxa";
            data['id'] = $(this).val();

            showPageLoading();
            $.ajax({
                type: 'POST',
                url: '/api_dangtin',
                data: data,
                dataType: 'json',
                error: function(){
                    hidePageLoading();
                    toastr.error(result.error);
                },
                success: function(result) {
                    $('.load_phuongxa').html(result.value);
                    $('#phuongxa').select2({
                        theme: "bootstrap",
                        width: '100%'
                    });

                    $('.load_duong').html(result.value1);
                    $('#duong').select2({
                        theme: "bootstrap",
                        width: '100%'
                    });
                    hidePageLoading();
                }
            });
        });

        var create_item = $('#create_item').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass);
                $(element).addClass("error_input");
                $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass("error_input");
                $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
            },
            rules: {
                title:{
                    required: true
                },
                address:{
                    required: true
                },
                tinhthanh:{
                    required: true
                },
                quanhuyen:{
                    required: true
                },
                phuongxa:{
                    required: true
                },
                duong:{
                    required: true
                },
                category_id:{
                    required: true
                },
                map:{
                    required: true
                },
                short_content:{
                    required: true
                }
            },
            messages: {
                title:{
                    required: 'Tên dự án'
                },
                address:{
                    required: 'Địa chỉ dự án'
                },
                tinhthanh:{
                    required: 'Tỉnh thành'
                },
                quanhuyen:{
                    required: 'Quận huyện'
                },
                phuongxa:{
                    required: 'Phường xã'
                },
                duong:{
                    required: 'Đường'
                },
                map:{
                    required: 'Bản đồ'
                },
                category_id:{
                    required: 'Danh mục'
                },
                short_content:{
                    required: 'Mô tả ngắn'
                }
            },
            submitHandler: function (form) {
        
                var data = {};
                $("#create_item").serializeArray().map(function(x){data[x.name] = x.value;});
                data['status_code'] = "add";
                data['slug'] = ChangeToSlug(data['title']);
                data['short_content'] = tinymce.get('short_content').getContent();

                var search_val = {};
                search_val[0] = $('#category_id option:selected').html();
                search_val[1] = $('#id_project option:selected').html();
                search_val[2] = $('#tinhthanh option:selected').html();
                search_val[3] = $('#quanhuyen option:selected').html();
                search_val[4] = $('#phuongxa option:selected').html();
                search_val[5] = $('#duong option:selected').html();
                search_val[6] = data['title'];
                data['search_val'] = search_val;

                showPageLoading();
                $.ajax({
                    type: 'POST',
                    url: '/api_crate_item',
                    data: data,
                    dataType: 'json',
                    error: function(){
                        hidePageLoading();
                        toastr.error(result.error);
                    },
                    success: function(result) {
                        if (result.code == 300) {
                            toastr.error(result.error);
                            hidePageLoading();
                            return false
                        }
                        toastr.success(result.message);
                        document.location.href = '/tao-tin-thue-b2/'+result.id;
                    }
                });
                return false;
            }
        });

        function nFormatter(num, digits) {
            var si = [
                { value: 1, symbol: "" },
                { value: 1E3, symbol: " Ngìn" },
                { value: 1E6, symbol: " Triệu" },
                { value: 1E9, symbol: " Tỷ" },
                { value: 1E12, symbol: " Nghìn tỷ" },
                { value: 1E15, symbol: " Triệu tỷ" },
                { value: 1E18, symbol: " E" }
            ];
            var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
            var i;
            for (i = si.length - 1; i > 0; i--) {
                if (num >= si[i].value) {
                break;
                }
            }
            return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
        }

        function ChangeToSlug(title)
        {
            var slug;
            //Đổi chữ hoa thành chữ thường
            slug = title.toLowerCase();
            //Đổi ký tự có dấu thành không dấu
            slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            slug = slug.replace(/đ/gi, 'd');
            //Xóa các ký tự đặt biệt
            slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
            //Đổi khoảng trắng thành ký tự gạch ngang
            slug = slug.replace(/ /gi, "-");
            //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
            //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
            slug = slug.replace(/\-\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-/gi, '-');
            slug = slug.replace(/\-\-/gi, '-');
            //Xóa các ký tự gạch ngang ở đầu và cuối
            slug = '@' + slug + '@';
            slug = slug.replace(/\@\-|\-\@|\@/gi, '');
            //In slug ra textbox có id “slug”
            return slug;
        }
    </script>  

</html>