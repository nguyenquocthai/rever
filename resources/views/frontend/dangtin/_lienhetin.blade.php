@if($nguoidung->nhanvien == 1)
<div class="form-group">
    <label class="text mb-md-0">Tên liên hệ</label>
    <div class="input">
        <input value="{{@$item_project->lh_name}}" type="text" name="lh_name" class="form-control">
    </div>
</div>
<div class="form-group">
    <label class="text mb-md-0">Địa chỉ</label>
    <div class="input">
        <input value="{{@$item_project->lh_address}}" type="text" name="lh_address" class="form-control">
    </div>
</div>
<div class="form-group">
    <label class="text mb-md-0">Di động</label>
    <div class="input">
        <input value="{{@$item_project->lh_phone}}" type="text" name="lh_phone" class="form-control">
    </div>
</div>
<div class="form-group">
    <label class="text mb-md-0">Emai</label>
    <div class="input">
        <input value="{{@$item_project->lh_email}}" type="text" name="lh_email" class="form-control">
    </div>
</div>
@else
<div class="form-group">
    <label class="text mb-md-0">Tên liên hệ</label>
    <div class="input">
        <div class="form-control">{{$nguoidung->name}}</div>
    </div>
</div>
<div class="form-group">
    <label class="text mb-md-0">Địa chỉ</label>
    <div class="input">
        <div class="form-control">{{$nguoidung->address}}</div>
    </div>
</div>
<div class="form-group">
    <label class="text mb-md-0">Di động</label>
    <div class="input">
        <div class="form-control">{{$nguoidung->phone}}</div>
    </div>
</div>
<div class="form-group">
    <label class="text mb-md-0">Emai</label>
    <div class="input">
        <div class="form-control">{{$nguoidung->email}}</div>
    </div>
</div>
@endif