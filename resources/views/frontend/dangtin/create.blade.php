<!DOCTYPE html>
<html>
    @include('backend.elements.head')
    @include('backend.elements.js')
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            @include('backend.elements.header')

                <!-- BEGIN CONTAINER -->
                <div class="page-container">
                    @include('backend.elements.sidebar')
                    <!-- BEGIN CONTENT -->
                    <div class="page-content-wrapper">

                        <!-- BEGIN CONTENT BODY -->
                        <div class="page-content store-dang-tin">
                            @include('frontend.dangtin._head')

                            <form id="dangtin" class="row">
                                <div class="col-sm-8">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption font-red-sunglo">
                                                <i class="icon-settings font-red-sunglo"></i>
                                                <span class="caption-subject bold uppercase" ng-bind="detail_block_title">Thông tin dự án</span>
                                            </div>
                                            <div class="actions box_lang_muti">
                                            </div>
                                        </div>
                                        <div class="portlet-body form">

                                            <div class="row">

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>
                                                            <span>Tên dự án</span>
                                                        </label>
                                                        <div class="input">
                                                            <input type="text" name="title" class="form-control"> 
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Loại dự án</label>
                                                        <div class="input">
                                                            <select class="form-control" name="category_id" id="category_id">
                                                                <option value="">-- Chọn loại dự án --</option>
                                                                @foreach($categories as $category)
                                                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="text mb-md-0">Tỉnh / Thành phố</label>
                                                        <div class="input">
                                                            <select id="tinhthanh" name="tinhthanh" class="form-control">
                                                                <option value="">-- Chọn tỉnh thành --</option>
                                                                @if (count($provinces) != 0)
                                                                    @foreach ($provinces as $province)
                                                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                                                    @endforeach
                                                                @else
                                                                
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="text mb-md-0">Quận / Huyện</label>
                                                        <div class="input load_quanhuyen">
                                                            <select id="quanhuyen" name="quanhuyen" class="form-control">
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row">

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Địa Chỉ </label>
                                                        <div class="input">
                                                            <input type="text" name="address" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Bản đồ </label>
                                                        <div class="input">
                                                            <input data-toggle="modal" data-target="#modalMap" readonly name="map" id="map" type="text" class="form-control toadobando">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-sm-6 hidden">
                                                    <div class="form-group">
                                                        <label>Ngày đăng</label>
                                                        <div class="input">
                                                            <input value="01/03/2019" type="text" name="start_day" class="form-control datepicker-here">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="lang_label">
                                                            <span>Tình trạng</span>
                                                        </label>
                                                        <select name="active" class="form-control">
                                                            <option value="1">-- Chọn trạng thái --</option>
                                                            <option value="1">Tạm dừng</option>
                                                            <option value="0">Kích hoạt</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label>Mô tả ngắn</label>
                                                <textarea name="short_content" id="short_content" rows="5" class="form-control"></textarea>
                                            </div>

                                        </div>

                                        <button type="submit" id="submit-project" class="btn green" type="button"><i class="fas fa-arrow-right"></i> Tiếp tục</button>

                                    </div>

                                </div>

                                <div class="col-sm-4">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption font-green-sharp">
                                                <i class="icon-settings font-green-sharp"></i>
                                                <span class="caption-subject bold uppercase">Thông tin liên hệ</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <div class="form-group">
                                                <label class="text mb-md-0">Tên liên hệ</label>
                                                <div class="input">
                                                    <input type="text" name="name_contact" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="text mb-md-0">Địa chỉ</label>
                                                <div class="input">
                                                    <input type="text" name="address_contact" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="text mb-md-0">Di động</label>
                                                <div class="input">
                                                    <input type="text" name="phone_contact" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="text mb-md-0">Emai</label>
                                                <div class="input">
                                                    <input type="text" name="email_contact" class="form-control">
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    
                                </div>

                            </form>

                            @include('frontend.dangtin._food')
                            <script>

                                $('.datepicker-here').datepicker({
                                    format: 'dd/mm/yyyy',
                                });

                                $('#tinhthanh, #category_id').select2({
                                    theme: "bootstrap",
                                    width: '100%'
                                });

                                $('#tinhthanh, #category_id').change(function(event) {
                                    if ($(this).val() != "") {
                                        $(this).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
                                    } else {
                                        $(this).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
                                    }
                                });

                                $('#tinhthanh').change(function(event) {
                                    var data = {};
                                    data['status_code'] = "quanhuyen";
                                    data['id'] = $(this).val();

                                    $('.block-page-all').addClass('active');
                                    $.ajax({
                                        type: 'POST',
                                        url: '/api_dangtin',
                                        data: data,
                                        dataType: 'json',
                                        error: function(){
                                            $('.block-page-all').removeClass('active');
                                            toastr.error(result.error);
                                        },
                                        success: function(result) {
                                            $('.load_quanhuyen').html(result.value);
                                            $('#quanhuyen').select2({
                                                theme: "bootstrap",
                                                width: '100%'
                                            });
                                            $('.block-page-all').removeClass('active');
                                        }
                                    });
                                });

                                var dangtin = $('#dangtin').validate({
                                    highlight: function(element, errorClass, validClass) {
                                        $(element).removeClass(errorClass);
                                        $(element).addClass("error_input");
                                        $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
                                    },
                                    unhighlight: function (element, errorClass, validClass) {
                                        $(element).removeClass("error_input");
                                        $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
                                    },
                                    rules: {
                                        title:{
                                            required: true
                                        },
                                        address:{
                                            required: true
                                        },
                                        tinhthanh:{
                                            required: true
                                        },
                                        category_id:{
                                            required: true
                                        },
                                        map:{
                                            required: true
                                        },
                                        short_content:{
                                            required: true
                                        },
                                        name_contact:{
                                            required: true
                                        },
                                        address_contact:{
                                            required: true
                                        },
                                        phone_contact:{
                                            required: true
                                        },
                                        email_contact:{
                                            required: true
                                        },
                                        
                                    },
                                    messages: {
                                        title:{
                                            required: 'Tên dự án'
                                        },
                                        address:{
                                            required: 'Địa chỉ dự án'
                                        },
                                        tinhthanh:{
                                            required: 'Tỉnh thành'
                                        },
                                        map:{
                                            required: 'Bản đồ'
                                        },
                                        category_id:{
                                            required: 'Danh mục'
                                        },
                                        short_content:{
                                            required: 'Nội dung tin đăng'
                                        },
                                        name_contact:{
                                            required: 'Tên liên hệ'
                                        },
                                        address_contact:{
                                            required: 'Địa chỉ liên hệ'
                                        },
                                        phone_contact:{
                                            required: 'Số điện thoại liên hệ'
                                        },
                                        email_contact:{
                                            required: 'Email liên hệ'
                                        },
                                    },
                                    submitHandler: function (form) {
                                
                                        var data = {};
                                        $("#dangtin").serializeArray().map(function(x){data[x.name] = x.value;});
                                        data['status_code'] = "add";
                                        data['slug'] = ChangeToSlug(data['title']);

                                        $('.block-page-all').addClass('active');
                                        $.ajax({
                                            type: 'POST',
                                            url: '/api_dangtin',
                                            data: data,
                                            dataType: 'json',
                                            error: function(){
                                                $('.block-page-all').removeClass('active');
                                                toastr.error(result.error);
                                            },
                                            success: function(result) {
                                                if (result.code == 300) {
                                                    toastr.error(result.error);
                                                    $('.block-page-all').removeClass('active');
                                                    return false
                                                }
                                                toastr.success(result.message);
                                                document.location.href = '/tao-dang-tin-b2/'+result.id;
                                            }
                                        });
                                        return false;
                                    }
                                });

                            </script>
                        </div>
                        <!-- END CONTENT BODY -->
                        
                    </div>
                    <!-- END CONTENTx -->

                </div>
                <!-- END CONTAINER -->

            @include('backend.elements.footer')
        </div>
    </body>
</html>