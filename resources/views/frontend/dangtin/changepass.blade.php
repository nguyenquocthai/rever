@extends('frontend.layouts.application')
@section('content')
<!--/head-block-->
    <div class="content-index bg-gray">
        <div class="container-fluid t-video-rp project-video without-video"></div>
        
        <div class="container">
            <div class="bg-white panel-wrap my-3 my-md-5">
                @include('frontend.elements.sidebar_dangtin')

                <section class="panel-content">
                    <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>Bảng thông tin</button>

                    <h2 class="title text-uppercase">Thay đổi mật khẩu</h2>
                    
                    <form id="user_form" class="panel-form" enctype="multipart/form-data">
                        <input type="hidden" name="status_code" value="changpass">
                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Thông tin liên hệ</span></h4>

                            <div class="panel-form__row">
                                <label class="text mb-md-0">Mật khẩu cũ <span class="cl_red">(*)</span></label>
                                <div class="input">
                                    <input name="pass_old" type="password" class="form-control">
                                </div>
                            </div>
                            <div class="panel-form__row">
                                <label class="text mb-md-0">Mật khẩu mới <span class="cl_red">(*)</span></label>
                                <div class="input">
                                    <input name="pass_new" type="password" class="form-control">
                                </div>
                            </div>
                            <div class="panel-form__row">
                                <label class="text mb-md-0">Nhập lại mật khẩu <span class="cl_red">(*)</span></label>
                                <div class="input">
                                    <input name="pass_new_re" type="password" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="panel-form--footer">
                            <button type="submit" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Lưu lại</span></button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    <script>
        var user_form = $('#user_form').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass);
            },
            rules: {
                pass_old:{
                    required: true
                },
                pass_new:{
                    required: true,
                    minlength: 8
                },
                pass_new_re:{
                    required: true
                },
            },
            messages: {
                pass_old:{
                    required: 'Mật khẩu củ không được để trống.'
                },
                pass_new:{
                    required: 'Mật khẩu mới không được để trống.',
                    minlength: 'Mật khẩu ít nhất 8 ký tự.'
                },
                pass_new_re:{
                    required: 'Nhập lại mật khẩu không được để trống.'
                },
            },
            submitHandler: function (form) {
        
                var data = {};
                $("#user_form").serializeArray().map(function(x){data[x.name] = x.value;});

                $('.block-page-all').addClass('active');
                $.ajax({
                    type: 'POST',
                    url: '/edit_profile',
                    data: data,
                    dataType: 'json',
                    error: function(){
                        $('.block-page-all').removeClass('active');
                        toastr.error(result.error);
                    },
                    success: function(result) {
                        if (result.code == 300) {
                            toastr.error(result.error);
                            $('.block-page-all').removeClass('active');
                            return false
                        }
                        $('.block-page-all').removeClass('active');
                        toastr.success(result.message);
                    }
                });
                return false;
            }
        });
    </script>
@endsection