<div class="col-sm-3 item_album_flats" data-id="{{$album_flat->id}}">
    <div class="item">
        <div class="list-ctr">
            <span data-id="{{$album_flat->id}}" class="modal_edit_flat"><i class="fas fa-pencil-alt"></i></span>
            <span data-id="{{$album_flat->id}}" class="img_flat"><i class="fas fa-images"></i></span>
            <span data-id="{{$album_flat->id}}" class="delete_flat"><i class="far fa-trash-alt"></i></span>
        </div>
        <div class="img_box">
            <img src="{{BladeGeneral::GetImg(['avatar' => $album_flat->avatar,'data' => 'flat', 'time' => $album_flat->updated_at])}}">
        </div>
        <div class="detail_box">
            <p><span>Tên tầng: </span>{{$album_flat->title}}</p>
            <p><span>Album: </span><b data-id="{{$album_flat->id}}" class="count_img_flats">{{$album_flat->count_album}}</b> Hình</p>
        </div>
    </div>
</div>