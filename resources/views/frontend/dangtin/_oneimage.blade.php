<div class="col-sm-3 item_album_project">
    <div class="item">
        <img src="{{BladeGeneral::GetImg(['avatar' => $project_album->name,'data' => 'project_album', 'time' => $project_album->updated_at])}}" alt="">
        <span class="delete_album_project" data-id="{{$project_album->id}}"><i class="far fa-trash-alt"></i></span>
    </div>
</div>