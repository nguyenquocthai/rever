<table class="table table-bordered">
    <tbody>
        <tr>
            <td>khách hàng</td>
            <td>{{$customer->name_contact}}</td>
        </tr>
        <tr>
            <td>Tin đăng</td>
            <td>{{$customer->title}}</td>
        </tr>
        <tr>
            <td>Số ĐT</td>
            <td>{{$customer->phone_contact}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{$customer->email_contact}}</td>
        </tr>
        @if($customer->status == 2)
        <tr>
            <td>Thương lượng giá</td>
            <td>{{$customer->negotiable_price}}</td>
        </tr>
        @endif
        @if($customer->status == 3)
        <tr>
            <td>Hỏi thông tin</td>
            <td>{{$customer->comment}}</td>
        </tr>
        @endif
    </tbody>
</table>