@extends('frontend.layouts.application')
@section('content')
<link href="/public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="/public/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

<!--/head-block-->
<div class="content-index bg-gray">
    <div class="container-fluid t-video-rp project-video without-video"></div>

    <div class="container">
        <div class="bg-white panel-wrap my-3 my-md-5">
            @include('frontend.elements.sidebar_dangtin')

            <section class="panel-content">
                <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>Bảng thông tin</button>

                <h2 class="title text-uppercase">Dự án yêu thích</h2>

                <div class="tab-container">

                    <div class="table-responsive load_thue_ban">
                        @include('frontend.dangtin.yeuthich_load')
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection