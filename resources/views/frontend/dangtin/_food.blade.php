<!-- Modal add -->
<div id="modal_tongquan" class="modal list_store fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thông tin tổng quan</h4>
            </div>
            <div class="modal-body custom-list-data1">
                <table id="tbl-dataz" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
                    <thead>
                        <tr>
                            <th><label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" value="1" name="test" id="all_id_player"><span></span></label></th>
                            <th>Id</th>
                            <th>Icon</th>
                            <th>Tiêu đề</th>
                            <th>Mô tả</th>
                        </tr>
                    </thead>

                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button onclick="addstore()" type="button" class="btn btn-primary">Thêm</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal add -->
<div id="modal_tienich" class="modal list_store fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thông tin tiện ích</h4>
            </div>
            <div class="modal-body custom-list-data1">
                <table id="tbl-dataz2" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
                    <thead>
                        <tr>
                            <th><label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" value="1" name="test" id="all_id_player2"><span></span></label></th>
                            <th>Id</th>
                            <th>Icon</th>
                            <th>Tiêu đề</th>
                        </tr>
                    </thead>

                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button onclick="addstore2()" type="button" class="btn btn-primary">Thêm</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="modalMap" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Chọn vị trí bất động sản</h4>
            </div>

            <div class="modal-body">
                <input id="pac-input" class="controls" type="text" placeholder="Nhập địa chỉ cần tìm...">
                <div id="googleMap" style="width: 100%; height: 500px;"></div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-close width90" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                    <span>Đóng</span>
                </button>

                <button type="button" class="btn btn-primary width90 apply_geolocation">
                    <i class="fa fa-check"></i>
                    <span>Chọn</span>
                </button>
            </div>
        </div>

    </div>
</div>
<script>
    var $scope = {};
    $('.icp-auto').iconpicker();
    $(document).on('click', '.iconpicker-item', function(event) {
        event.preventDefault();
    });
    //-------------------------------------------------------------------------------
    $('#modalMap').on('shown.bs.modal', function() {
        // GOOGLE MAP
        var map = "10.8230989,106.6296638"
        $scope.pointer = $scope.pointer || {};

        var geo = map.split(',');
        if (geo.length == 2) {
            $scope.pointer.latitude = geo[0];
            $scope.pointer.longitude = geo[1];
        }

        init_google_map($scope, {
            marker: $scope.marker,
            pointer: $scope.pointer,
        });
    });

    //----------------------------------------------------------------------------
    function init_google_map($scope, options) {
        if (!$scope.map) {
            var p = { lat: parseFloat(options.pointer.latitude), lng: parseFloat(options.pointer.longitude) };

            if (isNaN(options.pointer.latitude) || isNaN(options.pointer.longitude))
                p = { lat: 10.8230989, lng: 106.6296638 };

            $scope.map = new google.maps.Map($('#googleMap')[0], {
                zoom: 17,
                center: p,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            placeMarker(p);

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');

            var searchBox = new google.maps.places.SearchBox(input);

            $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        return;
                    }

                    placeMarker(place.geometry.location);

                    options.pointer.latitude = place.geometry.location.lat();
                    options.pointer.longitude = place.geometry.location.lng();
                    options.pointer.description = place.formatted_address;

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                $scope.map.fitBounds(bounds);
            });
        }
        google.maps.event.addListener($scope.map, 'click', function(event) {
            placeMarker(event.latLng);

            options.pointer.latitude = event.latLng.lat();
            options.pointer.longitude = event.latLng.lng();
        });

        function placeMarker(location) {
            if (options.marker == null) {
            options.marker = new google.maps.Marker({
                position: location,
                map: $scope.map
            });
            } else {
                options.marker.setPosition(location);
            }
        }
    };

    $('.apply_geolocation').click(function(event) {
        $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude)
        $("#modalMap").modal('toggle');
    });

    $(document).on('change', '.file_image', function(event) {
        var images = $(this).closest('.file_upload_box').find('.image_review');
        var noimg = "/public/img/no-image.png";
        var file = this.files[0];
        if (typeof(file) === 'undefined') {
            $(this).val('');
            images.attr('src', noimg);
            return false;
        }
        var fileType = file["type"];
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
        if ($.inArray(fileType, ValidImageTypes) < 0) {
            toastr.error('Hình ảnh không hợp lệ', null, {timeOut: 4000});
            $(this).val('');
            images.attr('src', noimg);
            return false;
        }
        if (file.size > 3000000) {
            toastr.error('Dung lượng hình phải bé hơn 3MB', null, {timeOut: 4000});
            $(this).val('');
            images.attr('src', noimg);
            return false;
        }

        var images = $(this).closest('.file_upload_box').find('.image_review');
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                images.attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        }
    });

    $('#all_id_player').change(function(){
        var check = this.checked ? '1' : '0';
        if (check == 1) {
            $('.check_player').prop('checked', true);
        } else {
            $('.check_player').prop('checked', false);
        }
    });

    $('#all_id_player2').change(function(){
        var check = this.checked ? '1' : '0';
        if (check == 1) {
            $('.check_player2').prop('checked', true);
        } else {
            $('.check_player2').prop('checked', false);
        }
    });

</script> 