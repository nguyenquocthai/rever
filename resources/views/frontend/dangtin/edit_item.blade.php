@extends('frontend.layouts.application')
@section('content')
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCQiaMU8DTCCDi90sT1ApqUYM737YYXymU&libraries=geometry,places" async></script>
<link href="/public/css/frondend/cmuuri.css" rel="stylesheet">
<link href="/public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="/public/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- muuri -->
<script src="/public/assets/global/plugins/muuri/scripts/vendor/web-animations-2.3.1.min.js"></script>
<script src="/public/assets/global/plugins/muuri/scripts/vendor/hammer-2.0.8.min.js"></script>
<script src="/public/assets/global/plugins/muuri/scripts/vendor/muuri-0.5.4.js"></script>
<!-- End muuri -->

<!--/head-block-->
<div class="content-index bg-gray store-dang-tin">
    <input type="hidden" id="key_edit_project_item" value="{{$id_project_item}}">
    <div class="container-fluid t-video-rp project-video without-video"></div>

    <div class="container">
        <div class="bg-white panel-wrap my-3 my-md-5">
            @include('frontend.elements.sidebar_dangtin')

            <section class="panel-content">
                <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>Bảng thông tin</button>

                <h2 class="title text-uppercase">Đăng tin thuê <small class="d-block">Quý khách nhập thông tin nhà đất cần bán hoặc cho thuê vào các mục dưới đây</small></h2>

                <!-- <form id="form-create-project" action="" class="panel-form"> -->
                    <div class="block">
                        <div class="panel-form__row">
                            <label class="text mb-md-0">Tiêu đề <span class="cl_red">(*)</span></label>
                            <div class="input">
                                <input type="text" name="title" value="{{ $item->title }}" class="form-control">
                            </div>
                        </div>
                        <div class="panel-form__row">
                            <label class="text mb-md-0">Danh mục<span class="cl_red">(*)</span></label>
                            <div class="input">
                                <select class="form-control" name="category_id" id="category_id">
                                     @foreach($categories as $category)
                                        <option value="{{$category->id}}" {{ $category->id == $item->category_id ? 'selected' : '' }} >{{$category->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="panel-form__row">
                            <label class="text mb-md-0">Dự án<span class="cl_red">(*)</span></label>
                            <div class="input">
                                <select class="form-control" name="id_project" id="id_project">
                                    @foreach($projects as $project)
                                    <option value="{{$project->id}}" {{ $project->id == $item->id_project ? 'selected' : '' }} >{{$project->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="panel-form__row">
                            <label class="text mb-md-0">Loại<span class="cl_red">(*)</span></label>
                            <div class="input">
                                <select class="form-control" name="type" id="type">
                                    <option value="0" {{ $item->type == 0  ? 'selected' : '' }}>Cho thuê</option>
                                    <option value="1" {{ $item->type == 1  ? 'selected' : '' }}>Cần bán</option>
                                </select>
                            </div>
                        </div>

                        <div class="panel-form__row">
                            <label class="text mb-md-0">Địa Chỉ </label>
                            <div class="input">
                                <input type="text" name="address" value="{{ $item->address }}" class="form-control">
                            </div>
                        </div>

                        <div class="panel-form__row toadobando_box">
                            <label class="text mb-md-0">Bản đồ </label>
                            <div class="input">
                                <div class="input-group"> <input value="{{ $item->map }}" readonly name="map" id="map" type="text" class="form-control toadobando"> <span class="input-group-btn"> <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modalMap">Chọn vị trí</button> </span> </div>
                            </div>
                        </div>

                        <div class="panel-form__row">
                            <label class="text mb-md-0">Đặc điểm nổi bật </label>
                            <div class="input">
                                <textarea name="highlight" id="highlight" rows="3" class="form-control">{{ $item->highlight }}</textarea>
                            </div>
                        </div>

                        <div class="panel-form__row">
                            <label class="text mb-md-0">Tình hình nội thất </label>
                            <div class="input">
                                <textarea name="interior_situation" id="interior_situation" rows="3" class="form-control">{{ $item->interior_situation }}</textarea>
                            </div>
                        </div>
                        <div class="panel-form__row">
                            <label class="text mb-md-0">Mô tả vị trí </label>
                            <div class="input">
                                <textarea name="location_description" id="location_description" rows="3" class="form-control">{{ $item->location_description }}</textarea>
                            </div>
                        </div>

                    </div>

                    <div class="block">
                        <h4 class="title-line"><span class="text"><i class="far fa-calendar-alt"></i>Lịch đăng tin</span></h4>

                        <div class="row row3">

                            <div class="col-12 col-md-4 col-xl-3 item">
                                <label class="d-block">Ngày bắt đầu</label>
                                <label class="select-box" style="background: #f5f5f5">
                                    <!-- <select name="" id="">
                                        <option value="">15/05/2015</option>
                                    </select>
                                    <i class="fa fa-angle-down"></i> -->
                                    <input type="text" name="start_day" value="{{ $item->start_day }}" class="form-control">
                                </label>
                            </div>
                            <div class="col-12 col-md-4 col-xl-3 item">
                                <label class="d-block">Ngày kết thúc</label>
                                <label class="select-box" style="background: #f5f5f5">
                                    <!-- <select name="" id="">
                                        <option value="">15/07/2015</option>
                                    </select>
                                    <i class="fa fa-angle-down"></i> -->
                                    <input type="text" name="end_day" value="{{ $item->end_day }}" class="form-control">
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="block">
                        <h4 class="title-line"><span class="text"><i class="fa fa-info-circle"></i>Giới thiệu</span></h4>

                        <div class="panel-form__row">
                            <label class="text mb-md-0">Nội dung tin đăng <span class="d-block cl_red">(*) Tối đa 3000 kí tự</span></label>
                            <div class="input">
                                <textarea name="short_content" id="short_content" rows="3" class="form-control">{{ $item->short_content }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="block border-top">
                        <div class="panel-form__row">
                            <label class="text mb-md-0">Hình ảnh <span class="d-block cl_red">(*) Tối đa 16 ảnh. Mỗi ảnh tối đa 2MB</span></label>
                            <div class="input d-flex flex-wrap img-upload__text">
                                <p class="description">Lựa chọn những hình ảnh phù hợp với nội dung bài đăng của bạn để đạt được hiệu quả tốt nhất.</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                            @if($item->avatar != null)
                                <img src="/public/img/upload/items/{{$item->avatar}}" alt="">
                            @endif
                            </div>
                            <div class="col-sm-9">
                                <div class="slick-cus panel-slider js-slider4 without-arrows">
                                    @foreach($item->item_album as $album)
                                    <div class="item div-album" data-id="{{$album->id}}">
                                        <figure class="img">
                                            <img src="/public/img/upload/itemalbums/{{$album->name}}" alt="">
                                        </figure>
                                        <button class="remove-album"><i class="far fa-trash-alt"></i> xóa</button>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <label class="d-block caption_img">Hình đại diện</label>
                                <form class="dropzone" id="avatar-project" action="/upload">
                                    <div class="dz-message "><i class="far fa-image"></i></div>
                                </form>
                            </div>
                            <div class="col-sm-9">
                                <label class="d-block caption_img">Album</label>
                                <form class="dropzone" id="album-project" action="/upload">
                                    <div class="dz-message"><i class="far fa-images"></i></div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="block">
                        <h4 class="title-line"><span class="text"><i class="fa fa-info-circle"></i>Tổng quan</span></h4>

                        <div class="row row3">
                            <div class="col-12 col-md-6 col-lg-12 col-xl-6 item">

                                <div class="panel-form__row style2">
                                    <label class="text mb-md-0">Diện tích</label>
                                    <div class="input">
                                        <input type="text" name="area" value="{{ $item->area }}" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row style2">
                                    <label class="text mb-md-0">Trạng thái</label>
                                    <div class="input">
                                        <input type="text" name="state" value="{{ $item->state }}" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row style2">
                                    <label class="text mb-md-0">Tháp</label>
                                    <div class="input">
                                        <input type="text" name="tower" value="{{ $item->tower }}" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-6 col-lg-12 col-xl-6 item">
                                <div class="panel-form__row style2">
                                    <label class="text mb-md-0">Mục đích</label>
                                    <div class="input">
                                        <input type="text" name="purpose" value="{{ $item->purpose }}" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row style2">
                                    <label class="text mb-md-0">Số phòng ngủ</label>
                                    <div class="input">
                                        <input type="text" name="number_bedroom" value="{{ $item->number_bedroom }}" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row style2">
                                    <label class="text mb-md-0">Số phòng tắm</label>
                                    <div class="input">
                                        <input type="text" name="number_bathroom" value="{{ $item->number_bathroom }}" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row style2">
                                    <label class="text mb-md-0">Giá</label>
                                    <div class="input">
                                        <input type="text" name="price" value="{{ $item->price }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="block">
                        <h4 class="title-line"><span class="text"><i class="fa fa-list-ul"></i>Tổng quan dự án</span></h4>

                        <div class="code-box-gid">
                            <section class="grid-demo content-store">
                                <div class="controls cf">
                                    <div class="control search">
                                        <div class="control-icon">
                                            <i class="material-icons">&#xE8B6;</i>
                                        </div>
                                        <input class="control-field search-field form-control " type="text" name="search" placeholder="Search..." />
                                    </div>
                                    <div class="control filter">
                                        <div class="control-icon">
                                            <i class="material-icons">&#xE152;</i>
                                        </div>
                                        <div class="select-arrow">
                                            <i class="material-icons">&#xE313;</i>
                                        </div>
                                        <select class="control-field filter-field form-control">
                                            <option value="" selected>All</option>
                                            <option value="red">Red</option>
                                            <option value="blue">Blue</option>
                                            <option value="green">Green</option>
                                        </select>
                                    </div>
                                    <div class="control sort">
                                        <div class="control-icon">
                                            <i class="material-icons">&#xE164;</i>
                                        </div>
                                        <div class="select-arrow">
                                            <i class="material-icons">&#xE313;</i>
                                        </div>
                                        <select class="control-field sort-field form-control">
                                            <option value="order" selected>Drag</option>
                                            <option value="title">Title (drag disabled)</option>
                                            <option value="color">Color (drag disabled)</option>
                                        </select>
                                    </div>
                                    <div class="control layout">
                                        <div class="control-icon">
                                            <i class="material-icons">&#xE871;</i>
                                        </div>
                                        <div class="select-arrow">
                                            <i class="material-icons">&#xE313;</i>
                                        </div>
                                        <select class="control-field layout-field form-control">
                                            <option value="left-top" selected>Left Top</option>
                                            <option value="left-top-fillgaps">Left Top (fill gaps)</option>
                                            <option value="right-top">Right Top</option>
                                            <option value="right-top-fillgaps">Right Top (fill gaps)</option>
                                            <option value="left-bottom">Left Bottom</option>
                                            <option value="left-bottom-fillgaps">Left Bottom (fill gaps)</option>
                                            <option value="right-bottom">Right Bottom</option>
                                            <option value="right-bottom-fillgaps">Right Bottom (fill gaps)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="grid load_tongquan">

                                </div>
                                <div class="grid-footer1">
                                    <button class="btn btn-primary themtq" type="button"><i class="fas fa-search-plus"></i> Thêm</button>
                                </div>
                                <div class="grid-footer">
                                    <button class="add-more-items btn btn-primary"><i class="fas fa-plus"></i> Add more items</button>
                                </div>
                            </section>
                        </div>
                    </div>

                    <div class="block">
                        <h4 class="title-line"><span class="text"><i class="fa fa-list-ul"></i>Tiện ích dự án</span></h4>

                        <div class="code-box-gid">
                            <section class="grid-demo2 content-store">
                                <div class="controls cf">
                                    <div class="control search">
                                        <div class="control-icon">
                                            <i class="material-icons">&#xE8B6;</i>
                                        </div>
                                        <input class="control-field search-field2 form-control " type="text" name="search" placeholder="Search..." />
                                    </div>
                                    <div class="control filter">
                                        <div class="control-icon">
                                            <i class="material-icons">&#xE152;</i>
                                        </div>
                                        <div class="select-arrow">
                                            <i class="material-icons">&#xE313;</i>
                                        </div>
                                        <select class="control-field filter2-field2 form-control">
                                            <option value="" selected>All</option>
                                            <option value="red">Red</option>
                                            <option value="blue">Blue</option>
                                            <option value="green">Green</option>
                                        </select>
                                    </div>
                                    <div class="control sort">
                                        <div class="control-icon">
                                            <i class="material-icons">&#xE164;</i>
                                        </div>
                                        <div class="select-arrow">
                                            <i class="material-icons">&#xE313;</i>
                                        </div>
                                        <select class="control-field sort-field2 form-control">
                                            <option value="order" selected>Drag</option>
                                            <option value="title">Title (drag disabled)</option>
                                            <option value="color">Color (drag disabled)</option>
                                        </select>
                                    </div>
                                    <div class="control layout">
                                        <div class="control-icon">
                                            <i class="material-icons">&#xE871;</i>
                                        </div>
                                        <div class="select-arrow">
                                            <i class="material-icons">&#xE313;</i>
                                        </div>
                                        <select class="control-field layout-field2 form-control">
                                            <option value="left-top" selected>Left Top</option>
                                            <option value="left-top-fillgaps">Left Top (fill gaps)</option>
                                            <option value="right-top">Right Top</option>
                                            <option value="right-top-fillgaps">Right Top (fill gaps)</option>
                                            <option value="left-bottom">Left Bottom</option>
                                            <option value="left-bottom-fillgaps">Left Bottom (fill gaps)</option>
                                            <option value="right-bottom">Right Bottom</option>
                                            <option value="right-bottom-fillgaps">Right Bottom (fill gaps)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="grid2 load_tienich">

                                </div>
                                <div class="grid-footer1">
                                    <button class="btn btn-primary themtienich" type="button"><i class="fas fa-search-plus"></i> Thêm</button>
                                </div>
                                <div class="grid-footer">
                                    <button class="add-more-items2 btn btn-primary"><i class="fas fa-plus"></i> Add more items</button>
                                </div>
                            </section>
                        </div>
                    </div>

                    <div class="block">
                        <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Liên hệ</span></h4>

                        <div class="panel-form__row">
                            <label class="text mb-md-0">Tên liên hệ</label>
                            <div class="input">
                                <input type="text" name="name_contact" value="{{ $item->name_contact }}" class="form-control">
                            </div>
                        </div>
                        <div class="panel-form__row">
                            <label class="text mb-md-0">Địa chỉ <span class="cl_red">(*)</span></label>
                            <div class="input">
                                <input type="text" name="address_contact" value="{{ $item->address_contact }}" class="form-control">
                            </div>
                        </div>
                        <div class="panel-form__row">
                            <label class="text mb-md-0">Di động</label>
                            <div class="input">
                                <input type="text" name="phone_contact" value="{{ $item->phone_contact }}" class="form-control">
                            </div>
                        </div>
                        <div class="panel-form__row">
                            <label class="text mb-md-0">Emai <span class="cl_red">(*)</span></label>
                            <div class="input">
                                <input type="text" name="email_contact" value="{{ $item->email_contact }}" class="form-control">

                                <label class="mt-2"><input type="checkbox"> Nhận email phản hồi</label>
                            </div>
                        </div>
                    </div>

                    <div class="panel-form--footer">
                        <button id="submit-project" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Đăng tin</span></button>
                    </div>
                <!-- </form> -->
            </section>
        </div>
    </div>
</div>
<script src="/public/assets/global/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript">
    Dropzone.autoDiscover = false;

    $('.remove-album').click(function() {
        var id = $(this).closest('.div-album').attr('data-id');
        $(this).closest('.div-album').remove();
        $('.block-page-all').addClass('active');
        $.ajax({
            url: '/ajax/remove_itemalbum',
            data: {id: id},
            type: 'POST',
            dataType: 'json',
            beforeSend: function(e) {
            },
            success: function(e) {
                $('.block-page-all').removeClass('active');
            }
        });
    });

    $('#submit-project').click(function() {

        var title = $("input[name=title]").val();
        if (title == '') {
            toastr.error('Vui lòng điền tiêu đề', null, {timeOut: 4000});
            return false;
        }

        var category_id = $('#category_id').find(":selected").val();
        if (category_id == undefined) {
            toastr.error('Vui lòng chọn danh mục', null, {timeOut: 4000});
            return false;
        }

        var id_project = $('#id_project').find(":selected").val();
        if (id_project == undefined) {
            toastr.error('Vui lòng chọn dự án', null, {timeOut: 4000});
            return false;
        }

        var slug = ChangeToSlug(title);
        var short_content = document.getElementById("short_content").value;
        var highlight = document.getElementById("highlight").value;
        var interior_situation = document.getElementById("interior_situation").value;
        var location_description = document.getElementById("location_description").value;
        var map = document.getElementById("map").value;
        var name_contact = $("input[name=name_contact]").val();
        var address_contact = $("input[name=address_contact]").val();
        var phone_contact = $("input[name=phone_contact]").val();
        var email_contact = $("input[name=email_contact]").val();

        var fd = new FormData();

        // project
        var files = [];
        files = files.concat(avatar_project.files.filter(function(e) {
            return e.accepted == true;
        }));

        for (var i = 0; i < files.length; i++) {
            if (files[i].constructor.name == 'File')
                fd.append('avatar', files[i]);
            else if (files[i].files && files[i].files[0] && files[i].files[0].constructor.name == 'File')
                fd.append('avatar', files[i].files[0]);
            else
                fd.append('avatar', 'uploaded');
        }

        //album
        var files = [];
        files = files.concat(album_project.files.filter(function(e) {
            return e.accepted == true;
        }));

        for (var i = 0; i < files.length; i++) {
            if (files[i].constructor.name == 'File')
                fd.append('files[' + i + ']', files[i]);
            else if (files[i].files && files[i].files[0] && files[i].files[0].constructor.name == 'File')
                fd.append('files[' + i + ']', files[i].files[0]);
            else
                fd.append('files[' + i + ']', 'uploaded');
        }

        // get tong quan
        var tongquans = [];
        $('.load_tongquan .card').each(function(index, el) {
            var vitri = $(this).find('.card-id').html();
            var id_tq = $(this).find('.card-id_tq').html();
            var data = $(this).find('.card-id_data i').attr('class');
            var title = $(this).find('.card-title').html();
            var title_en = $(this).find('.card-title_en').html();
            var value = $(this).find('.card-value').html();
            var value_en = $(this).find('.card-value_en').html();

            var item_tq = {
                vitri:vitri,
                id_tq:id_tq,
                data:data,
                title:title,
                title_en:title_en,
                value:value,
                value_en:value_en
            };
            tongquans.push(item_tq);
        });
        var jsontg = JSON.stringify(tongquans);

        // get tien ich
        var tienichs = [];
        $('.load_tienich .card').each(function(index, el) {
            var vitri = $(this).find('.card-id').html();
            var id_tq = $(this).find('.card-id_tq').html();
            var data = $(this).find('.card-id_data i').attr('class');
            var title = $(this).find('.card-title').html();
            var title_en = $(this).find('.card-title_en').html();

            var item_ti = {
                vitri:vitri,
                id_tq:id_tq,
                data:data,
                title:title,
                title_en:title_en,
            };
            tienichs.push(item_ti);
        });
        var jsonti = JSON.stringify(tienichs);

        var form = {};
        form['id'] = <?= $id ?>;
        form['title'] = title;
        form['id_project'] = id_project;
        form['category_id'] = category_id;
        form['type'] = $('#type').find(":selected").val();
        form['address'] = $("input[name=address]").val();
        form['start_day'] = $("input[name=start_day]").val();
        form['end_day'] = $("input[name=end_day]").val();

        form['area'] = $("input[name=area]").val();
        form['number_bedroom'] = $("input[name=number_bedroom]").val();
        form['number_bathroom'] = $("input[name=number_bathroom]").val();
        form['state'] = $("input[name=state]").val();
        form['tower'] = $("input[name=tower]").val();
        form['purpose'] = $("input[name=purpose]").val();

        form['price'] = $("input[name=price]").val();
        form['slug'] = slug;
        form['highlight'] = highlight;
        form['interior_situation'] = interior_situation;
        form['location_description'] = location_description;
        form['map'] = map;
        form['short_content'] = short_content;
        form['name_contact'] = name_contact;
        form['address_contact'] = address_contact;
        form['phone_contact'] = phone_contact;
        form['email_contact'] = email_contact;

        form['tongquans'] = jsontg;
        form['tienichs'] = jsonti;


        fd = objectToFormData(form, fd);
        $('.block-page-all').addClass('active');
        $.ajax({
            url: '/ajax/edit_item',
            type: 'POST',
            data: fd,
            contentType: false,
             processData: false,
            beforeSend: function(e) {
            },
            success: function(e) {
                switch (e.code) {
                    case 200:
                        toastr.success('Cập nhật thành công', null, {timeOut: 4000});
                        $('.block-page-all').removeClass('active');
                        window.location.href = "/dang-tin";
                        break;
                    default:
                        toastr.error(e.error, null, {timeOut: 4000});
                        $('.block-page-all').removeClass('active');
                        break;
                }
            }
        });
    });

    function objectToFormData(obj, form, namespace) {
        var fd = form || new FormData();
        var formKey;

        for(var property in obj) {
            if(obj.hasOwnProperty(property)) {
                if(namespace) {
                    formKey = namespace + '[' + property + ']';
                } else {
                    formKey = property;
                }
                if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                    objectToFormData(obj[property], fd, formKey);
                } else {
                    fd.append(formKey, obj[property]);
                }
            }
        }
        return fd;
    };

    function ChangeToSlug(title)
    {
        var slug;
        //Đổi chữ hoa thành chữ thường
        slug = title.toLowerCase();
        //Đổi ký tự có dấu thành không dấu
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        //Xóa các ký tự đặt biệt
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        //Đổi khoảng trắng thành ký tự gạch ngang
        slug = slug.replace(/ /gi, "-");
        //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
        //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
        slug = slug.replace(/\-\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-/gi, '-');
        slug = slug.replace(/\-\-/gi, '-');
        //Xóa các ký tự gạch ngang ở đầu và cuối
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        //In slug ra textbox có id “slug”
        return slug;
    }

    var avatar_project = new Dropzone('#avatar-project', {
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: '.jpeg,.jpg,.png,.gif',
        autoProcessQueue: false,
    });

    var album_project = new Dropzone('#album-project', {
        maxFiles: 10,
        addRemoveLinks: true,
        acceptedFiles: '.jpeg,.jpg,.png,.gif',
        autoProcessQueue: false,
    });


</script>

<!-- Modal add -->
<div id="modal_tongquan" class="modal list_store fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thông tin tổng quan</h4>
            </div>
            <div class="modal-body custom-list-data1">
                <table id="tbl-dataz" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
                    <thead>
                        <tr>
                            <th><label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" value="1" name="test" id="all_id_player"><span></span></label></th>
                            <th>id</th>
                            <th>data</th>
                            <th>title</th>
                            <th>title_en</th>
                            <th>value</th>
                            <th>value_en</th>
                        </tr>
                    </thead>

                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button onclick="addstore()" type="button" class="btn btn-primary">Thêm</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal add -->
<div id="modal_tienich" class="modal list_store fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thông tin tiện ích</h4>
            </div>
            <div class="modal-body custom-list-data1">
                <table id="tbl-dataz2" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
                    <thead>
                        <tr>
                            <th><label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" value="1" name="test" id="all_id_player2"><span></span></label></th>
                            <th>id</th>
                            <th>data</th>
                            <th>title</th>
                            <th>title_en</th>
                        </tr>
                    </thead>

                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button onclick="addstore2()" type="button" class="btn btn-primary">Thêm</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="modalMap" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Chọn vị trí bất động sản</h4>
            </div>

            <div class="modal-body">
                <input id="pac-input" class="controls" type="text" placeholder="Nhập địa chỉ cần tìm...">
                <div id="googleMap" style="width: 100%; height: 500px;"></div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-close width90" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                    <span>Đóng</span>
                </button>

                <button type="button" class="btn btn-primary width90 apply_geolocation">
                    <i class="fa fa-check"></i>
                    <span>Chọn</span>
                </button>
            </div>
        </div>

    </div>
</div>

<script src="/public/js/cmuuri.js"></script>

<script>
    var $scope = {};
    //-------------------------------------------------------------------------------
    $('#modalMap').on('shown.bs.modal', function() {
        // GOOGLE MAP
        var map = $('.toadobando').val();
        $scope.pointer = $scope.pointer || {};

        var geo = map.split(',');
        if (geo.length == 2) {
            $scope.pointer.latitude = geo[0];
            $scope.pointer.longitude = geo[1];
        }

        init_google_map($scope, {
            marker: $scope.marker,
            pointer: $scope.pointer,
        });
    });

    //----------------------------------------------------------------------------
    function init_google_map($scope, options) {
        if (!$scope.map) {
            var p = { lat: parseFloat(options.pointer.latitude), lng: parseFloat(options.pointer.longitude) };

            if (isNaN(options.pointer.latitude) || isNaN(options.pointer.longitude))
                p = { lat: 10.8230989, lng: 106.6296638 };

            $scope.map = new google.maps.Map($('#googleMap')[0], {
                zoom: 17,
                center: p,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            placeMarker(p);

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');

            var searchBox = new google.maps.places.SearchBox(input);

            $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        return;
                    }

                    placeMarker(place.geometry.location);

                    options.pointer.latitude = place.geometry.location.lat();
                    options.pointer.longitude = place.geometry.location.lng();
                    options.pointer.description = place.formatted_address;

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                $scope.map.fitBounds(bounds);
            });
        }
        google.maps.event.addListener($scope.map, 'click', function(event) {
            placeMarker(event.latLng);

            options.pointer.latitude = event.latLng.lat();
            options.pointer.longitude = event.latLng.lng();
        });

        function placeMarker(location) {
            if (options.marker == null) {
            options.marker = new google.maps.Marker({
                position: location,
                map: $scope.map
            });
            } else {
                options.marker.setPosition(location);
            }
        }
    };

    $('.apply_geolocation').click(function(event) {
        $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude)
        $("#modalMap").modal('toggle');
    });

</script> 
@endsection