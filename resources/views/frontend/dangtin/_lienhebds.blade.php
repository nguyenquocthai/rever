<table id="tbl-dataz" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
    <thead>
        <tr>
            <th class="text-uppercase">Khách hàng</th>
            <th class="text-uppercase">Tin đăng</th>
            <th class="text-uppercase">Số điện thoại</th>
            <th class="text-uppercase">Loại</th>
            <th class="text-uppercase">Ngày tạo</th>
            <th class="text-uppercase">Thao tác</th>
        </tr>
    </thead>
    <tbody class="load_seach">
        @if (count($customers) != 0)
            @foreach ($customers as $customer)
            <tr>

                <td>{{$customer->name_contact}}</td>
                <td>
                    <a href="/cho-thue/{{$customer->slug}}" title="">{{$customer->title}}</a>
                </td>
                <td>{{$customer->phone_contact}}</td>
                <td>
                    @if($customer->status == 1)
                    Xem nhà
                    @elseif($customer->status == 2)
                    Thương lượng giá
                    @elseif($customer->status == 3)
                    Hỏi thông tin
                    @else
                    Không xác định
                    @endif
                </td>
                <td>
                    <span class="d-inline-block"><span class="hidden">{{$customer->created_at}}</span>{{date('d/m/Y', strtotime($customer->created_at))}}</span>
                </td>

                <td>
                    <a data-id="{{@$customer->id}}" href="javascript:;" class="reset-btn btn-ctrl xem_lienhe">Xem</a>
                    <a href="javascript:;" class="reset-btn btn-ctrl delete_lienhe" data-id="{{@$customer->id}}"><i class="far fa-trash-alt"></i> Xóa</a>
                </td>
            </tr>
            @endforeach
        @else
        
        @endif
    </tbody>
</table>

<script>
    var datatable = $('#tbl-dataz').DataTable({
        order: [[ 4, 'desc' ]],
        columnDefs: [
            { sortable: false, searchable: false, targets: [ 5 ] },
        ],
        displayStart: 0,
        displayLength: 5,
        "autoWidth": false
    });
    datatable.search('').draw();
</script>