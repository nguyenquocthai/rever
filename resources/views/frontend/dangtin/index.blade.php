@extends('frontend.layouts.application')
@section('content')
<!--/head-block-->
    <div class="content-index bg-gray">
        <div class="container-fluid t-video-rp project-video without-video"></div>

        <div class="container">
            <div class="bg-white panel-wrap my-3 my-md-5">
                @include('frontend.elements.sidebar_dangtin')

                <section class="panel-content">
                    <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>Bảng thông tin</button>

                    <h2 class="title text-uppercase">Quản lý tin dự án</h2>

                    <nav class="tab-nav">
                        <ul class="tab-nav__list reset-list">
                            <li class="item"><a href="javascript:;" data-id="" class="item status_value d-block active">Tất cả</a></li>
                            <li class="item"><a href="javascript:;" data-id="0" class="item status_value d-block">Xuất bản</a></li>
                            <li class="item"><a href="javascript:;" data-id="2" class="item status_value d-block">Nháp</a></li>
                            <li class="item"><a href="javascript:;" data-id="1" class="item status_value d-block">Chờ duyệt</a></li>
                            <li class="item"><a href="javascript:;" data-id="3" class="item status_value d-block">Hết hạn</a></li>
                            <li class="item"><a href="javascript:;" data-id="4" class="item status_value d-block">Đã xóa</a></li>
                            <li class="item"><a href="javascript:;" data-id="5" class="item status_value d-block">Trả lại</a></li>
                        </ul>

                        <a href="/tao-dang-tin" class="btn-create"><button class="reset-btn btn-create text-uppercase"><i class="fa fa-plus-circle"></i>Đăng tin mới</button></a>
                    </nav>

                    <div class="tab-container">
                        <form id="form_search" class="bg-gray p-1 sale-form">
                            <input type="hidden" name="active" class="active_status">
                            <div class="row row3">
                                <div class="col-12 item">
                                    <div class="searchbox">
                                        <div class="searchbox-col">
                                            <div class="searchbox-col flex-100 searchbox-input datebox">
                                                <input name="updated_at" type="text" class="form-control ngaycapnhat" placeholder="Ngày cập nhật">
                                                <span class="reset-btn icon"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                        </div>
                                        <div class="searchbox-col">
                                            <input name="address" type="text" class="form-control" placeholder="Địa chỉ...">
                                        </div>
                                        <div class="searchbox-col flex-100 searchbox-input">
                                            <input name="title" type="text" class="form-control" placeholder="Tên dự án...">
                                            <button type="button" class="btn_search reset-btn icon"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase">Tin đăng</th>
                                        <th class="text-uppercase">Giá</th>
                                        <th class="text-uppercase">Diện tích</th>
                                        <th class="text-uppercase">Thời hạn</th>
                                        
                                        <th class="text-uppercase">Thao tác khác</th>
                                    </tr>
                                </thead>
                                <tbody class="load_seach">
                                    @include('frontend.dangtin._search')
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <script>
        var myDatepicker = $('.ngaycapnhat').datepicker({
            autoClose:true,
            dateFormat: 'dd/mm/yyyy',
            language: 'en'
        }).data('datepicker');

        //var myDatepicker = $('#my-elem').datepicker().data('datepicker');

        $('.btn_search').click(function(event) {
            search_action();
        });

        $('.datebox .reset-btn').click(function(event) {
            myDatepicker.show();
        });

        $('.status_value').click(function(event) {
            $('.status_value').removeClass('active');
            $(this).addClass('active');
            var id = $(this).attr('data-id');
            $('.active_status').val(id);
            search_action();
        });

        function search_action() {
            var data = {};
            $("#form_search").serializeArray().map(function(x){data[x.name] = x.value;});
            $('.block-page-all').addClass('active');
            $.ajax({
                type: 'POST',
                url: '/api_search',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                },
                success: function(result) {
                    $('.load_seach').html(result.value);
                    $('.block-page-all').removeClass('active');
                }
            });
        }
    </script>
@endsection