<!DOCTYPE html>
<html>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyCQiaMU8DTCCDi90sT1ApqUYM737YYXymU&libraries=geometry,places" async></script>
    @include('Backend.Elements.head')
    
    <script src="/public/assets/global/plugins/jquery.min.js"></script>
    @include('frontend.dangtin._head')
    <style>
        #dangtin .error,
        #create_item .error {
          display: none !important; }
        #dangtin .error_select,
        #create_item .error_select {
          border-color: red !important; }
        #dangtin .form-control,
        #create_item .form-control {
          border: 1px solid #ddd; }
          #dangtin .form-control.error_input,
          #create_item .form-control.error_input {
            border-color: red; }
    </style>
    
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            @include('Backend.Elements.header')

                <!-- BEGIN CONTAINER -->
                <div class="page-container">
                    @include('Backend.Elements.sidebar')
                    <!-- BEGIN CONTENT -->
                    <div class="page-content-wrapper">
                        <div class="page-content">
                            <div class="portlet light bordered hinhanh_box">
                                <div class="portlet-title">
                                    <div class="caption font-red-sunglo">
                                        <i class="icon-settings font-red-sunglo"></i>
                                        <span class="caption-subject bold uppercase">Thông tin khởi tạo</span>
                                    </div>
                                    <div class="actions box_lang_muti"></div>
                                </div>
                                <div class="portlet-body form">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col">Mục tiêu</th>
                                                <th scope="col">Nội dung</th>
                                                <th scope="col">Tình trạng</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Tên bất động sản</td>
                                                <td>{{$item_project->title}}</td>
                                                <td>
                                                    @if($item_project->title != null) 
                                                    <i class="fas fa-check"></i>
                                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Danh mục</td>
                                                <td>{{$item_project->catname}}</td>
                                                <td>
                                                    @if($item_project->category_id != 0) 
                                                    <i class="fas fa-check"></i>
                                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Tỉnh / Thành phố</td>
                                                <td>{{$item_project->tinhthanh}}</td>
                                                <td>@if($item_project->tinhthanh != "") 
                                                    <i class="fas fa-check"></i>
                                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Quận / Huyện</td>
                                                <td>{{$item_project->quanhuyen}}</td>
                                                <td>@if($item_project->quanhuyen != "") 
                                                    <i class="fas fa-check"></i>
                                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Bản đồ</td>
                                                <td>{{$item_project->map}}</td>
                                                <td>@if($item_project->map != "") 
                                                    <i class="fas fa-check"></i>
                                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Địa chỉ</td>
                                                <td>{{$item_project->address}}</td>
                                                <td>@if($item_project->address != "") 
                                                    <i class="fas fa-check"></i>
                                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Mô tả ngắn</td>
                                                <td>{!!$item_project->short_content!!}</td>
                                                <td>@if($item_project->short_content != "") 
                                                    <i class="fas fa-check"></i>
                                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Giá thuê</td>
                                                <td>
                                                    @if($item_project->allprice != 0)
                                                    {{$item_project->allprice_mark}} / tháng
                                                    @else
                                                    {{$item_project->allprice_mark}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($item_project->allprice_mark != 'null') 
                                                    <i class="fas fa-check"></i>
                                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Hình đại diện</td>
                                                <td><a target="_blank" href="{{BladeGeneral::GetImg(['avatar' => $item_project->avatar,'data' => 'item', 'time' => $item_project->updated_at])}}" title="">
                                                    <img width="100" class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $item_project->avatar,'data' => 'item_project', 'time' => $item_project->updated_at])}}" alt="...">
                                                </a></td>
                                                <td>
                                                    @if($item_project->avatar != null) 
                                                    <i class="fas fa-check"></i>
                                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Album hình</td>
                                                <td><a target="_blank" href="/tao-tin-thue-b2/{{$item_project->id}}" title="">Có {{$item_project->album_count}} hình trong album</a></td>
                                                <td>
                                                    @if($item_project->album_count > 4) 
                                                    <i class="fas fa-check"></i>
                                                    @else <i class="fas fa-exclamation-triangle"></i> Tối thiểu 5 hình
                                                    @endif
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            
                            <div class="panel-form--footer">
                                <a href="/tao-tin-thue-b2/{{$item_project->id}}" class="btn btn-primary">Quay lại</a>
                                @if($item_project->step != 3)
                                <button data-id="{{$item_project->id}}" type="button" id="submit-item_project" class="btn btn-success">Hoàn thành</button>
                                @else
                                <a href="/admin/tin-thue" class="btn btn-success">Danh sách thuê</a>
                                @endif
                            </div>
                            
                        </div>

                    </div>
                    <!-- END CONTENTx -->

                </div>
                <!-- END CONTAINER -->

            @include('Backend.Elements.footer')
        </div>
    </body>
    @include('frontend.dangtin.admin._food')

    <script>
        $('#submit-item_project').click(function(event) {
            var data = {};
            showPageLoading();
            data['status_code'] = 'submit_item_project';
            data['id'] = $(this).attr('data-id');
            $.ajax({
                type: 'POST',
                url: '/api_crate_item',
                data: data,
                dataType: 'json',
                error: function(){
                    hidePageLoading();
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        hidePageLoading();
                        return false
                    }
                    document.location.href = '/tao-tin-thue-b3/'+result.id;
                }
            });
        });
    </script>
</html>