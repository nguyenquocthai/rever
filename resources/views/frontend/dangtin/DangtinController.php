<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\ItemProject;
use App\Model\Project;
use App\Model\ProjectAlbums;
use App\Model\AlbumFlats;
use App\Model\Product_cat;
use App\Model\Product_cat1;
use App\Exceptions\SessionUser;
use App\Exceptions\ErrorCodes;
use App\Extensions\ShopCommon;
use App\Extensions\ShopUpload;
use App\Extensions\UploadMany;
use App\Model\yeuthichs;
use App\Model\Nguoidung;
use App\Model\img_flats;
use App\Model\ItemAlbum;
use App\Model\User;
use Illuminate\Http\Request;
use Session;

class DangtinController extends BaseController
{

    public function __construct()
    {
        $this->boot();

        $this->middleware(function ($request, $next) {
            if(session('userSession') == null) {
                return redirect('/');
            }

            $nguoidung = DB::table('nguoidungs')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', session('userSession')->id]
                ])
                ->first();
            View::share('nguoidung', $nguoidung);
            return $next($request);
        });

        $phongngu = [1,2,3,4,5,6,7,8,9,10];
        View::share('phongngu', $phongngu);

        $phuonghuongs = ['Đông','Tây','Nam','Bắc','Đông Đông Bắc','Đông Đông Nam','Tây Tây Bắc','Tây Tây Nam','Bắc Đông Bắc','Bắc Tây Bắc','Nam Đông Nam','Nam Tây Nam'];
        View::share('phuonghuongs', $phuonghuongs);
        View::share('active_nguoidung', 'active');

        $categories = Product_cat1::where([
            ['del_flg', '=', 0],
            ['type', '=', 0]
        ])->orderBy('id', 'desc')->get();
        View::share('categories', $categories);

        $categorie_bans = Product_cat1::where([
            ['del_flg', '=', 0],
            ['type', '=', 1]
        ])->orderBy('id', 'desc')->get();
        View::share('categorie_bans', $categorie_bans);

        $provinces = DB::table('za_provinces')
            ->select('*')
            ->orderBy('id', 'asc')
            ->get();
        View::share('provinces', $provinces);

        $projects = DB::table('projects')
            ->select('id', 'title')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        View::share('projects', $projects);
    }

    public function index()
    {
        if(session('userSession')){

           $projects = Project::where([
                ['user_id', '=', session('userSession')->id],
                ['del_flg', '=', 0],
            ])->orderBy('id', 'desc')->get();

            foreach ($projects as $key => $value) {
                $product_cat = DB::table('product_cats')
                     ->select('title')
                     ->where([
                         ['del_flg', '=', 0],
                         ['id', '=', $value->category_id]
                     ])
                     ->first(); 
                $projects[$key]->catname = $product_cat->title;
            }
            return view('frontend.dangtin.index')->with(compact('projects'));
        }
        elseif(session('userSession') == null) {
            return redirect('/');
        }

    }

    public function dangtinthue()
    {
        if(session('userSession')){
            $item_projects = ItemProject::where([
                ['user_id', '=', session('userSession')->id],
                ['del_flg', '=', 0],
                ['type', '=', 0],
                ['active', '!=', 4],
            ])->orderBy('id', 'desc')->get();

            $projects = DB::table('projects')
                ->select('id', 'title')
                ->where([
                    ['del_flg', '=', 0]
                ])
                ->orderBy('id', 'desc')
                ->get();

            foreach ($item_projects as $key => $value) {
                $product_cat1 = DB::table('product_cat1s')
                     ->select('title')
                     ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $value->category_id]
                     ])
                     ->first(); 

                if ($value->fromprice == null || $value->toprice == null) {
                    $item_projects[$key]->price = $value->fromprice.$value->toprice.' tr/tháng';
                }
                if($value->fromprice == null && $value->toprice == null){
                    $item_projects[$key]->price = "null";
                }
                if($value->fromprice != null && $value->toprice != null) {
                    $item_projects[$key]->price = $value->fromprice.' - '.$value->toprice.' tr/tháng';
                }
                $item_projects[$key]->catname = $product_cat1->title;
            }

            $product_cat1s = DB::table('product_cat1s')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['type', '=', 0]
                ])
                ->orderBy('id', 'desc')
                ->get();

            $dangtinthue = "active";

            return view('frontend.dangtin.dangtinthue')->with(compact('item_projects', 'projects', 'product_cat1s', 'dangtinthue'));
        }
        elseif(session('userSession') == null) {
            return redirect('/');
        }
    }

    public function dangtinban()
    {
        if(session('userSession')){
            $item_projects = ItemProject::where([
                ['user_id', '=', session('userSession')->id],
                ['del_flg', '=', 0],
                ['type', '=', 1],
                ['active', '!=', 4],
            ])->orderBy('id', 'desc')->get();

            $projects = DB::table('projects')
                ->select('id', 'title')
                ->where([
                    ['del_flg', '=', 0]
                ])
                ->orderBy('id', 'desc')
                ->get();

            $product_cat1s = DB::table('product_cat1s')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['type', '=', 1]
                ])
                ->orderBy('id', 'desc')
                ->get();


            $dangtinban = "active";
            return view('frontend.dangtin.dangtinban')->with(compact('item_projects', 'projects', 'product_cat1s', 'dangtinban'));
        }
        elseif(session('userSession') == null) {
            return redirect('/');
        }
    }

    public function lienhebds(Request $request)
    {
        if(session('userSession')){

            $customers = DB::table('customers')
                ->join('item_projects', 'item_projects.id', '=', 'customers.id_itemproject')
                ->select('customers.*', 'item_projects.title', 'item_projects.slug')
                ->where([
                    ['customers.del_flg', '=', 0],
                    ['customers.user_id', '=', session('userSession')->id]
                ])
                ->orderBy('customers.id', 'desc')
                ->get();

            $lienhebds = "active";
            return view('frontend.dangtin.lienhebds')->with(compact('lienhebds', 'customers'));
        }
        elseif(session('userSession') == null) {
            return redirect('/');
        }
    }

    //-------------------------------------------------------------------------------
    public function api_lienhebds(Request $request)
    {
        try {
            $customer = DB::table('customers')
                ->join('item_projects', 'item_projects.id', '=', 'customers.id_itemproject')
                ->select('customers.*', 'item_projects.title', 'item_projects.slug')
                ->where([
                    ['customers.del_flg', '=', 0],
                    ['customers.id', '=', $request->id],
                ])
                ->first();

            $view = View::make('frontend/dangtin/_lienhebds_detail', ['customer' => $customer]);

            $data['code'] = 200;
            $data['data'] = $view->render();
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    public function create_project()
    {
        $check_user = $this->check_user();
        if ($check_user['code'] == 300) {
            return redirect('/admin/login');
        }
        $user_find = $check_user['value'];
        $admin_id = $user_find->id;

        $categories = Product_cat::where([
            ['del_flg', '=', 0],
        ])->orderBy('id', 'desc')->get();

        $provinces = DB::table('za_provinces')
            ->select('*')
            ->orderBy('id', 'asc')
            ->get();

        return view('frontend.dangtin.create')->with(compact('categories', 'provinces'));
    }

    public function create_project_b1($id)
    {
        $check_user = $this->check_user();
        if ($check_user['code'] == 300) {
            return redirect('/admin/login');
        }
        $user_find = $check_user['value'];
        $admin_id = $user_find->id;

        $project = DB::table('projects')
            ->select('*')
            ->where([
                ['id', '=', $id],
                ['user_id', '=', $admin_id]
            ])
            ->first();

        if(!$project) {
            return redirect('/404');
        }

        $categories = Product_cat::where([
            ['del_flg', '=', 0],
        ])->orderBy('id', 'desc')->get();

        $provinces = DB::table('za_provinces')
            ->select('*')
            ->orderBy('id', 'asc')
            ->get();

        $districts = DB::table('zb_districts')
            ->select('*')
            ->where([
                ['id', '=', $project->tinhthanh]
            ])
            ->orderBy('id', 'asc')
            ->get();

        $b1 = "active";

        return view('frontend.dangtin.create_b1')->with(compact('categories', 'provinces', 'project', 'districts', 'b1'));
    }

    public function create_project_b2($id)
    {
        $check_user = $this->check_user();
        if ($check_user['code'] == 300) {
            return redirect('/admin/login');
        }
        $user_find = $check_user['value'];
        $admin_id = $user_find->id;

        $project = DB::table('projects')
            ->select('*')
            ->where([
                ['id', '=', $id],
                ['user_id', '=', $admin_id]
            ])
            ->first();

        if(!$project) {
            return redirect('/404');
        }

        $categories = Product_cat::where([
            ['del_flg', '=', 0],
        ])->orderBy('id', 'desc')->get();

        $provinces = DB::table('province')
            ->select('*')
            ->orderBy('provinceid', 'asc')
            ->get();

        $project_albums = DB::table('project_albums')
            ->select('*')
            ->where([
                ['project_id', '=', $id]
            ])
            ->orderBy('id', 'desc')
            ->get();

        $album_flats = DB::table('album_flats')
            ->select('*')
            ->where([
                ['project_id', '=', $id]
            ])
            ->orderBy('id', 'desc')
            ->get();

        foreach ($album_flats as $key => $value) {

            $count_img_flats = DB::table('img_flats')
                ->where([
                    ['album_flat_id', '=', $value->id]
                ])
                ->count();

            $album_flats[$key]->count_album = $count_img_flats;
        }

        $b2 = "active";

        return view('frontend.dangtin.create_b2')->with(compact('categories', 'provinces', 'project', 'b2', 'project_albums', 'album_flats'));
    }

    public function create_project_b3($id)
    {
        $check_user = $this->check_user();
        if ($check_user['code'] == 300) {
            return redirect('/admin/login');
        }
        $user_find = $check_user['value'];
        $admin_id = $user_find->id;

        $project = DB::table('projects')
            ->select('*')
            ->where([
                ['id', '=', $id],
                ['user_id', '=', $admin_id]
            ])
            ->first();

        if(!$project) {
            return redirect('/404');
        }

        if ($project->fromprice == null || $project->toprice == null) {
            $project->price = $project->fromprice.$project->toprice.' tr/m<sup>2</sup>';
        }
        if($project->fromprice == null && $project->toprice == null){
            $project->price = "null";
        }
        if($project->fromprice != null && $project->toprice != null) {
            $project->price = $project->fromprice.' - '.$project->toprice.' tr/m<sup>2</sup>';
        }

        if ($project->thue_fromprice == null || $project->thue_toprice == null) {
            $project->thue_price = $project->thue_fromprice.$project->thue_toprice.' tr/tháng';
        }
        if($project->thue_fromprice == null && $project->thue_toprice == null){
            $project->thue_price = "null";
        }
        if($project->thue_fromprice != null && $project->thue_toprice != null) {
            $project->thue_price = $project->thue_fromprice.' - '.$project->thue_toprice.' tr/tháng';
        }

        if ($project->email_flg == 0) {
            $project->email_flg = "Không";
        } else {
            $project->email_flg = "Có";
        }

        $product_cat = DB::table('product_cats')
            ->select('title')
            ->where([
                ['id', '=', $project->category_id]
            ])
            ->first();
        $project->catname = @$product_cat->title;

        $province = DB::table('province')
            ->select('name')
            ->where([
                ['provinceid', '=', $project->tinhthanh]
            ])
            ->first();
        $project->tinhthanh = @$province->name;

        $district = DB::table('district')
            ->select('name')
            ->where([
                ['districtid', '=', $project->quanhuyen]
            ])
            ->first();
        $project->quanhuyen = @$district->name;

        $album_flats = DB::table('album_flats')
            ->select('id')
            ->where([
                ['project_id', '=', $project->id]
            ])
            ->count();
        $project->flats = $album_flats;

        $project_albums = DB::table('project_albums')
            ->select('id')
            ->where([
                ['project_id', '=', $project->id]
            ])
            ->count();
        $project->album_count = $project_albums;

        $tongquans = DB::table('tongquans')
            ->select('id')
            ->where([
                ['id_product', '=', $project->id],
                ['id_group', '=', 3]
            ])
            ->count();
        $project->tongquans = $tongquans;

        $tienichs = DB::table('tienichs')
            ->select('id')
            ->where([
                ['id_product', '=', $project->id],
                ['id_group', '=', 3]
            ])
            ->count();
        $project->tienichs = $tienichs;

        if(!$project) {
            return redirect('/404');
        }

        $categories = Product_cat::where([
            ['del_flg', '=', 0],
        ])->orderBy('id', 'desc')->get();

        $provinces = DB::table('province')
            ->select('*')
            ->orderBy('provinceid', 'asc')
            ->get();

        $b3 = "active";

        return view('frontend.dangtin.create_b3')->with(compact('categories', 'provinces', 'project', 'b3'));
    }

    public function create_item()
    {
        $dangtinthue = "active";
        return view('frontend.dangtin.create_item')->with(compact('dangtinthue'));
    }

    public function create_item_b1($id)
    {

        $item_project = DB::table('item_projects')
            ->select('*')
            ->where([
                ['id', '=', $id],
                ['user_id', '=', session('userSession')->id]
            ])
            ->first();

        if(!$item_project) {
            return redirect('/404');
        }

        $districts = DB::table('zb_districts')
            ->select('*')
            ->where([
                ['province_id', '=', $item_project->tinhthanh]
            ])
            ->orderBy('id', 'asc')
            ->get();

        $wards = DB::table('zc_wards')
            ->select('*')
            ->where([
                ['district_id', '=', $item_project->quanhuyen]
            ])
            ->orderBy('id', 'asc')
            ->get();

        $streets = DB::table('zd_streets')
            ->select('*')
            ->where([
                ['district_id', '=', $item_project->quanhuyen]
            ])
            ->orderBy('id', 'asc')
            ->get();

        $b1 = "active";
        $dangtinthue = "active";

        return view('frontend.dangtin.create_item_b1')->with(compact('item_project', 'districts', 'b1', 'dangtinthue', 'wards', 'streets'));
    }

    public function create_item_b2($id)
    {
        if(session('userSession') == null) {
            return redirect('/');
        }

        $item_project = DB::table('item_projects')
            ->select('*')
            ->where([
                ['id', '=', $id],
                ['user_id', '=', session('userSession')->id]
            ])
            ->first();

        if(!$item_project) {
            return redirect('/404');
        }

        $item_albums = DB::table('item_albums')
            ->select('*')
            ->where([
                ['item_id', '=', $id],
            ])
            ->orderBy('id', 'desc')
            ->get();

        $thongtin = json_decode($item_project->thongtin);

        $b2 = "active";
        $dangtinthue = "active";

        return view('frontend.dangtin.create_item_b2')->with(compact('item_project', 'b2', 'item_albums', 'dangtinthue', 'thongtin'));
    }

    public function create_item_b3($id)
    {
        $item_project = DB::table('item_projects')
            ->select('*')
            ->where([
                ['id', '=', $id],
                ['user_id', '=', session('userSession')->id]
            ])
            ->first();

        if(!$item_project) {
            return redirect('/404');
        }

        $product_cat = DB::table('product_cats')
            ->select('title')
            ->where([
                ['id', '=', $item_project->category_id]
            ])
            ->first();
        $item_project->catname = @$product_cat->title;

        $province = DB::table('za_provinces')
            ->select('name')
            ->where([
                ['id', '=', $item_project->tinhthanh]
            ])
            ->first();
        $item_project->tinhthanh = @$province->name;

        $district = DB::table('zb_districts')
            ->select('name')
            ->where([
                ['id', '=', $item_project->quanhuyen]
            ])
            ->first();
        $item_project->quanhuyen = @$district->name;

        $item_albums = DB::table('item_albums')
            ->select('id')
            ->where([
                ['item_id', '=', $item_project->id]
            ])
            ->count();
        $item_project->album_count = $item_albums;

        $categories = Product_cat::where([
            ['del_flg', '=', 0],
        ])->orderBy('id', 'desc')->get();

        $nguoidung = DB::table('nguoidungs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', session('userSession')->id]
            ])
            ->first();


        $provinces = DB::table('za_provinces')
            ->select('*')
            ->orderBy('id', 'asc')
            ->get();

        $b3 = "active";
        $dangtinthue = "active";

        return view('frontend.dangtin.create_item_b3')->with(compact('categories', 'nguoidung', 'provinces', 'item_project', 'b3', 'dangtinthue'));
    }

    public function create_item_ban()
    {
        if(session('userSession') == null) {
            return redirect('/');
        }

        $nguoidung = DB::table('nguoidungs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', session('userSession')->id]
            ])
            ->first();

        $dangtinban = "active";

        return view('frontend.dangtin.create_item_ban')->with(compact('nguoidung', 'dangtinban'));
    }

    public function create_item_ban_b1($id)
    {
        $item_project = DB::table('item_projects')
            ->select('*')
            ->where([
                ['id', '=', $id],
                ['user_id', '=', session('userSession')->id]
            ])
            ->first();

        if(!$item_project) {
            return redirect('/404');
        }

        $districts = DB::table('zb_districts')
            ->select('*')
            ->where([
                ['province_id', '=', $item_project->tinhthanh]
            ])
            ->orderBy('id', 'asc')
            ->get();

        $wards = DB::table('zc_wards')
            ->select('*')
            ->where([
                ['district_id', '=', $item_project->quanhuyen]
            ])
            ->orderBy('id', 'asc')
            ->get();

        $streets = DB::table('zd_streets')
            ->select('*')
            ->where([
                ['district_id', '=', $item_project->quanhuyen]
            ])
            ->orderBy('id', 'asc')
            ->get();

        $b1 = "active";
        $dangtinban = "active";

        return view('frontend.dangtin.create_item_ban_b1')->with(compact('item_project', 'districts', 'b1', 'dangtinban', 'wards', 'streets'));
    }

    public function create_item_ban_b2($id)
    {
        if(session('userSession') == null) {
            return redirect('/');
        }

        $item_project = DB::table('item_projects')
            ->select('*')
            ->where([
                ['id', '=', $id],
                ['user_id', '=', session('userSession')->id]
            ])
            ->first();

        if(!$item_project) {
            return redirect('/404');
        }

        $item_albums = DB::table('item_albums')
            ->select('*')
            ->where([
                ['item_id', '=', $id],
            ])
            ->orderBy('id', 'desc')
            ->get();

        $b2 = "active";
        $dangtinban = "active";

        $thongtin = json_decode($item_project->thongtin);

        return view('frontend.dangtin.create_item_ban_b2')->with(compact('item_project', 'b2', 'item_albums', 'dangtinban', 'thongtin'));
    }

    public function create_item_ban_b3($id)
    {
        $item_project = DB::table('item_projects')
            ->select('*')
            ->where([
                ['id', '=', $id],
                ['user_id', '=', session('userSession')->id]
            ])
            ->first();

        if(!$item_project) {
            return redirect('/404');
        }

        $product_cat = DB::table('product_cats')
            ->select('title')
            ->where([
                ['id', '=', $item_project->category_id]
            ])
            ->first();
        $item_project->catname = @$product_cat->title;

        $province = DB::table('za_provinces')
            ->select('name')
            ->where([
                ['id', '=', $item_project->tinhthanh]
            ])
            ->first();
        $item_project->tinhthanh = @$province->name;

        $district = DB::table('zb_districts')
            ->select('name')
            ->where([
                ['id', '=', $item_project->quanhuyen]
            ])
            ->first();
        $item_project->quanhuyen = @$district->name;

        $item_albums = DB::table('item_albums')
            ->select('id')
            ->where([
                ['item_id', '=', $item_project->id]
            ])
            ->count();
        $item_project->album_count = $item_albums;

        $categories = Product_cat::where([
            ['del_flg', '=', 0],
        ])->orderBy('id', 'desc')->get();

        $nguoidung = DB::table('nguoidungs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', session('userSession')->id]
            ])
            ->first();


        $provinces = DB::table('za_provinces')
            ->select('*')
            ->orderBy('id', 'asc')
            ->get();

        $b3 = "active";
        $dangtinban = "active";

        return view('frontend.dangtin.create_item_ban_b3')->with(compact('categories', 'nguoidung', 'provinces', 'item_project', 'b3', 'dangtinban'));
    }

    public function edit_item($id)
    {
        $item = ItemProject::where([
            ['user_id', '=', @session('userSession')->id],
            ['id', '=', $id],
            ['del_flg', '=', 0],
        ])->with(['item_album'=> function ($item_album) {
            $item_album->where('del_flg', 0);
        }])->first();

        if(!$item) return redirect('/');

        $projects = Project::where([
            ['user_id', '=', @session('userSession')->id],
            ['del_flg', '=', 0],
        ])->orderBy('id', 'desc')->get();

        $categories = Product_cat1::where([
            ['del_flg', '=', 0],
        ])->orderBy('id', 'desc')->get();

        $id_item = $item->id;

        $id_project_item = $item->id;

        return view('frontend.dangtin.edit_item')->with(compact('item', 'id', 'id_item', 'projects', 'id_project_item', 'categories'));
    }

    public function edit($slug)
    {
        $project = Project::where([
            ['id', '=', $slug],
            ['del_flg', '=', 0],
            ['user_id', '=', @session('userSession')->id]
        ])->with(['project_album'=> function ($project_album) {
            $project_album->where('del_flg', 0);
        }])->with(['album_flat'=> function ($album_flat) {
            $album_flat->where('del_flg', 0);
        }])->first();

        if(!$project) return redirect('/');

        $categories = Product_cat::where([
            ['del_flg', '=', 0],
        ])->orderBy('id', 'desc')->get();

        $id_project = $project->id;

        return view('frontend.dangtin.edit')->with(compact('project', 'slug', 'id_project', 'categories'));
    }

    public function profile()
    {
        if(session('userSession') == null) {
            return redirect('/');
        }

        $nguoidung = DB::table('nguoidungs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', session('userSession')->id]
            ])
            ->first();

        if (!$nguoidung) {
            session()->forget('userSession');
            return redirect('/404');
        }

        return view('frontend.dangtin.profile')->with(compact('nguoidung'));
    }

    public function changepass()
    {
        if(session('userSession') == null) {
            return redirect('/');
        }

        $nguoidung = DB::table('nguoidungs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', session('userSession')->id]
            ])
            ->first();

        return view('frontend.dangtin.changepass')->with(compact('nguoidung'));
    }

    public function delete ($id) {
        $data = [];

        $project = Project::where('id', $id)->update(['del_flg' => 1]);

        $data['code'] = 200;
        return response()->json($data, 200);

    }

    public function deletetinthue ($id) {
        $data = [];

        $project = ItemProject::where('id', $id)->update(['del_flg' => 1]);

        $data['code'] = 200;
        return response()->json($data, 200);
    }

    public function get_tq_ti($id)
    {
        $tongquans = DB::table('tongquans')
            ->select('*')
            ->where([
                ['id_product', '=', $id],
                ['id_group', '=', 3]
            ])
            ->orderBy('vitri', 'asc')
            ->get();

        $tienichs = DB::table('tienichs')
            ->select('*')
            ->where([
                ['id_product', '=', $id],
                ['id_group', '=', 3]
            ])
            ->orderBy('vitri', 'asc')
            ->get();

        $data['tongquans'] = $tongquans;
        $data['tienichs'] = $tienichs;
        return response()->json($data, 200);
    }

    public function get_tq_ti_item($id)
    {
        $tongquans = DB::table('tongquans')
            ->select('*')
            ->where([
                ['id_product', '=', $id],
                ['id_group', '=', 4]
            ])
            ->orderBy('vitri', 'asc')
            ->get();

        $tienichs = DB::table('tienichs')
            ->select('*')
            ->where([
                ['id_product', '=', $id],
                ['id_group', '=', 4]
            ])
            ->orderBy('vitri', 'asc')
            ->get();

        $data['tongquans'] = $tongquans;
        $data['tienichs'] = $tienichs;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function yeuthich()
    {
        if(session('userSession') == null) {
            return redirect('/');
        }

        $projects = DB::table('projects')
            ->join('yeuthichs', 'projects.id', '=', 'yeuthichs.idsp')
            ->select('projects.*')
            ->where([
                ['projects.del_flg', '=', 0],
                ['yeuthichs.iduser', '=', session('userSession')->id],
            ])
            ->get();

        return view('frontend.dangtin.yeuthich')->with(compact('projects'));
    }

    //-------------------------------------------------------------------------------
    public function yeuthich2()
    {
        if(session('userSession') == null) {
            return redirect('/');
        }

        $item_projects = DB::table('item_projects')
            ->join('yeuthichs', 'item_projects.id', '=', 'yeuthichs.idsp')
            ->select('item_projects.*')
            ->where([
                ['item_projects.del_flg', '=', 0],
                ['yeuthichs.iduser', '=', session('userSession')->id],
            ])
            ->get();

        return view('frontend.dangtin.yeuthich2')->with(compact('item_projects'));
    }

    //-------------------------------------------------------------------------------
    public function api_search(Request $request)
    {
        try {

            if(session('userSession') == null) {
                $data['code'] = 300;
                $data['error'] = 'Yêu cầu đăng nhập';
                return response()->json($data, 200);
            }

            if ($request->status_code == "chothue") {
                $where = [
                    ['del_flg', '=', 0],
                    ['user_id', '=', session('userSession')->id]
                ];

                if ($request->id_project != "") {
                    $where[] = ['id_project', '=', $request->id_project];
                }

                if ($request->category_id != "") {
                    $where[] = ['category_id', '=', $request->category_id];
                }

                if ($request->type != "") {
                    $where[] = ['type', '=', $request->type];
                }

                if ($request->active != "") {
                    $where[] = ['active', '=', $request->active];
                } else {
                    $where[] = ['active', '!=', 4];
                }

                if ($request->title != "") {
                    $where[] = ['title', 'like', '%' . $request->title . '%'];
                }

                if ($request->updated_at != "") {
                    $str = $request->updated_at;
                    $date = date_create_from_format('d/m/Y', $str);
                    $date2 =  $date->format('Y-m-d');

                    $where[] = ['updated_at', 'like', '%' . $date2 . '%'];
                }

                if ($request->address != "") {
                    $where[] = ['address', 'like', '%' . $request->address . '%'];
                }

                $item_projects = DB::table('item_projects')
                    ->select('*')
                    ->where($where)
                    ->orderBy('id', 'desc')
                    ->get();

                $view = View::make('frontend/dangtin/_searchchothue', ['item_projects' => $item_projects]);
                $value = $view->render();

                $data['code'] = 200;
                $data['value'] = $value;
                $data['message'] = 'Lưu thành công';
                return response()->json($data, 200);
            }

            $where = [
                ['del_flg', '=', 0]
            ];

            if ($request->active != "") {
                $where[] = ['active', '=', $request->active];
            }

            if ($request->title != "") {
                $where[] = ['title', 'like', '%' . $request->title . '%'];
            }

            if ($request->updated_at != "") {
                $str = $request->updated_at;
                $date = date_create_from_format('d/m/Y', $str);
                $date2 =  $date->format('Y-m-d');

                $where[] = ['updated_at', 'like', '%' . $date2 . '%'];
            }

            if ($request->address != "") {
                $where[] = ['address', 'like', '%' . $request->address . '%'];
            }

            

            $projects = DB::table('projects')
                ->select('*')
                ->where($where)
                ->orderBy('id', 'desc')
                ->get();

            foreach ($projects as $key => $value) {
                $product_cat = DB::table('product_cats')
                     ->select('title')
                     ->where([
                         ['del_flg', '=', 0],
                         ['id', '=', $value->category_id]
                     ])
                     ->first(); 
                $projects[$key]->catname = $product_cat->title;
            }

            $view = View::make('frontend/dangtin/_search', ['projects' => $projects]);
            $value = $view->render();

            $data['code'] = 200;
            $data['value'] = $value;
            $data['message'] = 'Lưu thành công';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function edit_profile(Request $request)
    {
        try {
            $nguoidung = DB::table('nguoidungs')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', session('userSession')->id]
                ])
                ->first();

            if (!$nguoidung) {
                $data['code'] = 300;
                $data['error'] = 'Lỗi kết nối';
                return response()->json($data, 200);
            }

            if ($request->status_code == "change_avatar") {

                if ($nguoidung) {
                    $avatar_name = $nguoidung->avatar;
                }

                $avatar = $request->avatar;
                $avatar_link['path'] = config('general.shop_user_path');
                $avatar_link['url'] = config('general.shop_user_url');
                $options['file_name'] = $avatar_name;

                $upload = ShopUpload::upload($avatar, $avatar_link, $options);
                if ($upload == 'error') {
                    $data['code'] = 300;
                    $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                    return response()->json($data, 200);
                }
                $file_name = $upload ? $upload['file_name'] : $avatar_name;

                // save
                $nguoidung = new Nguoidung();
                $nguoidung->exists = true;
                $nguoidung->id = session('userSession')->id;
                $nguoidung->avatar = $file_name;
                $nguoidung->updated_at = date("Y-m-d H:i:s");
                $nguoidung->save();

                $data['code'] = 200;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            if ($request->status_code == "changpass") {
                $get_password = DB::table('nguoidungs')
                    ->select('password')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $nguoidung->id],
                    ])
                    ->first();

                if (password_verify($request->pass_old, $get_password->password)) {

                    $new_password = bcrypt($request->new_password);

                } else {
                    $data['code'] = 300;
                    $data['error'] = 'Mật khẩu cũ không chính xác.';
                    return response()->json($data, 200);
                }

                if ($request->pass_new == '') {
                    $data['code'] = 300;
                    $data['error'] = 'Chưa nhập mật khẩu mới.';
                    return response()->json($data, 200);
                }

                if ($request->pass_new != $request->pass_new_re) {
                    $data['code'] = 300;
                    $data['error'] = 'Lập lại mật khẩu không chính xác.';
                    return response()->json($data, 200);
                }

                // save
                $tb_nguoidung = Nguoidung::find($nguoidung->id);
                $tb_nguoidung->password = bcrypt($request->pass_new);
                $tb_nguoidung->save();

                $data['code'] = 200;
                $data['message'] = 'Mật khẩu mới đã được cập nhật.';
                return response()->json($data, 200);
            } 

            $email_check = DB::table('nguoidungs')
                ->select('email')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '!=', session('userSession')->id],
                    ['email', '=', $request->email]
                ])
                ->first();

            if ($email_check) {
                $data['code'] = 300;
                $data['error'] = 'Email này đã có người sử dụng';
                return response()->json($data, 200);
            }

            $gioithieu = [];
            $gioithieu['vanphong'] =  @$request->vanphong;
            $gioithieu['khuvuc'] =  @$request->khuvuc;
            $gioithieu['ngoaingu'] =  @$request->ngoaingu;
            $gioithieu['thanhtich'] =  @$request->thanhtich;
            $gioithieu['facebook'] =  @$request->facebook;
            
            // save
            $tb_nguoidung = Nguoidung::find($nguoidung->id);
            $tb_nguoidung->name = $request->name;
            $tb_nguoidung->callname = $request->callname;
            $tb_nguoidung->ngaysinh = $request->ngaysinh;
            $tb_nguoidung->gioitinh = $request->gioitinh;
            $tb_nguoidung->intro = $request->intro;
            $tb_nguoidung->phone = $request->phone;
            $tb_nguoidung->phone2 = $request->phone2;
            $tb_nguoidung->email = $request->email;
            $tb_nguoidung->address = $request->address;
            $tb_nguoidung->gioithieu = json_encode($gioithieu);
            $tb_nguoidung->save();
            
            session()->put('userSession', $tb_nguoidung);

            $data['code'] = 200;
            $data['message'] = 'Cập nhật thành công';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function api_dangtin(Request $request)
    {
        try {

            if ($request->status_code == "quanhuyen") {

                $districts = DB::table('zb_districts')
                    ->select('*')
                    ->where([
                        ['province_id', '=', $request->id]
                    ])
                    ->orderBy('id', 'asc')
                    ->get();

                $quanhuyen = '<select id="quanhuyen" name="quanhuyen" class="form-control local"><option value="">-- Quận / Huyện --</option>';
                foreach ($districts as $key => $value) {
                    $quanhuyen = $quanhuyen.'<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                $quanhuyen = $quanhuyen.'</select>';

                $data['code'] = 200;
                $data['value'] = $quanhuyen;
                return response()->json($data, 200);
            }

            if ($request->status_code == "phuongxa") {

                $wards = DB::table('zc_wards')
                    ->select('*')
                    ->where([
                        ['district_id', '=', $request->id]
                    ])
                    ->orderBy('id', 'asc')
                    ->get();

                $ward = '<select id="phuongxa" name="phuongxa" class="form-control local"><option value="">-- Phường / Xã --</option>';
                foreach ($wards as $key => $value) {
                    $ward = $ward.'<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                $ward = $ward.'</select>';

                // get duong
                $streets = DB::table('zd_streets')
                    ->select('*')
                    ->where([
                        ['district_id', '=', $request->id]
                    ])
                    ->orderBy('id', 'asc')
                    ->get();

                $street = '<select id="duong" name="duong" class="form-control local"><option value="">-- Đường --</option>';
                foreach ($streets as $key => $value) {
                    $street = $street.'<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                $street = $street.'</select>';

                $data['code'] = 200;
                $data['value'] = $ward;
                $data['value1'] = $street;
                return response()->json($data, 200);
            }

            if ($request->status_code == "quanhuyen2") {

                $districts = DB::table('projects')
                    ->join('m_districts', 'projects.quanhuyen', '=', 'm_districts.id')
                    ->select('m_districts.m_district_nm', 'm_districts.id', DB::raw('count(m_districts.id) as soluong'))
                    ->where([
                        ['projects.del_flg', '=', 0],
                        ['projects.active', '=', 0],
                        ['projects.tinhthanh', '=', $request->id]
                    ])
                    ->groupBy('m_districts.m_district_nm', 'm_districts.id')
                    ->orderBy('soluong', 'desc')
                    ->get();

                $quanhuyen = '<span data-value="Quận huyện" class="dropdown-item select_reset">Tất cả</span>';
                foreach ($districts as $key => $value) {
                    $quanhuyen = $quanhuyen.'<span data-value="'.$value->id.'" class="dropdown-item select_here">'.$value->m_district_nm.' ('.$value->soluong.')</span>';
                }

                $data['code'] = 200;
                $data['value'] = $quanhuyen;
                return response()->json($data, 200);
            }

            $check_user = $this->check_user();
            if ($check_user['code'] == 300) {
                $data['code'] = 300;
                $data['error'] = 'Yêu cầu đăng nhập';
                return response()->json($data, 200);
            }
            $user_find = $check_user['value'];
            $admin_id = $user_find->id;

            if ($request->status_code == "submit_project") {
                $project = DB::table('projects')
                    ->select('*')
                    ->where([
                        ['id', '=', $request->id],
                        ['user_id', '=', $admin_id]
                    ])
                    ->first();

                if (!$project) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy.';
                    return response()->json($data, 200);
                }

                if ($project->step == 1) {
                    $data['code'] = 300;
                    $data['error'] = 'Chưa hoàn thành chi tiết dự án.';
                    return response()->json($data, 200);
                } else if ($project->step == 2){
                    $db_project = Project::find($request->id);
                    $db_project->step = 3;
                    $db_project->updated_at = date("Y-m-d H:i:s");
                    $db_project->save();
            
                    $data['code'] = 200;
                    $data['id'] = $db_project->id;
                    $data['message'] = 'Đã cập nhật mới.';
                    return response()->json($data, 200);
                } else {
                    $data['code'] = 300;
                    $data['error'] = 'Lỗi không xác định.';
                    return response()->json($data, 200);
                } 
            }

            if ($request->status_code == "dangtin_b2") {

                $project = DB::table('projects')
                    ->select('*')
                    ->where([
                        ['id', '=', $request->id],
                        ['user_id', '=', $admin_id]
                    ])
                    ->first();

                if (!$project) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy.';
                    return response()->json($data, 200);
                }

                if ($project->avatar == null) {
                    $data['code'] = 300;
                    $data['error'] = 'Chưa chọn hình đại diện dự án.';
                    return response()->json($data, 200);
                }

                $db_project = Project::find($request->id);
                $db_project->user_id = $admin_id;
                $db_project->sale_file = $request->sale_file;
                $db_project->investor = $request->investor;
                $db_project->contractors = $request->contractors;
                $db_project->tower = $request->tower;
                $db_project->apartments = $request->apartments;
                $db_project->project_scale = $request->project_scale;
                $db_project->floors = $request->floors;
                $db_project->fromprice = $request->fromprice;
                $db_project->toprice = $request->toprice;
                $db_project->thue_fromprice = $request->thue_fromprice;
                $db_project->thue_toprice = $request->thue_toprice;
                if($project->step == 1) {
                    $db_project->step = 2;
                }
                $db_project->date_delivery = $request->date_delivery;
                $db_project->landscape_architecture = $request->landscape_architecture;
                $db_project->content_sale = $request->content_sale;
                $db_project->updated_at = date("Y-m-d H:i:s");
                $db_project->save();

                // save tongquans
                DB::table('tongquans')->where([
                    ['id_product', '=', $db_project->id],
                    ['id_group', '=', 3]
                ])->delete();
                $tongquans = json_decode($request->tongquans, true);
                if ($tongquans != '') {
                    foreach ($tongquans as $tongquan) {

                        DB::table('tongquans')->insert(
                            [
                                'title' => $tongquan['title'],
                                'value' => $tongquan['value'],
                                'data' => $tongquan['data'],
                                'id_product' => $db_project->id,
                                'id_group' => 3,
                                'vitri' => $tongquan['vitri'],
                            ]
                        );
                    }
                }
                // End save tongquans

                // save tienichs
                DB::table('tienichs')->where([
                    ['id_product', '=', $db_project->id],
                    ['id_group', '=', 3]
                ])->delete();
                $tienichs = json_decode($request->tienichs, true);
                if ($tienichs != '') {
                   foreach ($tienichs as $tienich) {

                        DB::table('tienichs')->insert(
                            [
                                'title' => $tienich['title'],
                                'data' => $tienich['data'],
                                'id_product' => $db_project->id,
                                'id_group' => 3,
                                'vitri' => $tienich['vitri'],
                            ]
                        );
                    }
                }
                // End save tienichs
        
                $data['code'] = 200;
                $data['id'] = $db_project->id;
                $data['message'] = 'Đã cập nhật mới.';
                return response()->json($data, 200);
            }

            if ($request->status_code == "delete_album_flats") {

                $album_flat = DB::table('album_flats')
                    ->select('*')
                    ->where([
                        ['id', '=', $request->id]
                    ])
                    ->first();

                if (!$album_flat) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy hình ảnh.';
                    return response()->json($data, 200);
                }

                $img_flats = DB::table('img_flats')
                    ->select('*')
                    ->where([
                        ['album_flat_id', '=', $album_flat->id]
                    ])
                    ->get();

                if(count($img_flats) != 0){
                    foreach ($img_flats as $key => $value) {
                        $image_path = config('general.imgflat_path').$value->avatar;
                        $db_img_flat = img_flats::find($value->id);
                        $db_img_flat->delete();
                        if(file_exists($image_path)) {
                            unlink($image_path);
                        }
                    }
                }

                $image_path = config('general.flat_path').$album_flat->avatar;
                $db_album_flat = AlbumFlats::find($album_flat->id);
                $db_album_flat->delete();
                if(file_exists($image_path)) {
                    unlink($image_path);
                }

                $data['code'] = 200;
                $data['message'] = 'Đã xóa mặt bằng tầng.';
                return response()->json($data, 200);
            }

            if ($request->status_code == "edit_flats") {

                $album_flat = DB::table('album_flats')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $request->id]
                    ])
                    ->first();

                if (!$album_flat) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy.';
                    return response()->json($data, 200);
                }

                if($request->title == "") {
                    $data['code'] = 300;
                    $data['error'] = 'Chưa nhập tên tầng.';
                    return response()->json($data, 200);
                }
                $avatar_name = $album_flat->avatar;

                if ($request->avatar) {

                    $avatar = $request->avatar;
                    $avatar_link['path'] = config('general.flat_path');
                    $avatar_link['url'] = config('general.flat_url');
                    $options['file_name'] = $avatar_name;

                    $upload = ShopUpload::upload($avatar, $avatar_link, $options);
                    if ($upload == 'error') {
                        $data['code'] = 300;
                        $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                        return response()->json($data, 200);
                    }
                    $file_name = $upload ? $upload['file_name'] : $avatar_name;
                }

                // save
                $db_flats = AlbumFlats::find($request->id);
                $db_flats->title = $request->title;
                if ($request->avatar) {
                    $db_flats->avatar = $file_name;
                }
                $db_flats->updated_at = date("Y-m-d H:i:s");
                $db_flats->save();

                $count_img_flats = DB::table('img_flats')
                    ->where([
                        ['album_flat_id', '=', $request->id]
                    ])
                    ->count();

                $db_flats->count_album = $count_img_flats;
                
                $view = View::make('frontend/dangtin/_oneflats', ['album_flat' => $db_flats]);
                $value = $view->render();

                $data['code'] = 200;
                $data['value'] = $value;
                $data['id'] = $request->id;
                return response()->json($data, 200);
            }

            if ($request->status_code == "modal_edit_flat") {

                $album_flat = DB::table('album_flats')
                    ->select('*')
                    ->where([
                        ['id', '=', $request->id]
                    ])
                    ->first();

                if (!$album_flat) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy.';
                    return response()->json($data, 200);
                }

                $view = View::make('frontend/dangtin/_form_edit_flats', ['album_flat' => $album_flat]);
                $value = $view->render();

                $data['code'] = 200;
                $data['value'] = $value;
                return response()->json($data, 200);
            }

            if ($request->status_code == "delete_img_flat") {
                $img_flat = DB::table('img_flats')
                    ->select('*')
                    ->where([
                        ['id', '=', $request->id]
                    ])
                    ->first();
                $album_flat_id = $img_flat->album_flat_id;

                if (!$img_flat) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy hình ảnh.';
                    return response()->json($data, 200);
                }

                $image_path = config('general.imgflat_path').$img_flat->avatar;
                $db_img_flat = img_flats::find($img_flat->id);
                $db_img_flat->delete();
                if(file_exists($image_path)) {
                    unlink($image_path);
                }

                $count_img_flats = DB::table('img_flats')
                    ->where([
                        ['album_flat_id', '=', $album_flat_id]
                    ])
                    ->count();

                $data['code'] = 200;
                $data['id'] = $album_flat_id;
                $data['count'] = $count_img_flats;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            if ($request->status_code == "upload_img_flat") {
                if (!$request->avatar) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy hình.';
                    return response()->json($data, 200);
                }

                $count_img_flats = DB::table('img_flats')
                    ->where([
                        ['album_flat_id', '=', $request->id]
                    ])
                    ->count();

                if ($count_img_flats >= 19) {
                    $data['code'] = 300;
                    $data['error'] = 'Tối đa 19 hình cho album dự án.';
                    return response()->json($data, 200);
                }

                $avatar = $request->avatar;
                $avatar_link['path'] = config('general.imgflat_path');
                $avatar_link['url'] = config('general.imgflat_url');
                $upload = ShopUpload::upload($avatar, $avatar_link);
                if ($upload == 'error') {
                    $data['code'] = 300;
                    $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                    return $data;
                }
                $file_name = $upload ? $upload['file_name'] : '';

                // save
                $db_img_flats = new img_flats;
                $db_img_flats->avatar = $file_name;
                $db_img_flats->album_flat_id = $request->id;
                $db_img_flats->size = $request->size;
                $db_img_flats->save();

                $view = View::make('frontend/dangtin/_itemimg_flats', ['img_flat' => $db_img_flats]);
                $value = $view->render();

                $data['code'] = 200;
                $data['value'] = $value;
                $data['count'] = $count_img_flats + 1;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            if ($request->status_code == "img_flat") {

                $img_flats = DB::table('img_flats')
                    ->select('*')
                    ->where([
                        ['album_flat_id', '=', $request->id]
                    ])
                    ->orderBy('id', 'desc')
                    ->get();

                $view = View::make('frontend/dangtin/_imgflats', ['img_flats' => $img_flats, 'id_flat' => $request->id]);
                $value = $view->render();

                $data['code'] = 200;
                $data['value'] = $value;
                return response()->json($data, 200);
            }

            if ($request->status_code == "add_flats") {
                if (!$request->avatar) {
                    $data['code'] = 300;
                    $data['error'] = 'Chưa chọn hình đại diện.';
                    return response()->json($data, 200);
                } else if($request->title == "") {
                    $data['code'] = 300;
                    $data['error'] = 'Chưa nhập tên tầng.';
                    return response()->json($data, 200);
                }

                $avatar = $request->avatar;
                $avatar_link['path'] = config('general.flat_path');
                $avatar_link['url'] = config('general.flat_url');
                $upload = ShopUpload::upload($avatar, $avatar_link);
                if ($upload == 'error') {
                    $data['code'] = 300;
                    $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                    return $data;
                }
                $file_name = $upload ? $upload['file_name'] : '';

                // save
                $db_flats = new AlbumFlats;
                $db_flats->title = $request->title;
                $db_flats->avatar = $file_name;
                $db_flats->project_id = $request->id;
                $db_flats->save();

                $db_flats->count_album = 0;
                
                $view = View::make('frontend/dangtin/_oneflats', ['album_flat' => $db_flats]);
                $value = $view->render();

                $data['code'] = 200;
                $data['value'] = $value;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            if ($request->status_code == "sale_file") {

                $project = DB::table('projects')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $request->id]
                    ])
                    ->first();

                if ($project) {
                    $avatar_name = $project->sale_file;
                } else {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy';
                    return response()->json($data, 200);
                }

                if ($request->avatar) {
                    $avatar = $request->avatar;
                    $avatar_link['path'] = config('general.salefile_path');
                    $avatar_link['url'] = config('general.salefile_url');
                    $options['file_name'] = $avatar_name;

                    $upload = ShopUpload::upload_file($avatar, $avatar_link, $options);
                    if ($upload == 'error') {
                        $data['code'] = 300;
                        $data['error'] = 'file vượt quá ' . config('general.notification_image');
                        return response()->json($data, 200);
                    }
                    $file_name = $upload ? $upload['file_name'] : $avatar_name;

                    // save
                    $db_project = Project::find($request->id);
                    $db_project->sale_file = $file_name;
                    $db_project->updated_at = date("Y-m-d H:i:s");
                    $db_project->save();

                    $data['code'] = 200;
                    $data['message'] = 'Cập nhật thành công';
                    return response()->json($data, 200);
                } else {
                    $data['code'] = 300;
                    $data['error'] = 'Không có hình';
                    return response()->json($data, 200);
                }
            }

            if ($request->status_code == "sale_image") {

                $project = DB::table('projects')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $request->id]
                    ])
                    ->first();

                if ($project) {
                    $avatar_name = $project->sale_image;
                } else {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy';
                    return response()->json($data, 200);
                }

                if ($request->avatar) {
                    $avatar = $request->avatar;
                    $avatar_link['path'] = config('general.sale_path');
                    $avatar_link['url'] = config('general.sale_url');
                    $options['file_name'] = $avatar_name;

                    $upload = ShopUpload::upload($avatar, $avatar_link, $options);
                    if ($upload == 'error') {
                        $data['code'] = 300;
                        $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                        return response()->json($data, 200);
                    }
                    $file_name = $upload ? $upload['file_name'] : $avatar_name;

                    // save
                    $db_project = Project::find($request->id);
                    $db_project->sale_image = $file_name;
                    $db_project->updated_at = date("Y-m-d H:i:s");
                    $db_project->save();

                    $data['code'] = 200;
                    $data['message'] = 'Cập nhật thành công';
                    return response()->json($data, 200);
                } else {
                    $data['code'] = 300;
                    $data['error'] = 'Không có hình';
                    return response()->json($data, 200);
                }
            }

            if ($request->status_code == "change_avatar") {

                $project = DB::table('projects')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $request->id]
                    ])
                    ->first();

                if ($project) {
                    $avatar_name = $project->avatar;
                }

                $avatar = $request->avatar;
                $avatar_link['path'] = config('general.project_path');
                $avatar_link['url'] = config('general.project_url');
                $options['file_name'] = $avatar_name;

                $upload = ShopUpload::upload($avatar, $avatar_link, $options);
                if ($upload == 'error') {
                    $data['code'] = 300;
                    $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                    return response()->json($data, 200);
                }
                $file_name = $upload ? $upload['file_name'] : $avatar_name;

                // save
                $db_project = Project::find($request->id);
                $db_project->avatar = $file_name;
                $db_project->updated_at = date("Y-m-d H:i:s");
                $db_project->save();

                $data['code'] = 200;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            if ($request->status_code == "upload_album_project") {

                if (!$request->album) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy hình.';
                    return response()->json($data, 200);
                }

                $count_project_album = DB::table('project_albums')
                    ->where([
                        ['project_id', '=', $request->id]
                    ])
                    ->count();

                $maximg = $count_project_album + count($request->album);

                if ($maximg >= 19) {
                    $data['code'] = 300;
                    $data['error'] = 'Tối đa 19 hình cho album dự án.';
                    return response()->json($data, 200);
                }

                $image_errors = [];
                $newdata = [];
                foreach ($request->album as $key => $value) {
                    $avatar = $value;
                    $avatar_link['path'] = config('general.project_album_path');
                    $avatar_link['url'] = config('general.project_album_url');
                    $upload = UploadMany::upload($avatar, $avatar_link);

                    if ($upload['code'] == 300) {
                       $image_errors[] = $upload;
                    } else {
                        $file_name = $upload['file_name'];
                        // save
                        $db_album = new ProjectAlbums;
                        $db_album->name = $file_name;
                        $db_album->project_id = $request->id;
                        $db_album->size = $request->size;
                        $db_album->save();

                        $newdata[] = $db_album;
                    }
                    
                }
                krsort($newdata);
                $view = View::make('frontend/dangtin/_albumproject', ['project_albums' => $newdata]);
                $value = $view->render();

                $data['error_log'] = "";
                if (count($image_errors) > 0) {
                    $view_error = View::make('frontend/dangtin/_albumitem_error', ['image_errors' => $image_errors]);
                    $error_log = $view_error->render();
                    $data['error_log'] = $error_log;
                }

                $data['code'] = 200;
                $data['value'] = $value;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            if ($request->status_code == "delete_album_project") {

                $project_album = DB::table('project_albums')
                    ->select('*')
                    ->where([
                        ['id', '=', $request->id]
                    ])
                    ->first();

                if (!$project_album) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy hình ảnh.';
                    return response()->json($data, 200);
                }

                $image_path = config('general.project_album_path').$project_album->name;
                $db_project_album = ProjectAlbums::find($project_album->id);
                $db_project_album->delete();
                if(file_exists($image_path)) {
                    unlink($image_path);
                }

                $data['code'] = 200;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            if ($request->status_code == "edit_b1") {

                $project = DB::table('projects')
                    ->select('*')
                    ->where([
                        ['id', '=', $request->id],
                        ['user_id', '=', $admin_id]
                    ])
                    ->first();

                if (!$project) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy.';
                    return response()->json($data, 200);
                }

                // validate
                $error_validate = Project::validate($request->id);
                $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $error = $this->show_error($validator->errors()->messages());
                    $data['code'] = 300;
                    $data['error'] = $error;
                    return response()->json($data, 200);
                }

                $db_project = Project::find($request->id);
                $db_project->user_id = $admin_id;
                $db_project->title = $request->title;
                $db_project->address = $request->address;
                $db_project->map = $request->map;
                $db_project->category_id = $request->category_id;
                $db_project->start_day = $request->start_day;
                $db_project->end_day = $request->end_day;
                $db_project->short_content = $request->short_content;
                $db_project->name_contact = $request->name_contact;
                $db_project->address_contact = $request->address_contact;
                $db_project->phone_contact = $request->phone_contact;
                $db_project->email_contact = $request->email_contact;
                $db_project->slug = $request->slug;
                $db_project->tinhthanh = $request->tinhthanh;
                $db_project->quanhuyen = $request->quanhuyen;
                $db_project->updated_at = date("Y-m-d H:i:s");
                $db_project->active = $request->active;
                
                if ($request->email_flg) {
                    $db_project->email_flg = 1;
                } else {
                    $db_project->email_flg = 0;
                }
                
                $db_project->save();
        
                $data['code'] = 200;
                $data['id'] = $db_project->id;
                $data['message'] = 'Đã cập nhật mới.';
                return response()->json($data, 200);
            }

            // validate
            $error_validate = Project::validate(0);
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
                $error = $this->show_error($validator->errors()->messages());
                $data['code'] = 300;
                $data['error'] = $error;
                return response()->json($data, 200);
            }

            $db_project = new Project;
            $db_project->user_id = $admin_id;
            $db_project->title = $request->title;
            $db_project->address = $request->address;
            $db_project->map = $request->map;
            $db_project->category_id = $request->category_id;
            $db_project->start_day = $request->start_day;
            $db_project->end_day = $request->end_day;
            $db_project->short_content = $request->short_content;
            $db_project->name_contact = $request->name_contact;
            $db_project->address_contact = $request->address_contact;
            $db_project->phone_contact = $request->phone_contact;
            $db_project->email_contact = $request->email_contact;
            $db_project->slug = $request->slug;
            $db_project->tinhthanh = $request->tinhthanh;
            $db_project->quanhuyen = $request->quanhuyen;
            $db_project->active = $request->active;
            
            if ($request->email_flg) {
                $db_project->email_flg = 1;
            }
            $db_project->save();
    
            $data['code'] = 200;
            $data['id'] = $db_project->id;
            $data['message'] = 'Tạo dự án thành công.';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function api_crate_item(Request $request)
    {
        try {

            if(session('userSession') == null) {
                $data['code'] = 300;
                $data['error'] = 'Yêu cầu đăng nhập';
                return response()->json($data, 200);
            }

            else if ($request->status_code == "resum_item_project") {
                $item_project = DB::table('item_projects')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $request->id],
                        ['user_id', '=', session('userSession')->id],

                    ])
                    ->first();

                if (!$item_project) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy.';
                    return response()->json($data, 200);
                }

                // save
                $db_itemproject = ItemProject::find($request->id);
                $db_itemproject->active = 1;
                $db_itemproject->updated_at = date("Y-m-d H:i:s");
                $db_itemproject->save();

                $data['code'] = 200;
                $data['id'] = $db_itemproject->id;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            else if ($request->status_code == "delete_item_project") {
                $item_project = DB::table('item_projects')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $request->id],
                        ['user_id', '=', session('userSession')->id],

                    ])
                    ->first();

                if (!$item_project) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy.';
                    return response()->json($data, 200);
                }

                // save
                $db_itemproject = ItemProject::find($request->id);
                $db_itemproject->active = 4;
                $db_itemproject->updated_at = date("Y-m-d H:i:s");
                $db_itemproject->save();

                $data['code'] = 200;
                $data['id'] = $db_itemproject->id;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            else if ($request->status_code == "submit_item_project") {
                $item_project = DB::table('item_projects')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $request->id],
                        ['user_id', '=', session('userSession')->id],

                    ])
                    ->first();

                if (!$item_project) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy.';
                    return response()->json($data, 200);
                }

                // save
                $db_itemproject = ItemProject::find($request->id);
                if($item_project->active == 2) {
                    $db_itemproject->active = 1;
                }
                if($item_project->step == 2) {
                    $db_itemproject->step = 3;
                }
                $db_itemproject->updated_at = date("Y-m-d H:i:s");
                $db_itemproject->save();

                $data['code'] = 200;
                $data['id'] = $db_itemproject->id;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            else if ($request->status_code == "dangtinb2") {
                $item_project = DB::table('item_projects')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $request->id],
                        ['user_id', '=', session('userSession')->id],

                    ])
                    ->first();

                if (!$item_project) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy.';
                    return response()->json($data, 200);
                } else if($request->content == ""){
                    $data['code'] = 300;
                    $data['error'] = 'Chưa nhập nội dung chi tiết.';
                    return response()->json($data, 200);
                }

                // save
                $db_itemproject = ItemProject::find($request->id);
                $db_itemproject->content = $request->content;
                if($item_project->step == 1) {
                    $db_itemproject->step = 2;
                }
                $db_itemproject->updated_at = date("Y-m-d H:i:s");
                $db_itemproject->thongtin = json_encode($request->thongtin);
                $db_itemproject->save();

                // save tongquans
                DB::table('tongquans')->where([
                    ['id_product', '=', $item_project->id],
                    ['id_group', '=', 4]
                ])->delete();
                $tongquans = json_decode($request->tongquans, true);
                if ($tongquans != '') {
                    foreach ($tongquans as $tongquan) {

                        DB::table('tongquans')->insert(
                            [
                                'title' => $tongquan['title'],
                                'value' => $tongquan['value'],
                                'data' => $tongquan['data'],
                                'id_product' => $item_project->id,
                                'id_group' => 4,
                                'vitri' => $tongquan['vitri'],
                            ]
                        );
                    }
                }
                // End save tongquans

                // save tienichs
                DB::table('tienichs')->where([
                    ['id_product', '=', $item_project->id],
                    ['id_group', '=', 4]
                ])->delete();
                $tienichs = json_decode($request->tienichs, true);
                if ($tienichs != '') {
                   foreach ($tienichs as $tienich) {

                        DB::table('tienichs')->insert(
                            [
                                'title' => $tienich['title'],
                                'data' => $tienich['data'],
                                'id_product' => $item_project->id,
                                'id_group' => 4,
                                'vitri' => $tienich['vitri'],
                            ]
                        );
                    }
                }
                // End save tienichs

                $data['code'] = 200;
                $data['id'] = $db_itemproject->id;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            else if ($request->status_code == "delete_album_item") {

                $item_album = DB::table('item_albums')
                    ->select('*')
                    ->where([
                        ['id', '=', $request->id]
                    ])
                    ->first();

                if (!$item_album) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy hình ảnh.';
                    return response()->json($data, 200);
                }

                $image_path = config('general.itemalbum_path').$item_album->name;
                $db_item_album = ItemAlbum::find($item_album->id);
                $db_item_album->delete();
                if(file_exists($image_path)) {
                    unlink($image_path);
                }

                $data['code'] = 200;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            else if ($request->status_code == "upload_album_item") {

                if (!$request->album) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy hình.';
                    return response()->json($data, 200);
                }

                $count_item_albums = DB::table('item_albums')
                    ->where([
                        ['item_id', '=', $request->id]
                    ])
                    ->count();

                $maximg = $count_item_albums + count($request->album);

                if ($maximg > 9) {
                    $data['code'] = 300;
                    $data['error'] = 'Tối đa 9 hình cho album này.';
                    return response()->json($data, 200);
                }

                $image_errors = [];
                $newdata = [];
                foreach ($request->album as $key => $value) {
                    $avatar = $value;
                    $avatar_link['path'] = config('general.itemalbum_path');
                    $avatar_link['url'] = config('general.itemalbum_url');
                    $upload = UploadMany::upload($avatar, $avatar_link);
                    if ($upload['code'] == 300) {
                        $image_errors[] = $upload;
                    } else {
                        $file_name = $upload['file_name'];
                        // save
                        $db_album = new ItemAlbum;
                        $db_album->name = $upload['file_name'];
                        $db_album->item_id = $request->id;
                        $db_album->size = $upload['size'];
                        $db_album->save();

                        $newdata[] = $db_album;
                    }
                }
                krsort($newdata);
                $view = View::make('frontend/dangtin/_albumitem', ['item_albums' => $newdata]);
                $value = $view->render();

                $data['error_log'] = "";
                if (count($image_errors) > 0) {
                    $view_error = View::make('frontend/dangtin/_albumitem_error', ['image_errors' => $image_errors]);
                    $error_log = $view_error->render();
                    $data['error_log'] = $error_log;
                }

                $data['code'] = 200;
                $data['value'] = $value;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            else if ($request->status_code == "change_avatar") {
                $item_project = DB::table('item_projects')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $request->id]
                    ])
                    ->first();

                if ($item_project) {
                    $avatar_name = $item_project->avatar;
                }

                $avatar = $request->avatar;
                $avatar_link['path'] = config('general.item_path');
                $avatar_link['url'] = config('general.item_url');
                $options['file_name'] = $avatar_name;

                $upload = ShopUpload::upload($avatar, $avatar_link, $options);
                if ($upload == 'error') {
                    $data['code'] = 300;
                    $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                    return response()->json($data, 200);
                }
                $file_name = $upload ? $upload['file_name'] : $avatar_name;

                // save
                $db_itemproject = ItemProject::find($request->id);
                $db_itemproject->avatar = $file_name;
                $db_itemproject->updated_at = date("Y-m-d H:i:s");
                $db_itemproject->save();

                $data['code'] = 200;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            else if ($request->status_code == "edit_b1") {

                $item_project = DB::table('item_projects')
                    ->select('*')
                    ->where([
                        ['id', '=', $request->id],
                        ['user_id', '=', session('userSession')->id]
                    ])
                    ->first();

                if (!$item_project) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy.';
                    return response()->json($data, 200);
                }

                // validate
                $error_validate = ItemProject::validate($request->id);
                $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $error = $this->show_error($validator->errors()->messages());
                    $data['code'] = 300;
                    $data['error'] = $error;
                    return response()->json($data, 200);
                }

                $update_values = $request->all();
                if ($request->area != null) {
                    $update_values['search_val'][] = $request->area.'m';
                }
                $update_values['search_val'][] = $request->allprice_mark;
                $value_search_tags = $update_values['search_val'];

                foreach ($update_values['search_val'] as $key => $value) {
                    $update_values['search_slug'][] = $this->slug($value);
                }
                $update_values['search_val'] = json_encode($update_values['search_val']);
                $update_values['search_slug'] = json_encode($update_values['search_slug']);
                unset($update_values['status_code']);
                if ($request->email_flg) {
                    $update_values['email_flg'] = 1;
                } else {
                    $update_values['email_flg'] = 0;
                }
                $update_values['user_id'] = session('userSession')->id;

                $this->DB_update($update_values, 'item_projects', $request->id);

                foreach ($value_search_tags as $key => $value) {
                    $db_search_tag = DB::table('db_search_tags')
                        ->select('*')
                        ->where([
                            ['title', '=', $value],
                        ])
                        ->first();

                    $update_values = [];
                    $update_values['title'] = $value;
                    $update_values['slug'] = $this->slug($value);

                    if (!$db_search_tag) {
                        $this->DB_insert($update_values, 'db_search_tags');
                    }
                }
        
                $data['code'] = 200;
                $data['id'] = $request->id;
                $data['message'] = 'Đã cập nhật mới.';
                return response()->json($data, 200);
            }

            else if ($request->status_code == "add"){

                // kiểm tra 5 tin đăng trên một ngày
                $count_item_projects = DB::table('item_projects')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['user_id', '=', session('userSession')->id],
                        ['type', '=', $request->type],
                        ['created_at', '>=', date("Y-m-d 00:00:00")],
                        ['created_at', '<=', date("Y-m-d 23:59:59")]
                    ])
                    ->count();
                if ($count_item_projects >= 5) {
                    $type = 'thuê';
                    if ($request->type == 1) {
                        $type = 'bán';
                    }
                    $data['code'] = 300;
                    $data['error'] = 'Tối đa 5 tin '.$type.' trên một ngày.';
                    return response()->json($data, 200);
                }

                // validate
                $error_validate = ItemProject::validate(0);
                $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
                if ($validator->fails()) {
                    $error = $this->show_error($validator->errors()->messages());
                    $data['code'] = 300;
                    $data['error'] = $error;
                    return response()->json($data, 200);
                }

                // $captcha = $this->google_captcha($request->recaptcha);

                // if($captcha['code'] == 300){
                //     $data['code'] = 300;
                //     $data['error'] = $captcha['error'];
                //     return response()->json($data, 200);
                // }

                $insert_values = $request->all();
                if ($request->area != null) {
                    $update_values['search_val'][] = $request->area.'m';
                }
                $update_values['search_val'][] = $request->allprice_mark;
                $value_search_tags = $update_values['search_val'];

                foreach ($insert_values['search_val'] as $key => $value) {
                    $insert_values['search_slug'][] = $this->slug($value);
                }
                $insert_values['search_val'] = json_encode($insert_values['search_val']);
                $insert_values['search_slug'] = json_encode($insert_values['search_slug']);

                unset($insert_values['recaptcha']);
                unset($insert_values['status_code']);
                if ($request->email_flg) {
                    $insert_values['email_flg'] = 1;
                } else {
                    $insert_values['email_flg'] = 0;
                }
                $insert_values['user_id'] = session('userSession')->id;
                $id_item_projects = $this->DB_insert($insert_values, 'item_projects');
                
                foreach ($insert_values['search_val'] as $key => $value) {
                    $db_search_tag = DB::table('db_search_tags')
                        ->select('*')
                        ->where([
                            ['title', '=', $value],
                        ])
                        ->first();

                    $insert_values = [];
                    $insert_values['title'] = $value;
                    $insert_values['slug'] = $this->slug($value);

                    if (!$db_search_tag) {
                        $this->DB_insert($insert_values, 'db_search_tags');
                    }
                }
        
                $data['code'] = 200;
                $data['id'] = $id_item_projects;
                $data['message'] = 'Tạo thành công.';
                return response()->json($data, 200);
            } 

            else{
                $data['code'] = 300;
                $data['error'] = 'Lệnh không tồn tại.';
                return response()->json($data, 200);
            }
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}