@if (count($image_errors) != 0)
	<p class="title-mod"><b>Có {{count($image_errors)}} file không tải lên được !</b> <span class="clear_error"><i class="fas fa-eraser"></i> Clear</span></p>
    @foreach ($image_errors as $image_error)
    <ul>
    	<li>Tên file: {{$image_error['file_name']}}</li>
    	<li>Kích thước: {{BladeGeneral::formatBytes($image_error['size'])}}</li>
    	<li>Lỗi: {!!$image_error['error']!!}</li>
    </ul>
    @endforeach
@else

@endif