@extends('frontend.layouts.application')
@section('content')
<!--/head-block-->
    <div class="content-index bg-gray">
        <div class="container-fluid t-video-rp project-video without-video"></div>
        
        <div class="container">
            <div class="bg-white panel-wrap my-3 my-md-5">
                @include('frontend.elements.sidebar_dangtin')

                <section class="panel-content">
                    <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>Bảng thông tin</button>

                    <h2 class="title text-uppercase">Thay đổi thông tin cá nhân</h2>
                    
                    <form autocomplete="off" id="user_form" class="panel-form" enctype="multipart/form-data">
                        <div class="d-flex flex-wrap block">
                            <div class="left">
                                <h4 class="title-line"><span class="text"><i class="far fa-image"></i>Hình đại diện</span></h4>
                                
                                <figure class="img-upload file_upload_box">
                                    <label class="img">
                                        <img class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $nguoidung->avatar,'data' => 'shop_user', 'time' => $nguoidung->updated_at])}}" alt="">
                                        <input id="avatar_user" type="file" class="d-none file_image" name="avatar">
                                    </label>
                                    <button class="reset-btn btn-main text-uppercase">Upload</button>
                                </figure>
                            </div>

                            <div class="right">
                                <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Thông tin cá nhân</span> <span class="badge badge-info">ID: {{$nguoidung->id}}</span></h4>

                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Họ và tên <span class="cl_red">(*)</span></label>
                                    <div class="input">
                                        <input type="text" name="name" class="form-control" value="{{$nguoidung->name}}">
                                    </div>
                                </div>
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Ngày sinh</label>
                                    <div class="input">
                                        <input name="ngaysinh" value="{{$nguoidung->ngaysinh}}" type="text" class="form-control ngaysinh">
                                    </div>
                                </div>
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Giới tính</label>
                                    <div class="input">
                                        @if($nguoidung->gioitinh == 3)
                                        <label class="mt-2 mr-3">
                                            <input type="radio" name="gioitinh" id="male" value="1"> Nam
                                        </label>
                                        <label class="mt-2">
                                            <input type="radio" name="gioitinh" id="female" value="0"> Nữ
                                        </label>
                                        @endif

                                        @if($nguoidung->gioitinh == 1)
                                        <label class="mt-2 mr-3">
                                            <input checked type="radio" name="gioitinh" id="male" value="1"> Nam
                                        </label>
                                        <label class="mt-2">
                                            <input type="radio" name="gioitinh" id="female" value="0"> Nữ
                                        </label>
                                        @endif

                                        @if($nguoidung->gioitinh == 0)
                                        <label class="mt-2 mr-3">
                                            <input type="radio" name="gioitinh" id="male" value="1"> Nam
                                        </label>
                                        <label class="mt-2">
                                            <input checked type="radio" name="gioitinh" id="female" value="0"> Nữ
                                        </label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Thông tin giới thiệu</span></h4>
                            <?php
                                $gioithieu = [];
                                if ($nguoidung->gioithieu != null) {
                                    $gioithieu = json_decode($nguoidung->gioithieu);
                                }
                            ?>
                            <div class="panel-form__row">
                                <label class="text mb-md-0">Văn phòng</label>
                                <div class="input">
                                    <input name="vanphong" value="{{@$gioithieu->vanphong}}" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="panel-form__row">
                                <label class="text mb-md-0">Facebook</label>
                                <div class="input">
                                    <input name="facebook" value="{{@$gioithieu->facebook}}" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="panel-form__row">
                                <label class="text mb-md-0">Khu vực</label>
                                <div class="input">
                                    <input name="khuvuc" value="{{@$gioithieu->khuvuc}}" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="panel-form__row">
                                <label class="text mb-md-0">Thành tích</label>
                                <div class="input">
                                    <input name="thanhtich" value="{{@$gioithieu->thanhtich}}" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="panel-form__row">
                                <label class="text mb-md-0">Ngoại ngữ</label>
                                <div class="input">
                                    <input name="ngoaingu" value="{{@$gioithieu->ngoaingu}}" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="panel-form__row flex-100 order-2">
                                <label class="text mb-md-0">Nội dung</label>
                                <div class="input">
                                    <textarea name="intro" rows="5" class="form-control">{{$nguoidung->intro}}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Thông tin liên hệ</span></h4>

                            <!-- <div class="panel-form__row">
                                <label class="text mb-md-0">Tên thường gọi</label>
                                <div class="input">
                                    <input name="callname" value="{{$nguoidung->callname}}" type="text" class="form-control">
                                </div>
                            </div> -->
                            
                            <div class="panel-form__row">
                                <label class="text mb-md-0">Di động <span class="cl_red">(*)</span></label>
                                <div class="input">
                                    <input name="phone" value="{{$nguoidung->phone}}" type="text" class="form-control">
                                </div>
                            </div>
<!--                             <div class="panel-form__row">
                                <label class="text mb-md-0">Điện thoại bàn</label>
                                <div class="input">
                                    <input name="phone2" value="{{$nguoidung->phone2}}" type="text" class="form-control">
                                </div>
                            </div> -->
                            <div class="panel-form__row">
                                <label class="text mb-md-0">Emai <span class="cl_red">(*)</span></label>
                                <div class="input">
                                    <input name="email" value="{{$nguoidung->email}}" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="panel-form__row">
                                <label class="text mb-md-0">Địa chỉ <span class="cl_red">(*)</span></label>
                                <div class="input">
                                    <input name="address" value="{{$nguoidung->address}}" type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="panel-form--footer">
                            <button type="submit" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Lưu lại</span></button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    <script>
        $('.ngaysinh').datepicker({
            autoClose:true,
            dateFormat: 'dd/mm/yyyy',
            language: 'en'
        })
        var user_form = $('#user_form').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass);
            },
            rules: {
                name:{
                    required: true
                },
                email:{
                    required: true
                },
                address:{
                    required: true
                },
                phone:{
                    required: true
                },
            },
            messages: {
                name:{
                    required: 'Chưa nhập họ tên.'
                },
                email:{
                    required: 'Chưa nhập email'
                },
                address:{
                    required: 'Chưa nhập địa chỉ'
                },
                phone:{
                    required: 'Chưa nhập số điện thoại'
                },
            },
            submitHandler: function (form) {
        
                var data = {};
                $("#user_form").serializeArray().map(function(x){data[x.name] = x.value;});

                $('.block-page-all').addClass('active');
                $.ajax({
                    type: 'POST',
                    url: '/edit_profile',
                    data: data,
                    dataType: 'json',
                    error: function(){
                        $('.block-page-all').removeClass('active');
                        toastr.error(result.error);
                    },
                    success: function(result) {
                        if (result.code == 300) {
                            toastr.error(result.error);
                            $('.block-page-all').removeClass('active');
                            return false;
                        }
                        $('.block-page-all').removeClass('active');
                        toastr.success(result.message);
                    }
                });
                return false;
            }
        });

        $('#avatar_user').change(function(event) {
            var file = this.files[0];
            if (typeof(file) === 'undefined') {
                return false;
            }
            var fileType = file["type"];
            var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
            if ($.inArray(fileType, ValidImageTypes) < 0) {
                toastr.error('Hình ảnh không hợp lệ', null, {timeOut: 4000});
                return false;
            }
            if (file.size > 1000000) {
                toastr.error('Dung lượng hình phải bé hơn 1MB', null, {timeOut: 4000});
                return false;
            }

            var images = $(this).closest('.file_upload_box').find('.image_review');
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    images.attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }

            var fd = new FormData();
            fd.append("status_code", "change_avatar");
            if ($(".file_image").get(0)){
                fd.append("avatar", $(".file_image").get(0).files[0]);
            }

            $('.block-page-all').addClass('active');
            $.ajax({
                type: 'POST',
                url: '/edit_profile',
                data: fd,
                dataType: 'json',
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function(result) {
                    $('.block-page-all').removeClass('active');
                }
            });
        });
    </script>
@endsection