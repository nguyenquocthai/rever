@extends('frontend.layouts.application')
@section('content')
<link href="/public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="/public/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

<!--/head-block-->
    <div class="content-index bg-gray">
        <div class="container-fluid t-video-rp project-video without-video"></div>

        <div class="container">
            <div class="bg-white panel-wrap my-3 my-md-5">
                @include('frontend.elements.sidebar_dangtin')

                <section class="panel-content">
                    <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>Bảng thông tin</button>

                    <h2 class="title text-uppercase">Quản lý tin cho thuê</h2>

                    <nav class="tab-nav">
                        <ul class="tab-nav__list reset-list">
                            <li class="item"><a href="javascript:;" data-id="" class="item status_value d-block active">Tất cả</a></li>
                            <li class="item"><a href="javascript:;" data-id="0" class="item status_value d-block">Xuất bản</a></li>
                            <li class="item"><a href="javascript:;" data-id="2" class="item status_value d-block">Nháp</a></li>
                            <li class="item"><a href="javascript:;" data-id="1" class="item status_value d-block">Chờ duyệt</a></li>
                            <li class="item"><a href="javascript:;" data-id="4" class="item status_value d-block">Đã xóa</a></li>
                            <li class="item"><a href="javascript:;" data-id="5" class="item status_value d-block">Trả lại</a></li>
                            <li class="item"><a href="javascript:;" data-id="6" class="item status_value d-block">Quá hạn</a></li>
                        </ul>

                        <a href="/tao-tin-thue" class="btn-create"><button class="reset-btn btn-create text-uppercase"><i class="fa fa-plus-circle"></i>Đăng tin mới</button></a>
                    </nav>

                    <div class="tab-container">
                        <form id="form_search" class="bg-gray p-1 sale-form search-thue-ban">
                            <input type="hidden" name="status_code" value="chothue">
                            <input type="hidden" name="active" class="active_status">
                            <div class="row row3">
                                <div class="col-12 col-lg-6 item">
                                    <div class="searchbox">
                                        <div class="searchbox-col searchbox-input datebox">
                                            <input name="updated_at" type="text" class="form-control ngaycapnhat" placeholder="Ngày cập nhật">
                                            <span class="reset-btn icon"><i class="far fa-calendar-alt"></i></span>
                                        </div>

                                        <div class="searchbox-col earchbox-input datebox">
                                            <select name="category_id" class="form-control">
                                                <option value="">Danh mục</option>
                                                @if (count($product_cat1s) != 0)
                                                    @foreach ($product_cat1s as $product_cat1)
                                                    <option value="{{$product_cat1->id}}">{{$product_cat1->title}}</option>
                                                    @endforeach
                                                @else
                                                
                                                @endif
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="col-12 col-lg-6 item">
                                    <div class="searchbox">
                                        <input type="hidden" name="type" value="0">
                                        
                                        <div class="searchbox-col flex-100 searchbox-input">
                                            <input name="title" type="text" class="form-control" placeholder="Tên bất động sản...">
                                            <button type="button" class="btn_search reset-btn icon"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="table-responsive load_thue_ban">
                            @include('frontend.dangtin._searchchothue')
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <script>
        
        var myDatepicker = $('.ngaycapnhat').datepicker({
            autoClose:true,
            dateFormat: 'dd/mm/yyyy',
            language: 'en'
        }).data('datepicker');

        //var myDatepicker = $('#my-elem').datepicker().data('datepicker');

        $('.btn_search').click(function(event) {
            search_action();
        });

        $('.datebox .reset-btn').click(function(event) {
            myDatepicker.show();
        });

        $('.status_value').click(function(event) {
            $('.status_value').removeClass('active');
            $(this).addClass('active');
            var id = $(this).attr('data-id');
            $('.active_status').val(id);
            search_action();
        });

        function search_action() {
            var data = {};
            $("#form_search").serializeArray().map(function(x){data[x.name] = x.value;});
            $('.block-page-all').addClass('active');
            $.ajax({
                type: 'POST',
                url: '/api_search',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                },
                success: function(result) {
                    $('.load_thue_ban').html(result.value);
                    $('.block-page-all').removeClass('active');
                }
            });
        }

        $(document).on('click', '.delete_thue_ban', function(event) {
            if (confirm("Xác nhận xóa ?")) {
                var here = $(this).closest('tr');
                var data = {};
                data['status_code'] = 'delete_item_project';
                data['id'] = $(this).attr('data-id');
                $('.block-page-all').addClass('active');
                $.ajax({
                    type: 'POST',
                    url: '/api_crate_item',
                    data: data,
                    dataType: 'json',
                    error: function(){
                        $('.block-page-all').removeClass('active');
                        toastr.error(result.error);
                    },
                    success: function(result) {
                        if (result.code == 300) {
                            toastr.error(result.error);
                            $('.block-page-all').removeClass('active');
                            return false
                        }
                        here.remove();
                        $('.block-page-all').removeClass('active');
                    }
                });
            }
            return false;
        });

        $(document).on('click', '.resum_thue_ban', function(event) {
            if (confirm("Xác nhận phục hồi ?")) {
                var here = $(this).closest('tr');
                var data = {};
                data['status_code'] = 'resum_item_project';
                data['id'] = $(this).attr('data-id');
                $('.block-page-all').addClass('active');
                $.ajax({
                    type: 'POST',
                    url: '/api_crate_item',
                    data: data,
                    dataType: 'json',
                    error: function(){
                        $('.block-page-all').removeClass('active');
                        toastr.error(result.error);
                    },
                    success: function(result) {
                        if (result.code == 300) {
                            toastr.error(result.error);
                            $('.block-page-all').removeClass('active');
                            return false
                        }
                        here.remove();
                        $('.block-page-all').removeClass('active');
                    }
                });
            }
            return false;
        });

        $(document).on('click', '.lammoi', function(event) {
            var here = $(this).closest('tr');
            var data = {};
            data['status_code'] = 'lammoi';
            data['id'] = $(this).attr('data-id');
            $('.block-page-all').addClass('active');
            $.ajax({
                type: 'POST',
                url: '/api_crate_item',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    search_action();
                    $('.block-page-all').removeClass('active');
                }
            });
        });
    </script>
@endsection