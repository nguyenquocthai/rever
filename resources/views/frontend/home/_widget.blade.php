<div class="col-lg-4">
    <!-- VALIDATED-->
    <section class="block block-aside">
        <div class="block-title-wrap">
            <h3 class="block-title"><i class="icon icon-shield cl_blue"></i>Authenticated</h3>
            <a class="reset-btn btn-more" href="#!">See all</a>
        </div>
        <div class="block-aside__wrap">

            @if (count($item_projects) != 0)
                @foreach ($item_projects as $key => $item_project)
                <article class="project-item ver">
                    <figure class="img embed-responsive hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $item_project->avatar,'data' => 'item_project', 'time' => $item_project->updated_at])}}" alt="{{$item_project->title}}"><a class="link" href="/rent/{{$item_project->slug}}" title="{{$item_project->title}}"></a><span class="icon icon-shield cl_blue"></span></figure>
                    <div class="content">
                        <a class="name link" href="/rent/{{$item_project->slug}}" title="{{$item_project->title}}">{{$item_project->title}}</a>
                        <ul class="reset-list list">
                            <li class="item">
                                <p class="address"><a class="link" href="/search?keyword={{$item_project->location}}">{{$item_project->location}}</a></p>
                            </li>
                            <li class="item price">{{$item_project->price}} C / month</li>
                        </ul>
                    </div>
                </article>
                @if($key == 4) @break @endif
                @endforeach
            @else
            
            @endif
        </div>
        <div class="block-aside__bot"><a class="link cl_blue" href="#!">See all</a></div>
    </section>
</div>