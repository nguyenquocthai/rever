<!-- FEATURED HOUSES-->
@if (count($item_projects) != 0)
<section class="block page-container">
    <h3 class="block-title"><i class="icon icon-nav-home-fill cl_main"></i>Certified property</h3>
    <div class="project-featured js-slider4">
        @foreach ($item_projects as $item_project)
        <div class="item">
            <article class="project-item ver">
                <figure class="img embed-responsive hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $item_project->avatar,'data' => 'item_project', 'time' => $item_project->updated_at])}}" alt="{{$item_project->title}}"><a class="link" href="/rent/{{$item_project->slug}}" title="{{$item_project->title}}"></a><span class="icon icon-shield cl_main"></span></figure>
                <div class="content">
                    <a style="height:1.5rem;margin-bottom: 0.1rem;" class="name link" href="/rent/{{$item_project->slug}}" title="{{$item_project->title}}">{{$item_project->title}}</a>
                    <p style="margin-bottom: .5rem;font-size: 16px;">{{$item_project->address}}</p>
                    <p class="" style="margin-bottom: 0.1rem">
                            <span class="mr-4">
                                <i class="icon icon-nav-roomate"></i>
                                @if($item_project->doituong == 0)
                                Men or Women
                                @endif
                                @if($item_project->doituong == 1)
                                Only Men
                                @endif
                                @if($item_project->doituong == 2)
                                Only Women
                                @endif
                            </span>
                            <span><i class="icon icon-squaremeter"></i>{{$item_project->area}} sqft</span></p>
                        <p class="" style="margin-bottom: 0.1rem">
                            <span class="mr-4"><a href="#"><i class="icon icon-bed-fill"></i>{{$item_project->phongngu}}</a></span>
                            <span><a href="#"><i class="icon icon-toilet-fill"></i>{{$item_project->phongtam}}</a></span></p>
                    <ul class="reset-list list">
                        <li class="item">
                            <!-- <p class="address"><a class="link" href="/search?keyword={{$item_project->location}}">{{$item_project->location}}</a></p> -->
                        </li>
                        <li class="item price">${{BladeGeneral::priceFormat($item_project->price)}} / Month</li>
                    </ul>
                </div>
            </article>
        </div>
        @endforeach
    </div>
</section>
@else
@endif