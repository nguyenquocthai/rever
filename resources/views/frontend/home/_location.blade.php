<style>
    .trading-list .item:not(:last-child){
        margin-right: 0;
    }
    .trading-list{
        overflow-x: hidden !important;
    }
</style>
<!-- TRADING-->
<section class="trading page-container">
    <h3 class="title-lg">Location</h3>
    <div class="reset-list trading-list">

        @if (count($locations) != 0)
            @foreach ($locations as $location)
            <div class="item">
                <div class="item_mod">
                    <figure class="img hasLink" style="background-image: url({{BladeGeneral::GetImg(['avatar' => $location->avatar,'data' => 'location', 'time' => $location->updated_at])}});">
                        <a class="link" href="/search?keyword={{$location->title}}" title="{{$location->title}}"></a>
                        <figcaption class="caption">{{$location->title}}</figcaption>
                    </figure>
                </div>
            </div>
            @endforeach
        @else
        
        @endif
    </div>
</section>
<script>
    $('.trading-list').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        cssEase: 'linear',
        dots:false,
        responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 5
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 450,
          settings: {
            slidesToShow: 2
          }
        }
      ]
    });
</script>