@extends('frontend.layouts.main') 
@section('content')

<main class="main">
      <!-- BANNER MAIN-->
      <section class="banner-main js-sliderBanner">
        @foreach ($sliders as $slider)
        <div class="item">
          <figure class="img embed-responsive mb-0" style="background-image: url(/public/img/upload/sliders/{{$slider->avatar}})"></figure>
        </div>
        @endforeach
        
      </section>
      <!-- SEARCH BOX-->
      <section class="section-spacing search">
        <div class="container">
          <div class="tab-parent js-tabParent">
            <div class="tab-nav dropdown js-tabNav">
              <button class="reset-btn button dropdown-toggle js-tabCur" type="button" data-toggle="dropdown">Thuê</button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <button class="dropdown-item js-tabLink" data-target="#tab-real-eastate" type="button">Thuê</button>
                <button class="dropdown-item js-tabLink" data-target="#tab-house" type="button">Bán</button>
              </div>
            </div>
            <div class="tab-content js-tabContent">
              <div class="tab-panel active" id="tab-real-eastate">
                <form class="searchbox" name="real-eastate" action="/search-thue" method="get" enctype="multipart/form-data">
                  <input class="input" type="text" name="keyword" placeholder="Tìm nhà cho thuê">
                  <input type="hidden" name="type" value="0"> 
                  <button class="reset-btn button" type="submit"><i class="zmdi zmdi-search"></i>Tìm kiếm</button>
                </form>
              </div>
              <div class="tab-panel" id="tab-house">
                <form class="searchbox" name="house" action="/search-ban" method="get" enctype="multipart/form-data">
                  <input class="input" type="text" name="keyword" placeholder="Tìm nhà bán">
                  <input type="hidden" name="type" value="1"> 
                  <button class="reset-btn button" type="submit"><i class="zmdi zmdi-search"></i>Tìm kiếm</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- INTRODUCTION-->
      <section class="section-spacing intro">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="intro-block">
                {!!$intro_left->content!!}
              </div>
            </div>
            <div class="col-md-6">
              <div class="intro-block">
                {!!$intro_right->content!!}
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- HOUSES FOR SALE-->
      <section class="section-spacing bg-gray-white">
        <div class="container">
          <h3 class="title-section fs_24">Nhà đất bán</h3>
          <div class="row">
            @foreach($bans as $ban)
            <?php $thongtin = json_decode($ban->thongtin) ?>
            <article class="col-12 col-md-6 col-lg-4">
              <div class="project-item ver transUp">
                <figure class="img embed-responsive hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $ban->avatar,'data' => 'item_project', 'time' => $ban->updated_at])}}" alt=""><a class="link" href="/ban/{{$ban->slug}}" title="{{$ban->title}}"></a></figure>
                <div class="content fs_14"><a class="name link" href="/ban/{{$ban->slug}}" title="{{$ban->title}}">{{$ban->title}}</a>
                  <p class="address"><span>{{$ban->address}}</p>
                  <ul class="reset-list list">
                    <li class="item"><i class="zmdi zmdi-airline-seat-individual-suite"></i>{{@$thongtin->phongtam}}
                    </li>
                    <li class="item"><i class="zmdi zmdi-photo-size-select-small"></i>{{@$ban->area}} m²
                    </li>
                    <li class="item price">{{@$ban->allprice_mark}}</li>
                  </ul>
                </div>
              </div>
            </article>
            @endforeach
            
          </div><a class="link more fs_18" href="/ban">Xem tất cả<i class="zmdi zmdi-chevron-right"></i></a>
        </div>
      </section>
      <!-- HOUSES FOR RENT-->
      <section class="section-spacing bg-gray-white">
        <div class="container">
          <h3 class="title-section fs_24">Nhà cho thuê</h3>
          <div class="row">
            @foreach($thues as $thue)
            <?php $thongtin = json_decode($thue->thongtin) ?>
            <article class="col-12 col-md-6 col-lg-4">
              <div class="project-item ver transUp">
                <figure class="img embed-responsive hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $thue->avatar,'data' => 'item_project', 'time' => $thue->updated_at])}}" alt=""><a class="link" href="/thue/{{$thue->slug}}" title="{{$thue->title}}"></a></figure>
                <div class="content fs_14"><a class="name link" href="/thue/{{$thue->slug}}" title="{{$thue->title}}">{{$thue->title}}</a>
                  <p class="address"><span>{{$thue->address}}</p>
                  <ul class="reset-list list">
                    <li class="item"><i class="zmdi zmdi-airline-seat-individual-suite"></i>{{@$thongtin->phongtam}}
                    </li>
                    <li class="item"><i class="zmdi zmdi-photo-size-select-small"></i>{{@$thue->area}} m²
                    </li>
                    <li class="item price">{{@$thue->allprice_mark}}</li>
                  </ul>
                </div>
              </div>
            </article>
            @endforeach

          </div><a class="link more fs_18" href="/thue">Xem tất cả<i class="zmdi zmdi-chevron-right"></i></a>
        </div>
      </section>
      <!-- NEWS-->
      <section class="section-spacing bg-gray-white">
        <div class="container">
          <h3 class="title-section fs_24">Thông tin nhà đất</h3>
          <div class="row">
            @foreach($blogs as $blog)
            <article class="col-12 col-md-6 col-lg-4">
              <div class="news-item ver transUp">
                <figure class="img embed-responsive hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $blog->avatar,'data' => 'new', 'time' => $blog->updated_at])}}" alt=""><a class="link" href="/tin-tuc/{{$blog->slug}}" title="{{$blog->title}}"></a></figure>
                <div class="content"><a class="name link" href="/tin-tuc/{{$blog->slug}}" title="{{$blog->title}}">{{$blog->title}} {{$blog->avatar}}</a>
                  <p class="date">{{date('d-m-Y', strtotime($blog->created_at))}}</p>
                </div>
              </div>
            </article>
            @endforeach
            
          </div><a class="link more fs_18" href="/tin-tuc">Xem tất cả<i class="zmdi zmdi-chevron-right"></i></a>
        </div>
      </section>
    </main>

@endsection