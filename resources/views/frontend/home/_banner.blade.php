<style>

.dropdown-content {
  display: none;
  position: absolute;
  background-color: white;
  min-width: 230px;
  overflow: auto;
  border: 1px solid #ddd;
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.show {display: block ;}
.dropdown-content-search{
    top: 60px;
    left: 0;
    width: 100%;
    border-radius: 1rem;
}
.search-1{
    overflow: unset;
    position: relative;
}
.input-search:hover{
    background-color: #f6f6f6;
}
</style>
<span class="js-pageVal" id="index"></span>
<!-- BANNER MAIN-->
<section class="banner-main" style="background-image: url({{BladeGeneral::GetImg(['avatar' => @$slider->avatar,'data' => 'slider', 'time' => @$slider->updated_at])}});">
    <div class="banner-container">
        <h3 class="title">{{$slider->title}}</h3>
        <form class="search" action="/search" method="get">
            <div class="search-container search-1" >
                <label class="selectbox place">
                    <select name="location">
                        <option value="">All</option>
                        @if (count($locations) != 0)
                            @foreach ($locations as $location)
                            <option value="{{$location->title}}">{{$location->title}}</option>
                            @endforeach
                        @else
                        
                        @endif
                    </select>
                </label>
                <input name="keyword" class="input" type="text" placeholder="Search By Location, City, Province, Street" id="myInput" >
                <div id="myDropdown " class="dropdown-content dropdown-content-search" >
                    @foreach ($item_project2s as $key => $item_project)
                    <a href="/rent/{{$item_project->slug}}" class="input-search">
                        <p class="mb-0">{{$item_project->title}}</p>
                        <p class="mb-0">{{$item_project->address}}</p>
                    </a>
                    @endforeach
                  </div>
                <script>
                    
                    $(document).ready(function(){
                      $("#myInput").on("keyup", function() {
                        $('.dropdown-content').addClass('show');
                        var value = $(this).val().toLowerCase();
                        $(".input-search").filter(function() {
                          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                        });
                      });
                    });

                </script>
                <button class="reset-btn button" type="submit"><i class="zmdi zmdi-search"></i></button>
                <button class="reset-btn btn-advance collapsed button" data-toggle="collapse" data-target="#collapseSearchAdvance" type="button"><i class="zmdi zmdi-settings"></i></button>
            </div>
            <div class="collapse" id="collapseSearchAdvance">
                <div class="collapse-inner">
                    <div class="search-row">
                        <div class="item">
                            <label class="selectbox form-search__input">
                                <select name="category_id">
                                    <option value="">Type of property</option>
                                    @foreach ($product_cat1s as $key => $product_cat1)
                                    <option value="{{$product_cat1->id}}">{{$product_cat1->title}}</option>
                                    @endforeach
                                    
                                </select>
                            </label>
                        </div>
                        <div class="item">
                            <label class="selectbox form-search__input">
                                <select name="bedroom">
                                    <option value="">Bedroom</option>
                                    <option value="studio">Studio</option>
                                    <option value="bed">1 Bed</option>
                                    <option value="1+den">1 + Den</option>
                                    <option value="2 beds">2 Beds</option>
                                    <option value="2+den">2 + Den</option>
                                    <option value="3 or more">3 Or More</option>
                                    <option value="">Others</option>
                                </select>
                            </label>
                        </div>

                        <div class="item">
                            <label class="selectbox form-search__input">
                                <select name="bathroom">
                                    <option value="">Bathroom</option>
                                    <option value="shared">Shared</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3 or more">3 Or More</option>
                                </select>
                            </label>
                        </div>
                        <div class="item">
                            <label class="selectbox form-search__input">
                                <select name="price">
                                    <option value="">Price</option>
                                    <option value="<500">Under $500</option>
                                    <option value="500,1000">From $500 to $1000</option>
                                    <option value="1000,2000">From $1000 to $2000</option>
                                    <option value=">2000">Over $2000</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- Khi nào có app sẽ mở phần này !!!
            <div class="store">
                <p>Tải app ngay để có trải ngiệm tuyệt vời!</p>
                <ul class="reset-list store-list">
                    <li class="item"><a class="link" href="#!" target="_blank"><img src="/public/theme/img/root/app-store.png" alt="" /></a></li>
                    <li class="item"><a class="link" href="#!" target="_blank"><img src="/public/theme/img/root/google-play.png" alt="" /></a></li>
                </ul>
            </div>
            
            -->
    </div>

    <script>
    jQuery(document).mouseup(function(e)
    {
        var container = $(".dropdown-content, #myInput");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
           $('.dropdown-content').removeClass('show');
        }
    });
    </script>
</section>  