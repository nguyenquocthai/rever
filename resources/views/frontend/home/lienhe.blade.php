@extends('frontend.layouts.main')
@section('content')
<main class="main">
  <section class="section-spacing" id="advisory">
  <div class="container">
    <h3 class="title-main text-center">Liên hệ tư vấn</h3>
    <div class="agent-contact">
      <div class="column left bg-white">
        <figure class="img"><img src="/public/img/upload/settings/{{@$info_web['logo']}}" alt=""></figure>
        <div class="content">
          <h4 class="name fs_20">{{@$nguoidung->callname}}</h4>
          <p class="position fs_16">{{@$info_web['name']}}</p>
          <div class="info fs_16"><a href="tel:{{@$info_web['phone2']}}">{{@$info_web['phone2']}}</a><a href="malto:{{@$info_web['email_kinhdoanh']}}">{{@$info_web['email_kinhdoanh']}}</a></div>
        </div>
      </div>
      <div class="column right bg-gray">
        <form id="contac_form" class="contact-form">
          <div class="item mb-3">
            <input name="name_contact" class="form-control" type="text" placeholder="Họ Tên">
            <input name="id_group" value="1" type="hidden" >
          </div>
          <div class="item mb-3">
            <input name="phone_contact" class="form-control" type="text" placeholder="Số điện thoại">
          </div>
          <div class="item mb-3">
            <input name="email_contact" class="form-control" type="email" placeholder="Email">
          </div>
          <div class="item mb-3">
            <textarea name="comment" class="form-control" rows="3" placeholder="Lời nhắn"></textarea>
          </div>
          <div class="item mb-3">
            <button class="reset-btn btn-red w-100" type="submit">Gửi thông tin</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<script>
  jQuery(document).ready(function($) {
      var html_contac_form = $('#contac_form').html();
      var contac_form = $('#contac_form').validate({
          highlight: function(element, errorClass, validClass) {
              $(element).removeClass(errorClass);
          },
          rules: {
              name_contact:{
                  required: true
              },
              phone_contact:{
                  required: true,
                  number:true,
                  minlength: 10,
                  maxlength: 10,
              },
              email_contact:{
                  required: true,
                  email:true
              },
              comment: {
                  required: true
              }
          },
          messages: {
              name_contact:{
                  required: 'Bạn chưa nhập tên.'
              },
              phone_contact:{
                  required: 'Bạn chưa nhập số điện thoại.',
                  number: 'Số điện thoại phải là số.',
                  minlength: 'Số điện thoại phải là 10 số.',
                  maxlength: 'Số điện thoại phải là 10 số.',
              },
              email_contact:{
                  required: 'Bạn chưa nhập email.',
                  email: 'Email chưa đúng.',
              },
              comment: {
                  required: 'Bạn chưa nhập lời nhắn.'
              }
          },
          submitHandler: function (form) {
      
              var data = {};
              $("#contac_form").serializeArray().map(function(x){data[x.name] = x.value;});
              data['status'] = 5;
             
              $('.block-page-all').addClass('active');
      
              $.ajax({
                  type: 'POST',
                  url: '/send_contact',
                  data: data,
                  dataType: 'json',
                  error: function(){
                      $('.block-page-all').removeClass('active');
                      toastr.error(result.error);
                  },
                  success: function(result) {
                      if (result.code == 300) {
                          toastr.error(result.error);
                          $('.block-page-all').removeClass('active');
                          return false
                      }
                      $('#contac_form').html(html_contac_form);
                      $('.block-page-all').removeClass('active');
                      toastr.success(result.message);
                  }
              });
              return false;
          }
      });
  });  
</script>
  
</main>
@endsection