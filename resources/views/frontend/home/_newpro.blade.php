<style type="text/css">
    .project-item.hor .content-block .price{
        text-align: right;
    }

    .a_view_detail{
        color: #fff !important;
    }
</style>
<div class="col-lg-12">
    <!-- HOUSES FOR RENT-->
    <section class="block">
        <div class="block-title-wrap">
            <h3 class="block-title">New property</h3>
            <a class="reset-btn btn-more" href="/rent-category">See all</a>
        </div>

        @if (count($item_project2s) != 0)
            @foreach ($item_project2s as $key => $item_project)
            <article class="project-item hor hasLink">
                <div class="img embed-responsive albums">
                    
                    @if (count($item_project->albums) != 0)
                        @foreach ($item_project->albums as $item_album)
                        <div class="item_albums">
                            <div class="img_mod">
                                <img src="{{BladeGeneral::GetImg(['avatar' => $item_album->name,'data' => 'item_album', 'time' => $item_album->updated_at])}}" alt="">
                            </div>
                            
                        </div>

                        @endforeach
                    @else
                    
                    @endif
                </div>
                <div class="content">
                    <a class="name link" href="/rent/{{$item_project->slug}}" title="{{$item_project->title}}">{{$item_project->title}}</a>
                    <div class="content-block">
                        <div class="left">
                            <p class="mb-2"><i class="icon icon-nav-home"></i>{{$item_project->cat_name}}</p>
                            <p class="mb-2">
                                <span class="mr-4">
                                    <i class="icon icon-nav-roomate"></i>
                                    @if($item_project->doituong == 0)
                                    Men or Women
                                    @endif
                                    @if($item_project->doituong == 1)
                                    Only Men
                                    @endif
                                    @if($item_project->doituong == 2)
                                    Only Women
                                    @endif
                                </span>
                                <span><i class="icon icon-squaremeter"></i>{{$item_project->area}} sqft</span></p>
                            <p class="mb-2">
                                <span class="mr-4"><a href="#"><i class="icon icon-bed-fill"></i>{{$item_project->phongngu}}</a></span>
                                <span><a href="#"><i class="icon icon-toilet-fill"></i>{{$item_project->phongtam}}</a></span></p>
                            <p class="mb-2"><i class="icon icon-location"></i>{{$item_project->address}}
                            </p>
                            <p><a href="/rent/{{$item_project->slug}}" class="reset-btn btn-main mx-auto mt-3 a_view_detail">View more details</a></p>
                        </div>
                        <div class="right">
                            <div class="price"><span class="price-val">${{BladeGeneral::priceFormat($item_project->price)}}</span><span class="price-unit">per month</span></div>
                        </div>
                    </div>
                </div>
            </article>
            @if($key == 10) @break @endif
            @endforeach
        @else
        
        @endif
    </section>
</div>

<script>
    $('.albums').slick();
</script>