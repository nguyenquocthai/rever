@extends('frontend.layouts.main')
@section('content')
<main class="main">
  <!-- BREADCRUMB-->
  <nav class="breadcrumbk">
    <div class="container">
      <ul class="reset-list breadcrumbk-list">
        <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>Home</a></li>
        <li class="item active">Contact</li>
      </ul>
    </div>
  </nav>
  <!-- CONTACT-->
  <section class="section-spacing contact">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-6 mb-3">
          <div class="block">
            <h3 class="block-title mb-2">Contact information</h3>
            <p class="mb-2"><strong>{{$info_web['name']}}</strong></p>
            <p class="mb-2"><i class="zmdi zmdi-home mr-2"></i>{{$info_web['address']}}</p>
            <p class="mb-2"><i class="zmdi zmdi-phone mr-2"></i><a class="link" href="tel:">{{$info_web['phone']}}</a></p>
            <p class="mb-2"><i class="zmdi zmdi-email mr-2"></i><a class="link" href="mailto:">{{$info_web['email_support']}}</a></p>
          </div>
        </div>
        <div class="col-12 col-lg-6 mb-3">
          <div class="block">
            <h3 class="block-title mb-2 text-center">You need support?</h3>
            <p class="mb-2 text-center">Please fill in the information below, we will contact you as soon as possible!</p>
            <form class="contact-form" method="post" enctype="multipart/form-data">
              <div class="item mb-3">
                <div class="mb-2"><span>Full name </span><span class="cl_red">*</span></div>
                <input class="form-control" type="text" placeholder="Full name" name="name">
              </div>
              <div class="item mb-3">
                <div class="mb-2"><span>Email </span><span class="cl_red">*</span></div>
                <input class="form-control" type="email" placeholder="Email" name="email">
              </div>
              <div class="item mb-3">
                <div class="mb-2"><span>Contact content </span><span class="cl_red">*</span></div>
                <textarea class="form-control" rows="5" placeholder="Contact content" name="content"></textarea>
              </div>
              <div class="item mb-3 text-center">
                <button class="reset-btn btn-main" type="submit">Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="mapbox embed-responsive">
      <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCQiaMU8DTCCDi90sT1ApqUYM737YYXymU&q={!!@$info_web['map']!!}" width="600" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
    </div> -->
  </section>
</main>


@endsection