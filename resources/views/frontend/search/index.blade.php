@extends('frontend.layouts.main')
@section('content')
@include('frontend.search._head')
<main class="main">
    <span class="js-pageVal" id="project"></span>
    <!-- PROJECTS-->
    <section class="project">
        <div class="filter">
            <div class="container">
                <nav class="breadcrumbk">
                    <ul class="reset-list breadcrumbk-list">
                        <li class="item"><a class="link" href="/">Home</a></li>
                        <li class="item active">Lease</li>
                    </ul>
                </nav>
                @include('frontend.search._fillter')
            </div>
        </div>
        <!-- PROJECT CONTENT-->
        <div class="project-content d-lg-flex">
            <div class="project-map">
                <div class="map" id="map"></div>
                <div class="infowindow-content" id="infowindow-content">
                    <figure class="img"><img src="" alt="" height="80" width="110"></figure>
                    <div class="content">
                        <a class="name link" id="title" href=""></a>
                        <p id="para"></p>
                        <div class="info"><span><i class="zmdi zmdi-airline-seat-individual-suite"></i></span><span><i class="zmdi zmdi-photo-size-select-small"></i></span><span><i class="zmdi zmdi-money"></i></span></div>
                    </div>
                </div>
            </div>
            <div class="project-list" id="project-list">
                @include('frontend.search._fillterbox')
                @include('frontend.search._itemsearch')
            </div>
        </div>
    </section>
    @include('frontend.search._foot')
@endsection