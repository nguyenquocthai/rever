<style>
    .filter-box{
        position: static;
        display: block !important;
        width: 100%;
    }
</style>
<div class="filter-box">
    <div>Please choose your filter</div>
    <ul>
        @if(isset($query_serach['price']))
        <?php
            if ($query_serach['price'] == '<500') {
                $price_mark = 'Under $500';
            } else if($query_serach['price'] == '>2000'){
                $price_mark = 'Over $2000';
            } else {
                $price = substr($query_serach['price'], 0, -1); //bỏ dấu , cuối cùng
                $price = explode(",",$query_serach['price']); //Chuyển thành array
                $price_min = $price[0];
                $price_max = $price[1];

                $price_mark = $price[0].'$ -> '.$price[1].'$';
            }
        ?>
        <li class="item_search tag_ft1 active">
            <span class="txt_val">{{$price_mark}}</span> 
            <span class="btn_del">x</span>  
        </li>
        @else
        <li class="item_search tag_ft1">
            <span class="txt_val"></span> 
            <span class="btn_del">x</span>  
        </li>
        @endif

        @if(isset($query_serach['area']))
        <?php
            if ($query_serach['area'] == '<500') {
                $area_mark = 'Under 500sqft';
            } else if($query_serach['area'] == '>2000'){
                $area_mark = 'Over 2000sqft';
            } else {
                $area = substr($query_serach['area'], 0, -1); //bỏ dấu , cuối cùng
                $area = explode(",",$query_serach['area']); //Chuyển thành array
                $area_min = $area[0];
                $area_max = $area[1];

                $area_mark = $area[0].'sqft -> '.$area[1].'sqft';
            }
        ?>
        <li class="item_search tag_ft2 active">
            <span class="txt_val">{{$area_mark}}</span> 
            <span class="btn_del">x</span>  
        </li>
        @else
        <li class="item_search tag_ft2">
            <span class="txt_val"></span> 
            <span class="btn_del">x</span>  
        </li>
        @endif

        @if(isset($query_serach['bedroom']))
        <?php
            $bedroom_mark = $query_serach['bedroom']
        ?>
        <li class="item_search tag_ft3 active">
            <span class="txt_val">{{$bedroom_mark}}</span> 
            <span class="btn_del">x</span>  
        </li>
        @else
        <li class="item_search tag_ft3">
            <span class="txt_val"></span> 
            <span class="btn_del">x</span>  
        </li>
        @endif


        @if(isset($query_serach['bathroom']))
        <?php
            $bathroom_mark = $query_serach['bathroom'];
        ?>
        <li class="item_search tag_ft4 active">
            <span class="txt_val">{{$bathroom_mark}}</span> 
            <span class="btn_del">x</span>  
        </li>
        @else
        <li class="item_search tag_ft4">
            <span class="txt_val"></span> 
            <span class="btn_del">x</span>  
        </li>
        @endif
   </ul>

   <div class="btn_bocx">
        <!-- <button class="btn close-filter-box">Close</button> -->
        <button class="btn apply-filter-box">Apply</button>
   </div>
</div>