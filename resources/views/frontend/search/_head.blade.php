<style>
    .project-list{
        position: relative;
    }

    .filter-box{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        z-index: 10;
        background: #fff;
        display: none;
    }

    .filter-box.active{
        display: block;
    }

    .filter-collapse{
        z-index: 20;
    }
    .filter-box ul{
        list-style: none;
        padding-left: 0;
        margin-bottom: 0;
        padding: 10px;
    }

    .filter-box ul li{
        padding: 2px 5px;
        background: #f7346f;
        color: #fff;
        display: none;
        margin-bottom: 5px;
    }

    .filter-box ul li.active{
        display: inline-block;
    }

    .btn_del{
        cursor: pointer;
        padding-left: 3px;
        padding-right: 3px;
    }

    .btn_bocx{
        text-align: right;
        border-top: 1px solid #f7346f;
        padding: 10px;
    }

    .btn_bocx .btn{
        border-radius: 0;
    }
    .project-list .project-item.ver .list .item {
        font-size: .8rem;
    }
</style>