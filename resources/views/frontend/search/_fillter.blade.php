<div class="filter-container">
    <button class="reset-btn filter-open js-filterTrigger"><i class="zmdi zmdi-filter-list"></i>Filter</button>
    <form id="form_serach" class="filter-wrap js-filterCollapse" action="/search" method="get">
        <button class="reset-btn filter-close" type="button"><i class="zmdi zmdi-chevron-left"></i>Back</button>
        <div class="filter-ctrl">
            <button class="reset-btn button button-red js-collapseHide" type="button">Cancel</button>
            <button class="reset-btn button button-black" name="collapseFilterundefined">Apply</button>
        </div>
        <div class="filter-list js-blurOff">
            <div class="filter-item">
                <button class="reset-btn btn-collapse collapsed" type="button" data-toggle="collapse" data-target="#collapseFilter1">Price</button>
                <div class="filter-collapse collapse js-filterContent" id="collapseFilter1">
                    <div class="inner">
                        <p class="title">Price</p>
                        <ul class="reset-list list">
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['price'] == '<500') checked @endif class="checkbox-input js-filterCheck" type="radio" name="price" value="<500"><span class="checkbox-text">Under $500</span>
                                </label>
                            </li>
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['price'] == '500,1000') checked @endif class="checkbox-input js-filterCheck" type="radio" name="price" value="500,1000"><span class="checkbox-text">$500 -> $1000</span>
                                </label>
                            </li>
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['price'] == '1000,2000') checked @endif class="checkbox-input js-filterCheck" type="radio" name="price" value="1000,2000"><span class="checkbox-text">$1000 -> $2000</span>
                                </label>
                            </li>
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['price'] == '>2000') checked @endif class="checkbox-input js-filterCheck" type="radio" name="price" value=">2000"><span class="checkbox-text">Over $2000</span>
                                </label>
                            </li>
                        </ul>
                        <div class="ctrl">
                            <button class="reset_value reset-btn button button-red js-collapseHide" type="button">Cancel</button>
                            <button class="reset-btn button button-black" name="collapseFilter1">Apply</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-item">
                <button class="reset-btn btn-collapse collapsed" type="button" data-toggle="collapse" data-target="#collapseFilter2">Size</button>
                <div class="filter-collapse collapse js-filterContent" id="collapseFilter2">
                    <div class="inner">
                        <p class="title">Size</p>
                        <ul class="reset-list list">
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['area'] == '<500') checked @endif class="checkbox-input js-filterCheck" type="radio" name="area" value="<500"><span class="checkbox-text">Under 500sqft</span>
                                </label>
                            </li>
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['area'] == '500,1000') checked @endif class="checkbox-input js-filterCheck" type="radio" name="area" value="500,1000"><span class="checkbox-text">500sqft - 1000sqft</span>
                                </label>
                            </li>
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['area'] == '1000,2000') checked @endif class="checkbox-input js-filterCheck" type="radio" name="area" value="1000,2000"><span class="checkbox-text">1000sqft - 2000sqft</span>
                                </label>
                            </li>
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['area'] == '>2000') checked @endif class="checkbox-input js-filterCheck" type="radio" name="area" value=">2000"><span class="checkbox-text">Over 2000sqft</span>
                                </label>
                            </li>
                        </ul>
                        <div class="ctrl">
                            <button class="reset_value reset-btn button button-red js-collapseHide" type="button">Cancel</button>
                            <button class="reset-btn button button-black">Apply</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-item">
                <button class="reset-btn btn-collapse collapsed" type="button" data-toggle="collapse" data-target="#collapseFilter3">Bedroom</button>
                <div class="filter-collapse collapse js-filterContent" id="collapseFilter3">
                    <div class="inner">
                        <p class="title">Bedroom</p>
                        <ul class="reset-list list">
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['bedroom'] == 'studio') checked @endif class="checkbox-input js-filterCheck" type="radio" name="bedroom" value="studio"><span class="checkbox-text">Studio</span>
                                </label>
                            </li>
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['bedroom'] == 'bed') checked @endif class="checkbox-input js-filterCheck" type="radio" name="bedroom" value="bed"><span class="checkbox-text">1 Bed</span>
                                </label>
                            </li>
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['bedroom'] == '1+den') checked @endif class="checkbox-input js-filterCheck" type="radio" name="bedroom" value="1+den"><span class="checkbox-text">1 + Den</span>
                                </label>
                            </li>
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['bedroom'] == '2 beds') checked @endif class="checkbox-input js-filterCheck" type="radio" name="bedroom" value="2 beds"><span class="checkbox-text">2 Beds</span>
                                </label>
                            </li>
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['bedroom'] == '2+den') checked @endif class="checkbox-input js-filterCheck" type="radio" name="bedroom" value="2+den"><span class="checkbox-text">2 + Den</span>
                                </label>
                            </li>
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['bedroom'] == '3 or more') checked @endif class="checkbox-input js-filterCheck" type="radio" name="bedroom" value="3 or more"><span class="checkbox-text">3 Or More</span>
                                </label>
                            </li>
                            <li class="item">
                                <label class="checkbox radio">
                                <input @if(@$query_serach['bedroom'] == '') checked @endif class="checkbox-input js-filterCheck" type="radio" name="bedroom" value=""><span class="checkbox-text">Others</span>
                                </label>
                            </li>
                        </ul>
                        <div class="ctrl">
                            <button class="reset_value reset-btn button button-red js-collapseHide" type="button">Cancel</button>
                            <button class="reset-btn button button-black" name="collapseFilter3">Apply</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-item">
                <button class="reset-btn btn-collapse collapsed" type="button" data-toggle="collapse" data-target="#collapseFilter4">Bathroom</button>
                <div class="filter-collapse collapse js-filterContent" id="collapseFilter4">
                    <div class="inner">
                        <div class="column">
                            <p class="title">Bathroom</p>
                            <ul class="reset-list list">
                                <li class="item">
                                    <label class="checkbox radio">
                                    <input @if(@$query_serach['bathroom'] == 'shared') checked @endif class="checkbox-input js-filterCheck" type="radio" name="bathroom" value="shared"><span class="checkbox-text">Shared</span>
                                    </label>
                                </li>
                                <li class="item">
                                    <label class="checkbox radio">
                                    <input @if(@$query_serach['bathroom'] == '1') checked @endif class="checkbox-input js-filterCheck" type="radio" name="bathroom" value="1"><span class="checkbox-text">1 Bathroom</span>
                                    </label>
                                </li>
                                <li class="item">
                                    <label class="checkbox radio">
                                    <input @if(@$query_serach['bathroom'] == '2') checked @endif class="checkbox-input js-filterCheck" type="radio" name="bathroom" value="2"><span class="checkbox-text">2 Bathroom</span>
                                    </label>
                                </li>
                                <li class="item">
                                    <label class="checkbox radio">
                                    <input @if(@$query_serach['bathroom'] == '3 or more') checked @endif class="checkbox-input js-filterCheck" type="radio" name="bathroom" value="3 or more"><span class="checkbox-text">3 Or More</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        
                        <div class="ctrl">
                            <button class="reset_value reset-btn button button-red js-collapseHide" type="button">Cancel</button>
                            <button class="reset-btn button button-black" name="collapseFilter4">Apply</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>