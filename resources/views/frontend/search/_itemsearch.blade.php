@if (count($item_projects) != 0)
    @foreach ($item_projects as $item_project)
    <article class="project-item ver" id="{{$item_project->id}}">
        <figure class="img embed-responsive hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => @$item_project->album->name,'data' => 'item_album', 'time' => @$item_project->album->updated_at])}}" alt=""><a class="link" target="_blank" href="/rent/{{$item_project->slug}}" title="{{$item_project->title}}"></a></figure>
        <div class="content">
            <a class="name link" target="_blank" href="/rent/{{$item_project->slug}}" title="{{$item_project->title}}"style="color: #f7346f;">${{BladeGeneral::priceFormat(@$item_project->price)}}</a>
            <p class="address">{{@$item_project->address}}</p>
            <div class="content-block">
                <ul class="reset-list list">
                    <li class="item">{{@$item_project->phongngu}} BD</li>
                    <li class="item">| {{@$item_project->phongtam}} BA</li>
                    <li class="item">{{@$item_project->baixe}} Parking</li>
                    <li class="item">| {{@$item_project->area}} sqft</li>
                </ul>
            </div>
        </div>
    </article>
    @endforeach
@else
<i>No results</i>
@endif