<?php
$name_apps = [
    'slider',
    'setting',
    'product_cat1',
    'product_cat1ban',
    'item_project',
    'item_album',
    'seo_page',
    'tongquan',
    'tienich',
    'store',
    'location',
    'nguoidung',
    'customer',
    'contact',
    'banner',
    'bannerfb',
    'intro',
    'new',
    'new_cat'
];

$general = [
    // SESSION_SHOPPING_CART
    'session_shopping_cart' => 'orderSession',

    // reCAPTCHA type:v3
    'sitekey' => '6LdbUZwUAAAAANX6n0EmebLspWl34X0E3U3Nzynp',
    'secretkey' => '6LdbUZwUAAAAADdNtJBFJi9TdgeWLxGDAAURkW4I',

    // SESSION_USER
    'session_user' => 'userSession',

    // permission_folder
    'permission_folder' => 0777,

    // notification exceeds capacity
    'notification_image' => '10 MB',

    // type File
    'type_image' => ['jpg', 'gif', 'png'],
    'name_apps' => $name_apps,

    //Loging strava
    'client_id' => '33165',
    'secret' => 'f77df19e2dea7f7d1fcb93bb8f13e84a03ff75ae',
    'linkstrava' => 'https://www.strava.com/oauth/authorize?client_id=33165&response_type=code&redirect_uri=http://irace.wistman.com/apistrava&approval_prompt=force',
];

// list image path
foreach ($name_apps as $key => $name) {
    $general[$name.'_path'] = public_path() . '/img/upload/'.$name.'s/';
    $general[$name.'_url'] = '/public/img/upload/'.$name.'s/';
}

return $general;
